REM ====================================
   REM ====        GET PAPI !!        ====
   REM ====          V1.0            ====
   REM ====  (c) 2018 STUDIO VETEA    ====
   REM ====================================

   INCLUDE "constants.bas"
   'Fond Vert
   MODE SCREEN_CS, CS_GREEN, GREEN, GREEN,GREEN
   
   'Déclaration Sprites/TILE
   WAIT
   DEFINE DEF00,4,SpritePapiDROITE_0
   WAIT
   DEFINE DEF04,3,SpriteIADroite
   WAIT
   DEFINE DEF07,3,SpriteIABAS
   WAIT
   DEFINE DEF10,3,SpriteIAHAUT
   wait
   DEFINE 13,2,TilesDecor
   WAIT
   
   'Variables
   DIM X1(5)            'POSX IA
   DIM Y1(5)            'POSY IA
   DIM INT(5)            'INT IA
   DIM Sens(5)            'DIRECTION IA
   DIM Chemin(5)         'DISTANCE PARCOURUE IA
   DIM CheminMAX(5)      'DISTANCE MAX IA
   DIM TempoAnimation(5)   'TEMPO ANIMATION IA
   DIM TempoIA(5)         'VITESSE IA
   DIM TempoIA_MAX(5)      'VITESSE MAXI IA

Debut:
   CLS
   'Position/Config Sprites
   for i=1 to 4
      X1(i)=random(100)+8
      Y1(i)=random(60)+8
      if y1(i)<15 then y1(i)=16
      Sens(i)=RAND % 4
      CheminMAX(i)=RAND % 75
      TempoIA(i)=0
      INT(i)=RAND % 50
      TempoIA_MAX(i)=(i*5)+1
   next
   Direction=1 '0 HAUT, 1 BAS, 2 DROITE, 3 GAUCHE
   VitessePapi=3
   TempoPapi=0
   Temps=0
   X=64
   Y=48
   Ax=0
   Ay=1
   'Background
   restore Map
   for i=0 to 239
      read value
      if value=0 then
         print at (SCREENPOS(Ax,Ay)) color CS_DARKGREEN, "\270"
      else
         '#ADR=$080A
         '#backtab(i)=#ADR+8*8
      end if
      Ax=Ax+1
      if Ax>19 then
         Ay=Ay+1
         Ax=0
      end if
   next
   
   'Affichage du Temps
   print at (SCREENPOS(6,0)) color CS_WHITE, "TIME :"
   print at (SCREENPOS(13,0)) color CS_WHITE, <3>temps

   'Boucle principale
   while 1
      'Gestion temps
      gosub gestionTime
      'Affichage Sprite
      gosub AffSprite
      'Detection de Papi COL0 avec les 4 autres Sprites Ennemis après une FRAME
      IF (COL0 and FRAME) AND HIT_SPRITE4+HIT_SPRITE5+HIT_SPRITE6+HIT_SPRITE7 THEN gosub CollisionSPRITE
      wait
   wend
   
'Gestion du temps
GestionTime: PROCEDURE
   FR=FR+1
   if FR=60 then
      temps=temps+1:FR=0
   end if
   print at (SCREENPOS(13,0)) color CS_WHITE, <3>temps
   return
   end
   
'Procedure de Collision Sprite
CollisionSPRITE: PROCEDURE
   FOR color = 0 TO 31
   WAIT
   SPRITE 0,X+HIT+VISIBLE,Y+ZOOMY2,SPR00+(color AND SPR_WHITE)
   SPRITE 1,X+HIT+VISIBLE,Y+ZOOMY2,SPR01+(color AND SPR_RED)
   SPRITE 2,X+HIT+VISIBLE,Y+ZOOMY2,SPR02+(color AND SPR_PINK)
   SPRITE 3,X+HIT+VISIBLE,Y+ZOOMY2,SPR03+(color AND SPR_BLACK)
   
   SOUND 0,(color and 7)*32+32,15
   SOUND 1,(color and 7)*36+32,15
   SOUND 2,(color and 7)*40+32,15
   NEXT
   SOUND 0,,0
   SOUND 1,,0
   SOUND 2,,0

   FOR loop = 0 to 100
   WAIT
   NEXT
   
   'INIT Sprite
   SPRITE 0,0,0
   SPRITE 1,0,0
   SPRITE 2,0,0
   SPRITE 3,0,0
   SPRITE 4,0,0
   SPRITE 5,0,0
   SPRITE 6,0,0
   SPRITE 7,0,0
   
   'goto debut
   return
   end

'Procedure de Gestion de Sprite
 AffSprite: PROCEDURE
   'Gestion Vitesse/Active sur si PAD Actif.
   if CONT then VitessePapi=VitessePapi+1
   
   'Animation quand PAD inactif
   if CONT=0 then
      #Miroir=0
      if Direction=0 then DEFINE DEF00, 4, SpritePapiHAUT_1
      if Direction=1 then DEFINE DEF00, 4, SpritePapiBAS_1
      if Direction=2 then DEFINE DEF00, 4, SpritePapiDROITE_1
      if Direction=3 then DEFINE DEF00, 4, SpritePapiDROITE_1:#miroir=FLIPX
   end if
   
   'On peut fixer la vitesse de Papi dans la condition.
   if VitessePapi>4 then
      VitessePapi=0
      TempoPapi=TempoPapi+1
      if TempoPapi>4 then TempoPapi=0
      
      'Papi Commando !
      'Commandes
      'PAD Actif !
      IF CONT1.UP THEN
         Y=Y-1
         Direction=0
         if TempoPapi=1 then DEFINE DEF00, 4, SpritePapiHAUT_0
         if TempoPapi=2 then DEFINE DEF00, 4, SpritePapiHAUT_1
         if TempoPapi=3 then DEFINE DEF00, 4, SpritePapiHAUT_2
         if TempoPapi=4 then DEFINE DEF00, 4, SpritePapiHAUT_1
         #miroir=0
      end if
      IF CONT1.DOWN THEN
         Y=Y+1
         Direction=1
         if TempoPapi=1 then DEFINE DEF00, 4, SpritePapiBAS_0
         if TempoPapi=2 then DEFINE DEF00, 4, SpritePapiBAS_1
         if TempoPapi=3 then DEFINE DEF00, 4, SpritePapiBAS_2
         if TempoPapi=4 then DEFINE DEF00, 4, SpritePapiBAS_1
         #miroir=0
      end if
      IF CONT1.LEFT THEN
         X=X-1
         Direction=3
         #miroir=FLIPX
         if TempoPapi=1 then DEFINE DEF00, 4, SpritePapiDROITE_0
         if TempoPapi=2 then DEFINE DEF00, 4, SpritePapiDROITE_1
         if TempoPapi=3 then DEFINE DEF00, 4, SpritePapiDROITE_2
         if TempoPapi=4 then DEFINE DEF00, 4, SpritePapiDROITE_1
      end if
      IF CONT1.RIGHT THEN
         X=X+1
         Direction=2
         if TempoPapi=1 then DEFINE DEF00, 4, SpritePapiDROITE_0
         if TempoPapi=2 then DEFINE DEF00, 4, SpritePapiDROITE_1
         if TempoPapi=3 then DEFINE DEF00, 4, SpritePapiDROITE_2
         if TempoPapi=4 then DEFINE DEF00, 4, SpritePapiDROITE_1
         #miroir=0
      end if
   end if
   
   'Affiche les sprites de Papi
   SPRITE 0,X+VISIBLE+HIT,Y+ZOOMY2+#miroir,SPR00+SPR_WHITE
   SPRITE 1,X+VISIBLE+HIT,Y+ZOOMY2+#miroir,SPR01+SPR_RED
   SPRITE 2,X+VISIBLE+HIT,Y+ZOOMY2+#miroir,SPR02+SPR_PINK
   SPRITE 3,X+VISIBLE+HIT,Y+ZOOMY2+#miroir,SPR03+SPR_BLACK
   
   'Limite
   if Y<16 then Y=16
   if Y>96 then Y=96
   if X>160 then X=160
   if X<8 then X=8
   
   'Sprites Ennemis
   for i=1 to 4
      'Limite Tableau
      if x1(i)<8 then sens(i)=2
      if x1(i)>160 then sens(i)=3
      if y1(i)<16 then sens(i)=1
      if y1(i)>96 then sens(i)=0
      
      'Tempo
      TempoIA(i)=TempoIA(i)+1
      
      'Au fur et a mesure que le temps passe, les choses se corsent ... ^^
      DIF=temps/16
      T_IA=TempoIA_MAX(i)-DIF
      if T_IA<2 then T_IA=2
      INT(i)=INT(i)-(DIF*2)
      
      'Gestion Sprite
      if TempoIA(i)>T_IA then
         'Tempo Animation
         TempoAnimation(i)=TempoAnimation(i)+1
         if TempoAnimation(i)>4 then TempoAnimation(i)=1
         TempoIA(i)=0
         
         'Gestion IA/Chemin - Pathfinding
         ' Unités Connes
         if Int(i)<25 then
            Chemin(i)=Chemin(i)+1
            if Chemin(i)>CheminMAX(i) then
               sens(i)=random(4)
               Chemin(i)=0
            end if
         else
            'Les unités Intelligentes suivent Papi
            'Calcul de distance
            a=x-x1(i)
            b=y-y1(i)
            'Algo de Pathfinding
            if a>b and b<=8 then
               if x1(i)<x then sens(i)=2 else sens(i)=3
            end if
            if a<=b and b>8 then
               if y1(i)<y then sens(i)=1 else sens(i)=0
            end if
            if y1(i)<y and a<8 then sens(i)=1
            if y1(i)>y and a<8 then sens(i)=0
         end if
         
         'Déplacement IA
         if sens(i)=0 then
            y1(i)=y1(i)-1
            if TempoAnimation(i)=1 then #SPR=SPR10
            if TempoAnimation(i)=2 then #SPR=SPR11
            if TempoAnimation(i)=3 then #SPR=SPR12
            if TempoAnimation(i)=4 then #SPR=SPR11
            #miroir1=0
         end if
         if sens(i)=1 then
            y1(i)=y1(i)+1
            if TempoAnimation(i)=1 then #SPR=SPR07
            if TempoAnimation(i)=2 then #SPR=SPR08
            if TempoAnimation(i)=3 then #SPR=SPR09
            if TempoAnimation(i)=4 then #SPR=SPR08
            #miroir1=0
         end if
         if sens(i)=2 then
            x1(i)=x1(i)+1
            if TempoAnimation(i)=1 then #SPR=SPR04
            if TempoAnimation(i)=2 then #SPR=SPR05
            if TempoAnimation(i)=3 then #SPR=SPR06
            if TempoAnimation(i)=4 then #SPR=SPR05
            #miroir1=0
         end if
         if sens(i)=3 then
            x1(i)=x1(i)-1
            if TempoAnimation(i)=1 then #SPR=SPR04
            if TempoAnimation(i)=2 then #SPR=SPR05
            if TempoAnimation(i)=3 then #SPR=SPR06
            if TempoAnimation(i)=4 then #SPR=SPR05
            #miroir1=FLIPX
         end if

         'Couleur Sprites
         if i=1 then #Color=SPR_BLACK
         if i=2 then #Color=SPR_YELLOW
         if i=3 then #Color=SPR_RED
         if i=4 then #Color=SPR_PINK
         
         'Affichage ennemis
         SPRITE 3+i,x1(i)+VISIBLE+HIT,y1(i)+ZOOMY2+#miroir1,#SPR+#Color
      end if
      
   next
   return

   END
   
Map:
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   Data 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

   
'Sprite Papi
SpritePapiHaut_0:
   'WHITE
   BITMAP ".#....#."
   BITMAP "........"
   BITMAP ".#....#."
   BITMAP "..#..#.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'RED
   BITMAP "........"
   BITMAP "#######."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'PINK
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "..####.."
   BITMAP "...##..."
   BITMAP "..#....."
   BITMAP ".#...#.."
   BITMAP "......#."
   BITMAP "........"
   'BLACK
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "...###.."
   BITMAP "..###..."
   BITMAP ".##.##.."
   BITMAP ".....##."
   
SpritePapiHaut_1:
   'WHITE
   BITMAP "........"
   BITMAP ".#....#."
   BITMAP "........"
   BITMAP ".#....#."
   BITMAP "..#..#.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'RED
   BITMAP "........"
   BITMAP "........"
   BITMAP ".######."
   BITMAP "#......."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'PINK
   BITMAP "........"
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "..####.."
   BITMAP "...##..."
   BITMAP "..#..#.."
   BITMAP ".#....#."
   BITMAP "........"
   'BLACK
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "...##..."
   BITMAP "..####.."
   BITMAP "..####.."
   
SpritePapiHaut_2:
   'WHITE
   BITMAP ".#....#."
   BITMAP "........"
   BITMAP ".#....#."
   BITMAP "..#..#.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'RED
   BITMAP "........"
   BITMAP "#######."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'PINK
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "..####.."
   BITMAP "...##..."
   BITMAP ".....#.."
   BITMAP "...#..#."
   BITMAP "..#....."
   BITMAP "........"
   'BLACK
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "..###..."
   BITMAP "...###.."
   BITMAP "..##.##."
   BITMAP ".##....."
   
SpritePapiBAS_0:
   'WHITE
   BITMAP ".#....#."
   BITMAP "..#..#.."
   BITMAP ".#....#."
   BITMAP ".#.##.#."
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'RED
   BITMAP "........"
   BITMAP "##.##.#."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'PINK
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "...##..."
   BITMAP "..#..#.."
   BITMAP "........"
   BITMAP ".#......"
   BITMAP "......#."
   BITMAP "........"
   'BLACK
   BITMAP "........"
   BITMAP "........"
   BITMAP "..#..#.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "..####.."
   BITMAP ".##.##.."
   BITMAP ".....##."
   
SpritePapiBAS_1:
   'WHITE
   BITMAP "........"
   BITMAP ".#....#."
   BITMAP "..#..#.."
   BITMAP ".#....#."
   BITMAP ".#.##.#."
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "........"
   'RED
   BITMAP "........"
   BITMAP "........"
   BITMAP ".#.##.#."
   BITMAP "#......."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'PINK
   BITMAP "........"
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "...##..."
   BITMAP "..#..#.."
   BITMAP "........"
   BITMAP ".#....#."
   BITMAP "........"
   'BLACK
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "..#..#.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "..####.."
   BITMAP "..####.."
   
SpritePapiBAS_2:
   'WHITE
   BITMAP ".#....#."
   BITMAP "..#..#.."
   BITMAP ".#....#."
   BITMAP ".#.##.#."
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'RED
   BITMAP "........"
   BITMAP "##.##.#."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'PINK
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "...##..."
   BITMAP "..#..#.."
   BITMAP "........"
   BITMAP "......#."
   BITMAP ".#......"
   BITMAP "........"
   'BLACK
   BITMAP "........"
   BITMAP "........"
   BITMAP "..#..#.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "..####.."
   BITMAP "..##.##."
   BITMAP ".##....."

SpritePapiDROITE_0:
   'WHITE
   BITMAP ".#......"
   BITMAP "..##.##."
   BITMAP ".#......"
   BITMAP ".##.#.#."
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'RED
   BITMAP "........"
   BITMAP "##..#..."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'PINK
   BITMAP "..#####."
   BITMAP "........"
   BITMAP "..#.#.#."
   BITMAP "...#.#.."
   BITMAP "........"
   BITMAP ".##...#."
   BITMAP ".#......"
   BITMAP "........"
   'BLACK
   BITMAP "........"
   BITMAP "........"
   BITMAP "...#.#.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "...###.."
   BITMAP "..####.."
   BITMAP "..#..##."
   
SpritePapiDROITE_1:
   'WHITE
   BITMAP "........"
   BITMAP ".#......"
   BITMAP "..##.##."
   BITMAP ".#......"
   BITMAP ".##.#.#."
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "........"
   'RED
   BITMAP "........"
   BITMAP "........"
   BITMAP ".#..#..."
   BITMAP "#......."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'PINK
   BITMAP "........"
   BITMAP "..#####."
   BITMAP "........"
   BITMAP "..#.#.#."
   BITMAP "...#.#.."
   BITMAP "........"
   BITMAP "..#...#."
   BITMAP "........"
   'BLACK
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "...#.#.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "...###.."
   BITMAP "..####.."
   
SpritePapiDROITE_2:
   'WHITE
   BITMAP ".#......"
   BITMAP "..##.##."
   BITMAP ".#......"
   BITMAP ".##.#.#."
   BITMAP "..####.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'RED
   BITMAP "........"
   BITMAP "##..#..."
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   BITMAP "........"
   'PINK
   BITMAP "..#####."
   BITMAP "........"
   BITMAP "..#.#.#."
   BITMAP "...#.#.."
   BITMAP "........"
   BITMAP "...#...."
   BITMAP ".#..#..."
   BITMAP "........"
   'BLACK
   BITMAP "........"
   BITMAP "........"
   BITMAP "...#.#.."
   BITMAP "........"
   BITMAP "........"
   BITMAP "..#.##.."
   BITMAP "..##.##."
   BITMAP ".#...#.."

'Sprite IA   
SpriteIAHAUT:
   BITMAP "..####.."
   BITMAP ".######."
   BITMAP ".######."
   BITMAP "..####.."
   BITMAP ".#####.."
   BITMAP "..#####."
   BITMAP ".##.##.."
   BITMAP ".....##."
   
   BITMAP "........"
   BITMAP "..####.."
   BITMAP ".######."
   BITMAP ".######."
   BITMAP "..####.."
   BITMAP ".######."
   BITMAP "..####.."
   BITMAP ".##..##."

   BITMAP "..####.."
   BITMAP ".######."
   BITMAP ".######."
   BITMAP "..####.."
   BITMAP "..#####."
   BITMAP ".#####.."
   BITMAP "..##.##."
   BITMAP ".##....."
   
SpriteIABAS:
   BITMAP "..####.."
   BITMAP ".######."
   BITMAP ".#.##.#."
   BITMAP "..####.."
   BITMAP ".#####.."
   BITMAP "..#####."
   BITMAP ".##.##.."
   BITMAP ".....##."
   
   BITMAP "........"
   BITMAP "..####.."
   BITMAP ".######."
   BITMAP ".#.##.#."
   BITMAP "..####.."
   BITMAP ".######."
   BITMAP "..####.."
   BITMAP ".##..##."
   
   BITMAP "..####.."
   BITMAP ".######."
   BITMAP ".#.##.#."
   BITMAP "..####.."
   BITMAP "..#####."
   BITMAP ".#####.."
   BITMAP "..##.##."
   BITMAP ".##....."
   
SpriteIADroite:
   BITMAP "..####.."
   BITMAP ".####.#."
   BITMAP ".###.##."
   BITMAP "..####.."
   BITMAP "...####."
   BITMAP "...###.."
   BITMAP "..##.##."
   BITMAP "..#....."

   BITMAP "........"
   BITMAP "..####.."
   BITMAP ".####.#."
   BITMAP ".###.##."
   BITMAP "..####.."
   BITMAP "...##..."
   BITMAP "...##..."
   BITMAP "...###.."

   BITMAP "..####.."
   BITMAP ".####.#."
   BITMAP ".###.##."
   BITMAP "..####.."
   BITMAP "...##..."
   BITMAP "..####.."
   BITMAP "...#.##."
   BITMAP "..#....."

TilesDecor:
   REM Rocher
   REM Terrain GRIS 17 (273)
   BITMAP "..####.."
   BITMAP ".####.#."
   BITMAP ".#####.#"
   BITMAP "######.#"
   BITMAP "######.#"
   BITMAP "########"
   BITMAP "########"
   BITMAP "########"
   BITMAP "########"
   
   REM Gazon/BG
   REM Terrain Calque Vert 1 13 (269)
   BITMAP "********"
   BITMAP "***.****"
   BITMAP "********"
   BITMAP "********"
   BITMAP "**.*****"
   BITMAP "********"
   BITMAP "*****.**"
   BITMAP "********"