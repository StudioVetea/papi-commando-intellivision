'//////////////////////////
'//// SpriteSheet jeu /////
'//////////////////////////

'///// Couteau /////
'Haut
CouteauH_1:
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "...#...."
    BITMAP "..###..."
    BITMAP "..###..."
CouteauH_2:
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "...#...."
    BITMAP "..###..."
    BITMAP "..###..."
    BITMAP "..###..."
    BITMAP "........"	
CouteauH_3:
    BITMAP "...#...."
    BITMAP "..###..."
    BITMAP "..###..."
    BITMAP "..###..."
    BITMAP "..###..."
    BITMAP "........"
    BITMAP ".#####.."
    BITMAP "..###..."
'Bas	
CouteauB_1:
    BITMAP "..###..."
    BITMAP "..###..."
    BITMAP "...#...."
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
CouteauB_2:
    BITMAP "........"
    BITMAP "..###..."
    BITMAP "..###..."
    BITMAP "..###..."
    BITMAP "...#...."
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
CouteauB_3:
    BITMAP "..###..."
    BITMAP ".#####.."
    BITMAP "........"
    BITMAP "..###..."
    BITMAP "..###..."
    BITMAP "..###..."
    BITMAP "..###..."
    BITMAP "...#...."
'Droit
CouteauD_1:
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "##......"
    BITMAP "###....."
    BITMAP "##......"
    BITMAP "........"
    BITMAP "........"
CouteauD_2:
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP ".###...."
    BITMAP ".####..."
    BITMAP ".###...."
    BITMAP "........"
    BITMAP "........"
CouteauD_3:
    BITMAP "........"
    BITMAP "........"
    BITMAP ".#......"
    BITMAP "##.####."
    BITMAP "##.#####"
    BITMAP "##.####."
    BITMAP ".#......"
    BITMAP "........"
	
'///// Viseur ////
Viseur:
	BITMAP "........"
	BITMAP "..#....#"
	BITMAP "........"
	BITMAP "....##.."
	BITMAP "....##.."
	BITMAP "........"
	BITMAP "..#....#"
	BITMAP "........"
	
'///// Gerbe Eau /////
Gerbe:
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "...#...."
	BITMAP "...#...."
	BITMAP "........"
	
Gerbe1:
	BITMAP "........"
	BITMAP ".#...#.."
	BITMAP "........"
	BITMAP "...#...."
	BITMAP "........"
	BITMAP "...#...."
	BITMAP "...#...."
	BITMAP "........"
	
Gerbe2:
	BITMAP "........"
	BITMAP ".#...#.."
	BITMAP "........"
	BITMAP "#.....#."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	
Gerbe3:
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "#.....#."
	BITMAP "........"

'///// Grenad /////
Grenade:
	BITMAP "........"
	BITMAP "........"
	BITMAP "...##..."
	BITMAP "..##.#.."
	BITMAP "..####.."
	BITMAP "...##..."
	BITMAP "........"
	BITMAP "........"
	
Grenade1:
	BITMAP "........"
	BITMAP "..####.."
	BITMAP ".####.#."
	BITMAP ".######."
	BITMAP ".######."
	BITMAP ".######."
	BITMAP "..####.."
	BITMAP "........"

Grenade2:
	BITMAP "..####.."
	BITMAP ".####.#."
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP ".######."
	BITMAP "..####.."

'///// Balle /////
Touch:
	BITMAP ".######."
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP ".######."
Touch1:
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	
Touch2:
    BITMAP "#...#..#"
    BITMAP ".#..#.#."
    BITMAP "........"
    BITMAP "......##"
    BITMAP "##......"
    BITMAP "........"
    BITMAP ".#.#..#."
    BITMAP "#..#...#"
	
Balle:
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "...##..."
	BITMAP "...##..."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	
Balle1:
	BITMAP "........"
	BITMAP "........"
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "........"

Balle2:
	BITMAP "........"
	BITMAP ".#....#."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP ".#....#."
	BITMAP "........"
	
Balle3:
	BITMAP "#......#"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "#......#"

'///// Robot IA /////
Robot1:
    BITMAP "..####.."
    BITMAP ".######."
    BITMAP "########"
    BITMAP ".#.#.#.#"
    BITMAP "########"
    BITMAP ".######."
    BITMAP "..####.."
    BITMAP "........"

    BITMAP "..####.."
    BITMAP ".######."
    BITMAP "########"
    BITMAP "#.#.#.#."
    BITMAP "########"
    BITMAP ".######."
    BITMAP "..####.."
    BITMAP "........"

'///// Ennemi SOLDAT /////
' BAS
Soldat_1B:
    BITMAP "..###..."
    BITMAP "..####.."
    BITMAP "..##...."
    BITMAP "...##..."
    BITMAP "..##.#.."
    BITMAP "...##..."
    BITMAP "..#..#.."
    BITMAP ".##..##."

    BITMAP "..###..."
    BITMAP "..####.."
    BITMAP "..##...."
    BITMAP "...##..."
    BITMAP "..##.#.."
    BITMAP "..###..."
    BITMAP ".##..#.."
    BITMAP ".....##."

    BITMAP "..###..."
    BITMAP "..####.."
    BITMAP "..##...."
    BITMAP "...##..."
    BITMAP "..##.#.."
    BITMAP "...###.."
    BITMAP "..#..##."
    BITMAP ".##....."

'Haut
Soldat_1H:
    BITMAP "..####.."
    BITMAP "..#####."
    BITMAP "..####.."
    BITMAP "...##..."
    BITMAP "..####.."
    BITMAP "...##..."
    BITMAP "..#..#.."
    BITMAP ".##..##."
	
    BITMAP "..####.."
    BITMAP "..#####."
    BITMAP "..####.."
    BITMAP "...##..."
    BITMAP "..####.."
    BITMAP "..###..."
    BITMAP ".##..#.."
    BITMAP ".....##."

    BITMAP "..####.."
    BITMAP "..#####."
    BITMAP "..####.."
    BITMAP "...##..."
    BITMAP "..####.."
    BITMAP "...###.."
    BITMAP "..#..##."
    BITMAP ".##....."

'Droit/Gauche
Soldat_1D:
    BITMAP "...###.."
    BITMAP "...####."
    BITMAP "...##..."
    BITMAP "..##.#.."
    BITMAP "..#####."
    BITMAP "...##..."
    BITMAP "..##.#.."
    BITMAP ".#...##."

    BITMAP "...###.."
    BITMAP "...####."
    BITMAP "...##..."
    BITMAP "..####.."
    BITMAP "..#####."
    BITMAP "...##..."
    BITMAP "..###..."
    BITMAP "..#.##.."
	
    BITMAP "...###.."
    BITMAP "...####."
    BITMAP "...##..."
    BITMAP "...###.."
    BITMAP "...####."
    BITMAP "...##..."
    BITMAP "...##..."
    BITMAP "...#.#.."

    BITMAP "...###.."
    BITMAP "...####."
    BITMAP "...##..."
    BITMAP "...###.."
    BITMAP "...####."
    BITMAP "...##..."
    BITMAP "..##.#.."
    BITMAP "..#....."	
	
'///// Papi Commando /////
PAPI_BAS_0:
'WHITE
	BITMAP "........"
	BITMAP ".#....#."
	BITMAP "........"
	BITMAP ".#....#."
	BITMAP ".#.##.#."
	BITMAP "..####.."
	BITMAP "........"
	BITMAP "........"
'BLACK
	BITMAP "........"
	BITMAP "........"
	BITMAP ".######."
	BITMAP "#.#..#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "...##..."
	BITMAP "..#..#.."
'PINK
	BITMAP "........"
	BITMAP "..####.."
	BITMAP "........"
	BITMAP "...##..."
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "..#..#.."
	BITMAP "...##..."
	
PAPI_BAS_1:
'WHITE
	BITMAP ".#....#."
	BITMAP "..#..#.."
	BITMAP ".#....#."
	BITMAP ".#.##.#."
	BITMAP "..####.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
'BLACK
	BITMAP "........"
	BITMAP "##.##.#."
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "...##..."
	BITMAP ".##..#.."
	BITMAP ".....##."
'PINK
	BITMAP "..####.."
	BITMAP "........"
	BITMAP "...##..."
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "..#..#.."
	BITMAP "...##..."
	BITMAP "........"
	
PAPI_BAS_2:
'WHITE
	BITMAP ".#....#."
	BITMAP "..#..#.."
	BITMAP ".#....#."
	BITMAP ".#.##.#."
	BITMAP "..####.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
'BLACK
	BITMAP "........"
	BITMAP "##.##.#."
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "...##..."
	BITMAP "..#..##."
	BITMAP ".##....."
'PINK
	BITMAP "..####.."
	BITMAP "........"
	BITMAP "...##..."
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "..#..#.."
	BITMAP "...##..."
	BITMAP "........"
	
PAPI_DROITE_0:
'WHITE
	BITMAP "........"
	BITMAP ".#......"
	BITMAP "........"
	BITMAP ".#......"
	BITMAP ".##.#.#."
	BITMAP "...###.."
	BITMAP "........"
	BITMAP "........"
'BLACK
	BITMAP "........"
	BITMAP "........"
	BITMAP ".######."
	BITMAP "#..#.#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "..##...."
	BITMAP "..####.."
'PINK
	BITMAP "........"
	BITMAP "..#####."
	BITMAP "........"
	BITMAP "..#.#.#."
	BITMAP "...#.#.."
	BITMAP "........"
	BITMAP "....###."
	BITMAP "........"	

PAPI_DROITE_1:
'WHITE
	BITMAP ".#......"
	BITMAP "..##.##."
	BITMAP ".#......"
	BITMAP ".##.#.#."
	BITMAP "..####.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
'BLACK
	BITMAP "........"
	BITMAP "##..#..."
	BITMAP "...#.#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "..#....."
	BITMAP "..####.."
	BITMAP "..#..##."
'PINK
	BITMAP "..#####."
	BITMAP "........"
	BITMAP "..#.#.#."
	BITMAP "...#.#.."
	BITMAP "........"
	BITMAP "...####."
	BITMAP "........"
	BITMAP "........"	
	
	
	
PAPI_DROITE_2:
'WHITE
	BITMAP ".#......"
	BITMAP "..##.##."
	BITMAP ".#......"
	BITMAP ".##.#.#."
	BITMAP "..####.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
'BLACK
	BITMAP "........"
	BITMAP "##..#..."
	BITMAP "...#.#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "..##...."
	BITMAP "..#####."
	BITMAP ".#...#.."
'PINK
	BITMAP "..#####."
	BITMAP "........"
	BITMAP "..#.#.#."
	BITMAP "...#.#.."
	BITMAP "........"
	BITMAP "....###."
	BITMAP "........"
	BITMAP "........"	
	
PAPI_HAUT_0:
'WHITE
	BITMAP "........"
	BITMAP ".#....#."
	BITMAP "........"
	BITMAP ".#....#."
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
'BLACK
	BITMAP "........"
	BITMAP "........"
	BITMAP ".######."
	BITMAP "#......."
	BITMAP "........"
	BITMAP "...##..."
	BITMAP "..####.."
	BITMAP "..####.."
'PINK
	BITMAP "........"
	BITMAP "..####.."
	BITMAP "........"
	BITMAP "..####.."
	BITMAP "...##..."
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "........"	
		
	
PAPI_HAUT_1:
'WHITE
	BITMAP ".#....#."
	BITMAP "........"
	BITMAP ".#....#."
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
'BLACK
	BITMAP "........"
	BITMAP "#######."
	BITMAP "........"
	BITMAP "........"
	BITMAP "...##..."
	BITMAP "..####.."
	BITMAP ".##.##.."
	BITMAP ".....##."
'PINK
	BITMAP "..####.."
	BITMAP "........"
	BITMAP "..####.."
	BITMAP "...##..."
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	
PAPI_HAUT_2:
'WHITE
	BITMAP ".#....#."
	BITMAP "........"
	BITMAP ".#....#."
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
'BLACK
	BITMAP "........"
	BITMAP "#######."
	BITMAP "........"
	BITMAP "........"
	BITMAP "...##..."
	BITMAP "..####.."
	BITMAP "..##.##."
	BITMAP ".##....."
'PINK
	BITMAP "..####.."
	BITMAP "........"
	BITMAP "..####.."
	BITMAP "...##..."
	BITMAP "..#..#.."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"	
	
	
	
	
	
	
	