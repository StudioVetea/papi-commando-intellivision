'######################################
'####     PAPI COMMANDO PROJECT    ####
'####     	  INTELLIVISION		   ####
'######################################


'==========================================
'===		INFO TEMPORISATION			===
'==========================================
' Tempo(0)= Papi
' Tempo(9)= Init Visée BUNKER
' Tempo(10)= Armes PAPI
' Tempo(2)= Tempo couverture tir du Tireur Bunker type CARD 32
' Tempo(3) - > Tempo(5) = Réservé IA

'CARD No 37 : ARMES du HUD Disponible.

'TypeIA : 1 = Robot Mine, 2 = Soldat




'///// Déclaration de toutes les variables /////
	option explicit

'///// Fonctions principales /////
	include "constants.bas"

'///// Mode graphique I /////
	mode SCREEN_FOREGROUND_BACKGROUND
	BORDER 0,2

'//////////////////////////////////////////////
'///// Chargement données graphiques GRAM /////
'//////////////////////////////////////////////
	wait
	DEFINE DEF00,3,PAPI_BAS_0
	wait	
	DEFINE DEF03,4,SOLDAT_1D
	wait
	DEFINE DEF07,3,SOLDAT_1B
	wait
	DEFINE DEF10,3,SOLDAT_1H
	wait
	DEFINE DEF13,2,Robot1
	wait
	DEFINE DEF15,1,Balle
	wait
	DEFINE 16,1,ARBRE_TRONC
	wait
	DEFINE 17,1,SAC_G
	wait
	DEFINE 18,1,SAC_M
	wait
	DEFINE 19,1,SAC_D
	wait
	DEFINE 20,1,SAC
	wait
	DEFINE 21,1,EAU_1
	wait	
	DEFINE 22,1,MUR_AVANT
    wait
    DEFINE 23,1,OmbreBatiment
    wait
	DEFINE 24,1,BARBELE_G
	wait	
	DEFINE 25,1,BARBELE_M
	wait
	DEFINE 26,1,BARBELE_D
	wait	
	DEFINE 27,1,TOITURE
	wait	
	DEFINE 28,1,MURMAISON_1
	wait	
	DEFINE 29,1,MURMAISON_ENTREE
	wait
	DEFINE 30,1,MURMAISON_2
	wait	
	DEFINE 31,1,TOITBUNKER
	wait	
	DEFINE 32,1,BUNKER_G
	wait	
	DEFINE 33,1,BUNKER_D
	wait
	DEFINE 34,1,BUNKER_M
    wait
    DEFINE 35,1,EntreeNoire
    wait	
	DEFINE 36,1,ViePapi
	wait	
	DEFINE 37,1,GrenadeTile
    wait
    DEFINE 38,1,MurMaison1
    wait
    DEFINE 39,1,MurMaison2
    wait
	DEFINE 40,1,PASSERELLE
    wait
    DEFINE 41,1,FrontWALL
    wait
	DEFINE 42,1,BLANK
	wait
	DEFINE 43,1,MURBRIQUE
	wait	
	DEFINE 44,1,MURBRIQUE_1
	wait	
	DEFINE 45,1,PONT_1
	wait	
	DEFINE 46,1,PONT_2
	wait	
	DEFINE DEF47,1,Balle
	wait
    DEFINE 48,1,HERBE1
	wait
	DEFINE 49,1,COINEAU_HD
	wait	
	DEFINE 50,1,COINEAU_HG
	wait
	DEFINE 51,1,COINEAU_BD
	wait	
	DEFINE 52,1,COINEAU_BG
	wait	
	DEFINE 53,1,COINEAU_HD1
	wait	
	DEFINE 54,1,COINEAU_HG1
	wait
	DEFINE 55,1,COINTEAU_BD1
	wait	
	DEFINE 56,1,COINEAU_BG1
	wait	
	DEFINE 57,1,IABUNKER_M
	wait
	DEFINE 58,1,GrenadeTile
	wait
    DEFINE 59,1,HautPont
	wait
	DEFINE 60,1,ImpactExplosion
	wait
	DEFINE 61,1,SOL_HERBE
	wait
	DEFINE 62,1,ROCHER
	wait
	DEFINE 63,1,ARBRE_CIME
	wait
	
'///// Constantes diverses /////
	CONST CARD_ARBRETRONC=$0800+16*8 + BG_DARKGREEN
	CONST CARD_SACG = $0800+17*8 + BG_YELLOW	
	CONST CARD_SACM = $0800+18*8 + BG_YELLOW
	CONST CARD_SACD = $0800+19*8 + BG_YELLOW
	CONST CARD_SAC = $0800+20*8 + BG_YELLOW
	CONST CARD_EAU1 = $0801+21*8 + BG_LIGHTBLUE
	CONST CARD_MURAVANT = $0800+22*8 + BG_GREY
	CONST CARD_OmbreBatiment=$804+ 23*8+BG_BLACK
	CONST CARD_BARBELEG = $0800+24*8 + BG_DARKGREEN
	CONST CARD_BARBELEM = $0800+25*8 + BG_DARKGREEN
	CONST CARD_BARBELED = $0800+26*8 + BG_DARKGREEN
	CONST CARD_TOITURE = $0800+27*8 + BG_RED
	CONST CARD_MURMAISON1 = $0800+28*8 + BG_GREY
	CONST CARD_MURMAISONENTREE = $0800+29*8 + BG_GREY
	CONST CARD_MURMAISON2 = $0800+30*8 + BG_GREY
	CONST CARD_TOITBUNKER = $0800+31*8 + BG_GREY
	CONST CARD_BUNKERG = $0800+32*8 + BG_GREY
	CONST CARD_BUNKERD = $0800+33*8 + BG_GREY
	CONST CARD_BUNKERM = $0800+34*8 + BG_GREY
	CONST CARD_BLACK= $0800+35*8 + BG_BLACK
	CONST CARD_VIES = $0802+36*8 + BG_BLACK
	CONST CARD_ITEM = $0806+37*8 + BG_BLACK
    CONST CARD_MurMaison11=$800+ 38*8+BG_GREY
    CONST CARD_MurMaison22=$800+ 39*8+BG_GREY
	CONST CARD_PASSERELLE = $0800+40*8 + BG_GREY
    CONST CARD_FrontWALL=$800+ 41*8+BG_GREY
	CONST CARD_BLANK = $0803+42*8 + BG_DARKGREEN
	CONST CARD_MURBRIQUE = $0800+43*8 + BG_GREY
	CONST CARD_MURBRIQUE_1 = $0800+44*8 + BG_GREY
	CONST CARD_PONT_1 = $0800+45*8 + BG_GREY
	CONST CARD_PONT_2 = $0800+46*8 + BG_GREY
    CONST CARD_HERBE1 = $0805+ 48*8+BG_DARKGREEN
	CONST CARD_COINEAU_HD = $0801+49*8 + BG_DARKGREEN
	CONST CARD_COINEAU_HG = $0801+50*8 + BG_DARKGREEN
	CONST CARD_COINEAU_BD = $0801+51*8 + BG_DARKGREEN
	CONST CARD_COINEAU_BG = $0801+52*8 + BG_DARKGREEN
	CONST CARD_COINEAU_HD1 = $0801+53*8 + BG_DARKGREEN
	CONST CARD_COINEAU_HG1 = $0801+54*8 + BG_DARKGREEN
	CONST CARD_COINEAU_BD1 = $0801+55*8 + BG_DARKGREEN
	CONST CARD_COINEAU_BG1 = $0801+56*8 + BG_DARKGREEN
	CONST CARD_IABUNKER_M = $0800+57*8 + BG_GREY
	CONST CARD_ITEM1 = $0802+58*8 + BG_BLACK
    CONST CARD_HautPont=$800+ 59*8+BG_DARKGREEN
	CONST CARD_ImpactBoum=$800+60*8 + BG_DARKGREEN
	CONST CARD_HERBE=$0805+61*8 + BG_DARKGREEN
	CONST CARD_ROCHER=$0800+62*8 + BG_DARKGREEN
	CONST CARD_ARBRECIME=$0805+63*8 + BG_DARKGREEN
	
	'Constante Collision Objets
	CONST SOL_TRONC= BG16+(FG_BLACK xor BG_DARKGREEN)
	CONST SOL_EAU = BG21 + (FG_BLUE xor BG_LIGHTBLUE)
    CONST COL_OmbreBatiment = BG23+ (FG_DARKGREEN xor BG_BLACK)
	CONST SOL_BARBELE_G= BG24+(FG_BLACK xor BG_DARKGREEN)	
	CONST SOL_BARBELE_M= BG25+(FG_BLACK xor BG_DARKGREEN)
	CONST SOL_BARBELE_D= BG26+(FG_BLACK xor BG_DARKGREEN)
    CONST COL_EntreeNoire=BG35+ (FG_BLACK xor BG_BLACK)
	CONST PASSERELLE=BG40+(FG_BLACK xor BG_GREY)
	CONST SOL_BLANK = BG42 + (FG_TAN xor BG_DARKGREEN)
	CONST PONTROUTE1=BG45 +(FG_BLACK xor BG_GREY)
	CONST PONTROUTE2=BG46 +(FG_BLACK xor BG_GREY)
	CONST COL_HERBE1=BG48+ (FG_GREEN xor BG_DARKGREEN)
	CONST SOL_COINEAU_HD=BG49+(FG_BLUE xor BG_DARKGREEN)
	CONST SOL_COINEAU_HG=BG50+(FG_BLUE xor BG_DARKGREEN)
	CONST SOL_COINEAU_BD=BG51+(FG_BLUE xor BG_DARKGREEN)
	CONST SOL_COINEAU_BG=BG52+(FG_BLUE xor BG_DARKGREEN)
	CONST SOL_COINEAU_HD1=BG53+(FG_BLUE xor BG_DARKGREEN)
	CONST SOL_COINEAU_HG1=BG54+(FG_BLUE xor BG_DARKGREEN)
	CONST SOL_COINEAU_BD1=BG55+(FG_BLUE xor BG_DARKGREEN)
	CONST SOL_COINEAU_BG1=BG56+(FG_BLUE xor BG_DARKGREEN)
	CONST COL_HautPont=BG59+ (FG_BLACK xor BG_DARKGREEN)
	CONST SOL_EXPLOSION = BG60+ ( FG_BLACK xor BG_DARKGREEN)
	CONST SOLHERBE = BG61+(FG_GREEN xor BG_DARKGREEN)
	CONST SOL_ROCHER=BG62+(FG_BLACK xor BG_DARKGREEN)
	CONST CIMEARBRE=BG63+(BG_DARKGREEN xor FG_GREEN)
	
	'MAP
	CONST MAP_HAUTEUR = 72
	CONST MAP_LARGEUR = 20
	CONST SCREEN_HAUTEUR = 12
	
	'Fonctions
	DEF FN ConvTILE(Nomb)=((Nomb+6)/8)*8
	DEF FN ConvTILE1(Nomb)=((Nomb+40-4)/8)*8
	DEF FN ConvTILE3(Nomb)=((Nomb+40+8)/8)*8
	DEF FN ConvTILE2(Nomb)=((Nomb-40+4)/8)*8
	
'////// Variables globales /////
'///// 8 bits /////
	Dim CoordX(10), CoordY(10), LifeBunker(8), Tempo(10),IABUNKER_CX(8),IABUNKER_CY(8),SensTirBunker(8),DieUnit(8),DieBunker(8),IDB(8),SpeedIA(4),VieSoldat(4)
	Dim i,j,c,d,f,bT,e,IDIA,value,offset_y,offset_d,position_y_zone,MaxObjet,DD,DG,DH,DB,TempoTile,COFX,COFY,NBBUNKER, DistanceArme, SensIA(4),DeadU,VelocityIA(4)
	Dim PosBunker,TirPapi,SensTIR, TempoTir, TypeArme, VitesseArme, DX_Arme, CouleurImpact,TirIA,ImpactTouch,TypeIA(4), MoveIA(4), NbIA,VisibleIA(4),TempoIA(4)
	Dim LifePapi, NbBalle, NbGrenade, Start, NbGrenadeM, ID, MAP_HAUTEUR,MAP_LARGEUR,MortPapi, Velocity,NbRoquette,eBunker,StartGame,AB,TirSoldat
	
'///// 16 bits /////
	Dim #Miroir,#Offset,#COL_TEST,#Visible, #Couleur, #ZoomOBjectX, #ZoomOBjectY,#Miroir1,#Miroir3,#SPR3,#SPR4,#SPR5,#Miroir4,#Miroir5,#Couleur3,#Couleur4,#Couleur5

'///// Init Variable
	gosub InitVariable
	gosub SonBlank
	
'///// Chargement et affichage de la Zone /////
	gosub DisplayZoneScreen
	'gosub Main

'/////////////////////////////
'///// Boucle principale /////
'/////////////////////////////	
'Main: procedure
	while(1)
		wait		
		STACK_CHECK
		'Démarrage
		if Startgame=0 then gosub DemGame else gosub GestionPAD
		if AB then gosub GestionBunker
		gosub GestionCollision
		gosub AnimTile
		gosub DisplayInfo
		gosub Gestion_Sprite
	wend


'///// Sous programmes /////
DemGame: procedure 
	DH=1
	if CoordY(0)<88 then StartGame=1
	end

'///// Init Variables /////
InitVariable: procedure
	cls
	position_y_zone=MAP_HAUTEUR-SCREEN_HAUTEUR
	NbIA=3
	bT=0
	TirSoldat=0
	DeadU=0
	AB=0
	MaxObjet=3+NbIA
	StartGame=0
	DD=0
	DG=0
	DH=0
	ID=0
	d=0
	DB=0
	c=0
	TirIA=0
	Velocity=0
	eBunker=0
	Start=0
	TypeArme=0
	DX_Arme=0
	ImpactTouch=0
	TirPapi=0
	VitesseArme=0
	SensTir=0
	TempoTir=0
	DistanceArme=0
	CouleurImpact=0
	NBBUNKER=0
	PosBunker=0
	offset_y=0
	offset_d=0
	SCROLL 0,0,0
	TempoTile=0
	for i=1 to 10
		CoordX(i)=0
		CoordY(i)=0
		if i<9 then 
			TempoIA(i)=0
			IABUNKER_CX(i)=0
			IABUNKER_CY(i)=0
			IDB(i)=0
			SensTirBunker(i)=0
			DieBunker(i)=0
			LifeBunker(i)=0
		end if
		Tempo(i)=0
	next
	gosub InitSprite
	'Init IA
	for IDIA=1 to NbIA
		SensIA(IDIA)=0
		MoveIA(iDIA)=0
		VisibleIA(IDIA)=0
		VisibleIA(IDIA)=1
		VieSoldat(IDIA)=0
		VelocityIA(IDIA)=0
		Gosub IATri
	next
	'Coordonnées IA Démarrage.
	CoordX(3)=80
	CoordY(3)=8
	CoordX(4)=80
	CoordY(4)=8
	CoordX(5)=80
	CoordY(5)=8
	CoordX(0)=80
	CoordY(0)=104
	
	if MortPapi=0 then
		MortPapi=0
		NbGrenade=10
		NbGrenadeM=5
		NbRoquette=10
		LifePapi=3
		NbBalle=40
		gosub TraitementElementMAP
	else
		'LifePapi=LifePapi-1
		MortPapi=0
		gosub INitSprite
		position_y_zone=MAP_HAUTEUR-SCREEN_HAUTEUR
		gosub SonBlank
		gosub TraitementElementMAP
		wait
		gosub DisplayZoneScreen
		wait
		gosub GestionPAD
	end if
	end
	
TestCOL: procedure
	d=1
	if  #COL_TEST<>PASSERELLE and #COL_TEST<>SOLHERBE and #COL_TEST<>SOL_BLANK and #COL_TEST<>SOL_EXPLOSION and #COL_TEST<>COL_OmbreBatiment and #COL_TEST<>COL_HERBE1 then d=0
	end

'///// Routines Collisions /////
'CARD_TEST: PROCEDURE
	'gosub PATHo
	'if i=0 then
		'if #COL_TEST=SOLHERBE then #BEHIND=0
		'if #COL_TEST=SOL_BLANK then #BEHIND=0
		'if #COL_TEST=PONTROUTE then #BEHIND=BEHIND:RET=1:return
		'if #COL_TEST=PONTROUTE1 then #BEHIND=BEHIND:RET=1:return
		'if #COL_TEST=CARD_HautPont then #BEHIND=BEHIND:RET=1:return
		'if #COL_TEST=PASSERELLE then #BEHIND=0:RET=1:return
	'end if
	'end 
	
DH_TEST: PROCEDURE
	if DH then 
		COFX=Coordx(i)-2
		COFY=CoordY(i)-3-offset_y
		gosub PATHo
		'if RET then return
		gosub TestCOL
		if d=0 then DH=0:return
		COFX=Coordx(i)-7
		gosub PATHo
		'if RET then return
		gosub TestCOL
		if d=0 then DH=0:return
	end if
	end 
	
DB_TEST: PROCEDURE
	if DB then 
		COFX=Coordx(i)-2
		COFY=CoordY(i)-offset_y
		gosub PATHo
		'if RET then return
		gosub TestCOL
		if d=0 then DB=0:return
		COFX=Coordx(i)-7
		gosub PATHo
		'if RET then return
		gosub TestCOL
		if d=0 then DB=0:return
	end if
	end
	
DD_TEST: PROCEDURE
	if DD then 
		COFX=Coordx(i)-1
		COFY=CoordY(i)-offset_y-2
		gosub PATHo
		'if RET then return
		gosub TestCOL
		if d=0 then DD=0:return
	end if
	end
	
DG_TEST: PROCEDURE
	if DG then 
		COFX=Coordx(i)-8
		COFY=CoordY(i)-offset_y-2
		gosub PATHo
		'if RET then return
		gosub TestCOL
		if d=0 then DG=0:return
	end if
	end

H_B: procedure
	COFX=Coordx(i)-2
	COFY=CoordY(i)-3-offset_y
	gosub PATHo
	gosub CollisionHARD_BALL
	if TirPapi=99 then return
	COFX=Coordx(i)-7
	gosub PATHo
	gosub CollisionHARD_BALL
	end

B_B: procedure
	COFX=Coordx(i)-2
	COFY=CoordY(i)-offset_y
	gosub PATHo
	gosub CollisionHARD_BALL
	if TirPapi=99 then return
	COFX=Coordx(i)-7
	gosub PATHo
	gosub CollisionHARD_BALL
	end
	
D_B: procedure
	COFX=Coordx(i)-1
	COFY=CoordY(i)-offset_y-2
	gosub PATHo
	gosub CollisionHARD_BALL
	end

G_B: procedure
	COFX=Coordx(i)-8
	COFY=CoordY(i)-offset_y-2
	gosub PATHo
	'if RET then return
	gosub CollisionHARD_BALL
	end

B_B1: procedure
	COFX=Coordx(i)-2
	COFY=CoordY(i)-offset_y
	gosub PATHo
	gosub CollisionHARD_BALL1
	if TirIA=99 then return
	COFX=Coordx(i)-7
	gosub PATHo
	gosub CollisionHARD_BALL1
	end
	
D_B1: procedure
	COFX=Coordx(i)-1
	COFY=CoordY(i)-offset_y-2
	gosub PATHo
	gosub CollisionHARD_BALL1
	end

G_B1: procedure
	COFX=Coordx(i)-8
	COFY=CoordY(i)-offset_y-2
	gosub PATHo
	'if RET then return
	gosub CollisionHARD_BALL1
	end

CollideObject: procedure
	j=0
	if  #COL_TEST<>SOLHERBE and #COL_TEST<>SOL_BLANK and #COL_TEST<>PASSERELLE and #COL_TEST<>SOL_EAU and #COL_TEST<>SOL_COINEAU_BD and #COL_TEST<>COL_OmbreBatiment and #COL_TEST<>COL_HERBE1 then
		if #COL_TEST<>SOL_COINEAU_BG and #COL_TEST<>SOL_COINEAU_HD and #COL_TEST<>SOL_COINEAU_HG and #COL_TEST<>SOL_COINEAU_BD1 and #COL_TEST<>SOL_COINEAU_BG1 and #COL_TEST<>SOL_COINEAU_HD1 then
			if #COL_TEST<>SOL_COINEAU_HG1 and #COL_TEST<>SOL_BARBELE_D and #COL_TEST<>SOL_BARBELE_M and #COL_TEST<>SOL_BARBELE_G and #COL_TEST<>SOL_EXPLOSION then j=1
		end if
	end if
	end
	
CollisionHARD_BALL: procedure
	gosub CollideObject
	if j=1 then 
		TirPapi=99
		if TypeArme=0 then
			if #COL_TEST=CARD_IABUNKER_M then
				d=1
				gosub Bunker_Hit
				if c=0 then return
				DieBunker(c)=1
				#backtab(#offset)=CARD_ImpactBoum
				gosub SonExplosionMAX
			end if
		end if
		if TypeArme=3 then 
			'Destruction d'arbre
			if #COL_TEST=SOL_ROCHER then return
			if #COL_TEST=CARD_IABUNKER_M then
				d=4
				gosub Bunker_Hit
				if c=0 then return
				DieBunker(c)=1
			end if
			#backtab(#offset)=CARD_ImpactBoum
			if #COL_TEST=SOL_TRONC then 
				#backtab(#offset)=CARD_ImpactBoum
				#offset=COFX/8+(COFY-8)/8*20
				#backtab(#offset)=CARD_HERBE
				return
			end if
			if #COL_TEST=CIMEARBRE then 
				#backtab(#offset)=CARD_ImpactBoum
				#offset=COFX/8+(COFY+8)/8*20
				#backtab(#offset)=CARD_HERBE
				return
			end if
		end if
	end if
	end
	
CollisionHARD_BALL1: procedure
	gosub CollideObject
	if j=1 then TirIA=99
	end
	
COLP: procedure
	gosub SonBlank
	MortPapi=1
	if Coordx(0)>80 then j=1 else j=2
	end
GestionCollision: PROCEDURE
	if MortPapi then return
	'Projectile Bunker / Papi
	'if COL0 AND HIT_SPRITE4 THEN gosub COLP:return
	'if COL1 AND HIT_SPRITE4 THEN gosub COLP:return
	'if COL2 AND HIT_SPRITE4 THEN gosub COLP:return

	'IA / PAPi
	'if (COL0+COL1+COL2) AND (HIT_SPRITE5+HIT_SPRITE6+HIT_SPRITE7) THEN gosub COLP:return

	'Armes / Decor
	if TirPapi then 
		i=1
		if TypeArme=0 then
			if TirPapi=99 then return
			gosub GestionCOL_Papi
		elseif TypeArme=3 then
			if TirPapi=99 then return
			gosub GestionCOL_Papi
		elseif TypeArme=4 then
			return
		end if
	end if
	'Tir Bunker
	if TirIA then
		i=2
		'Tir couvert
		Tempo(2)=Tempo(2)+1
		if Tempo(2)>12 then 
			Tempo(2)=0
			if SensTirBunker(ID)=0 then gosub B_B1
			if SensTirBunker(ID)=2 then gosub D_B1
			if SensTirBunker(ID)=1 then gosub G_B1
		end if
	end if
	end

GestionCOL_Papi: procedure
	if SensTir=1 then Gosub H_B
	if SensTir=2 then gosub B_B
	if SensTIR=3 then gosub D_B
	if SensTIR=4 then gosub G_B
	end

'///// Routine Animation Tile /////
AnimTile: PROCEDURE
	TempoTile=TempoTile+1
	if TempoTile=45 then TempoTile=0
	'EAU / Herbe
	if TempoTile=5 then DEFINE DEF21,1,EAU_1:return 
	if TempoTile=10 then DEFINE DEF21,1,EAU_2:return 
	if TempoTile=6 then DEFINE DEF42,1,BLANK:return 
	if TempoTile=15 then DEFINE DEF21,1,EAU_3:return 
	if TempoTile=20 then DEFINE DEF21,1,EAU_4:return 
	if TempoTile=16 then DEFINE DEF42,1,BLANK1:return
	if TempoTile=25 then DEFINE DEF21,1,EAU_5:return
	if TempoTile=30 then DEFINE DEF21,1,EAU_6:return
	if TempoTile=26 then DEFINE DEF42,1,BLANK2:return
	if TempoTile=35 then DEFINE DEF21,1,EAU_7:return 
	if TempoTile=40 then DEFINE DEF21,1,EAU_8:return 
	if TempoTile=36 then DEFINE DEF42,1,BLANK1:return
	end


'///// Routines animation Sprite /////
GestionANIMATION: PROCEDURE
	Tempo(0)=Tempo(0)+1
	if Tempo(0)>4 then Tempo(0)=0
	'Papi 
	i=0
	if MortPapi then gosub GestionDirPapi_B:return

	if DH then
		Sound 1,7000,7
		#miroir=0
		SensTir=1
		if Tempo(i)=1 then DEFINE DEF00, 3, Papi_HAUT_0
		if Tempo(i)=2 then DEFINE DEF00, 3, Papi_HAUT_1
		if Tempo(i)=3 then DEFINE DEF00, 3, Papi_HAUT_0
		if Tempo(i)=4 then DEFINE DEF00, 3, Papi_HAUT_2
	elseif DB then 
		Sound 1,7000,7
		#miroir=0
		SensTir=2 
		gosub GestionDirPapi_B
	elseif DD then 
		Sound 1,7000,7
		#miroir=0
		SensTir=3
		Gosub GestionDirPapi_D
	elseif DG then 
		Sound 1,7000,7
		#Miroir=FLIPX
		SensTir=4
		Gosub GestionDirPapi_D
	end if
	end
GestionDirPapi_D: procedure
		if Tempo(i)=1 then DEFINE DEF00, 3, Papi_DROITE_0
		if Tempo(i)=2 then DEFINE DEF00, 3, Papi_DROITE_1
		if Tempo(i)=3 then DEFINE DEF00, 3, Papi_DROITE_0
		if Tempo(i)=4 then DEFINE DEF00, 3, Papi_DROITE_2
	end
GestionDirPapi_B: procedure
	if Tempo(i)=1 then DEFINE DEF00, 3, Papi_BAS_0
	if Tempo(i)=2 then DEFINE DEF00, 3, Papi_BAS_1
	if Tempo(i)=3 then DEFINE DEF00, 3, Papi_BAS_0
	if Tempo(i)=4 then DEFINE DEF00, 3, Papi_BAS_2
	end

'///// Gestion Sprite /////
Gestion_Sprite: PROCEDURE
	'Déplacement 
	Tempo(1)=Tempo(1)+1
	if Tempo(1)>1 then 
		Gosub GestionANIMATION
		Tempo(1)=0
		'Collision Papi / Decor
		if CONT then i=0:gosub DH_TEST:gosub DB_TEST:gosub DD_TEST:gosub DG_TEST
		if DH then coordy(0)=coordy(0)-1
		if DB then coordy(0)=coordy(0)+1
		if DD then coordx(0)=coordx(0)+1
		if DG then coordx(0)=coordx(0)-1
		gosub ScrollingMAP
	end if
	end
	
'///// Routines PAD /////
InitDirection: procedure
	DH=0
	DG=0
	DB=0
	DD=0
	end
GestionPAD: PROCEDURE
	'Info Item en cours ? On quitte !
	if tempo(10) then gosub InitDirection:return
	
	'Si Couteau alors on quitte
	if TirPapi and TypeArme=4 then gosub InitDirection:return

	'Mort Papi ?
	if MortPapi then gosub InitDirection:return
	'Controle Joueur
	'if CONT=0 then 
		'if DH then DEFINE DEF00, 3, Papi_HAUT_0:#Miroir=0
		'if DB then DEFINE DEF00, 3, Papi_BAS_0:#Miroir=0
		'if DD then DEFINE DEF00, 3, Papi_DROITE_0:#Miroir=0
		'if DG then DEFINE DEF00, 3, Papi_DROITE_0:#Miroir=FLIPX
	'end if
	'Déplacement
	if CONT.UP then DH=1 else DH=0
	if CONT.DOWN then DB=1 else DB=0
	if CONT.LEFT then DG=1 else DG=0
	if CONT.RIGHT then DD=1 else DD=0
	
	'Changement d'armes / Ecran Pause Choix arme et info
	if CONT.B2 and Start=99 then 
		TypeArme=TypeArme+1
		'gosub SonValidation
		if TypeArme>4 then TypeArme=0
		'Pistolet
		if TypeArme=0 then 
			Start=1
			Tempo(10)=0
			wait	
			DEFINE 37,1,PistoletTile
			wait
		'Grenade
		elseIf TypeArme=1 then 
			Start=2
			Tempo(10)=0
			wait	
			DEFINE 37,1,GrenadeTile
			wait
			DEFINE DEF47,1,Viseur
			#ZoomOBjectY=ZOOMY2
			#ZoomOBjectX=0
			wait
		'Maxi Grenade
		elseIf TypeArme=2 then 
			Start=3
			Tempo(10)=0
			wait	
			DEFINE 37,1,GrenadeTile
			wait
		'Bazooka
		elseIf TypeArme=3 then 
			Start=4
			Tempo(10)=0
			wait	
			DEFINE 37,1,BazookaTile
			wait
		'Couteau
		elseif TypeArme=4 then
			Start=5
			tempo(10)=0
			wait
			DEFINE 37,1,Couteau
			wait
			DEFINE DEF47,1,CouteauH_1
			wait
		end if
	end if
	
	'Conditions sécurité
	if DH and DB then DH=0:DB=0
	if DG and DD then DG=0:DD=0
	
	'Tir à l'arrêt
	'if DH=0 and DD=0 and DG=0 and DB=0 then
	'Tir
	if CONT.B0 and TirPapi=0 then 
		if TypeArme=0 and NbBalle then 
			wait
			Gosub InitArmes
			DEFINE DEF47,1,Balle
			VitesseArme=5
			DistanceArme=70
			#Couleur=SPR_YELLOW
			#ZoomOBjectY=ZOOMY2
			#ZoomOBjectX=0
			NbBalle=NbBalle-1
			Gosub Sontir
			if NbBalle<1 then NbBalle=0:Start=1
			wait
		end if
		'On utilise les grenades/Bazooka uniquement à l'arrêt !
		if DH=0 and DD=0 and DG=0 and DB=0 then
			if TypeArme=1 and NbGrenade then 
				wait
				Gosub InitArmes
				Gosub SonBlank
				DEFINE DEF47,1,Balle
				VitesseArme=2
				DistanceArme=40
				#Couleur=SPR_BLACK
				#ZoomOBjectY=ZOOMY2
				#ZoomOBjectX=0
				NbGrenade=NbGrenade-1
				if NbGrenade<1 then NbGrenade=0:Start=2
				wait
			end if
			if TypeArme=2 and NbGrenadeM then 
				wait
				Gosub InitArmes
				Gosub SonBlank
				DEFINE DEF47,1,Balle
				VitesseArme=2
				DistanceArme=40
				#Couleur=SPR_RED
				#ZoomOBjectY=ZOOMY2
				#ZoomOBjectX=0
				NbGrenadeM=NbGrenadeM-1
				if NbGrenadeM<1 then NbGrenadeM=0:Start=3
				wait
			end if
			if TypeArme=3 and NbRoquette then 
				wait
				Gosub InitArmes
				Gosub SonBlank
				Gosub SonBazooka
				DEFINE DEF47,1,Balle
				VitesseArme=2
				DistanceArme=90
				#Couleur=SPR_RED
				#ZoomOBjectY=ZOOMY2
				#ZoomOBjectX=0
				NbRoquette=NbRoquette-1
				if NbRoquette<1 then NbRoquette=0:Start=4
				wait
			end if
		end if
		if TypeArme=4 then
			wait
			gosub InitArmes
			gosub SonBlank
			gosub SonCouteau
			DEFINE DEF47,1,CouteauH_1
			VitesseArme=0
			DistanceArme=0
			#Couleur=SPR_WHITE
			#ZoomOBjectY=ZOOMY2
			#ZoomOBjectX=0
			wait
		end if
	end if
	'end if
	i=0
	end
	
'///// Init Armes /////
InitArmes: procedure
	TirPapi=SensTIR
	CouleurImpact=0
	TempoTir=0
	DeadU=0
	if TypeArme=4 then 
		if SensTIR=1 then CoordX(1)=CoordX(0):CoordY(1)=CoordY(0)-8
		if SensTIR=2 then CoordX(1)=CoordX(0):CoordY(1)=CoordY(0)+8
		if SensTIR=3 then CoordX(1)=CoordX(0)+8:CoordY(1)=CoordY(0)
		if SensTIR=4 then CoordX(1)=CoordX(0)-8:CoordY(1)=CoordY(0)
	else
		CoordX(1)=CoordX(0)
		CoordY(1)=CoordY(0)
	end if
	DX_Arme=0
	end
	
'///// Impact Touch /////
ImpactHIT: procedure
	#couleur=SPR_RED
	if TempoTir%2=0 then 
		if DeadU=0 then DEFINE DEF47, 1, Touch else DEFINE DEF47, 1, Touch2
	else 
		DEFINE DEF47, 1, Touch1
	end if

	end
	
'///// Routines des sprites /////
IAHitBOX: procedure
	TirPapi=99
	ImpactTouch=1
	VieSoldat(c)=VieSoldat(c)-f
	f=0
	if VieSoldat(c)=0 then gosub IAV:return
	if VieSoldat(c)>50 then gosub IAV
	end
IAV: procedure
	VisibleIA(c)=2
	TempoIA(c)=0
	DeadU=1
	TempoTir=0
	VelocityIA(c)=0
	if RANDOM(100)<50 then SensIA(c)=0 else SensIA(c)=1
	end
IATOUCH: procedure
	if TirPapi=99 then return
	if TypeArme=0 then f=1
	if TypeArme=3 then f=4
	if TypeArme=1 then f=3
	if TypeArme=2 then f=8
	if COL3 and HIT_SPRITE5 then c=1:gosub IAHitBOX:return
	if COL3 and HIT_SPRITE6 then c=2:gosub IAHitBOX:return
	if COL3 and HIT_SPRITE7 then c=3:gosub IAHitBOX
	end	
IATOUCH1: procedure
	if TypeArme=1 then f=3 else f=8
	if COL3 and HIT_SPRITE5 then c=1:gosub IAHitBOX
	if COL3 and HIT_SPRITE6 then c=2:gosub IAHitBOX
	if COL3 and HIT_SPRITE7 then c=3:gosub IAHitBOX
	end
GCout: procedure
	f=8:gosub IATOUCH:gosub SonExplosion
	end
RoutineSprite: PROCEDURE
	i=0
	gosub LimS
	'Mort ?
	if MortPapi then
		Sound 0,(250-Velocity*10+FRAME) and 255,10
		Sound 1,(300+Velocity*10+FRAME) and 255,10
		Sound 2,(50-Velocity*2+FRAME) and 255,10
		wait
		Velocity=Velocity+1
		if j=2 then CoordX(0)=CoordX(0)+3 else CoordX(0)=CoordX(0)-3
		CoordY(0)=CoordY(0)-(10-Velocity)
		if CoordY(0)>100 then Gosub InitVariable
		if CoordX(0)>160 then Gosub InitVariable
		if CoordX(0)<8 then Gosub InitVariable
	end if
	#Visible=VISIBLE
	SPRITE 0,Coordx(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir,SPR00+SPR_WHITE
	SPRITE 1,Coordx(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir,SPR01+SPR_BLACK
	SPRITE 2,Coordx(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir,SPR02+SPR_PINK
	'end if
	
	'Armes Papi
	i=1
	#Miroir1=0
	'Couteau ?
	if TypeArme=4 then
		if TirPapi then
			TempoTir=TempoTir+1
			if DeadU then
				gosub ImpactHIT
				if TempoTir=12 then
					gosub SonBlank
					TirPapi=0
					TempoTir=0
					ImpactTouch=0
				end if
			else
				#couleur=SPR_WHITE
				if SensTIR=1 then 
					if TempoTir=1 then DEFINE DEF47,1,CouteauH_1
					if TempoTir=2 then DEFINE DEF47,1,CouteauH_2
					if TempoTir=3 then DEFINE DEF47,1,CouteauH_3:gosub GestionCouteauBunker:gosub GCout
					if TempoTir=4 then DEFINE DEF47,1,CouteauH_2
					if TempoTir=5 then DEFINE DEF47,1,CouteauH_1
				elseif SensTIR=2 then
					if TempoTir=1 then DEFINE DEF47,1,CouteauB_1
					if TempoTir=2 then DEFINE DEF47,1,CouteauB_2
					if TempoTir=3 then DEFINE DEF47,1,CouteauB_3:gosub GCout
					if TempoTir=4 then DEFINE DEF47,1,CouteauB_2
					if TempoTir=5 then DEFINE DEF47,1,CouteauB_1
				elseif SensTIR=3 then
					if TempoTir=1 then DEFINE DEF47,1,CouteauD_1
					if TempoTir=2 then DEFINE DEF47,1,CouteauD_2
					if TempoTir=3 then DEFINE DEF47,1,CouteauD_3:gosub GCout
					if TempoTir=4 then DEFINE DEF47,1,CouteauD_2
					if TempoTir=5 then DEFINE DEF47,1,CouteauD_1
				elseif SensTIR=4 then
					#Miroir1=FLIPX
					if TempoTir=1 then DEFINE DEF47,1,CouteauD_1
					if TempoTir=2 then DEFINE DEF47,1,CouteauD_2
					if TempoTir=3 then DEFINE DEF47,1,CouteauD_3:gosub GCout
					if TempoTir=4 then DEFINE DEF47,1,CouteauD_2
					if TempoTir=5 then DEFINE DEF47,1,CouteauD_1
				end if
				if TempoTir>5 then #visible=0 else #visible=VISIBLE
				if TempoTir=12 then
					gosub SonBlank
					TirPapi=0
					TempoTir=0
					ImpactTouch=0
				end if
			end if
		else
			#Visible=0	
		end if
	else
		if TirPapi then
			d=0
			#Visible=VISIBLE
			'Déplacement
			if TirPapi=1 then coordy(i)=Coordy(i)-VitesseArme:gosub GestionDistanceArme
			if TirPapi=2 then coordy(i)=Coordy(i)+VitesseArme:gosub GestionDistanceArme
			if TirPapi=3 then coordx(i)=Coordx(i)+VitesseArme:gosub GestionDistanceArme
			if TirPapi=4 then coordx(i)=Coordx(i)-VitesseArme:gosub GestionDistanceArme
			'Limite
			gosub LimS
			if TypeArme=0 then 
				'Tir / IA
				if TirPapi then gosub IATOUCH
				if d=1 then TirPapi=99
			end if
			'Grenade Papi
			if TypeArme=1 then 
				if d=1 then c=0
				gosub GestionGrenade
			end if
			if TypeArme=2 then 
				if d=1 then c=0
				gosub GestionGrenadeMaxi
			end if
			if TypeArme=3 then 
				if d=1 then TirPapi=99
				#Couleur=FRAME and 6
				gosub GestionBazooka
			end if
			'Balle Papi
			if TypeArme=0 then 
				#Couleur=SPR_YELLOW
				'if (FRAME and 3) then DEFINE DEF47, 1, Balle
				'Balle
				if TirPapi=99 then
					TempoTir=TempoTir+1
					if ImpactTouch then
						if TempoTir=1 then gosub SonExplosion
						gosub ImpactHIT
					else
						if TempoTir=1 then gosub SonImpact
						if TempoTir=1 then DEFINE DEF47, 1, Balle:#Couleur=SPR_WHITE
						if TempoTir=3 then DEFINE DEF47, 1, Balle1
						if TempoTir=5 then DEFINE DEF47, 1, Balle2
					end if
					
					if TempoTir=7 then
						gosub SonBlank
						TirPapi=0
						TempoTir=0
						ImpactTouch=0
						Sprite 3,0,0
						Coordx(i)=0
						Coordy(i)=0
					end if
				end if
			end if
		else
			#Visible=0	
			'Gestion viseur en mode grenade
			if TypeArme and MortPapi=0 then
				#Visible=VISIBLE
				#Couleur=SPR_WHITE

				if SensTIR=1 then 
					Coordx(i)=ConvTILE(Coordx(0))
					Coordy(i)=ConvTILE2(Coordy(0))
				elseif SensTIR=2 then
					Coordx(i)=ConvTILE(Coordx(0))
					Coordy(i)=ConvTILE1(Coordy(0))
				elseif SensTIR=3 then
					Coordx(i)=ConvTILE3(Coordx(0))
					Coordy(i)=ConvTILE(Coordy(0))
				elseif SensTIR=4 then
					Coordx(i)=ConvTILE2(Coordx(0))
					Coordy(i)=ConvTILE(Coordy(0))
				end if
				'Limite
				if CoordX(i)<8 then CoordX(i)=8:#Visible=0
				if CoordX(i)>160 then CoordX(i)=160:#Visible=0
				if coordy(i)>100 then coordy(i)=98:#Visible=0
				#ZoomOBjectY=ZOOMY2
				#ZoomOBjectX=0
				#Couleur=FRAME and 6
			end if
		end if
	end if
	gosub SPRTIR
	
	'Tir IA 1
	i=2
	if TirIA then
		#Visible=Visible
		if SensTirBunker(ID)=1 then
			CoordX(i)=CoordX(i)-2
			CoordY(i)=CoordY(i)+2
		elseif SensTirBunker(ID)=0 then
			CoordY(i)=CoordY(i)+2
		elseif SensTirBunker(ID)=2 then
			CoordX(i)=CoordX(i)+2
			CoordY(i)=CoordY(i)+2
		end if
		'Limite
		gosub LimS
		
		if TirIA=99 then
			gosub SonBlank
			TirIA=0
			Tempo(2)=0
			#Visible=0
			CoordX(i)=0
			CoordY(i)=0
			PosBunker=0
			Tempo(9)=0
		end if
		SPRITE 4,CoordX(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2,SPR15+SPR_YELLOW
	end if
	
	'Soldat / IA 
	i=3
	gosub RoutIA
	i=4
	gosub RoutIA
	i=5
	gosub RoutIA
	Sound 1,,0
	end
SPRTIR: procedure
	SPRITE 3,Coordx(i)+HIT+#Visible,CoordY(i)-offset_y+#ZoomOBjectX+#ZoomOBjectY+#Miroir1,SPR47+#couleur
	end
LimS: procedure
	f=0
	'Limite MAP/Joueur ?
	if position_y_zone=0 then
		if coordy(0)<16 then coordy(0)=16
	end if
	if coordx(i)>160 then coordx(i)=160:f=1
	if coordx(i)<8 then coordx(i)=8:f=1
	if coordy(i)>98 then coordy(i)=98:f=1
	if i=2 and f=1 then TirIA=99
	if i=1 and f=1 then d=1
	f=0
	end
RoutIA: procedure
	if i>=MaxObjet then return
	IDIA=i-2
	'IA qui saute de partout !
	if VisibleIA(IDIA)=2 then 
		VelocityIA(IDIA)=VelocityIA(IDIA)+1
		if SensIA(IDIA) then CoordX(i)=CoordX(i)+3 else CoordX(i)=CoordX(i)-3
		CoordY(i)=CoordY(i)-(7-VelocityIA(IDIA))
		if CoordY(i)>100 then VisibleIA(IDIA)=0
		if CoordX(i)>160 then VisibleIA(IDIA)=0
		if CoordX(i)<8 then VisibleIA(IDIA)=0
		Gosub IAP
	elseif VisibleIA(IDIA)=1 then
		#Visible=VISIBLE 
		gosub IAP
		gosub GestionIA
		gosub LimS
	elseif VisibleIA(IDIA)=0 then 
		if IDIA=1 then SPRITE 5,0,0
		if IDIA=2 then SPRITE 6,0,0
		if IDIA=3 then SPRITE 7,0,0
		gosub UpdateIA
	end if
	end
	
'///// Traitement des différets éléments de la MAP /////	
TraitementElementMAP: procedure
	for #Offset=0 to (MAP_Hauteur*MAP_LARGEUR)+SCREEN_HAUTEUR
		value=Zone1(MAP_LARGEUR+#Offset)
		'Bunker Tireur
		if value=32 then NBBUNKER=NBBUNKER+1
	next
	end
	

'///// Distance Arme /////
GestionDistanceArme: procedure
		'Distance ?
		DX_Arme=DX_Arme+VitesseArme
		if DX_Arme>DistanceArme then TirPapi=99
		end
		
'///// Routines d'affichage des Tiles/Scrolling. /////	
PrintLigneHaut:	procedure
	gosub PrintLigneBas
	for i=0 to MAP_LARGEUR
		gosub PL
	next
	end
	
VALDATA: procedure
	value=Zone1((MAP_LARGEUR*(position_y_zone))+i)
	end

D_DATA: procedure
	d=(MAP_LARGEUR*(position_y_zone))+i
	end
	
PrintLigneBas:	procedure
	for i=(SCREEN_HAUTEUR-1)*MAP_LARGEUR to (SCREEN_HAUTEUR*MAP_LARGEUR)-1
		gosub VALDATA
		if value=32 then AB=AB-1
	next
	end

PL: procedure
	gosub VALDATA
	gosub D_DATA
	'Gestion destruction Bunker MAP
	if value=32 then 
		AB=AB+1
		#backtab(i)=CARD_IABUNKER_M
		for j=1 to NBBUNKER
			if IDB(j)=0 then 
				#backtab(i)=CARD_IABUNKER_M 
				LifeBunker(j)=6
				IDB(j)=d
				eBunker=j
				IABUNKER_CX(eBunker)=(i*8)+8
				IABUNKER_CY(eBunker)=10
				exit for
			end if
		next
		PosBunker=0
		return
	end if
	gosub DefTile
	end
	
'///// Routine principale de scrolling /////	
ScrollingMAP: PROCEDURE
	if DH then
		'Limite ?
		if position_y_zone=0 then gosub RoutineSprite:return

		if coordy(0)<70 then
			if position_y_zone>0 then	
				IF offset_y=7 THEN 
					offset_d=3
					offset_y=0 
				ELSE 
					offset_y=offset_y+1
				end if
				CoordY(0)=70
				SCROLL 0,offset_y,offset_d
				for i=3 to 5
					'Offset Elements jeu
					coordy(i)=coordy(i)+1
					if Coordy(i)>102 then 
						f=i-2
						VisibleIA(f)=0 
					else 
						gosub IAP
					end if
				next
				GOSUB RoutineSprite
				if TirPapi then Coordy(1)=Coordy(1)+1
				if NBBUNKER then 
					for j=1 to eBunker
						IABUNKER_CY(j)= IABUNKER_CY(j)+1
					next
					if TirIA then CoordY(2)=CoordY(2)+1
				end if
				offset_d=0
				'Raffraichissement MAP
				IF offset_y=0 THEN 
					position_y_zone=position_y_zone-1
					WAIT
					GOSUB PrintLigneHaut
				end if
			end if
		else	
			GOSUB RoutineSprite:return
		end if
	else
		GOSUB RoutineSprite:return
	end if
	end 
	
IAP: procedure
	if i=3 then SPRITE 5,CoordX(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir3,#SPR3+#couleur3
	if i=4 then SPRITE 6,CoordX(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir4,#SPR4+#couleur4
	if i=5 then SPRITE 7,CoordX(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir5,#SPR5+#couleur5
	end

'###########################################################
'############ Nouvelle Zone en ROM.RAM #####################
'###########################################################

ASM ORG $C040

'/////////////////////
'///// Bank SONS /////
'/////////////////////
SonBlank: procedure
	SOUND 0,1,0
	SOUND 1,1,0
	SOUND 2,1,0
	SOUND 4,,$38
	end
	
SonCouteau: procedure
	SOUND 4,$03,$0007
	Sound 2,$00c8,$30
	Sound 3,$03E8,$0004
	end
	
SonBazooka: procedure
	SOUND 4,$05,$0007
	Sound 2,$0bb8,$30
	Sound 3,$0fff,$000d
	end

SonImpact: procedure
	SOUND 4,$05,$0007
	Sound 2,$0FA0,$30
	Sound 3,$0447,$0000
	end 
	
SonTir: procedure
	SOUND 4,$0c,$0007
	Sound 2,$0FA0,$30
	Sound 3,$0800,$0000
	end

SonExplosion: procedure
	SOUND 4,$19,$0007
	Sound 2,$0FA0,$30
	Sound 3,$0FFF,$0000
	end
	
SonExplosionMax: procedure
	SOUND 4,$1E,$0000
	Sound 2,$0FA0,$30
	Sound 3,$0FFF,$0000
	end	

SonValidation: procedure
	SOUND 4,$15,$0038
	Sound 2,$00c8,$30
	Sound 3,$0FFF,$0000
	end
	
'///// Gestion IA /////
GestionIA: procedure
	'Couleur Unité / Vitesse	
	f=SpeedIA(IDIA)
	if i=3 then
		if f=5 then #couleur3=SPR_TAN
		if f=4 then #couleur3=SPR_YELLOW 
		if f=3 then #couleur3=SPR_RED 
		if f=2 then #couleur3=SPR_WHITE
		if f=1 then #couleur3=SPR_BLACK
	elseif i=4 then
		if f=5 then #couleur4=SPR_TAN
		if f=4 then #couleur4=SPR_YELLOW 
		if f=3 then #couleur4=SPR_RED 
		if f=2 then #couleur4=SPR_WHITE
		if f=1 then #couleur4=SPR_BLACK
	elseif i=5 then
		if f=5 then #couleur5=SPR_TAN
		if f=4 then #couleur5=SPR_YELLOW 
		if f=3 then #couleur5=SPR_RED 
		if f=2 then #couleur5=SPR_WHITE
		if f=1 then #couleur5=SPR_BLACK
	end if
	
	'Robot
	if TypeIA(IDIA)=1 then
		'Mort ?
		if VisibleIA(IDIA)=2 then
			TempoIA(IDIA)=TempoIA(IDIA)+1
		end if
		Tempo(i)=Tempo(i)+1
		if Tempo(i)>=SpeedIA(IDIA) then
			TempoIA(IDIA)=TempoIA(IDIA)+1
			f=TempoIA(IDIA)
			Tempo(i)=0
			if f>2 then TempoIA(IDIA)=0
			if f=1 then 
				if i=3 then #SPR3=SPR13:#Miroir3=0
				if i=4 then #SPR4=SPR13:#Miroir4=0
				if i=5 then #SPR5=SPR13:#Miroir5=0
			end if
			if f=2 then 
				if i=3 then #SPR3=SPR14:#Miroir3=0
				if i=4 then #SPR4=SPR14:#Miroir4=0
				if i=5 then #SPR5=SPR14:#Miroir5=0
			end if
			gosub PATHL
		end if
	'Soldat
	elseif TypeIA(IDIA)=2 then
		'Animation
		Tempo(i)=Tempo(i)+1
		if Tempo(i)>=SpeedIA(IDIA)then
			TempoIA(IDIA)=TempoIA(IDIA)+1
			f=TempoIA(IDIA)
			Tempo(i)=0
			gosub PATHL
			if SensIA(IDIA)<2 then
				if f=1 then 
					if i=3 then #SPR3=SPR03
					if i=4 then #SPR4=SPR03
					if i=5 then #SPR5=SPR03
				end if
				if f=2 then 
					if i=3 then #SPR3=SPR04
					if i=4 then #SPR4=SPR04
					if i=5 then #SPR5=SPR04
				end if
				if f=3 then 
					if i=3 then #SPR3=SPR05
					if i=4 then #SPR4=SPR05
					if i=5 then #SPR5=SPR05
				end if
				if f=4 then 
					if i=3 then #SPR3=SPR06
					if i=4 then #SPR4=SPR06
					if i=5 then #SPR5=SPR06
				end if
				if f>4 then TempoIA(IDIA)=0
				if SensIA(IDIA)=1 then 
					if i=3 then #miroir3=FLIPX
					if i=4 then #miroir4=FLIPX
					if i=5 then #miroir5=FLIPX
				else 
					if i=3 then #miroir3=0
					if i=4 then #miroir4=0
					if i=5 then #miroir5=0
				end if
			elseif SensIA(IDIA)=3 then
				if f=1 then 
					if i=3 then #SPR3=SPR07
					if i=4 then #SPR4=SPR07
					if i=5 then #SPR5=SPR07
				end if
				if f=2 then 
					if i=3 then #SPR3=SPR08
					if i=4 then #SPR4=SPR08
					if i=5 then #SPR5=SPR08
				end if
				if f=3 then 
					if i=3 then #SPR3=SPR09
					if i=4 then #SPR4=SPR09
					if i=5 then #SPR5=SPR09
				end if
				if f>3 then TempoIA(IDIA)=0
				if i=3 then #miroir3=0
				if i=4 then #miroir4=0
				if i=5 then #miroir5=0
			elseif SensIA(IDIA)=2 then
				if f=1 then 
					if i=3 then #SPR3=SPR10
					if i=4 then #SPR4=SPR10
					if i=5 then #SPR5=SPR10
				end if
				if f=2 then 
					if i=3 then #SPR3=SPR11
					if i=4 then #SPR4=SPR11
					if i=5 then #SPR5=SPR11
				end if
				if f=3 then 
					if i=3 then #SPR3=SPR12
					if i=4 then #SPR4=SPR12
					if i=5 then #SPR5=SPR12
				end if
				if f>3 then TempoIA(IDIA)=0
				if i=3 then #miroir3=0
				if i=4 then #miroir4=0
				if i=5 then #miroir5=0
			end if
		end if
	end if
	end
	
PATHL: procedure
	if StartGame and MortPapi=0 then gosub PathIA
	end
	
IATri: procedure
	VieSoldat(IDIA)=RANDOM(5):if VieSoldat(IDIA)=0 then VieSoldat(IDIA)=1
	SpeedIA(IDIA)=VieSoldat(IDIA):if SpeedIA(IDIA)=0 then SpeedIA(IDIA)=1
	TypeIA(IDIA)=RANDOM(3):if TypeIA(IDIA)=0 then TypeIA(IDIA)=1:if TypeIA(IDIA)>2 then TypeIA(IDIA)=2
	end
	
'///// Update MAP Information ITEM /////
UIA: procedure
	CoordY(i)=offset_y
	Coordx(i)=c*8
	TempoIA(IDIA)=0
	Tempo(i)=0
	#Visible=Visible
	c=0
	Gosub IATri
	VisibleIA(IDIA)=1
	end
UpdateIA: procedure
	Tempo(i)=Tempo(i)+1
	if Tempo(i)>50 then
		Tempo(i)=0
		c=0
		for j=0 to MAP_LARGEUR
			c=c+1
			value=Zone1((MAP_LARGEUR*(position_y_zone))+j)
			if value=27 and RANDOM(100)<75  then gosub UIA:exit for
			if value=0 and RANDOM(100)<75 then gosub UIA:exit for
		next
	end if
	end

PATHO: procedure
	#offset = COFX/8+COFY/8*20
	#COL_TEST = PEEK($0200+#offset)
	end

PathIA: procedure
	IF (Coordx(i) AND 7)+((Coordy(i)-offset_y) AND 7) THEN GOTO loop_moving2

	if (FRAME and RANDOM(32))=0 then gosub SEARCHIA
	
	MoveIA(IDIA)=0
	'D
	COFX=Coordx(i)-1+8
	COFY=CoordY(i)-offset_y-2
	gosub PATHO
	gosub TestCOL
	if d=0 then MoveIA(IDIA)=MoveIA(IDIA)+1
	
	'G
	COFX=Coordx(i)-16
	gosub PATHO
	gosub TestCOL
	if d=0 then MoveIA(IDIA)=MoveIA(IDIA)+2
	'B
	COFX=Coordx(i)-2
	COFY=CoordY(i)-offset_y-16
	gosub PATHO
	gosub TestCOL
	if d=0 then MoveIA(IDIA)=MoveIA(IDIA)+4
	'H
	COFX=Coordx(i)-2
	COFY=CoordY(i)-3-offset_y+8
	gosub PATHO
	gosub TestCOL
	if d=0 then MoveIA(IDIA)=MoveIA(IDIA)+8
	
	ON SensIA(IDIA) GOTO loop_moving_horiz,loop_moving_horiz,loop_moving_vert,loop_moving_vert

	' If it was moving horizontally, go now for vertical
loop_moving_horiz:
	IF abs(Coordy(0)-Coordy(i)-offset_y)<=RANDOM(96) AND (MoveIA(IDIA) AND 3) = 0 THEN GOTO loop_moving2
	IF Coordy(0) < Coordy(i)-offset_y THEN GOTO loop_moving_horiz_1
	IF (MoveIA(IDIA) AND 8) = 0 THEN SensIA(IDIA) = 3:GOTO loop_moving2
loop_moving_horiz_1:
	IF (MoveIA(IDIA) AND 4) = 0 THEN SensIA(IDIA) = 2:GOTO loop_moving2
	IF (MoveIA(IDIA) AND 8) = 0 THEN SensIA(IDIA) = 3
	GOTO loop_moving2

	' If it was moving vertically, go now for horizontal
loop_moving_vert:
	IF abs(Coordx(0) - Coordx(i))<=RANDOM(96) AND (MoveIA(IDIA) AND 12) = 0 THEN GOTO loop_moving2
	IF Coordx(0) <  Coordx(i) THEN GOTO loop_moving_vert_1
	IF (MoveIA(IDIA) AND 1) = 0 THEN SensIA(IDIA) = 0:GOTO loop_moving2
loop_moving_vert_1:
	IF (MoveIA(IDIA) AND 2) = 0 THEN SensIA(IDIA) = 1:GOTO loop_moving2
	IF (MoveIA(IDIA) AND 1) = 0 THEN SensIA(IDIA) = 0
	GOTO loop_moving2

loop_moving2:
		if Coordy(i)>100-offset_y then VisibleIA(IDIA)=0:return
		IF SensIA(IDIA) = 0 THEN Coordx(i) = Coordx(i) +1'D
		IF SensIA(IDIA) = 1 THEN Coordx(i) = Coordx(i) - 1 'G
		IF SensIA(IDIA)= 2 THEN Coordy(i) = Coordy(i) - 1  'b
		IF SensIA(IDIA) = 3 THEN Coordy(i) = Coordy(i) +1 'h
	end
	
SEARCHIA: procedure
	if abs(Coordx(0)-Coordx(i))<RANDOM(48) then 
		if Coordy(0)>=Coordy(i) then SensIA(IDIA)=3:return else SensIA(IDIA)=2:return
	elseif abs(Coordy(0)-Coordy(i))<RANDOM(48) then
		if Coordx(0)>=Coordx(i) then SensIA(IDIA)=0:return else SensIA(IDIA)=1
	end if
	end

'///// Gestion Bunker /////
GestionBunker: procedure
	if MortPapi then return
	if TirIA then return
	f=CoordY(0)
	e=CoordX(0)
	'Gestion des différents Bunker rencontrés.
	if NBBUNKER then
		for i=1 to eBunker
			if (f>IABUNKER_CY(i)+8) and IDB(i) and DieBunker(i)=0 then 
				if abs(f-IABUNKER_CY(i))<55 then
					if abs(e-IABUNKER_CX(i))<40 then
						ID=i
						exit for
					end if
				end if
			else
				ID=0
			end if
		next
	end if
	
	if ID and bT=0 then DEFINE DEF57,1,IABUNKER_M:wait:bT=1
	if ID=0 and bT=1 then DEFINE DEF57,1,BUNKER_M:wait:bT=0
	
	if ID=0 then return
	if DieBunker(ID) then return
	
	'Pos X du Tireur / Ciblage
	Tempo(9)=Tempo(9)+1
	if Tempo(9)>30 then
		Tempo(9)=0
		PosBunker=0
		'Animation du Ciblage & Direction Tir IA.
		if e<(IABUNKER_CX(ID)-8) then SensTirBunker(ID)=1:PosBunker=1
		if (e>=(IABUNKER_CX(ID)-8) and e<=IABUNKER_CX(ID)+8) then SensTirBunker(ID)=0:PosBunker=1
		if e>IABUNKER_CX(ID)+8 then SensTirBunker(ID)=2:PosBunker=1
	end if

	if PosBunker then
		'Choix du Tir IA 
		#Visible=0
		if (abs(f-IABUNKER_CY(ID)))<60 then
			if (abs(e-IABUNKER_CX(ID)))<55 then
				TirIA=1
				Gosub SonBlank
				gosub SonTir
				CoordX(2)=IABUNKER_CX(ID)
				CoordY(2)=IABUNKER_CY(ID)+4
				if SensTirBunker(ID)=1 then DEFINE DEF57,1,IABUNKER_G:wait:return
				if SensTirBunker(ID)=0 then DEFINE DEF57,1,IABUNKER_M:wait:return
				if SensTirBunker(ID)=2 then DEFINE DEF57,1,IABUNKER_D:wait:return
			end if
		end if
	end if
	
	end

'///// Affichage de la Zone de jeu sur l'écran complet /////
DisplayZoneScreen: procedure
	for i=0 to 239
		gosub VALDATA
		gosub DefTile
	next
	end
	
'///// Gestion Bazooka /////
GestionBazooka: procedure
	'Tir / IA
	if TirPapi then gosub IATOUCH
	if TirPapi=99 then
		Sound 0,,0
		TempoTir=TempoTir+1
		if TempoTir=1 then 
			gosub SonExplosion
			#Couleur=SPR_RED 
			DEFINE DEF47, 1, Balle
		end if
		if ImpactTouch then
			#Couleur=SPR_WHITE
			gosub ImpactHIT
		else
			#Couleur=SPR_RED 
			if TempoTir=4 then DEFINE DEF47, 1, Balle1
			if TempoTir=7 then DEFINE DEF47, 1, Balle2
			if TempoTir=10 then DEFINE DEF47, 1, Balle3
		end if

		if TempoTir=13 then 
			gosub RAC
		end if
	end if
	end
RAC: procedure
	gosub SonBlank
	TirPapi=0
	TempoTir=0
	ImpactTouch=0
	wait
	j=0
	Coordx(i)=0
	Coordy(i)=0
	DEFINE DEF47, 1, viseur
	end

'///// Gestion Grenade /////
GestionGrenade: procedure
	gosub GestionZoomGrenade
	if TirPapi then Sound 0,100-DX_Arme,10
	if TirPapi=99 then
		Sound 0,,0
		gosub GestionCollisionExplosif
		TempoTir=TempoTir+1
		if TempoTir=1 then 
			gosub SonExplosion
			'#ZoomOBjectY=ZOOMY2
			if CouleurImpact=1 then 
				#Couleur=SPR_RED 
				DEFINE DEF47, 1, Balle
			elseif CouleurImpact=2 then 
				#Couleur=SPR_WHITE
				DEFINE DEF47, 1, Gerbe
			end if

		end if
		if ImpactTouch then
			#Couleur=SPR_WHITE
			gosub ImpactHIT
		else
			gosub A1
		end if
		
		if TempoTir=13 then 
			gosub RAC
		end if
	end if
	end

'///// ZOOM Grenade /////
GestionZoomGrenade: procedure
	if DX_Arme=1 then DEFINE DEF47, 1, Balle
	if DX_Arme=7 then DEFINE DEF47, 1, Grenade
	if DX_Arme=12 then DEFINE DEF47, 1, Grenade1
	if DX_Arme=20 then DEFINE DEF47, 1, Grenade2
	if DX_Arme=28 then DEFINE DEF47, 1, Grenade1
	if DX_Arme=33 then DEFINE DEF47, 1, Grenade1
	if DX_Arme=DistanceArme then wait:DEFINE DEF47, 1, Balle
	end

GestionGrenadeMaxi: procedure
	gosub GestionZoomGrenade
	if TirPapi then Sound 0,100-DX_Arme,10
	if TirPapi=99 then
		sound 0,,0
		TempoTir=TempoTir+1
		if TempoTir=1 then 
			gosub GestionCollisionExplosifMaxi
			gosub SonExplosionMAX
			if CouleurImpact=1 then 
				#ZoomOBjectX=ZOOMX2
				#ZoomOBjectY=ZOOMY4
				#Couleur=SPR_RED 
				DEFINE DEF47, 1, Balle
			elseif CouleurImpact=2 then 
				#Couleur=SPR_WHITE
				#ZoomOBjectX=0
				#ZoomOBjectY=ZOOMY2
				DEFINE DEF47, 1, Gerbe
			end if
		end if
		gosub A1
		if TempoTir=13 then 
			gosub SonBlank
			TirPapi=0
			TempoTir=0
			wait
			j=0
			DEFINE DEF47, 1, viseur
		end if
	end if
	end
	
A1: procedure
	if CouleurImpact=1 then
		#Couleur=SPR_RED 
		if TempoTir=4 then DEFINE DEF47, 1, Balle1
		if TempoTir=7 then DEFINE DEF47, 1, Balle2
		if TempoTir=10 then DEFINE DEF47, 1, Balle3
	elseif CouleurImpact=2 then
		#Couleur=SPR_WHITE
		if TempoTir=4 then DEFINE DEF47, 1, Gerbe1
		if TempoTir=7 then DEFINE DEF47, 1, Gerbe2
		if TempoTir=10 then DEFINE DEF47, 1, Gerbe3
	end if
	end

'///// Affichage Informations /////
DisplayInfo: procedure
	if Start=99 then return
	Gosub DisplayItem
	end

'///// Init Sprite //////
InitSprite: procedure
	'INIT Sprite
	SPRITE 0,0,0
	SPRITE 1,0,0
	SPRITE 2,0,0
	SPRITE 3,0,0
	SPRITE 4,0,0
	SPRITE 5,0,0
	SPRITE 6,0,0
	SPRITE 7,0,0
	end

'///// Affichage Vies /////	
DisplayLife: procedure
	Tempo(10)=Tempo(10)+1
	Print AT SCREENPOS(0,10) COLOR 7,"LIFES "
	for i=1 to LifePapi
		PRINT AT SCREENPOS(5+i,10) COLOR CS_YELLOW,CARD_VIES
	next
	if Tempo(10)>25 then
		gosub UpdateMAPITEM
	end if
	end
	
'///// Update ITEM /////
DisplayItem: procedure
	Tempo(10)=Tempo(10)+1
	if Start=0 then
		Print AT SCREENPOS(0,10) COLOR 7,"LIFES "
		for i=1 to LifePapi
			PRINT AT SCREENPOS(5+i,10) COLOR CS_RED,CARD_VIES
		next
	end if
	
	if Start=1 then
		c=NbBalle
		gosub DI
	elseif Start=2 then
		c=NbGrenade
		Print AT SCREENPOS(0,10) COLOR CS_YELLOW,CARD_ITEM
		if NbGrenade then PRINT AT SCREENPOS(1,10) COLOR 7,<2>NbGrenade else PRINT AT SCREENPOS(1,10) COLOR 7,"NO AMMO"
	elseif Start=3 then
		c=NbGrenadeM
		gosub DI1
	elseif  Start=4 then
		c=NbRoquette
		gosub DI
	elseif Start=5 then
		Print AT SCREENPOS(0,10) COLOR CS_YELLOW,CARD_ITEM
	end if
	if Tempo(10)>30 then
		gosub UpdateMAPITEM
	end if
	end
DI1: procedure
		Print AT SCREENPOS(0,10) COLOR CS_YELLOW,CARD_ITEM1
		if c then PRINT AT SCREENPOS(1,10) COLOR 7,<2>c else PRINT AT SCREENPOS(1,10) COLOR 7,"NO AMMO"
	end
DI: procedure
		Print AT SCREENPOS(0,10) COLOR CS_YELLOW,CARD_ITEM
		if c then PRINT AT SCREENPOS(1,10) COLOR 7,<2>c else PRINT AT SCREENPOS(1,10) COLOR 7,"NO AMMO"
	end

'///// Update MAP Information ITEM /////
UpdateMAPITEM: procedure
	Start=99
	Tempo(10)=0
	c=0
	for i=(SCREEN_HAUTEUR-2)*(MAP_LARGEUR) to (SCREEN_HAUTEUR*MAP_LARGEUR)-2
		c=c+1
		gosub VALDATA
		gosub D_DATA
		gosub DefTile
		if c>8 then return
	next
	end
	
'//////////////////////////////////////
'///// Collision Grenade/Explosif /////
'//////////////////////////////////////

'///// Grenades normales
GestionCollisionExplosif: procedure
		if CouleurImpact=0 then
			COFX=Coordx(i)-2
			COFY=(CoordY(i)-2)-offset_y
			#offset=COFX/8+COFY/8*20
			#COL_TEST = PEEK($0200+#offset)
			CouleurImpact=1
			'Destruction d'arbre
			if #COL_TEST=SOL_TRONC then 
				#backtab(#offset)=CARD_ImpactBoum
				#offset=COFX/8+(COFY-8)/8*20
				#backtab(#offset)=CARD_HERBE
				return
			end if
			if #COL_TEST=CIMEARBRE then 
				#backtab(#offset)=CARD_ImpactBoum
				#offset=COFX/8+(COFY+8)/8*20
				#backtab(#offset)=CARD_HERBE
				return
			end if
			if #COL_TEST=CARD_IABUNKER_M then
				d=3
				gosub Bunker_Hit
				if c=0 then return
				DieBunker(c)=1
			end if
			gosub IATOUCH1
			if #COL_TEST=SOL_ROCHER then return
			if #COL_TEST<>SOL_EAU then #backtab(#offset)=CARD_ImpactBoum
			if #COL_TEST=PASSERELLE then #backtab(#offset)=CARD_EAU1:return
			if #COL_TEST=SOL_EAU then CouleurImpact=2
		end if
	end
'////// Grenades fragmentation /////
GestionCollisionExplosifMaxi: procedure
	if CouleurImpact=0 then
		if Coordy(i)>90 then c=8 else c=0
		COFX=Coordx(i)-2
		COFY=(CoordY(i)-2)-offset_y
		#offset=COFX/8+COFY/8*20
		#COL_TEST = PEEK($0200+#offset)
		CouleurImpact=1
		d=8
		gosub Bunker_Hit
		gosub IATOUCH1
		'Destruction d'arbre
		if #COL_TEST=SOL_TRONC then 
			#backtab(#offset)=CARD_ImpactBoum
			#offset=COFX/8+(COFY-8)/8*20
			#backtab(#offset)=CARD_HERBE
			return
		end if
		if #COL_TEST=CIMEARBRE then 
			#backtab(#offset)=CARD_ImpactBoum
			#offset=COFX/8+(COFY+8-c)/8*20
			#backtab(#offset)=CARD_HERBE
			return
		end if
		#offset=COFX/8+COFY/8*20
		#COL_TEST = PEEK($0200+#offset)
		CouleurImpact=1
		d=8
		gosub Bunker_Hit
		gosub IATOUCH1
		gosub CM
		
		#offset=COFX/8+(COFY-8)/8*20
		#COL_TEST = PEEK($0200+#offset)
		d=8
		gosub Bunker_Hit
		gosub IATOUCH1
		gosub CM
		
		#offset=COFX/8+(COFY+8-c)/8*20
		#COL_TEST = PEEK($0200+#offset)
		d=8
		gosub Bunker_Hit
		gosub IATOUCH1
		gosub CM
		
		#offset=(COFX+8)/8+(COFY)/8*20
		#COL_TEST = PEEK($0200+#offset)
		d=8
		gosub Bunker_Hit
		gosub IATOUCH1
		gosub CM
		
		#offset=(COFX-8)/8+(COFY)/8*20
		#COL_TEST = PEEK($0200+#offset)
		d=8
		gosub Bunker_Hit
		gosub IATOUCH1
		gosub CM
		if #COL_TEST=SOL_EAU then CouleurImpact=2
	end if
end

IAT2: procedure
	VisibleIA(c)=2:VelocityIA(c)=0
	end
CM: procedure
		if #COL_TEST<>SOL_EAU and #COL_TEST<>SOL_ROCHER and #COL_TEST<>CARD_IABUNKER_M then 
			#backtab(#offset)=CARD_ImpactBoum
			if #COL_TEST=PASSERELLE then #backtab(#offset)=CARD_EAU1
		end if
		if ImpactTouch then #backtab(#offset)=CARD_ImpactBoum
	end
	
'///// Couteau vs Bunker /////
GestionCouteauBunker: procedure
	COFX=Coordx(i)-2
	COFY=(CoordY(i)-2)-offset_y
	#offset=COFX/8+COFY/8*20
	#COL_TEST = PEEK($0200+#offset)
	d=8
	gosub Bunker_Hit
	if ImpactTouch then #backtab(#offset)=CARD_ImpactBoum:gosub SonExplosion
	end

'///// Bunker touché ! //////
Bunker_Hit: procedure
	if #COL_TEST=CARD_IABUNKER_M and bT then
		c=0
		for i=1 to NBBUNKER
			if abs(coordy(1)-IABUNKER_CY(i))<16 then
				if abs(Coordx(1)-IABUNKER_CX(i))<16 then
					LifeBunker(i)=LifeBunker(i)-d
					if LifeBunker(i)>50 then LifeBunker(i)=0
					if LifeBunker(i)=0 then DieBunker(i)=1:c=i
					ImpactTouch=1
					exit for
				end if
			end if
		next
	end if
	end
	

'///// Elaboration MAP /////	
DefTile: procedure
	if value=0 then #backtab(i)=CARD_HERBE:return
	if value=1 then #backtab(i)=CARD_ROCHER:return
	if value=2 then #backtab(i)=CARD_ARBRECIME:return
	if value=3 then #backtab(i)=CARD_ARBRETRONC:return
	if value=4 then #backtab(i)=CARD_SACG:return 
	if value=5 then #backtab(i)=CARD_SACM:return 
	if value=6 then #backtab(i)=CARD_SACD:return 
	if value=7 then #backtab(i)=CARD_SAC:return
	if value=8 then #backtab(i)=CARD_EAU1:return
	if value=10 then #backtab(i)=CARD_MURAVANT:return 
	if value=11 then #backtab(i)=CARD_FrontWALL:return
	if value=12 then #backtab(i)=CARD_BARBELEG:return 
	if value=13 then #backtab(i)=CARD_BARBELEM:return 
	if value=14 then #backtab(i)=CARD_BARBELED:return
	if value=15 then #backtab(i)=CARD_TOITURE:return
	if value=16 then #backtab(i)=CARD_MURMAISON1:return 
	if value=17 then #backtab(i)=CARD_MURMAISONENTREE:return 
	if value=18 then #backtab(i)=CARD_MURMAISON2:return 
	if value=19 then #backtab(i)=CARD_TOITBUNKER:return 
	if value=20 then #backtab(i)=CARD_BUNKERG
	if value=21 then #backtab(i)=CARD_BUNKERD
	if value=22 then #backtab(i)=CARD_BUNKERM:return 
	if value=23 then #backtab(i)=CARD_PASSERELLE:return 
	if value=24 then #backtab(i)=CARD_MurMaison11:return 
	if value=25 then #backtab(i)=CARD_MurMaison22:return 
	if value=26 then #backtab(i)=CARD_OmbreBatiment:return 
	if value=27 then #backtab(i)=CARD_HERBE1:return 
	if value=29 then #backtab(i)=CARD_TOITURE:return 
	if value=30 then #backtab(i)=CARD_BLANK:return 
	if value=31 then #backtab(i)=CARD_MURBRIQUE:return 
	if value=49 then #backtab(i)=CARD_HautPont:return 
	if value=50 then #backtab(i)=CARD_BLACK:return
	if value=51 then #backtab(i)=CARD_PONT_2:return 
	if value=52 then #backtab(i)=CARD_MURBRIQUE_1:return 
	if value=53 then #backtab(i)=CARD_COINEAU_BG1:return 
	if value=54 then #backtab(i)=CARD_COINEAU_BD1:return 
	if value=55 then #backtab(i)=CARD_COINEAU_HG1:return 
	if value=56 then #backtab(i)=CARD_COINEAU_HD1:return 
	if value=57 then #backtab(i)=CARD_COINEAU_BG:return 
	if value=58 then #backtab(i)=CARD_COINEAU_BD:return 
	if value=59 then #backtab(i)=CARD_COINEAU_HG:return 
	if value=60 then #backtab(i)=CARD_COINEAU_HD:return 
	if value=63 then #backtab(i)=CARD_PONT_1:return 
	end	
	
	
	
	
	
	
	

'######################
'####  Zones Jeu   ####
'######################
	include "ZoneJeu.bas"
	
	

'#################################
'##### Ressources Graphiques #####
'#################################

'//////////////////
'//// TileSet /////
'//////////////////

	include "TileSet.bas"

'//////////////////////
'//// SpriteSheet /////
'//////////////////////

	include "SpriteSheet.bas"