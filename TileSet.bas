'/////////////////////////
'//// TileSet Divers /////
'/////////////////////////

'///// Couteau /////
Couteau:
    BITMAP "........"
    BITMAP "..#....."
    BITMAP ".#......"
    BITMAP "##.####."
    BITMAP "##..####"
    BITMAP "##.####."
    BITMAP ".#......"
    BITMAP "..#....."
	
'///// Bazooka /////
BazookaTILE:
	BITMAP "........"
	BITMAP "#......#"
	BITMAP "########"
	BITMAP "#......#"
	BITMAP "########"
	BITMAP "########"
	BITMAP "#.###..#"
	BITMAP "..#....."

'///// Impact Explosion /////
ImpactExplosion:
	BITMAP "..####.."
	BITMAP ".######."
	BITMAP "########"
	BITMAP "##....##"
	BITMAP "#..#...#"
	BITMAP "....#..."
	BITMAP ".#....#."
	BITMAP "..####.."
	
'///// Grenades /////
GrenadeTILE:
	BITMAP "...##.#."
	BITMAP "..####.."
	BITMAP ".###..#."
	BITMAP ".####.#."
	BITMAP ".######."
	BITMAP ".######."
	BITMAP ".######."
	BITMAP "..####.."

'///// Pistolet /////
PistoletTILE:
	BITMAP "......#."
	BITMAP ".######."
	BITMAP ".######."
	BITMAP ".##.#..."
	BITMAP ".###...."
	BITMAP ".##....."
	BITMAP ".##....."
	BITMAP "........"

'///// Vies Papi /////
ViePapi:
	BITMAP ".##..##."
	BITMAP "#####..#"
	BITMAP "######.#"
	BITMAP "########"
	BITMAP "########"
	BITMAP ".######."
	BITMAP "..####.."
	BITMAP "...##..."

'//// Sol Herbe ////
SOL_HERBE:
	BITMAP "......#."
	BITMAP ".#...#.."
	BITMAP "#....#.."
	BITMAP "#....##."
	BITMAP "..#....."
	BITMAP "..#.#..."
	BITMAP ".#...#.."
	BITMAP ".##....."
	
HERBE1:
    BITMAP "........"
    BITMAP "..#...#."
    BITMAP ".#......"
    BITMAP "....#..."
    BITMAP "...#...."
    BITMAP "...##..."
    BITMAP "........"
    BITMAP "........"
	
'//// Rocher ////
ROCHER:
	BITMAP "........"
	BITMAP "..#####."
	BITMAP ".##.####"
	BITMAP "########"
	BITMAP "#.######"
	BITMAP "#.######"
	BITMAP ".#.#####"
	BITMAP "#######."

'//// Arbre ////
ARBRE_CIME:
	BITMAP "...###.#"
	BITMAP "#.#####."
	BITMAP ".#######"
	BITMAP "########"
	BITMAP ".#######"
	BITMAP "##.#####"
	BITMAP "####.###"
	BITMAP "..#####."

ARBRE_TRONC:
	BITMAP "...###.."	'BG	:	DarkGreen/FG : Black
	BITMAP "...##..."
	BITMAP ".*.##..."
	BITMAP "..*##..."
	BITMAP "...##..."
	BITMAP "...##..."
	BITMAP "...##*.."
	BITMAP ".**....."
	
'///// Sacs /////
SAC_G:
	BITMAP ".#######"
	BITMAP "#...*..."
	BITMAP "#......."
	BITMAP "#......."
	BITMAP "#......."
	BITMAP "##.#.#.#"
	BITMAP "#.#.#.#."
	BITMAP ".#######"

SAC_M:
	BITMAP "*#######"
	BITMAP "....*..."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP ".#.###.#"
	BITMAP "#.#.#.#."
	BITMAP "########"
	
SAC_D:
	BITMAP "#######."
	BITMAP "...*...#"
	BITMAP ".......#"
	BITMAP ".......#"
	BITMAP ".......#"
	BITMAP "*#.#.#.#"
	BITMAP "#.#.#.##"
	BITMAP "#######."

SAC:
	BITMAP "*######*"
	BITMAP "#......#"
	BITMAP "#......#"
	BITMAP "#......#"
	BITMAP "#......#"
	BITMAP "##.#.#.#"
	BITMAP "#.#.#.##"
	BITMAP "########"
	
'///// Eau /////
EAU_1:
	BITMAP "########"	'BG	:	LIGHTBLUE/FG : BLUE
	BITMAP "#...####"
	BITMAP "########"
	BITMAP "####..##"
	BITMAP "########"
	BITMAP "#..#####"
	BITMAP "#####..#"
	BITMAP "########"

EAU_2:
	BITMAP "########"
	BITMAP "...####."
	BITMAP "########"
	BITMAP "###..###"
	BITMAP "########"
	BITMAP "..######"
	BITMAP "####..##"
	BITMAP "########"
	
EAU_3:
	BITMAP "########"
	BITMAP "..####.."
	BITMAP "########"
	BITMAP "##..####"
	BITMAP "########"
	BITMAP ".######."
	BITMAP "###..###"
	BITMAP "########"
	
EAU_4:
	BITMAP "########"
	BITMAP ".####..."
	BITMAP "########"
	BITMAP "#..#####"
	BITMAP "########"
	BITMAP "######.."
	BITMAP "##..###*"
	BITMAP "########"
	
EAU_5:
	BITMAP "########"
	BITMAP "####...."
	BITMAP "########"
	BITMAP "..#####*"
	BITMAP "########"
	BITMAP "#####..*"
	BITMAP "#..###**"
	BITMAP "########"
	
EAU_6:
	BITMAP "########"
	BITMAP "###....*"
	BITMAP "########"
	BITMAP ".#####*."
	BITMAP "########"
	BITMAP "####..**"
	BITMAP "..###***"
	BITMAP "########"
	
EAU_7:
	BITMAP "########"
	BITMAP "##....**"
	BITMAP "########"
	BITMAP "#####*.."
	BITMAP "########"
	BITMAP "###..***"
	BITMAP ".###***."
	BITMAP "########"
	
EAU_8:
	BITMAP "########"
	BITMAP "#....***"
	BITMAP "########"
	BITMAP "#####..*"
	BITMAP "########"
	BITMAP "##..****"
	BITMAP "###***.."
	BITMAP "########"
	
	
	
'///// Murs /////
FrontWALL:
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
	
MUR_AVANT:
	BITMAP "........"	'BG	:	GREY/FG : BLACK
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP ".#.#.#.#"
	BITMAP "#.#.#.#."
	BITMAP ".#.#.#.#"
	BITMAP "#.#.#.#."

'///// Barbelés /////	'BG :	DarkGreen/FG : BLACK
BARBELE_G:
	BITMAP "........"
	BITMAP "........"
	BITMAP ".....#.#"
	BITMAP "...#..#."
	BITMAP "....##.#"
	BITMAP "...#..#."
	BITMAP "...#####"
	BITMAP "..#.#.#."

BARBELE_M:
	BITMAP "........"
	BITMAP "........"
	BITMAP "#.#.#.#."
	BITMAP ".#...#.."
	BITMAP "###.###."
	BITMAP ".#.#.#.#"
	BITMAP "########"
	BITMAP ".#...#.."
	
BARBELE_D:
	BITMAP "........"
	BITMAP "........"
	BITMAP "#.#....."
	BITMAP ".#..#..."
	BITMAP "####...."
	BITMAP ".#..#..."
	BITMAP "#####..."
	BITMAP ".#.#.#.."

'///// Toiture /////	'BG :	RED/FG : BLACK
TOITURE:
	BITMAP "#.#.#.#."
	BITMAP "###.###."
	BITMAP "#.#.#.#."
	BITMAP "#.###.##"
	BITMAP "#.#.#.#."
	BITMAP "###.###."
	BITMAP "#.#.#.#."
	BITMAP "#.#.#.#."
	
'///// Mur Maison /////	  'BG : GREY/FG : BLACK
MURMAISON_1:
	BITMAP "#.#.#.#."
	BITMAP ".#.#.#.#"
	BITMAP "........"
	BITMAP "..###..."
	BITMAP ".#.*.#.."
	BITMAP ".#.*.#.."
	BITMAP ".#####.."
	BITMAP "........"
	
MurMaison1:
    BITMAP "#.#.#.#."
    BITMAP "#.#.#.#."
    BITMAP "#.#.#.#."
    BITMAP "#.###.##"
    BITMAP "###.###."
    BITMAP "#.#.#.#."
    BITMAP "#.#.#.#."
    BITMAP "#.#.#.#."
	
MurMaison2:
    BITMAP "########"
    BITMAP "########"
    BITMAP "#.#.#.#."
    BITMAP "#.###.##"
    BITMAP "###.###."
    BITMAP "#.#.#.#."
    BITMAP "#.#.#.#."
    BITMAP "#.#.#.#."
	
EntreeNoire:
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
    BITMAP "........"
	
MURMAISON_ENTREE:
	BITMAP "#.#.#.#."
	BITMAP ".#.#.#.#"
	BITMAP "........"
	BITMAP "........"
	BITMAP "..####.."
	BITMAP ".######."
	BITMAP ".####.#."
	BITMAP ".######."
MURMAISON_2:
	BITMAP "#.#.#.#."
	BITMAP ".#.#.#.#"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	
'///// Bunkers /////	'BG : GREY/FG : BLACK
TOITBUNKER:
	BITMAP "#.#.#.#."
	BITMAP ".#.#.#.#"
	BITMAP "#.#.#.#."
	BITMAP ".#.#.#.#"
	BITMAP "#.#.#.#."
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	
BUNKER_G:
	BITMAP "........"
	BITMAP "...#####"
	BITMAP "..#....."
	BITMAP "..#.####"
	BITMAP "..#.####"
	BITMAP "..#.####"
	BITMAP "..#.####"
	BITMAP ".#.#...."

BUNKER_D:
	BITMAP "........"
	BITMAP "#####..."
	BITMAP ".....#.."
	BITMAP "####.#.."
	BITMAP "####.#.."
	BITMAP "####.#.."
	BITMAP "####.#.."
	BITMAP "....#.#."
	
BUNKER_M:
	BITMAP "........"
	BITMAP "########"
	BITMAP "........"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "........"
	
'///// Ombre Batiment ///// BG : Brown / FG : DARKGREEN
OmbreBatiment:
    BITMAP "........"
    BITMAP "........"
    BITMAP "#.#.#.#."
    BITMAP ".#.#.#.#"
    BITMAP "########"
    BITMAP "########"
    BITMAP "########"
    BITMAP "########"

'///// Passerelle /////		BG : YELLOW/FG : TAN
PASSERELLE:
    BITMAP ".#.#.#.#"
    BITMAP ".#.#.#.#"
    BITMAP ".#.#.#.#"
    BITMAP ".#.#.#.#"
    BITMAP ".#.#.#.#"
    BITMAP ".#.###.#"
    BITMAP ".###.###"
    BITMAP ".#.#.#.#"

'///// BLANK /////
BLANK:
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "...*.#.."
	BITMAP "....#..."
	BITMAP "....#..*"
	BITMAP "...#..*."


	
BLANK1:	
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP ".*.#...."
	BITMAP "..*#...."
	BITMAP "...#..*."
	BITMAP "...#..*."


	
BLANK2:	
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "*.*....."
	BITMAP ".*#....."
	BITMAP "..#...*."
	BITMAP "...#..*."


	
'///// Mur Brique /////		'BG : GREY/FG : BLACK
MURBRIQUE:
	BITMAP "########"
	BITMAP "..#...#."
	BITMAP "########"
	BITMAP "#...#..."
	BITMAP "########"
	BITMAP "..#...#."
	BITMAP "########"
	BITMAP "#...#..."
	
MURBRIQUE_1:
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "........"
	BITMAP "########"
	BITMAP "..#...#."
	BITMAP "########"
	BITMAP "#...#..."
	
'///// PONT DETAIL /////	'BG : GREY/FG : BLACK
PONT_1:
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "........"
	
PONT_2:
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	BITMAP "........"
	BITMAP "########"
	BITMAP "########"
	BITMAP "########"
	
HautPont:
    BITMAP "........"
    BITMAP "########"
    BITMAP "########"
    BITMAP "########"
    BITMAP "########"
    BITMAP "########"
    BITMAP "########"
    BITMAP "########"
	

'///// Coin EAU /////	'BG : DARKGREEN/FG : BLUE
COINEAU_HD:
	BITMAP "##......"
	BITMAP "####...."
	BITMAP "#####..."
	BITMAP "######.."
	BITMAP "######.."
	BITMAP "#######."
	BITMAP "#######."
	BITMAP "########"

COINEAU_HG:
	BITMAP "......##"
	BITMAP "....####"
	BITMAP "...#####"
	BITMAP "..######"
	BITMAP "..######"
	BITMAP ".#######"
	BITMAP ".#######"
	BITMAP "########"

COINEAU_BD:
	BITMAP "########"
	BITMAP ".#######"
	BITMAP ".#######"
	BITMAP "..######"
	BITMAP "..######"
	BITMAP "...#####"
	BITMAP "....####"
	BITMAP "......##"
	
COINEAU_BG:
	BITMAP "########"
	BITMAP "#######."
	BITMAP "#######."
	BITMAP "######.."
	BITMAP "######.."
	BITMAP "#####..."
	BITMAP "####...."
	BITMAP "##......"
	
COINEAU_HD1:
	BITMAP "..######"
	BITMAP "....####"
	BITMAP ".....###"
	BITMAP "......##"
	BITMAP "......##"
	BITMAP ".......#"
	BITMAP ".......#"
	BITMAP "........"
	
COINEAU_HG1:
	BITMAP "######.."
	BITMAP "####...."
	BITMAP "###....."
	BITMAP "##......"
	BITMAP "##......"
	BITMAP "#......."
	BITMAP "#......."
	BITMAP "........"

COINTEAU_BD1:
	BITMAP "........"
	BITMAP "#......."
	BITMAP "#......."
	BITMAP "##......"
	BITMAP "##......"
	BITMAP "###....."
	BITMAP "####...."
	BITMAP "######.."

COINEAU_BG1:
	BITMAP "........"
	BITMAP ".......#"
	BITMAP ".......#"
	BITMAP "......##"
	BITMAP "......##"
	BITMAP ".....###"
	BITMAP "....####"
	BITMAP "..######"
	
'///// ENNEMI BUNKER /////	BG : GREY/FG : BLACK
IABUNKER_M:
	BITMAP "........"
	BITMAP "########"
	BITMAP "........"
	BITMAP "##...###"
	BITMAP "#.###.##"
	BITMAP "##.#.###"
	BITMAP "###.####"
	BITMAP "..#.#..."

IABUNKER_D:
	BITMAP "........"
	BITMAP "########"
	BITMAP "........"
	BITMAP "#..#####"
	BITMAP ".##.####"
	BITMAP "#.#.####"
	BITMAP "##.#####"
	BITMAP "..#.#..."

IABUNKER_G:
	BITMAP "........"
	BITMAP "########"
	BITMAP "........"
	BITMAP "#####..#"
	BITMAP "####.##."
	BITMAP "####.#.#"
	BITMAP "#####.##"
	BITMAP "...#.#.."
	




	


