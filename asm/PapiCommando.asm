	; IntyBASIC compiler v1.4.1 Jun/12/2019
        ;
        ; Prologue for IntyBASIC programs
        ; by Oscar Toledo G.  http://nanochess.org/
        ;
        ; Revision: Jan/30/2014. Spacing adjustment and more comments.
        ; Revision: Apr/01/2014. It now sets the starting screen pos. for PRINT
	    ; Revision: Aug/26/2014. Added PAL detection code.
	    ; Revision: Dec/12/2014. Added optimized constant multiplication routines
	    ;                        by James Pujals.
	    ; Revision: Jan/25/2015. Added marker for automatic title replacement.
	    ;                        (option --title of IntyBASIC)
	    ; Revision: Aug/06/2015. Turns off ECS sound. Seed random generator using
	    ;                        trash in 16-bit RAM. Solved bugs and optimized
	    ;                        macro for constant multiplication.
        ; Revision: Jan/12/2016. Solved bug in PAL detection.
        ; Revision: May/03/2016. Changed in _mode_select initialization.
	    ; Revision: Jul/31/2016. Solved bug in multiplication by 126 and 127.
	; Revision: Sep/08/2016. Now CLRSCR initializes screen position for PRINT, this
	;                        solves bug when user programs goes directly to PRINT.
        ;

        ROMW 16
        ORG $5000

        ; This macro will 'eat' SRCFILE directives if the assembler doesn't support the directive.
        IF ( DEFINED __FEATURE.SRCFILE ) = 0
            MACRO SRCFILE x, y
            ; macro must be non-empty, but a comment works fine.
            ENDM
        ENDI

        ;
        ; ROM header
        ;
        BIDECLE _ZERO           ; MOB picture base
        BIDECLE _ZERO           ; Process table
        BIDECLE _MAIN           ; Program start
        BIDECLE _ZERO           ; Background base image
        BIDECLE _ONES           ; GRAM
        BIDECLE _TITLE          ; Cartridge title and date
        DECLE   $03C0           ; No ECS title, jump to code after title,
                                ; ... no clicks
                                
_ZERO:  DECLE   $0000           ; Border control
        DECLE   $0000           ; 0 = color stack, 1 = f/b mode
        
_ONES:  DECLE   $0001, $0001    ; Initial color stack 0 and 1: Blue
        DECLE   $0001, $0001    ; Initial color stack 2 and 3: Blue
        DECLE   $0001           ; Initial border color: Blue

C_WHT:  EQU $0007

CLRSCR: MVII #$200,R4           ; Used also for CLS
	MVO R4,_screen		; Set up starting screen position for PRINT
        MVII #$F0,R1
FILLZERO:
        CLRR R0
MEMSET:
        MVO@ R0,R4
        DECR R1
        BNE MEMSET
        JR R5

        ;
        ; Title, Intellivision EXEC will jump over it and start
        ; execution directly in _MAIN
        ;
	; Note mark is for automatic replacement by IntyBASIC
_TITLE:
	BYTE 120,'PapiCommando',0
        
        ;
        ; Main program
        ;
_MAIN:
        DIS
        MVII #STACK,R6

_MAIN0:
        ;
        ; Clean memory
        ;
        MVII #$00e,R1           ; 14 of sound (ECS)
        MVII #$0f0,R4           ; ECS PSG
        CALL FILLZERO
        MVII #$0fe,R1           ; 240 words of 8 bits plus 14 of sound
        MVII #$100,R4           ; 8-bit scratch RAM
        CALL FILLZERO

	; Seed random generator using 16 bit RAM (not cleared by EXEC)
	CLRR R0
	MVII #$02F0,R4
	MVII #$0110/4,R1        ; Includes phantom memory for extra randomness
_MAIN4:                         ; This loop is courtesy of GroovyBee
	ADD@ R4,R0
	ADD@ R4,R0
	ADD@ R4,R0
	ADD@ R4,R0
	DECR R1
	BNE _MAIN4
	MVO R0,_rand

        MVII #$058,R1           ; 88 words of 16 bits
        MVII #$308,R4           ; 16-bit scratch RAM
        CALL FILLZERO

        CALL CLRSCR             ; Clean up screen

        MVII #_pal1_vector,R0 ; Points to interrupt vector
        MVO R0,ISRVEC
        SWAP R0
        MVO R0,ISRVEC+1

        EIS

_MAIN1:	MVI _ntsc,R0
	CMPI #3,R0
	BNE _MAIN1
	CLRR R2
_MAIN2:	INCR R2
	MVI _ntsc,R0
	CMPI #4,R0
	BNE _MAIN2

        ; 596 for PAL in jzintv
        ; 444 for NTSC in jzintv
        CMPI #520,R2
        MVII #1,R0
        BLE _MAIN3
        CLRR R0
_MAIN3: MVO R0,_ntsc

        CALL _wait
	CALL _init_music
        MVII #2,R0
        MVO R0,_mode_select
        MVII #$038,R0
        MVO R0,$01F8            ; Configures sound
        MVO R0,$00F8            ; Configures sound (ECS)
        CALL _wait

;* ======================================================================== *;
;*  These routines are placed into the public domain by their author.  All  *;
;*  copyright rights are hereby relinquished on the routines and data in    *;
;*  this file.  -- James Pujals (DZ-Jay), 2014                              *;
;* ======================================================================== *;

; Modified by Oscar Toledo G. (nanochess), Aug/06/2015
; * Tested all multiplications with automated test.
; * Accelerated multiplication by 7,14,15,28,31,60,62,63,112,120,124
; * Solved bug in multiplication by 23,39,46,47,55,71,78,79,87,92,93,94,95,103,110,111,119
; * Improved sequence of instructions to be more interruptible.

;; ======================================================================== ;;
;;  MULT reg, tmp, const                                                    ;;
;;  Multiplies "reg" by constant "const" and using "tmp" for temporary      ;;
;;  calculations.  The result is placed in "reg."  The multiplication is    ;;
;;  performed by an optimal combination of shifts, additions, and           ;;
;;  subtractions.                                                           ;;
;;                                                                          ;;
;;  NOTE:   The resulting contents of the "tmp" are undefined.              ;;
;;                                                                          ;;
;;  ARGUMENTS                                                               ;;
;;      reg         A register containing the multiplicand.                 ;;
;;      tmp         A register for temporary calculations.                  ;;
;;      const       The constant multiplier.                                ;;
;;                                                                          ;;
;;  OUTPUT                                                                  ;;
;;      reg         Output value.                                           ;;
;;      tmp         Trashed.                                                ;;
;;      .ERR.Failed True if operation failed.                               ;;
;; ======================================================================== ;;
MACRO   MULT reg, tmp, const
;
    LISTING "code"

_mul.const      QSET    %const%
_mul.done       QSET    0

        IF (%const% > $7F)
_mul.const      QSET    (_mul.const SHR 1)
                SLL     %reg%,  1
        ENDI

        ; Multiply by $00 (0)
        IF (_mul.const = $00)
_mul.done       QSET    -1
                CLRR    %reg%
        ENDI

        ; Multiply by $01 (1)
        IF (_mul.const = $01)
_mul.done       QSET    -1
                ; Nothing to do
        ENDI

        ; Multiply by $02 (2)
        IF (_mul.const = $02)
_mul.done       QSET    -1
                SLL     %reg%,  1
        ENDI

        ; Multiply by $03 (3)
        IF (_mul.const = $03)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $04 (4)
        IF (_mul.const = $04)
_mul.done       QSET    -1
                SLL     %reg%,  2
        ENDI

        ; Multiply by $05 (5)
        IF (_mul.const = $05)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $06 (6)
        IF (_mul.const = $06)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $07 (7)
        IF (_mul.const = $07)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $08 (8)
        IF (_mul.const = $08)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  1
        ENDI

        ; Multiply by $09 (9)
        IF (_mul.const = $09)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $0A (10)
        IF (_mul.const = $0A)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $0B (11)
        IF (_mul.const = $0B)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $0C (12)
        IF (_mul.const = $0C)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $0D (13)
        IF (_mul.const = $0D)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $0E (14)
        IF (_mul.const = $0E)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $0F (15)
        IF (_mul.const = $0F)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $10 (16)
        IF (_mul.const = $10)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  2
        ENDI

        ; Multiply by $11 (17)
        IF (_mul.const = $11)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $12 (18)
        IF (_mul.const = $12)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $13 (19)
        IF (_mul.const = $13)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $14 (20)
        IF (_mul.const = $14)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $15 (21)
        IF (_mul.const = $15)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $16 (22)
        IF (_mul.const = $16)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $17 (23)
        IF (_mul.const = $17)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  1
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $18 (24)
        IF (_mul.const = $18)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $19 (25)
        IF (_mul.const = $19)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $1A (26)
        IF (_mul.const = $1A)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $1B (27)
        IF (_mul.const = $1B)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $1C (28)
        IF (_mul.const = $1C)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $1D (29)
        IF (_mul.const = $1D)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $1E (30)
        IF (_mul.const = $1E)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $1F (31)
        IF (_mul.const = $1F)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
		ADDR	%reg%,	%reg%
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $20 (32)
        IF (_mul.const = $20)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  2
		ADDR	%reg%,	%reg%
        ENDI

        ; Multiply by $21 (33)
        IF (_mul.const = $21)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
		ADDR	%reg%,	%reg%
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $22 (34)
        IF (_mul.const = $22)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $23 (35)
        IF (_mul.const = $23)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $24 (36)
        IF (_mul.const = $24)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $25 (37)
        IF (_mul.const = $25)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $26 (38)
        IF (_mul.const = $26)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $27 (39)
        IF (_mul.const = $27)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $28 (40)
        IF (_mul.const = $28)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $29 (41)
        IF (_mul.const = $29)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $2A (42)
        IF (_mul.const = $2A)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $2B (43)
        IF (_mul.const = $2B)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $2C (44)
        IF (_mul.const = $2C)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $2D (45)
        IF (_mul.const = $2D)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $2E (46)
        IF (_mul.const = $2E)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  1
		SUBR	%tmp%,  %reg%
        ENDI

        ; Multiply by $2F (47)
        IF (_mul.const = $2F)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  1
		SUBR	%tmp%,  %reg%
        ENDI

        ; Multiply by $30 (48)
        IF (_mul.const = $30)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $31 (49)
        IF (_mul.const = $31)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $32 (50)
        IF (_mul.const = $32)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $33 (51)
        IF (_mul.const = $33)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $34 (52)
        IF (_mul.const = $34)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $35 (53)
        IF (_mul.const = $35)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $36 (54)
        IF (_mul.const = $36)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $37 (55)
        IF (_mul.const = $37)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
		SLL	%reg%,	1
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $38 (56)
        IF (_mul.const = $38)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $39 (57)
        IF (_mul.const = $39)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $3A (58)
        IF (_mul.const = $3A)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $3B (59)
        IF (_mul.const = $3B)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $3C (60)
        IF (_mul.const = $3C)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $3D (61)
        IF (_mul.const = $3D)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $3E (62)
        IF (_mul.const = $3E)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
		ADDR	%reg%,	%reg%
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $3F (63)
        IF (_mul.const = $3F)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $40 (64)
        IF (_mul.const = $40)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  2
                SLL     %reg%,  2
        ENDI

        ; Multiply by $41 (65)
        IF (_mul.const = $41)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $42 (66)
        IF (_mul.const = $42)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
		ADDR	%reg%,	%reg%
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $43 (67)
        IF (_mul.const = $43)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
		ADDR	%reg%,	%reg%
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $44 (68)
        IF (_mul.const = $44)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $45 (69)
        IF (_mul.const = $45)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $46 (70)
        IF (_mul.const = $46)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $47 (71)
        IF (_mul.const = $47)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $48 (72)
        IF (_mul.const = $48)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $49 (73)
        IF (_mul.const = $49)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $4A (74)
        IF (_mul.const = $4A)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $4B (75)
        IF (_mul.const = $4B)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $4C (76)
        IF (_mul.const = $4C)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $4D (77)
        IF (_mul.const = $4D)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $4E (78)
        IF (_mul.const = $4E)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $4F (79)
        IF (_mul.const = $4F)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $50 (80)
        IF (_mul.const = $50)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $51 (81)
        IF (_mul.const = $51)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $52 (82)
        IF (_mul.const = $52)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $53 (83)
        IF (_mul.const = $53)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $54 (84)
        IF (_mul.const = $54)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $55 (85)
        IF (_mul.const = $55)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $56 (86)
        IF (_mul.const = $56)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $57 (87)
        IF (_mul.const = $57)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  1
		SUBR    %reg%,	%tmp%
                SLL     %reg%,  2
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $58 (88)
        IF (_mul.const = $58)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $59 (89)
        IF (_mul.const = $59)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $5A (90)
        IF (_mul.const = $5A)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $5B (91)
        IF (_mul.const = $5B)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $5C (92)
        IF (_mul.const = $5C)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  1
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $5D (93)
        IF (_mul.const = $5D)
_mul.done       QSET    -1
		MOVR	%reg%,	%tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  1
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $5E (94)
        IF (_mul.const = $5E)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  1
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $5F (95)
        IF (_mul.const = $5F)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                ADDR	%reg%,	%reg%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  1
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $60 (96)
        IF (_mul.const = $60)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  2
		ADDR	%reg%,	%reg%
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $61 (97)
        IF (_mul.const = $61)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $62 (98)
        IF (_mul.const = $62)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $63 (99)
        IF (_mul.const = $63)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $64 (100)
        IF (_mul.const = $64)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $65 (101)
        IF (_mul.const = $65)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $66 (102)
        IF (_mul.const = $66)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $67 (103)
        IF (_mul.const = $67)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  1
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $68 (104)
        IF (_mul.const = $68)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $69 (105)
        IF (_mul.const = $69)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $6A (106)
        IF (_mul.const = $6A)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $6B (107)
        IF (_mul.const = $6B)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $6C (108)
        IF (_mul.const = $6C)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $6D (109)
        IF (_mul.const = $6D)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $6E (110)
        IF (_mul.const = $6E)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $6F (111)
        IF (_mul.const = $6F)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
		SUBR	%tmp%,	%reg%
        ENDI

        ; Multiply by $70 (112)
        IF (_mul.const = $70)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $71 (113)
        IF (_mul.const = $71)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $72 (114)
        IF (_mul.const = $72)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $73 (115)
        IF (_mul.const = $73)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $74 (116)
        IF (_mul.const = $74)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $75 (117)
        IF (_mul.const = $75)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $76 (118)
        IF (_mul.const = $76)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $77 (119)
        IF (_mul.const = $77)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $78 (120)
        IF (_mul.const = $78)
_mul.done       QSET    -1
                SLL     %reg%,  2
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $79 (121)
        IF (_mul.const = $79)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $7A (122)
        IF (_mul.const = $7A)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $7B (123)
        IF (_mul.const = $7B)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  1
                ADDR    %reg%,  %tmp%
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $7C (124)
        IF (_mul.const = $7C)
_mul.done       QSET    -1
                SLL     %reg%,  2
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
		ADDR	%reg%,	%reg%
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $7D (125)
        IF (_mul.const = $7D)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SUBR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
		ADDR	%reg%,	%reg%
                ADDR    %tmp%,  %reg%
        ENDI

        ; Multiply by $7E (126)
        IF (_mul.const = $7E)
_mul.done       QSET    -1
                SLL     %reg%,  1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SLL     %reg%,  2
                SUBR    %tmp%,  %reg%
        ENDI

        ; Multiply by $7F (127)
        IF (_mul.const = $7F)
_mul.done       QSET    -1
                MOVR    %reg%,  %tmp%
                SLL     %reg%,  2
                SLL     %reg%,  2
                SLL     %reg%,  2
                SLL     %reg%,  1
                SUBR    %tmp%,  %reg%
        ENDI

        IF  (_mul.done = 0)
            ERR $("Invalid multiplication constant \'%const%\', must be between 0 and ", $#($7F), ".")
        ENDI

    LISTING "prev"
ENDM

;; ======================================================================== ;;
;;  EOF: pm:mac:lang:mult                                                   ;;
;; ======================================================================== ;;

	;FILE C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas
	;[1] '######################################
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1
	;[2] '####     PAPI COMMANDO PROJECT    ####
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2
	;[3] '####     	  INTELLIVISION		   ####
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",3
	;[4] '######################################
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",4
	;[5] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",5
	;[6] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",6
	;[7] '==========================================
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",7
	;[8] '===		INFO TEMPORISATION			===
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",8
	;[9] '==========================================
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",9
	;[10] ' Tempo(0)= Papi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",10
	;[11] ' Tempo(9)= Init Visée BUNKER
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",11
	;[12] ' Tempo(10)= Armes PAPI
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",12
	;[13] ' Tempo(2)= Tempo couverture tir du Tireur Bunker type CARD 32
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",13
	;[14] ' Tempo(3) - > Tempo(5) = Réservé IA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",14
	;[15] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",15
	;[16] 'CARD No 37 : ARMES du HUD Disponible.
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",16
	;[17] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",17
	;[18] 'TypeIA : 1 = Robot Mine, 2 = Soldat
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",18
	;[19] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",19
	;[20] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",20
	;[21] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",21
	;[22] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",22
	;[23] '///// Déclaration de toutes les variables /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",23
	;[24] 	option explicit
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",24
	;[25] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",25
	;[26] '///// Fonctions principales /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",26
	;[27] 	include "constants.bas"
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",27
	;FILE C:\IntyBASIC SDK\lib\constants.bas
	;[1] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",1
	;[2] REM HEADER - CONSTANTS.BAS
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",2
	;[3] REM
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",3
	;[4] REM Started by Mark Ball, July 2015
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",4
	;[5] REM
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",5
	;[6] REM Constants for use in IntyBASIC
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",6
	;[7] REM
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",7
	;[8] REM HISTORY
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",8
	;[9] REM -------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",9
	;[10] REM 1.00F 05/07/15 - First version.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",10
	;[11] REM 1.01F 07/07/15 - Added disc directions.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",11
	;[12] REM                - Added background modes.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",12
	;[13] REM                - Minor comment changes.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",13
	;[14] REM 1.02F 08/07/15 - Renamed constants.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",14
	;[15] REM                - Added background access information.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",15
	;[16] REM                - Adjustments to layout.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",16
	;[17] REM 1.03F 08/07/15 - Fixed comment delimiter.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",17
	;[18] REM 1.04F 11/07/15 - Added useful functions.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",18
	;[19] REM                - Added controller movement mask.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",19
	;[20] REM 1.05F 11/07/15 - Added BACKGROUND constants.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",20
	;[21] REM 1.06F 11/07/15 - Changed Y, X order to X, Y in DEF FN functions.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",21
	;[22] REM 1.07F 11/07/15 - Added colour stack advance.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",22
	;[23] REM 1.08F 12/07/15 - Added functions for sprite position handling.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",23
	;[24] REM 1.09F 12/07/15 - Added a function for resetting a sprite.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",24
	;[25] REM 1.10F 13/07/15 - Added keypad constants.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",25
	;[26] REM 1.11F 13/07/15 - Added side button constants.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",26
	;[27] REM 1.12F 13/07/15 - Updated sprite functions.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",27
	;[28] REM 1.13F 19/07/15 - Added border masking constants.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",28
	;[29] REM 1.14F 20/07/15 - Added a combined border masking constant.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",29
	;[30] REM 1.15F 20/07/15 - Renamed border masking constants to BORDER_HIDE_xxxx.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",30
	;[31] REM 1.16F 28/09/15 - Fixed disc direction typos.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",31
	;[32] REM 1.17F 30/09/15 - Fixed DISC_SOUTH_WEST value.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",32
	;[33] REM 1.18F 05/12/15 - Fixed BG_XXXX colours.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",33
	;[34] REM 1.19F 01/01/16 - Changed name of BACKTAB constant to avoid confusion with #BACKTAB array.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",34
	;[35] REM                - Added pause key constants.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",35
	;[36] REM 1.20F 14/01/16 - Added coloured squares mode's pixel colours.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",36
	;[37] REM 1.21F 15/01/16 - Added coloured squares mode's X and Y limits.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",37
	;[38] REM 1.22F 23/01/16 - Added PSG constants.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",38
	;[39] REM 1.23F 24/01/16 - Fixed typo in PSG comments.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",39
	;[40] REM 1.24F 16/11/16 - Added toggle DEF FN's for sprite's BEHIND, HIT and VISIBLE.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",40
	;[41] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",41
	;[42] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",42
	;[43] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",43
	;[44] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",44
	;[45] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",45
	;[46] REM Background information.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",46
	;[47] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",47
	;[48] CONST BACKTAB_ADDR					= $0200		' Start of the BACKground TABle (BACKTAB) in RAM.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",48
	;[49] CONST BACKGROUND_ROWS				= 12		' Height of the background in cards.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",49
	;[50] CONST BACKGROUND_COLUMNS			= 20		' Width of the background in cards.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",50
	;[51] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",51
	;[52] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",52
	;[53] REM Background GRAM cards.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",53
	;[54] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",54
	;[55] CONST BG00							= $0800
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",55
	;[56] CONST BG01							= $0808
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",56
	;[57] CONST BG02							= $0810
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",57
	;[58] CONST BG03							= $0818
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",58
	;[59] CONST BG04							= $0820
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",59
	;[60] CONST BG05							= $0828
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",60
	;[61] CONST BG06							= $0830
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",61
	;[62] CONST BG07							= $0838
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",62
	;[63] CONST BG08							= $0840
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",63
	;[64] CONST BG09							= $0848
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",64
	;[65] CONST BG10							= $0850
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",65
	;[66] CONST BG11							= $0858
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",66
	;[67] CONST BG12							= $0860
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",67
	;[68] CONST BG13							= $0868
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",68
	;[69] CONST BG14							= $0870
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",69
	;[70] CONST BG15							= $0878
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",70
	;[71] CONST BG16							= $0880
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",71
	;[72] CONST BG17							= $0888
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",72
	;[73] CONST BG18							= $0890
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",73
	;[74] CONST BG19							= $0898
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",74
	;[75] CONST BG20							= $08A0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",75
	;[76] CONST BG21							= $08A8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",76
	;[77] CONST BG22							= $08B0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",77
	;[78] CONST BG23							= $08B8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",78
	;[79] CONST BG24							= $08C0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",79
	;[80] CONST BG25							= $08C8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",80
	;[81] CONST BG26							= $08D0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",81
	;[82] CONST BG27							= $08D8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",82
	;[83] CONST BG28							= $08E0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",83
	;[84] CONST BG29							= $08E8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",84
	;[85] CONST BG30							= $08F0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",85
	;[86] CONST BG31							= $08F8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",86
	;[87] CONST BG32							= $0900
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",87
	;[88] CONST BG33							= $0908
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",88
	;[89] CONST BG34							= $0910
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",89
	;[90] CONST BG35							= $0918
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",90
	;[91] CONST BG36							= $0920
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",91
	;[92] CONST BG37							= $0928
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",92
	;[93] CONST BG38							= $0930
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",93
	;[94] CONST BG39							= $0938
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",94
	;[95] CONST BG40							= $0940
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",95
	;[96] CONST BG41							= $0948
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",96
	;[97] CONST BG42							= $0950
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",97
	;[98] CONST BG43							= $0958
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",98
	;[99] CONST BG44							= $0960
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",99
	;[100] CONST BG45							= $0968
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",100
	;[101] CONST BG46							= $0970
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",101
	;[102] CONST BG47							= $0978
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",102
	;[103] CONST BG48							= $0980
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",103
	;[104] CONST BG49							= $0988
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",104
	;[105] CONST BG50							= $0990
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",105
	;[106] CONST BG51							= $0998
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",106
	;[107] CONST BG52							= $09A0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",107
	;[108] CONST BG53							= $09A8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",108
	;[109] CONST BG54							= $09B0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",109
	;[110] CONST BG55							= $09B8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",110
	;[111] CONST BG56							= $09C0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",111
	;[112] CONST BG57							= $09C8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",112
	;[113] CONST BG58							= $09D0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",113
	;[114] CONST BG59							= $09D8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",114
	;[115] CONST BG60							= $09E0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",115
	;[116] CONST BG61							= $09E8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",116
	;[117] CONST BG62							= $09F0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",117
	;[118] CONST BG63							= $09F8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",118
	;[119] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",119
	;[120] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",120
	;[121] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",121
	;[122] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",122
	;[123] REM GRAM card index numbers.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",123
	;[124] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",124
	;[125] REM Note: For use with the "define" command.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",125
	;[126] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",126
	;[127] CONST DEF00							= $0000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",127
	;[128] CONST DEF01							= $0001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",128
	;[129] CONST DEF02							= $0002
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",129
	;[130] CONST DEF03							= $0003
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",130
	;[131] CONST DEF04							= $0004
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",131
	;[132] CONST DEF05							= $0005
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",132
	;[133] CONST DEF06							= $0006
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",133
	;[134] CONST DEF07							= $0007
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",134
	;[135] CONST DEF08							= $0008
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",135
	;[136] CONST DEF09							= $0009
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",136
	;[137] CONST DEF10							= $000A
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",137
	;[138] CONST DEF11							= $000B
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",138
	;[139] CONST DEF12							= $000C
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",139
	;[140] CONST DEF13							= $000D
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",140
	;[141] CONST DEF14							= $000E
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",141
	;[142] CONST DEF15							= $000F
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",142
	;[143] CONST DEF16							= $0010
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",143
	;[144] CONST DEF17							= $0011
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",144
	;[145] CONST DEF18							= $0012
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",145
	;[146] CONST DEF19							= $0013
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",146
	;[147] CONST DEF20							= $0014
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",147
	;[148] CONST DEF21							= $0015
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",148
	;[149] CONST DEF22							= $0016
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",149
	;[150] CONST DEF23							= $0017
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",150
	;[151] CONST DEF24							= $0018
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",151
	;[152] CONST DEF25							= $0019
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",152
	;[153] CONST DEF26							= $001A
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",153
	;[154] CONST DEF27							= $001B
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",154
	;[155] CONST DEF28							= $001C
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",155
	;[156] CONST DEF29							= $001D
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",156
	;[157] CONST DEF30							= $001E
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",157
	;[158] CONST DEF31							= $001F
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",158
	;[159] CONST DEF32							= $0020
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",159
	;[160] CONST DEF33							= $0021
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",160
	;[161] CONST DEF34							= $0022
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",161
	;[162] CONST DEF35							= $0023
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",162
	;[163] CONST DEF36							= $0024
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",163
	;[164] CONST DEF37							= $0025
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",164
	;[165] CONST DEF38							= $0026
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",165
	;[166] CONST DEF39							= $0027
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",166
	;[167] CONST DEF40							= $0028
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",167
	;[168] CONST DEF41							= $0029
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",168
	;[169] CONST DEF42							= $002A
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",169
	;[170] CONST DEF43							= $002B
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",170
	;[171] CONST DEF44							= $002C
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",171
	;[172] CONST DEF45							= $002D
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",172
	;[173] CONST DEF46							= $002E
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",173
	;[174] CONST DEF47							= $002F
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",174
	;[175] CONST DEF48							= $0030
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",175
	;[176] CONST DEF49							= $0031
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",176
	;[177] CONST DEF50							= $0032
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",177
	;[178] CONST DEF51							= $0033
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",178
	;[179] CONST DEF52							= $0034
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",179
	;[180] CONST DEF53							= $0035
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",180
	;[181] CONST DEF54							= $0036
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",181
	;[182] CONST DEF55							= $0037
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",182
	;[183] CONST DEF56							= $0038
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",183
	;[184] CONST DEF57							= $0039
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",184
	;[185] CONST DEF58							= $003A
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",185
	;[186] CONST DEF59							= $003B
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",186
	;[187] CONST DEF60							= $003C
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",187
	;[188] CONST DEF61							= $003D
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",188
	;[189] CONST DEF62							= $003E
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",189
	;[190] CONST DEF63							= $003F
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",190
	;[191] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",191
	;[192] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",192
	;[193] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",193
	;[194] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",194
	;[195] REM Screen modes.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",195
	;[196] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",196
	;[197] REM Note: For use with the "mode" command.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",197
	;[198] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",198
	;[199] CONST SCREEN_COLOR_STACK			= $0000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",199
	;[200] CONST SCREEN_FOREGROUND_BACKGROUND	= $0001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",200
	;[201] REM Abbreviated versions.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",201
	;[202] CONST SCREEN_FB						= $0001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",202
	;[203] CONST SCREEN_CS						= $0000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",203
	;[204] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",204
	;[205] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",205
	;[206] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",206
	;[207] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",207
	;[208] REM COLORS - Border.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",208
	;[209] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",209
	;[210] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",210
	;[211] REM - For use with the commands "mode 0" and "mode 1".
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",211
	;[212] REM - For use with the "border" command.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",212
	;[213] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",213
	;[214] CONST BORDER_BLACK					= $0000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",214
	;[215] CONST BORDER_BLUE					= $0001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",215
	;[216] CONST BORDER_RED					= $0002
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",216
	;[217] CONST BORDER_TAN					= $0003
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",217
	;[218] CONST BORDER_DARKGREEN				= $0004
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",218
	;[219] CONST BORDER_GREEN					= $0005
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",219
	;[220] CONST BORDER_YELLOW					= $0006
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",220
	;[221] CONST BORDER_WHITE					= $0007
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",221
	;[222] CONST BORDER_GREY					= $0008
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",222
	;[223] CONST BORDER_CYAN					= $0009
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",223
	;[224] CONST BORDER_ORANGE					= $000A
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",224
	;[225] CONST BORDER_BROWN					= $000B
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",225
	;[226] CONST BORDER_PINK					= $000C
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",226
	;[227] CONST BORDER_LIGHTBLUE				= $000D
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",227
	;[228] CONST BORDER_YELLOWGREEN			= $000E
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",228
	;[229] CONST BORDER_PURPLE					= $000F
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",229
	;[230] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",230
	;[231] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",231
	;[232] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",232
	;[233] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",233
	;[234] REM BORDER - Edge masks.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",234
	;[235] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",235
	;[236] REM Note: For use with the "border color, edge" command.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",236
	;[237] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",237
	;[238] CONST BORDER_HIDE_LEFT_EDGE			= $0001		' Hide the leftmost column of the background.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",238
	;[239] CONST BORDER_HIDE_TOP_EDGE			= $0002		' Hide the topmost row of the background.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",239
	;[240] CONST BORDER_HIDE_TOP_LEFT_EDGE		= $0003		' Hide both the topmost row and leftmost column of the background.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",240
	;[241] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",241
	;[242] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",242
	;[243] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",243
	;[244] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",244
	;[245] REM COLORS - Mode 0 (Color Stack).
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",245
	;[246] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",246
	;[247] REM Stack
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",247
	;[248] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",248
	;[249] REM Note: For use as the last 4 parameters used in the "mode 1" command.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",249
	;[250] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",250
	;[251] CONST STACK_BLACK					= $0000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",251
	;[252] CONST STACK_BLUE					= $0001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",252
	;[253] CONST STACK_RED						= $0002
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",253
	;[254] CONST STACK_TAN						= $0003
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",254
	;[255] CONST STACK_DARKGREEN				= $0004
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",255
	;[256] CONST STACK_GREEN					= $0005
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",256
	;[257] CONST STACK_YELLOW					= $0006
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",257
	;[258] CONST STACK_WHITE					= $0007
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",258
	;[259] CONST STACK_GREY					= $0008
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",259
	;[260] CONST STACK_CYAN					= $0009
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",260
	;[261] CONST STACK_ORANGE					= $000A
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",261
	;[262] CONST STACK_BROWN					= $000B
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",262
	;[263] CONST STACK_PINK					= $000C
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",263
	;[264] CONST STACK_LIGHTBLUE				= $000D
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",264
	;[265] CONST STACK_YELLOWGREEN				= $000E
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",265
	;[266] CONST STACK_PURPLE					= $000F
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",266
	;[267] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",267
	;[268] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",268
	;[269] REM Foreground.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",269
	;[270] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",270
	;[271] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",271
	;[272] REM - For use with "peek/poke" commands that access BACKTAB.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",272
	;[273] REM - Only one foreground colour permitted per background card.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",273
	;[274] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",274
	;[275] CONST CS_BLACK						= $0000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",275
	;[276] CONST CS_BLUE						= $0001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",276
	;[277] CONST CS_RED						= $0002
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",277
	;[278] CONST CS_TAN						= $0003
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",278
	;[279] CONST CS_DARKGREEN					= $0004
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",279
	;[280] CONST CS_GREEN						= $0005
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",280
	;[281] CONST CS_YELLOW						= $0006
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",281
	;[282] CONST CS_WHITE						= $0007
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",282
	;[283] CONST CS_GREY						= $1000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",283
	;[284] CONST CS_CYAN						= $1001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",284
	;[285] CONST CS_ORANGE						= $1002
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",285
	;[286] CONST CS_BROWN						= $1003
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",286
	;[287] CONST CS_PINK						= $1004
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",287
	;[288] CONST CS_LIGHTBLUE					= $1005
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",288
	;[289] CONST CS_YELLOWGREEN				= $1006
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",289
	;[290] CONST CS_PURPLE						= $1007
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",290
	;[291] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",291
	;[292] CONST CS_CARD_DATA_MASK				= $07F8		' Mask to get the background card's data.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",292
	;[293] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",293
	;[294] CONST CS_ADVANCE					= $2000		' Advance the colour stack by one position.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",294
	;[295] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",295
	;[296] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",296
	;[297] REM Coloured squares mode.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",297
	;[298] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",298
	;[299] REM Notes :
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",299
	;[300] REM - Only available in colour stack mode.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",300
	;[301] REM - Pixels in each BACKTAB card are arranged in the following manner:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",301
	;[302] REM +-------+-------+
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",302
	;[303] REM | Pixel | Pixel |
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",303
	;[304] REM |   0   |   1   !
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",304
	;[305] REM +-------+-------+
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",305
	;[306] REM | Pixel | Pixel |
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",306
	;[307] REM |   2   |   3   !
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",307
	;[308] REM +-------+-------+
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",308
	;[309] REM
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",309
	;[310] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",310
	;[311] CONST CS_COLOUR_SQUARES_ENABLE		= $1000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",311
	;[312] CONST CS_PIX0_BLACK					= 0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",312
	;[313] CONST CS_PIX0_BLUE					= 1
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",313
	;[314] CONST CS_PIX0_RED					= 2
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",314
	;[315] CONST CS_PIX0_TAN					= 3
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",315
	;[316] CONST CS_PIX0_DARKGREEN				= 4
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",316
	;[317] CONST CS_PIX0_GREEN					= 5
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",317
	;[318] CONST CS_PIX0_YELLOW				= 6
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",318
	;[319] CONST CS_PIX0_BACKGROUND			= 7
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",319
	;[320] CONST CS_PIX1_BLACK					= 0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",320
	;[321] CONST CS_PIX1_BLUE					= 1*8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",321
	;[322] CONST CS_PIX1_RED					= 2*8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",322
	;[323] CONST CS_PIX1_TAN					= 3*8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",323
	;[324] CONST CS_PIX1_DARKGREEN				= 4*8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",324
	;[325] CONST CS_PIX1_GREEN					= 5*8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",325
	;[326] CONST CS_PIX1_YELLOW				= 6*8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",326
	;[327] CONST CS_PIX1_BACKGROUND			= 7*8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",327
	;[328] CONST CS_PIX2_BLACK					= 0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",328
	;[329] CONST CS_PIX2_BLUE					= 1*64
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",329
	;[330] CONST CS_PIX2_RED					= 2*64
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",330
	;[331] CONST CS_PIX2_TAN					= 3*64
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",331
	;[332] CONST CS_PIX2_DARKGREEN				= 4*64
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",332
	;[333] CONST CS_PIX2_GREEN					= 5*64
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",333
	;[334] CONST CS_PIX2_YELLOW				= 6*64
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",334
	;[335] CONST CS_PIX2_BACKGROUND			= 7*64
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",335
	;[336] CONST CS_PIX3_BLACK					= 0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",336
	;[337] CONST CS_PIX3_BLUE					= $0200
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",337
	;[338] CONST CS_PIX3_RED					= $0400
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",338
	;[339] CONST CS_PIX3_TAN					= $0600
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",339
	;[340] CONST CS_PIX3_DARKGREEN				= $2000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",340
	;[341] CONST CS_PIX3_GREEN					= $2200
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",341
	;[342] CONST CS_PIX3_YELLOW				= $2400
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",342
	;[343] CONST CS_PIX3_BACKGROUND			= $2600
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",343
	;[344] CONST CS_PIX_MASK					= CS_COLOUR_SQUARES_ENABLE+CS_PIX0_BACKGROUND+CS_PIX1_BACKGROUND+CS_PIX2_BACKGROUND+CS_PIX3_BACKGROUND
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",344
	;[345] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",345
	;[346] CONST CS_PIX_X_MIN					= 0		' Minimum x coordinate.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",346
	;[347] CONST CS_PIX_X_MAX					= 39	' Maximum x coordinate.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",347
	;[348] CONST CS_PIX_Y_MIN					= 0		' Minimum Y coordinate.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",348
	;[349] CONST CS_PIX_Y_MAX					= 23	' Maximum Y coordinate.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",349
	;[350] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",350
	;[351] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",351
	;[352] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",352
	;[353] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",353
	;[354] REM COLORS - Mode 1 (Foreground Background)
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",354
	;[355] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",355
	;[356] REM Foreground.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",356
	;[357] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",357
	;[358] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",358
	;[359] REM - For use with "peek/poke" commands that access BACKTAB.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",359
	;[360] REM - Only one foreground colour permitted per background card.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",360
	;[361] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",361
	;[362] CONST FG_BLACK						= $0000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",362
	;[363] CONST FG_BLUE						= $0001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",363
	;[364] CONST FG_RED						= $0002
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",364
	;[365] CONST FG_TAN						= $0003
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",365
	;[366] CONST FG_DARKGREEN					= $0004
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",366
	;[367] CONST FG_GREEN						= $0005
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",367
	;[368] CONST FG_YELLOW						= $0006
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",368
	;[369] CONST FG_WHITE						= $0007
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",369
	;[370] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",370
	;[371] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",371
	;[372] REM Background.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",372
	;[373] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",373
	;[374] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",374
	;[375] REM - For use with "peek/poke" commands that access BACKTAB.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",375
	;[376] REM - Only one background colour permitted per background card.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",376
	;[377] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",377
	;[378] CONST BG_BLACK						= $0000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",378
	;[379] CONST BG_BLUE						= $0200
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",379
	;[380] CONST BG_RED						= $0400
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",380
	;[381] CONST BG_TAN						= $0600
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",381
	;[382] CONST BG_DARKGREEN					= $2000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",382
	;[383] CONST BG_GREEN						= $2200
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",383
	;[384] CONST BG_YELLOW						= $2400
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",384
	;[385] CONST BG_WHITE						= $2600
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",385
	;[386] CONST BG_GREY						= $1000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",386
	;[387] CONST BG_CYAN						= $1200
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",387
	;[388] CONST BG_ORANGE						= $1400
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",388
	;[389] CONST BG_BROWN						= $1600
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",389
	;[390] CONST BG_PINK						= $3000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",390
	;[391] CONST BG_LIGHTBLUE					= $3200
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",391
	;[392] CONST BG_YELLOWGREEN				= $3400
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",392
	;[393] CONST BG_PURPLE						= $3600
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",393
	;[394] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",394
	;[395] CONST FGBG_CARD_DATA_MASK			= $01F8		' Mask to get the background card's data.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",395
	;[396] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",396
	;[397] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",397
	;[398] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",398
	;[399] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",399
	;[400] REM Sprites.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",400
	;[401] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",401
	;[402] REM Note: For use with "sprite" command.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",402
	;[403] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",403
	;[404] REM X
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",404
	;[405] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",405
	;[406] REM Note: Add these constants to the sprite command's X parameter.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",406
	;[407] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",407
	;[408] CONST HIT							= $0100		' Enable the sprite's collision detection.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",408
	;[409] CONST VISIBLE						= $0200		' Make the sprite visible.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",409
	;[410] CONST ZOOMX2						= $0400		' Make the sprite twice the width.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",410
	;[411] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",411
	;[412] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",412
	;[413] REM Y
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",413
	;[414] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",414
	;[415] REM Note: Add these constants to the sprite command's Y parameter.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",415
	;[416] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",416
	;[417] CONST DOUBLEY						= $0080		' Make a double height sprite (with 2 GRAM cards).
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",417
	;[418] CONST ZOOMY2						= $0100		' Make the sprite twice (x2) the normal height.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",418
	;[419] CONST ZOOMY4						= $0200		' Make the sprite quadruple (x4) the normal height.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",419
	;[420] CONST ZOOMY8						= $0300		' Make the sprite octuple (x8) the normal height.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",420
	;[421] CONST FLIPX							= $0400		' Flip/mirror the sprite in X.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",421
	;[422] CONST FLIPY							= $0800		' Flip/mirror the sprite in Y.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",422
	;[423] CONST MIRROR						= $0C00		' Flip/mirror the sprite in both X and Y.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",423
	;[424] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",424
	;[425] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",425
	;[426] REM A
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",426
	;[427] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",427
	;[428] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",428
	;[429] REM - Combine to create the sprite command's A parameter.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",429
	;[430] REM - Only one colour per sprite.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",430
	;[431] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",431
	;[432] CONST GRAM							= $0800		' Sprite's data is located in GRAM.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",432
	;[433] CONST BEHIND						= $2000		' Sprite is behind the background.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",433
	;[434] CONST SPR_BLACK						= $0000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",434
	;[435] CONST SPR_BLUE						= $0001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",435
	;[436] CONST SPR_RED						= $0002
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",436
	;[437] CONST SPR_TAN						= $0003
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",437
	;[438] CONST SPR_DARKGREEN					= $0004
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",438
	;[439] CONST SPR_GREEN						= $0005
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",439
	;[440] CONST SPR_YELLOW					= $0006
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",440
	;[441] CONST SPR_WHITE						= $0007
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",441
	;[442] CONST SPR_GREY						= $1000
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",442
	;[443] CONST SPR_CYAN						= $1001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",443
	;[444] CONST SPR_ORANGE					= $1002
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",444
	;[445] CONST SPR_BROWN						= $1003
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",445
	;[446] CONST SPR_PINK						= $1004
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",446
	;[447] CONST SPR_LIGHTBLUE					= $1005
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",447
	;[448] CONST SPR_YELLOWGREEN				= $1006
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",448
	;[449] CONST SPR_PURPLE					= $1007
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",449
	;[450] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",450
	;[451] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",451
	;[452] REM GRAM numbers.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",452
	;[453] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",453
	;[454] REM Note: For use in the sprite command's parameter A.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",454
	;[455] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",455
	;[456] CONST SPR00							= $0800
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",456
	;[457] CONST SPR01							= $0808
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",457
	;[458] CONST SPR02							= $0810
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",458
	;[459] CONST SPR03							= $0818
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",459
	;[460] CONST SPR04							= $0820
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",460
	;[461] CONST SPR05							= $0828
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",461
	;[462] CONST SPR06							= $0830
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",462
	;[463] CONST SPR07							= $0838
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",463
	;[464] CONST SPR08							= $0840
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",464
	;[465] CONST SPR09							= $0848
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",465
	;[466] CONST SPR10							= $0850
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",466
	;[467] CONST SPR11							= $0858
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",467
	;[468] CONST SPR12							= $0860
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",468
	;[469] CONST SPR13							= $0868
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",469
	;[470] CONST SPR14							= $0870
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",470
	;[471] CONST SPR15							= $0878
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",471
	;[472] CONST SPR16							= $0880
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",472
	;[473] CONST SPR17							= $0888
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",473
	;[474] CONST SPR18							= $0890
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",474
	;[475] CONST SPR19							= $0898
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",475
	;[476] CONST SPR20							= $08A0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",476
	;[477] CONST SPR21							= $08A8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",477
	;[478] CONST SPR22							= $08B0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",478
	;[479] CONST SPR23							= $08B8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",479
	;[480] CONST SPR24							= $08C0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",480
	;[481] CONST SPR25							= $08C8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",481
	;[482] CONST SPR26							= $08D0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",482
	;[483] CONST SPR27							= $08D8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",483
	;[484] CONST SPR28							= $08E0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",484
	;[485] CONST SPR29							= $08E8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",485
	;[486] CONST SPR30							= $08F0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",486
	;[487] CONST SPR31							= $08F8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",487
	;[488] CONST SPR32							= $0900
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",488
	;[489] CONST SPR33							= $0908
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",489
	;[490] CONST SPR34							= $0910
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",490
	;[491] CONST SPR35							= $0918
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",491
	;[492] CONST SPR36							= $0920
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",492
	;[493] CONST SPR37							= $0928
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",493
	;[494] CONST SPR38							= $0930
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",494
	;[495] CONST SPR39							= $0938
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",495
	;[496] CONST SPR40							= $0940
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",496
	;[497] CONST SPR41							= $0948
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",497
	;[498] CONST SPR42							= $0950
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",498
	;[499] CONST SPR43							= $0958
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",499
	;[500] CONST SPR44							= $0960
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",500
	;[501] CONST SPR45							= $0968
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",501
	;[502] CONST SPR46							= $0970
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",502
	;[503] CONST SPR47							= $0978
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",503
	;[504] CONST SPR48							= $0980
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",504
	;[505] CONST SPR49							= $0988
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",505
	;[506] CONST SPR50							= $0990
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",506
	;[507] CONST SPR51							= $0998
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",507
	;[508] CONST SPR52							= $09A0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",508
	;[509] CONST SPR53							= $09A8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",509
	;[510] CONST SPR54							= $09B0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",510
	;[511] CONST SPR55							= $09B8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",511
	;[512] CONST SPR56							= $09C0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",512
	;[513] CONST SPR57							= $09C8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",513
	;[514] CONST SPR58							= $09D0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",514
	;[515] CONST SPR59							= $09D8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",515
	;[516] CONST SPR60							= $09E0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",516
	;[517] CONST SPR61							= $09E8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",517
	;[518] CONST SPR62							= $09F0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",518
	;[519] CONST SPR63							= $09F8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",519
	;[520] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",520
	;[521] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",521
	;[522] REM Sprite collision.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",522
	;[523] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",523
	;[524] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",524
	;[525] REM - For use with variables COL0, COL1, COL2, COL3, COL4, COL5, COL6 and COL7.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",525
	;[526] REM - More than one collision can occur simultaneously.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",526
	;[527] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",527
	;[528] CONST HIT_SPRITE0					= $0001		' Sprite collided with sprite 0.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",528
	;[529] CONST HIT_SPRITE1					= $0002		' Sprite collided with sprite 1.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",529
	;[530] CONST HIT_SPRITE2					= $0004		' Sprite collided with sprite 2.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",530
	;[531] CONST HIT_SPRITE3					= $0008		' Sprite collided with sprite 3.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",531
	;[532] CONST HIT_SPRITE4					= $0010		' Sprite collided with sprite 4.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",532
	;[533] CONST HIT_SPRITE5					= $0020		' Sprite collided with sprite 5.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",533
	;[534] CONST HIT_SPRITE6					= $0040		' Sprite collided with sprite 6.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",534
	;[535] CONST HIT_SPRITE7					= $0080		' Sprite collided with sprite 7.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",535
	;[536] CONST HIT_BACKGROUND				= $0100		' Sprite collided with a background pixel.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",536
	;[537] CONST HIT_BORDER					= $0200		' Sprite collided with the top/bottom/left/right border.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",537
	;[538] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",538
	;[539] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",539
	;[540] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",540
	;[541] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",541
	;[542] REM DISC - Compass.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",542
	;[543] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",543
	;[544] REM   NW         N         NE
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",544
	;[545] REM     \   NNW  |  NNE   /
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",545
	;[546] REM       \      |      /
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",546
	;[547] REM         \    |    /
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",547
	;[548] REM    WNW    \  |  /    ENE
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",548
	;[549] REM             \|/
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",549
	;[550] REM  W ----------+---------- E
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",550
	;[552] REM             /|REM    WSW    /  |  \    ESE
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",552
	;[556] REM         /    |    REM       /      |      REM     /   SSW  |  SSE   REM   SW         S         SE
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",556
	;[557] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",557
	;[558] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",558
	;[559] REM - North points upwards on the hand controller.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",559
	;[560] REM - Directions are listed in a clockwise manner.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",560
	;[561] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",561
	;[562] CONST DISC_NORTH					= $0004
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",562
	;[563] CONST DISC_NORTH_NORTH_EAST 		= $0014
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",563
	;[564] CONST DISC_NORTH_EAST				= $0016
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",564
	;[565] CONST DISC_EAST_NORTH_EAST			= $0006
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",565
	;[566] CONST DISC_EAST						= $0002
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",566
	;[567] CONST DISC_EAST_SOUTH_EAST			= $0012
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",567
	;[568] CONST DISC_SOUTH_EAST				= $0013
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",568
	;[569] CONST DISC_SOUTH_SOUTH_EAST			= $0003
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",569
	;[570] CONST DISC_SOUTH					= $0001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",570
	;[571] CONST DISC_SOUTH_SOUTH_WEST			= $0011
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",571
	;[572] CONST DISC_SOUTH_WEST				= $0019
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",572
	;[573] CONST DISC_WEST_SOUTH_WEST			= $0009
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",573
	;[574] CONST DISC_WEST						= $0008
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",574
	;[575] CONST DISC_WEST_NORTH_WEST			= $0018
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",575
	;[576] CONST DISC_NORTH_WEST				= $001C
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",576
	;[577] CONST DISC_NORTH_NORTH_WEST			= $000C
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",577
	;[578] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",578
	;[579] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",579
	;[580] REM DISC - Compass abbreviated versions.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",580
	;[581] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",581
	;[582] CONST DISC_N						= $0004
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",582
	;[583] CONST DISC_NNE						= $0014
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",583
	;[584] CONST DISC_NE						= $0016
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",584
	;[585] CONST DISC_ENE						= $0006
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",585
	;[586] CONST DISC_E						= $0002
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",586
	;[587] CONST DISC_ESE						= $0012
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",587
	;[588] CONST DISC_SE						= $0013
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",588
	;[589] CONST DISC_SSE						= $0003
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",589
	;[590] CONST DISC_S						= $0001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",590
	;[591] CONST DISC_SSW						= $0011
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",591
	;[592] CONST DISC_SW						= $0019
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",592
	;[593] CONST DISC_WSW						= $0009
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",593
	;[594] CONST DISC_W						= $0008
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",594
	;[595] CONST DISC_WNW						= $0018
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",595
	;[596] CONST DISC_NW						= $001C
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",596
	;[597] CONST DISC_NNW						= $000C
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",597
	;[598] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",598
	;[599] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",599
	;[600] REM DISC - Directions.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",600
	;[601] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",601
	;[602] CONST DISC_UP						= $0004
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",602
	;[603] CONST DISC_UP_RIGHT					= $0016		' Up and right diagonal.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",603
	;[604] CONST DISC_RIGHT					= $0002
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",604
	;[605] CONST DISC_DOWN_RIGHT				= $0013		' Down  and right diagonal.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",605
	;[606] CONST DISC_DOWN						= $0001
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",606
	;[607] CONST DISC_DOWN_LEFT				= $0019		' Down and left diagonal.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",607
	;[608] CONST DISC_LEFT						= $0008
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",608
	;[609] CONST DISC_UP_LEFT					= $001C		' Up and left diagonal.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",609
	;[610] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",610
	;[611] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",611
	;[612] REM DISK - Mask.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",612
	;[613] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",613
	;[614] CONST DISK_MASK						= $001F
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",614
	;[615] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",615
	;[616] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",616
	;[617] REM Controller - Keypad.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",617
	;[618] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",618
	;[619] CONST KEYPAD_0						= 72
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",619
	;[620] CONST KEYPAD_1						= 129
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",620
	;[621] CONST KEYPAD_2						= 65
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",621
	;[622] CONST KEYPAD_3						= 33
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",622
	;[623] CONST KEYPAD_4						= 130
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",623
	;[624] CONST KEYPAD_5						= 66
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",624
	;[625] CONST KEYPAD_6						= 34
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",625
	;[626] CONST KEYPAD_7						= 132
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",626
	;[627] CONST KEYPAD_8						= 68
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",627
	;[628] CONST KEYPAD_9						= 36
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",628
	;[629] CONST KEYPAD_CLEAR					= 136
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",629
	;[630] CONST KEYPAD_ENTER					= 40
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",630
	;[631] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",631
	;[632] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",632
	;[633] REM Controller - Pause buttons (1+9 or 3+7 held down simultaneously).
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",633
	;[634] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",634
	;[635] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",635
	;[636] REM - Key codes for 3+7 and 1+9 are the same (165).
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",636
	;[637] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",637
	;[638] CONST KEYPAD_PAUSE					= (KEYPAD_1 XOR KEYPAD_9)
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",638
	;[639] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",639
	;[640] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",640
	;[641] REM Controller - Side buttons.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",641
	;[642] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",642
	;[643] CONST BUTTON_TOP_LEFT				= $A0		' Top left and top right are the same button.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",643
	;[644] CONST BUTTON_TOP_RIGHT				= $A0		' Note: Bit 6 is low.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",644
	;[645] CONST BUTTON_BOTTOM_LEFT			= $60		' Note: Bit 7 is low.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",645
	;[646] CONST BUTTON_BOTTOM_RIGHT			= $C0		' Note: Bit 5 is low
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",646
	;[647] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",647
	;[648] REM Abbreviated versions.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",648
	;[649] CONST BUTTON_1						= $A0		' Top left or top right.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",649
	;[650] CONST BUTTON_2						= $60		' Bottom left.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",650
	;[651] CONST BUTTON_3						= $C0		' Bottom right.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",651
	;[652] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",652
	;[653] REM Mask.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",653
	;[654] CONST BUTTON_MASK					= $E0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",654
	;[655] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",655
	;[656] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",656
	;[657] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",657
	;[658] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",658
	;[659] REM Programmable Sound Generator (PSG)
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",659
	;[660] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",660
	;[661] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",661
	;[662] REM - For use with the SOUND command
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",662
	;[663] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",663
	;[664] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",664
	;[665] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",665
	;[666] REM Internal sound hardware.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",666
	;[667] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",667
	;[668] CONST PSG_CHANNELA					= 0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",668
	;[669] CONST PSG_CHANNELB					= 1
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",669
	;[670] CONST PSG_CHANNELC					= 2
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",670
	;[671] CONST PSG_ENVELOPE					= 3
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",671
	;[672] CONST PSG_MIXER						= 4
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",672
	;[673] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",673
	;[674] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",674
	;[675] REM ECS sound hardware.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",675
	;[676] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",676
	;[677] CONST PSG_ECS_CHANNELA				= 5
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",677
	;[678] CONST PSG_ECS_CHANNELB				= 6
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",678
	;[679] CONST PSG_ECS_CHANNELC				= 7
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",679
	;[680] CONST PSG_ECS_ENVELOPE				= 8
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",680
	;[681] CONST PSG_ECS_MIXER					= 9
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",681
	;[682] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",682
	;[683] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",683
	;[684] REM PSG - Volume control.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",684
	;[685] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",685
	;[686] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",686
	;[687] REM - For use in the volume field of the SOUND command.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",687
	;[688] REM - Internal channels: PSG_CHANNELA, PSG_CHANNELB, PSG_CHANNELC
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",688
	;[689] REM - ECS channels: PSG_ECS_CHANNELA, PSG_ECS_CHANNELB, PSG_ECS_CHANNELC
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",689
	;[690] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",690
	;[691] CONST PSG_VOLUME_MAX				= 15	' Maximum channel volume.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",691
	;[692] CONST PSG_ENVELOPE_ENABLE			= 48	' Channel volume is controlled by envelope generator.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",692
	;[693] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",693
	;[694] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",694
	;[695] REM PSG - Mixer control.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",695
	;[696] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",696
	;[697] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",697
	;[698] REM - Internal channel: PSG_MIXER
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",698
	;[699] REM - ECS channel: PSG_ECS_MIXER
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",699
	;[700] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",700
	;[701] CONST PSG_TONE_CHANNELA_DISABLE		= $01	' Disable channel A tone.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",701
	;[702] CONST PSG_TONE_CHANNELB_DISABLE		= $02	' Disable channel B tone.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",702
	;[703] CONST PSG_TONE_CHANNELC_DISABLE		= $04	' Disable channel C tone.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",703
	;[704] CONST PSG_NOISE_CHANNELA_DISABLE	= $08	' Disable channel A noise.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",704
	;[705] CONST PSG_NOISE_CHANNELB_DISABLE	= $10	' Disable channel B noise.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",705
	;[706] CONST PSG_NOISE_CHANNELC_DISABLE	= $20	' Disable channel C noise.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",706
	;[707] CONST PSG_MIXER_DEFAULT				= $38 	' All notes enabled. all noise disabled.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",707
	;[708] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",708
	;[709] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",709
	;[710] REM PSG - Envelope control.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",710
	;[711] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",711
	;[712] REM Notes:
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",712
	;[713] REM - Internal channel: PSG_ENVELOPE
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",713
	;[714] REM - ECS channel: PSG_ECS_ENVELOPE
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",714
	;[715] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",715
	;[716] CONST PSG_ENVELOPE_HOLD								= $01
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",716
	;[717] CONST PSG_ENVELOPE_ALTERNATE						= $02
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",717
	;[718] CONST PSG_ENVELOPE_ATTACK							= $04
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",718
	;[719] CONST PSG_ENVELOPE_CONTINUE							= $08
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",719
	;[720] CONST PSG_ENVELOPE_SINGLE_SHOT_RAMP_DOWN_AND_OFF	= $00 '\______
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",720
	;[721] CONST PSG_ENVELOPE_SINGLE_SHOT_RAMP_UP_AND_OFF		= $04 '/______
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",721
	;[724] CONST PSG_ENVELOPE_CYCLE_RAMP_DOWN_SAWTOOTH			= $08 '\\\\\\CONST PSG_ENVELOPE_CYCLE_RAMP_DOWN_TRIANGLE			= $0A '\/\/\/CONST PSG_ENVELOPE_SINGLE_SHOT_RAMP_DOWN_AND_MAX	= $0B '\^^^^^^
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",724
	;[725] CONST PSG_ENVELOPE_CYCLE_RAMP_UP_SAWTOOTH			= $0C '///////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",725
	;[726] CONST PSG_ENVELOPE_SINGLE_SHOT_RAMP_UP_AND_MAX		= $0D '/^^^^^^
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",726
	;[727] CONST PSG_ENVELOPE_CYCLE_RAMP_UP_TRIANGLE			= $0E '/\/\/\/
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",727
	;[728] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",728
	;[729] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",729
	;[730] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",730
	;[731] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",731
	;[732] REM Useful functions.
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",732
	;[733] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",733
	;[734] DEF FN screenpos(aColumn, aRow)				=               (((aRow)*BACKGROUND_COLUMNS)+(aColumn))
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",734
	;[735] DEF FN screenaddr(aColumn, aRow)			= (BACKTAB_ADDR+(((aRow)*BACKGROUND_COLUMNS)+(aColumn)))
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",735
	;[736] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",736
	;[737] DEF FN setspritex(aSpriteNo,anXPosition)	= #mobshadow(aSpriteNo  )=(#mobshadow(aSpriteNo  ) and $ff00)+anXPosition
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",737
	;[738] DEF FN setspritey(aSpriteNo,aYPosition)		= #mobshadow(aSpriteNo+8)=(#mobshadow(aSpriteNo+8) and $ff80)+aYPosition
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",738
	;[739] DEF FN resetsprite(aSpriteNo)				= sprite aSpriteNo, 0, 0, 0
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",739
	;[740] DEF FN togglespritevisible(aSpriteNo)		= #mobshadow(aSpriteNo   )=#mobshadow(aSpriteNo)    xor VISIBLE
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",740
	;[741] DEF FN togglespritehit(aSpriteNo)			= #mobshadow(aSpriteNo   )=#mobshadow(aSpriteNo)    xor HIT
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",741
	;[742] DEF FN togglespritebehind(aSpriteNo)		= #mobshadow(aSpriteNo+16)=#mobshadow(aSpriteNo+16) xor BEHIND
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",742
	;[743] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",743
	;[744] REM /////////////////////////////////////////////////////////////////////////
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",744
	;[745] 
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",745
	;[746] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",746
	;[747] REM END
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",747
	;[748] REM -------------------------------------------------------------------------
	SRCFILE "C:\IntyBASIC SDK\lib\constants.bas",748
	;ENDFILE
	;FILE C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas
	;[28] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",28
	;[29] '///// Mode graphique I /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",29
	;[30] 	mode SCREEN_FOREGROUND_BACKGROUND
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",30
	MVII #3,R0
	MVO R0,_mode_select
	;[31] 	BORDER 0,2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",31
	CLRR R0
	MVO R0,_border_color
	MVII #2,R0
	MVO R0,_border_mask
	;[32] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",32
	;[33] '//////////////////////////////////////////////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",33
	;[34] '///// Chargement données graphiques GRAM /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",34
	;[35] '//////////////////////////////////////////////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",35
	;[36] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",36
	CALL _wait
	;[37] 	DEFINE DEF00,3,PAPI_BAS_0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",37
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_BAS_0,R0
	MVO R0,_gram_bitmap
	;[38] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",38
	CALL _wait
	;[39] 	DEFINE DEF03,4,SOLDAT_1D
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",39
	MVII #3,R0
	MVO R0,_gram_target
	MVII #4,R0
	MVO R0,_gram_total
	MVII #label_SOLDAT_1D,R0
	MVO R0,_gram_bitmap
	;[40] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",40
	CALL _wait
	;[41] 	DEFINE DEF07,3,SOLDAT_1B
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",41
	MVII #7,R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_SOLDAT_1B,R0
	MVO R0,_gram_bitmap
	;[42] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",42
	CALL _wait
	;[43] 	DEFINE DEF10,3,SOLDAT_1H
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",43
	MVII #10,R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_SOLDAT_1H,R0
	MVO R0,_gram_bitmap
	;[44] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",44
	CALL _wait
	;[45] 	DEFINE DEF13,2,Robot1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",45
	MVII #13,R0
	MVO R0,_gram_target
	MVII #2,R0
	MVO R0,_gram_total
	MVII #label_ROBOT1,R0
	MVO R0,_gram_bitmap
	;[46] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",46
	CALL _wait
	;[47] 	DEFINE DEF15,1,Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",47
	MVII #15,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
	;[48] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",48
	CALL _wait
	;[49] 	DEFINE 16,1,ARBRE_TRONC
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",49
	MVII #16,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_ARBRE_TRONC,R0
	MVO R0,_gram_bitmap
	;[50] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",50
	CALL _wait
	;[51] 	DEFINE 17,1,SAC_G
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",51
	MVII #17,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_SAC_G,R0
	MVO R0,_gram_bitmap
	;[52] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",52
	CALL _wait
	;[53] 	DEFINE 18,1,SAC_M
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",53
	MVII #18,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_SAC_M,R0
	MVO R0,_gram_bitmap
	;[54] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",54
	CALL _wait
	;[55] 	DEFINE 19,1,SAC_D
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",55
	MVII #19,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_SAC_D,R0
	MVO R0,_gram_bitmap
	;[56] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",56
	CALL _wait
	;[57] 	DEFINE 20,1,SAC
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",57
	MVII #20,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_SAC,R0
	MVO R0,_gram_bitmap
	;[58] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",58
	CALL _wait
	;[59] 	DEFINE 21,1,EAU_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",59
	MVII #21,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_EAU_1,R0
	MVO R0,_gram_bitmap
	;[60] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",60
	CALL _wait
	;[61] 	DEFINE 22,1,MUR_AVANT
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",61
	MVII #22,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_MUR_AVANT,R0
	MVO R0,_gram_bitmap
	;[62]     wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",62
	CALL _wait
	;[63]     DEFINE 23,1,OmbreBatiment
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",63
	MVII #23,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_OMBREBATIMENT,R0
	MVO R0,_gram_bitmap
	;[64]     wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",64
	CALL _wait
	;[65] 	DEFINE 24,1,BARBELE_G
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",65
	MVII #24,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BARBELE_G,R0
	MVO R0,_gram_bitmap
	;[66] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",66
	CALL _wait
	;[67] 	DEFINE 25,1,BARBELE_M
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",67
	MVII #25,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BARBELE_M,R0
	MVO R0,_gram_bitmap
	;[68] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",68
	CALL _wait
	;[69] 	DEFINE 26,1,BARBELE_D
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",69
	MVII #26,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BARBELE_D,R0
	MVO R0,_gram_bitmap
	;[70] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",70
	CALL _wait
	;[71] 	DEFINE 27,1,TOITURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",71
	MVII #27,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_TOITURE,R0
	MVO R0,_gram_bitmap
	;[72] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",72
	CALL _wait
	;[73] 	DEFINE 28,1,MURMAISON_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",73
	MVII #28,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_MURMAISON_1,R0
	MVO R0,_gram_bitmap
	;[74] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",74
	CALL _wait
	;[75] 	DEFINE 29,1,MURMAISON_ENTREE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",75
	MVII #29,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_MURMAISON_ENTREE,R0
	MVO R0,_gram_bitmap
	;[76] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",76
	CALL _wait
	;[77] 	DEFINE 30,1,MURMAISON_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",77
	MVII #30,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_MURMAISON_2,R0
	MVO R0,_gram_bitmap
	;[78] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",78
	CALL _wait
	;[79] 	DEFINE 31,1,TOITBUNKER
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",79
	MVII #31,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_TOITBUNKER,R0
	MVO R0,_gram_bitmap
	;[80] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",80
	CALL _wait
	;[81] 	DEFINE 32,1,BUNKER_G
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",81
	MVII #32,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BUNKER_G,R0
	MVO R0,_gram_bitmap
	;[82] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",82
	CALL _wait
	;[83] 	DEFINE 33,1,BUNKER_D
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",83
	MVII #33,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BUNKER_D,R0
	MVO R0,_gram_bitmap
	;[84] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",84
	CALL _wait
	;[85] 	DEFINE 34,1,BUNKER_M
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",85
	MVII #34,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BUNKER_M,R0
	MVO R0,_gram_bitmap
	;[86]     wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",86
	CALL _wait
	;[87]     DEFINE 35,1,EntreeNoire
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",87
	MVII #35,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_ENTREENOIRE,R0
	MVO R0,_gram_bitmap
	;[88]     wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",88
	CALL _wait
	;[89] 	DEFINE 36,1,ViePapi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",89
	MVII #36,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_VIEPAPI,R0
	MVO R0,_gram_bitmap
	;[90] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",90
	CALL _wait
	;[91] 	DEFINE 37,1,GrenadeTile
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",91
	MVII #37,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GRENADETILE,R0
	MVO R0,_gram_bitmap
	;[92]     wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",92
	CALL _wait
	;[93]     DEFINE 38,1,MurMaison1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",93
	MVII #38,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_MURMAISON1,R0
	MVO R0,_gram_bitmap
	;[94]     wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",94
	CALL _wait
	;[95]     DEFINE 39,1,MurMaison2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",95
	MVII #39,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_MURMAISON2,R0
	MVO R0,_gram_bitmap
	;[96]     wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",96
	CALL _wait
	;[97] 	DEFINE 40,1,PASSERELLE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",97
	MVII #40,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_PASSERELLE,R0
	MVO R0,_gram_bitmap
	;[98]     wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",98
	CALL _wait
	;[99]     DEFINE 41,1,FrontWALL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",99
	MVII #41,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_FRONTWALL,R0
	MVO R0,_gram_bitmap
	;[100]     wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",100
	CALL _wait
	;[101] 	DEFINE 42,1,BLANK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",101
	MVII #42,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BLANK,R0
	MVO R0,_gram_bitmap
	;[102] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",102
	CALL _wait
	;[103] 	DEFINE 43,1,MURBRIQUE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",103
	MVII #43,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_MURBRIQUE,R0
	MVO R0,_gram_bitmap
	;[104] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",104
	CALL _wait
	;[105] 	DEFINE 44,1,MURBRIQUE_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",105
	MVII #44,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_MURBRIQUE_1,R0
	MVO R0,_gram_bitmap
	;[106] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",106
	CALL _wait
	;[107] 	DEFINE 45,1,PONT_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",107
	MVII #45,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_PONT_1,R0
	MVO R0,_gram_bitmap
	;[108] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",108
	CALL _wait
	;[109] 	DEFINE 46,1,PONT_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",109
	MVII #46,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_PONT_2,R0
	MVO R0,_gram_bitmap
	;[110] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",110
	CALL _wait
	;[111] 	DEFINE DEF47,1,Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",111
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
	;[112] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",112
	CALL _wait
	;[113]     DEFINE 48,1,HERBE1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",113
	MVII #48,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_HERBE1,R0
	MVO R0,_gram_bitmap
	;[114] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",114
	CALL _wait
	;[115] 	DEFINE 49,1,COINEAU_HD
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",115
	MVII #49,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COINEAU_HD,R0
	MVO R0,_gram_bitmap
	;[116] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",116
	CALL _wait
	;[117] 	DEFINE 50,1,COINEAU_HG
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",117
	MVII #50,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COINEAU_HG,R0
	MVO R0,_gram_bitmap
	;[118] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",118
	CALL _wait
	;[119] 	DEFINE 51,1,COINEAU_BD
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",119
	MVII #51,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COINEAU_BD,R0
	MVO R0,_gram_bitmap
	;[120] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",120
	CALL _wait
	;[121] 	DEFINE 52,1,COINEAU_BG
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",121
	MVII #52,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COINEAU_BG,R0
	MVO R0,_gram_bitmap
	;[122] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",122
	CALL _wait
	;[123] 	DEFINE 53,1,COINEAU_HD1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",123
	MVII #53,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COINEAU_HD1,R0
	MVO R0,_gram_bitmap
	;[124] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",124
	CALL _wait
	;[125] 	DEFINE 54,1,COINEAU_HG1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",125
	MVII #54,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COINEAU_HG1,R0
	MVO R0,_gram_bitmap
	;[126] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",126
	CALL _wait
	;[127] 	DEFINE 55,1,COINTEAU_BD1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",127
	MVII #55,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COINTEAU_BD1,R0
	MVO R0,_gram_bitmap
	;[128] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",128
	CALL _wait
	;[129] 	DEFINE 56,1,COINEAU_BG1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",129
	MVII #56,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COINEAU_BG1,R0
	MVO R0,_gram_bitmap
	;[130] 	wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",130
	CALL _wait
	;[131] 	DEFINE 57,1,IABUNKER_M
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",131
	MVII #57,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_IABUNKER_M,R0
	MVO R0,_gram_bitmap
	;[132] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",132
	CALL _wait
	;[133] 	DEFINE 58,1,GrenadeTile
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",133
	MVII #58,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GRENADETILE,R0
	MVO R0,_gram_bitmap
	;[134] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",134
	CALL _wait
	;[135]     DEFINE 59,1,HautPont
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",135
	MVII #59,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_HAUTPONT,R0
	MVO R0,_gram_bitmap
	;[136] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",136
	CALL _wait
	;[137] 	DEFINE 60,1,ImpactExplosion
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",137
	MVII #60,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_IMPACTEXPLOSION,R0
	MVO R0,_gram_bitmap
	;[138] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",138
	CALL _wait
	;[139] 	DEFINE 61,1,SOL_HERBE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",139
	MVII #61,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_SOL_HERBE,R0
	MVO R0,_gram_bitmap
	;[140] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",140
	CALL _wait
	;[141] 	DEFINE 62,1,ROCHER
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",141
	MVII #62,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_ROCHER,R0
	MVO R0,_gram_bitmap
	;[142] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",142
	CALL _wait
	;[143] 	DEFINE 63,1,ARBRE_CIME
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",143
	MVII #63,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_ARBRE_CIME,R0
	MVO R0,_gram_bitmap
	;[144] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",144
	CALL _wait
	;[145] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",145
	;[146] '///// Constantes diverses /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",146
	;[147] 	CONST CARD_ARBRETRONC=$0800+16*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",147
	;[148] 	CONST CARD_SACG = $0800+17*8 + BG_YELLOW	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",148
	;[149] 	CONST CARD_SACM = $0800+18*8 + BG_YELLOW
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",149
	;[150] 	CONST CARD_SACD = $0800+19*8 + BG_YELLOW
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",150
	;[151] 	CONST CARD_SAC = $0800+20*8 + BG_YELLOW
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",151
	;[152] 	CONST CARD_EAU1 = $0801+21*8 + BG_LIGHTBLUE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",152
	;[153] 	CONST CARD_MURAVANT = $0800+22*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",153
	;[154] 	CONST CARD_OmbreBatiment=$804+ 23*8+BG_BLACK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",154
	;[155] 	CONST CARD_BARBELEG = $0800+24*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",155
	;[156] 	CONST CARD_BARBELEM = $0800+25*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",156
	;[157] 	CONST CARD_BARBELED = $0800+26*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",157
	;[158] 	CONST CARD_TOITURE = $0800+27*8 + BG_RED
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",158
	;[159] 	CONST CARD_MURMAISON1 = $0800+28*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",159
	;[160] 	CONST CARD_MURMAISONENTREE = $0800+29*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",160
	;[161] 	CONST CARD_MURMAISON2 = $0800+30*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",161
	;[162] 	CONST CARD_TOITBUNKER = $0800+31*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",162
	;[163] 	CONST CARD_BUNKERG = $0800+32*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",163
	;[164] 	CONST CARD_BUNKERD = $0800+33*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",164
	;[165] 	CONST CARD_BUNKERM = $0800+34*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",165
	;[166] 	CONST CARD_BLACK= $0800+35*8 + BG_BLACK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",166
	;[167] 	CONST CARD_VIES = $0802+36*8 + BG_BLACK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",167
	;[168] 	CONST CARD_ITEM = $0806+37*8 + BG_BLACK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",168
	;[169]     CONST CARD_MurMaison11=$800+ 38*8+BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",169
	;[170]     CONST CARD_MurMaison22=$800+ 39*8+BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",170
	;[171] 	CONST CARD_PASSERELLE = $0800+40*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",171
	;[172]     CONST CARD_FrontWALL=$800+ 41*8+BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",172
	;[173] 	CONST CARD_BLANK = $0803+42*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",173
	;[174] 	CONST CARD_MURBRIQUE = $0800+43*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",174
	;[175] 	CONST CARD_MURBRIQUE_1 = $0800+44*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",175
	;[176] 	CONST CARD_PONT_1 = $0800+45*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",176
	;[177] 	CONST CARD_PONT_2 = $0800+46*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",177
	;[178]     CONST CARD_HERBE1 = $0805+ 48*8+BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",178
	;[179] 	CONST CARD_COINEAU_HD = $0801+49*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",179
	;[180] 	CONST CARD_COINEAU_HG = $0801+50*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",180
	;[181] 	CONST CARD_COINEAU_BD = $0801+51*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",181
	;[182] 	CONST CARD_COINEAU_BG = $0801+52*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",182
	;[183] 	CONST CARD_COINEAU_HD1 = $0801+53*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",183
	;[184] 	CONST CARD_COINEAU_HG1 = $0801+54*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",184
	;[185] 	CONST CARD_COINEAU_BD1 = $0801+55*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",185
	;[186] 	CONST CARD_COINEAU_BG1 = $0801+56*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",186
	;[187] 	CONST CARD_IABUNKER_M = $0800+57*8 + BG_GREY
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",187
	;[188] 	CONST CARD_ITEM1 = $0802+58*8 + BG_BLACK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",188
	;[189]     CONST CARD_HautPont=$800+ 59*8+BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",189
	;[190] 	CONST CARD_ImpactBoum=$800+60*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",190
	;[191] 	CONST CARD_HERBE=$0805+61*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",191
	;[192] 	CONST CARD_ROCHER=$0800+62*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",192
	;[193] 	CONST CARD_ARBRECIME=$0805+63*8 + BG_DARKGREEN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",193
	;[194] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",194
	;[195] 	'Constante Collision Objets
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",195
	;[196] 	CONST SOL_TRONC= BG16+(FG_BLACK xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",196
	;[197] 	CONST SOL_EAU = BG21 + (FG_BLUE xor BG_LIGHTBLUE)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",197
	;[198]     CONST COL_OmbreBatiment = BG23+ (FG_DARKGREEN xor BG_BLACK)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",198
	;[199] 	CONST SOL_BARBELE_G= BG24+(FG_BLACK xor BG_DARKGREEN)	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",199
	;[200] 	CONST SOL_BARBELE_M= BG25+(FG_BLACK xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",200
	;[201] 	CONST SOL_BARBELE_D= BG26+(FG_BLACK xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",201
	;[202]     CONST COL_EntreeNoire=BG35+ (FG_BLACK xor BG_BLACK)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",202
	;[203] 	CONST PASSERELLE=BG40+(FG_BLACK xor BG_GREY)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",203
	;[204] 	CONST SOL_BLANK = BG42 + (FG_TAN xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",204
	;[205] 	CONST PONTROUTE1=BG45 +(FG_BLACK xor BG_GREY)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",205
	;[206] 	CONST PONTROUTE2=BG46 +(FG_BLACK xor BG_GREY)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",206
	;[207] 	CONST COL_HERBE1=BG48+ (FG_GREEN xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",207
	;[208] 	CONST SOL_COINEAU_HD=BG49+(FG_BLUE xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",208
	;[209] 	CONST SOL_COINEAU_HG=BG50+(FG_BLUE xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",209
	;[210] 	CONST SOL_COINEAU_BD=BG51+(FG_BLUE xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",210
	;[211] 	CONST SOL_COINEAU_BG=BG52+(FG_BLUE xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",211
	;[212] 	CONST SOL_COINEAU_HD1=BG53+(FG_BLUE xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",212
	;[213] 	CONST SOL_COINEAU_HG1=BG54+(FG_BLUE xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",213
	;[214] 	CONST SOL_COINEAU_BD1=BG55+(FG_BLUE xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",214
	;[215] 	CONST SOL_COINEAU_BG1=BG56+(FG_BLUE xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",215
	;[216] 	CONST COL_HautPont=BG59+ (FG_BLACK xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",216
	;[217] 	CONST SOL_EXPLOSION = BG60+ ( FG_BLACK xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",217
	;[218] 	CONST SOLHERBE = BG61+(FG_GREEN xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",218
	;[219] 	CONST SOL_ROCHER=BG62+(FG_BLACK xor BG_DARKGREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",219
	;[220] 	CONST CIMEARBRE=BG63+(BG_DARKGREEN xor FG_GREEN)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",220
	;[221] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",221
	;[222] 	'MAP
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",222
	;[223] 	CONST MAP_HAUTEUR = 72
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",223
	;[224] 	CONST MAP_LARGEUR = 20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",224
	;[225] 	CONST SCREEN_HAUTEUR = 12
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",225
	;[226] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",226
	;[227] 	'Fonctions
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",227
	;[228] 	DEF FN ConvTILE(Nomb)=((Nomb+6)/8)*8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",228
	;[229] 	DEF FN ConvTILE1(Nomb)=((Nomb+40-4)/8)*8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",229
	;[230] 	DEF FN ConvTILE3(Nomb)=((Nomb+40+8)/8)*8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",230
	;[231] 	DEF FN ConvTILE2(Nomb)=((Nomb-40+4)/8)*8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",231
	;[232] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",232
	;[233] '////// Variables globales /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",233
	;[234] '///// 8 bits /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",234
	;[235] 	Dim CoordX(10), CoordY(10), LifeBunker(8), Tempo(10),IABUNKER_CX(8),IABUNKER_CY(8),SensTirBunker(8),DieUnit(8),DieBunker(8),IDB(8),SpeedIA(4),VieSoldat(4)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",235
	;[236] 	Dim i,j,c,d,f,bT,e,IDIA,value,offset_y,offset_d,position_y_zone,MaxObjet,DD,DG,DH,DB,TempoTile,COFX,COFY,NBBUNKER, DistanceArme, SensIA(4),DeadU,VelocityIA(4)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",236
	;[237] 	Dim PosBunker,TirPapi,SensTIR, TempoTir, TypeArme, VitesseArme, DX_Arme, CouleurImpact,TirIA,ImpactTouch,TypeIA(4), MoveIA(4), NbIA,VisibleIA(4),TempoIA(4)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",237
	;[238] 	Dim LifePapi, NbBalle, NbGrenade, Start, NbGrenadeM, ID, MAP_HAUTEUR,MAP_LARGEUR,MortPapi, Velocity,NbRoquette,eBunker,StartGame,AB,TirSoldat
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",238
	;[239] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",239
	;[240] '///// 16 bits /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",240
	;[241] 	Dim #Miroir,#Offset,#COL_TEST,#Visible, #Couleur, #ZoomOBjectX, #ZoomOBjectY,#Miroir1,#Miroir3,#SPR3,#SPR4,#SPR5,#Miroir4,#Miroir5,#Couleur3,#Couleur4,#Couleur5
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",241
	;[242] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",242
	;[243] '///// Init Variable
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",243
	;[244] 	gosub InitVariable
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",244
	CALL label_INITVARIABLE
	;[245] 	gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",245
	CALL label_SONBLANK
	;[246] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",246
	;[247] '///// Chargement et affichage de la Zone /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",247
	;[248] 	gosub DisplayZoneScreen
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",248
	CALL label_DISPLAYZONESCREEN
	;[249] 	'gosub Main
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",249
	;[250] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",250
	;[251] '/////////////////////////////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",251
	;[252] '///// Boucle principale /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",252
	;[253] '/////////////////////////////	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",253
	;[254] 'Main: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",254
	;[255] 	while(1)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",255
T1:
	MVII #1,R0
	TSTR R0
	BEQ T2
	;[256] 		wait		
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",256
	CALL _wait
	;[257] 		STACK_CHECK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",257
	;[258] 		'Démarrage
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",258
	;[259] 		if Startgame=0 then gosub DemGame else gosub GestionPAD
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",259
	MVI var_STARTGAME,R0
	TSTR R0
	BNE T3
	CALL label_DEMGAME
	B T4
T3:
	CALL label_GESTIONPAD
T4:
	;[260] 		if AB then gosub GestionBunker
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",260
	MVI var_AB,R0
	TSTR R0
	BEQ T5
	CALL label_GESTIONBUNKER
T5:
	;[261] 		gosub GestionCollision
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",261
	CALL label_GESTIONCOLLISION
	;[262] 		gosub AnimTile
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",262
	CALL label_ANIMTILE
	;[263] 		gosub DisplayInfo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",263
	CALL label_DISPLAYINFO
	;[264] 		gosub Gestion_Sprite
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",264
	CALL label_GESTION_SPRITE
	;[265] 	wend
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",265
	B T1
T2:
	;[266] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",266
	;[267] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",267
	;[268] '///// Sous programmes /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",268
	;[269] DemGame: procedure 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",269
	; DEMGAME
label_DEMGAME:	PROC
	BEGIN
	;[270] 	DH=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",270
	MVII #1,R0
	MVO R0,var_DH
	;[271] 	if CoordY(0)<88 then StartGame=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",271
	MVI array_COORDY,R0
	CMPI #88,R0
	BGE T6
	MVII #1,R0
	MVO R0,var_STARTGAME
T6:
	;[272] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",272
	RETURN
	ENDP
	;[273] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",273
	;[274] '///// Init Variables /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",274
	;[275] InitVariable: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",275
	; INITVARIABLE
label_INITVARIABLE:	PROC
	BEGIN
	;[276] 	cls
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",276
	CALL CLRSCR
	;[277] 	position_y_zone=MAP_HAUTEUR-SCREEN_HAUTEUR
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",277
	MVII #60,R0
	MVO R0,var_POSITION_Y_ZONE
	;[278] 	NbIA=3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",278
	MVII #3,R0
	MVO R0,var_NBIA
	;[279] 	bT=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",279
	CLRR R0
	MVO R0,var_BT
	;[280] 	TirSoldat=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",280
	MVO R0,var_TIRSOLDAT
	;[281] 	DeadU=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",281
	NOP
	MVO R0,var_DEADU
	;[282] 	AB=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",282
	MVO R0,var_AB
	;[283] 	MaxObjet=3+NbIA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",283
	MVI var_NBIA,R0
	ADDI #3,R0
	MVO R0,var_MAXOBJET
	;[284] 	StartGame=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",284
	CLRR R0
	MVO R0,var_STARTGAME
	;[285] 	DD=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",285
	MVO R0,var_DD
	;[286] 	DG=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",286
	NOP
	MVO R0,var_DG
	;[287] 	DH=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",287
	MVO R0,var_DH
	;[288] 	ID=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",288
	NOP
	MVO R0,var_ID
	;[289] 	d=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",289
	MVO R0,var_D
	;[290] 	DB=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",290
	NOP
	MVO R0,var_DB
	;[291] 	c=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",291
	MVO R0,var_C
	;[292] 	TirIA=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",292
	NOP
	MVO R0,var_TIRIA
	;[293] 	Velocity=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",293
	MVO R0,var_VELOCITY
	;[294] 	eBunker=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",294
	NOP
	MVO R0,var_EBUNKER
	;[295] 	Start=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",295
	MVO R0,var_START
	;[296] 	TypeArme=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",296
	NOP
	MVO R0,var_TYPEARME
	;[297] 	DX_Arme=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",297
	MVO R0,var_DX_ARME
	;[298] 	ImpactTouch=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",298
	NOP
	MVO R0,var_IMPACTTOUCH
	;[299] 	TirPapi=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",299
	MVO R0,var_TIRPAPI
	;[300] 	VitesseArme=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",300
	NOP
	MVO R0,var_VITESSEARME
	;[301] 	SensTir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",301
	MVO R0,var_SENSTIR
	;[302] 	TempoTir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",302
	NOP
	MVO R0,var_TEMPOTIR
	;[303] 	DistanceArme=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",303
	MVO R0,var_DISTANCEARME
	;[304] 	CouleurImpact=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",304
	NOP
	MVO R0,var_COULEURIMPACT
	;[305] 	NBBUNKER=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",305
	MVO R0,var_NBBUNKER
	;[306] 	PosBunker=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",306
	NOP
	MVO R0,var_POSBUNKER
	;[307] 	offset_y=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",307
	MVO R0,var_OFFSET_Y
	;[308] 	offset_d=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",308
	NOP
	MVO R0,var_OFFSET_D
	;[309] 	SCROLL 0,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",309
	MVO R0,_scroll_x
	NOP
	MVO R0,_scroll_y
	MVO R0,_scroll_d
	;[310] 	TempoTile=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",310
	NOP
	MVO R0,var_TEMPOTILE
	;[311] 	for i=1 to 10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",311
	MVII #1,R0
	MVO R0,var_I
T7:
	;[312] 		CoordX(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",312
	CLRR R0
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[313] 		CoordY(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",313
	ADDI #(array_COORDY-array_COORDX) AND $FFFF,R3
	MVO@ R0,R3
	;[314] 		if i<9 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",314
	MVI var_I,R0
	CMPI #9,R0
	BGE T8
	;[315] 			TempoIA(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",315
	CLRR R0
	ADDI #(array_TEMPOIA-array_COORDY) AND $FFFF,R3
	MVO@ R0,R3
	;[316] 			IABUNKER_CX(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",316
	ADDI #(array_IABUNKER_CX-array_TEMPOIA) AND $FFFF,R3
	MVO@ R0,R3
	;[317] 			IABUNKER_CY(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",317
	ADDI #(array_IABUNKER_CY-array_IABUNKER_CX) AND $FFFF,R3
	MVO@ R0,R3
	;[318] 			IDB(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",318
	ADDI #(array_IDB-array_IABUNKER_CY) AND $FFFF,R3
	MVO@ R0,R3
	;[319] 			SensTirBunker(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",319
	ADDI #(array_SENSTIRBUNKER-array_IDB) AND $FFFF,R3
	MVO@ R0,R3
	;[320] 			DieBunker(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",320
	ADDI #(array_DIEBUNKER-array_SENSTIRBUNKER) AND $FFFF,R3
	MVO@ R0,R3
	;[321] 			LifeBunker(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",321
	ADDI #(array_LIFEBUNKER-array_DIEBUNKER) AND $FFFF,R3
	MVO@ R0,R3
	;[322] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",322
T8:
	;[323] 		Tempo(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",323
	CLRR R0
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[324] 	next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",324
	MVI var_I,R0
	INCR R0
	MVO R0,var_I
	CMPI #10,R0
	BLE T7
	;[325] 	gosub InitSprite
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",325
	CALL label_INITSPRITE
	;[326] 	'Init IA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",326
	;[327] 	for IDIA=1 to NbIA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",327
	MVII #1,R0
	MVO R0,var_IDIA
T9:
	;[328] 		SensIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",328
	CLRR R0
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
	;[329] 		MoveIA(iDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",329
	ADDI #(array_MOVEIA-array_SENSIA) AND $FFFF,R3
	MVO@ R0,R3
	;[330] 		VisibleIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",330
	ADDI #(array_VISIBLEIA-array_MOVEIA) AND $FFFF,R3
	MVO@ R0,R3
	;[331] 		VisibleIA(IDIA)=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",331
	MVII #1,R0
	MVO@ R0,R3
	;[332] 		VieSoldat(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",332
	CLRR R0
	ADDI #(array_VIESOLDAT-array_VISIBLEIA) AND $FFFF,R3
	MVO@ R0,R3
	;[333] 		VelocityIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",333
	ADDI #(array_VELOCITYIA-array_VIESOLDAT) AND $FFFF,R3
	MVO@ R0,R3
	;[334] 		Gosub IATri
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",334
	CALL label_IATRI
	;[335] 	next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",335
	MVI var_IDIA,R0
	INCR R0
	MVO R0,var_IDIA
	CMP var_NBIA,R0
	BLE T9
	;[336] 	'Coordonnées IA Démarrage.
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",336
	;[337] 	CoordX(3)=80
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",337
	MVII #80,R0
	MVO R0,array_COORDX+3
	;[338] 	CoordY(3)=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",338
	MVII #8,R0
	MVO R0,array_COORDY+3
	;[339] 	CoordX(4)=80
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",339
	MVII #80,R0
	MVO R0,array_COORDX+4
	;[340] 	CoordY(4)=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",340
	MVII #8,R0
	MVO R0,array_COORDY+4
	;[341] 	CoordX(5)=80
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",341
	MVII #80,R0
	MVO R0,array_COORDX+5
	;[342] 	CoordY(5)=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",342
	MVII #8,R0
	MVO R0,array_COORDY+5
	;[343] 	CoordX(0)=80
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",343
	MVII #80,R0
	MVO R0,array_COORDX
	;[344] 	CoordY(0)=104
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",344
	MVII #104,R0
	MVO R0,array_COORDY
	;[345] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",345
	;[346] 	if MortPapi=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",346
	MVI var_MORTPAPI,R0
	TSTR R0
	BNE T10
	;[347] 		MortPapi=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",347
	MVO R0,var_MORTPAPI
	;[348] 		NbGrenade=10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",348
	MVII #10,R0
	MVO R0,var_NBGRENADE
	;[349] 		NbGrenadeM=5
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",349
	MVII #5,R0
	MVO R0,var_NBGRENADEM
	;[350] 		NbRoquette=10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",350
	MVII #10,R0
	MVO R0,var_NBROQUETTE
	;[351] 		LifePapi=3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",351
	MVII #3,R0
	MVO R0,var_LIFEPAPI
	;[352] 		NbBalle=40
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",352
	MVII #40,R0
	MVO R0,var_NBBALLE
	;[353] 		gosub TraitementElementMAP
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",353
	CALL label_TRAITEMENTELEMENTMAP
	;[354] 	else
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",354
	B T11
T10:
	;[355] 		'LifePapi=LifePapi-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",355
	;[356] 		MortPapi=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",356
	CLRR R0
	MVO R0,var_MORTPAPI
	;[357] 		gosub INitSprite
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",357
	CALL label_INITSPRITE
	;[358] 		position_y_zone=MAP_HAUTEUR-SCREEN_HAUTEUR
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",358
	MVII #60,R0
	MVO R0,var_POSITION_Y_ZONE
	;[359] 		gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",359
	CALL label_SONBLANK
	;[360] 		gosub TraitementElementMAP
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",360
	CALL label_TRAITEMENTELEMENTMAP
	;[361] 		wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",361
	CALL _wait
	;[362] 		gosub DisplayZoneScreen
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",362
	CALL label_DISPLAYZONESCREEN
	;[363] 		wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",363
	CALL _wait
	;[364] 		gosub GestionPAD
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",364
	CALL label_GESTIONPAD
	;[365] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",365
T11:
	;[366] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",366
	RETURN
	ENDP
	;[367] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",367
	;[368] TestCOL: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",368
	; TESTCOL
label_TESTCOL:	PROC
	BEGIN
	;[369] 	d=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",369
	MVII #1,R0
	MVO R0,var_D
	;[370] 	if  #COL_TEST<>PASSERELLE and #COL_TEST<>SOLHERBE and #COL_TEST<>SOL_BLANK and #COL_TEST<>SOL_EXPLOSION and #COL_TEST<>COL_OmbreBatiment and #COL_TEST<>COL_HERBE1 then d=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",370
	MVI var_&COL_TEST,R0
	CMPI #6464,R0
	MVII #65535,R0
	BNE T13
	INCR R0
T13:
	MVI var_&COL_TEST,R1
	CMPI #10733,R1
	MVII #65535,R1
	BNE T14
	INCR R1
T14:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10579,R1
	MVII #65535,R1
	BNE T15
	INCR R1
T15:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10720,R1
	MVII #65535,R1
	BNE T16
	INCR R1
T16:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #2236,R1
	MVII #65535,R1
	BNE T17
	INCR R1
T17:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10629,R1
	MVII #65535,R1
	BNE T18
	INCR R1
T18:
	ANDR R1,R0
	BEQ T12
	CLRR R0
	MVO R0,var_D
T12:
	;[371] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",371
	RETURN
	ENDP
	;[372] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",372
	;[373] '///// Routines Collisions /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",373
	;[374] 'CARD_TEST: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",374
	;[375] 	'gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",375
	;[376] 	'if i=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",376
	;[377] 		'if #COL_TEST=SOLHERBE then #BEHIND=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",377
	;[378] 		'if #COL_TEST=SOL_BLANK then #BEHIND=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",378
	;[379] 		'if #COL_TEST=PONTROUTE then #BEHIND=BEHIND:RET=1:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",379
	;[380] 		'if #COL_TEST=PONTROUTE1 then #BEHIND=BEHIND:RET=1:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",380
	;[381] 		'if #COL_TEST=CARD_HautPont then #BEHIND=BEHIND:RET=1:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",381
	;[382] 		'if #COL_TEST=PASSERELLE then #BEHIND=0:RET=1:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",382
	;[383] 	'end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",383
	;[384] 	'end 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",384
	;[385] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",385
	;[386] DH_TEST: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",386
	; DH_TEST
label_DH_TEST:	PROC
	BEGIN
	;[387] 	if DH then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",387
	MVI var_DH,R0
	TSTR R0
	BEQ T19
	;[388] 		COFX=Coordx(i)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",388
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	MVO R0,var_COFX
	;[389] 		COFY=CoordY(i)-3-offset_y
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",389
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #3,R0
	SUB var_OFFSET_Y,R0
	MVO R0,var_COFY
	;[390] 		gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",390
	CALL label_PATHO
	;[391] 		'if RET then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",391
	;[392] 		gosub TestCOL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",392
	CALL label_TESTCOL
	;[393] 		if d=0 then DH=0:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",393
	MVI var_D,R0
	TSTR R0
	BNE T20
	MVO R0,var_DH
	RETURN
T20:
	;[394] 		COFX=Coordx(i)-7
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",394
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #7,R0
	MVO R0,var_COFX
	;[395] 		gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",395
	CALL label_PATHO
	;[396] 		'if RET then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",396
	;[397] 		gosub TestCOL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",397
	CALL label_TESTCOL
	;[398] 		if d=0 then DH=0:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",398
	MVI var_D,R0
	TSTR R0
	BNE T21
	MVO R0,var_DH
	RETURN
T21:
	;[399] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",399
T19:
	;[400] 	end 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",400
	RETURN
	ENDP
	;[401] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",401
	;[402] DB_TEST: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",402
	; DB_TEST
label_DB_TEST:	PROC
	BEGIN
	;[403] 	if DB then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",403
	MVI var_DB,R0
	TSTR R0
	BEQ T22
	;[404] 		COFX=Coordx(i)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",404
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	MVO R0,var_COFX
	;[405] 		COFY=CoordY(i)-offset_y
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",405
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	MVO R0,var_COFY
	;[406] 		gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",406
	CALL label_PATHO
	;[407] 		'if RET then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",407
	;[408] 		gosub TestCOL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",408
	CALL label_TESTCOL
	;[409] 		if d=0 then DB=0:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",409
	MVI var_D,R0
	TSTR R0
	BNE T23
	MVO R0,var_DB
	RETURN
T23:
	;[410] 		COFX=Coordx(i)-7
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",410
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #7,R0
	MVO R0,var_COFX
	;[411] 		gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",411
	CALL label_PATHO
	;[412] 		'if RET then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",412
	;[413] 		gosub TestCOL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",413
	CALL label_TESTCOL
	;[414] 		if d=0 then DB=0:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",414
	MVI var_D,R0
	TSTR R0
	BNE T24
	MVO R0,var_DB
	RETURN
T24:
	;[415] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",415
T22:
	;[416] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",416
	RETURN
	ENDP
	;[417] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",417
	;[418] DD_TEST: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",418
	; DD_TEST
label_DD_TEST:	PROC
	BEGIN
	;[419] 	if DD then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",419
	MVI var_DD,R0
	TSTR R0
	BEQ T25
	;[420] 		COFX=Coordx(i)-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",420
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	DECR R0
	MVO R0,var_COFX
	;[421] 		COFY=CoordY(i)-offset_y-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",421
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	SUBI #2,R0
	MVO R0,var_COFY
	;[422] 		gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",422
	CALL label_PATHO
	;[423] 		'if RET then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",423
	;[424] 		gosub TestCOL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",424
	CALL label_TESTCOL
	;[425] 		if d=0 then DD=0:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",425
	MVI var_D,R0
	TSTR R0
	BNE T26
	MVO R0,var_DD
	RETURN
T26:
	;[426] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",426
T25:
	;[427] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",427
	RETURN
	ENDP
	;[428] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",428
	;[429] DG_TEST: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",429
	; DG_TEST
label_DG_TEST:	PROC
	BEGIN
	;[430] 	if DG then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",430
	MVI var_DG,R0
	TSTR R0
	BEQ T27
	;[431] 		COFX=Coordx(i)-8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",431
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #8,R0
	MVO R0,var_COFX
	;[432] 		COFY=CoordY(i)-offset_y-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",432
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	SUBI #2,R0
	MVO R0,var_COFY
	;[433] 		gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",433
	CALL label_PATHO
	;[434] 		'if RET then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",434
	;[435] 		gosub TestCOL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",435
	CALL label_TESTCOL
	;[436] 		if d=0 then DG=0:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",436
	MVI var_D,R0
	TSTR R0
	BNE T28
	MVO R0,var_DG
	RETURN
T28:
	;[437] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",437
T27:
	;[438] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",438
	RETURN
	ENDP
	;[439] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",439
	;[440] H_B: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",440
	; H_B
label_H_B:	PROC
	BEGIN
	;[441] 	COFX=Coordx(i)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",441
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	MVO R0,var_COFX
	;[442] 	COFY=CoordY(i)-3-offset_y
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",442
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #3,R0
	SUB var_OFFSET_Y,R0
	MVO R0,var_COFY
	;[443] 	gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",443
	CALL label_PATHO
	;[444] 	gosub CollisionHARD_BALL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",444
	CALL label_COLLISIONHARD_BALL
	;[445] 	if TirPapi=99 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",445
	MVI var_TIRPAPI,R0
	CMPI #99,R0
	BNE T29
	RETURN
T29:
	;[446] 	COFX=Coordx(i)-7
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",446
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #7,R0
	MVO R0,var_COFX
	;[447] 	gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",447
	CALL label_PATHO
	;[448] 	gosub CollisionHARD_BALL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",448
	CALL label_COLLISIONHARD_BALL
	;[449] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",449
	RETURN
	ENDP
	;[450] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",450
	;[451] B_B: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",451
	; B_B
label_B_B:	PROC
	BEGIN
	;[452] 	COFX=Coordx(i)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",452
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	MVO R0,var_COFX
	;[453] 	COFY=CoordY(i)-offset_y
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",453
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	MVO R0,var_COFY
	;[454] 	gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",454
	CALL label_PATHO
	;[455] 	gosub CollisionHARD_BALL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",455
	CALL label_COLLISIONHARD_BALL
	;[456] 	if TirPapi=99 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",456
	MVI var_TIRPAPI,R0
	CMPI #99,R0
	BNE T30
	RETURN
T30:
	;[457] 	COFX=Coordx(i)-7
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",457
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #7,R0
	MVO R0,var_COFX
	;[458] 	gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",458
	CALL label_PATHO
	;[459] 	gosub CollisionHARD_BALL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",459
	CALL label_COLLISIONHARD_BALL
	;[460] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",460
	RETURN
	ENDP
	;[461] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",461
	;[462] D_B: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",462
	; D_B
label_D_B:	PROC
	BEGIN
	;[463] 	COFX=Coordx(i)-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",463
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	DECR R0
	MVO R0,var_COFX
	;[464] 	COFY=CoordY(i)-offset_y-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",464
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	SUBI #2,R0
	MVO R0,var_COFY
	;[465] 	gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",465
	CALL label_PATHO
	;[466] 	gosub CollisionHARD_BALL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",466
	CALL label_COLLISIONHARD_BALL
	;[467] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",467
	RETURN
	ENDP
	;[468] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",468
	;[469] G_B: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",469
	; G_B
label_G_B:	PROC
	BEGIN
	;[470] 	COFX=Coordx(i)-8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",470
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #8,R0
	MVO R0,var_COFX
	;[471] 	COFY=CoordY(i)-offset_y-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",471
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	SUBI #2,R0
	MVO R0,var_COFY
	;[472] 	gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",472
	CALL label_PATHO
	;[473] 	'if RET then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",473
	;[474] 	gosub CollisionHARD_BALL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",474
	CALL label_COLLISIONHARD_BALL
	;[475] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",475
	RETURN
	ENDP
	;[476] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",476
	;[477] B_B1: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",477
	; B_B1
label_B_B1:	PROC
	BEGIN
	;[478] 	COFX=Coordx(i)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",478
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	MVO R0,var_COFX
	;[479] 	COFY=CoordY(i)-offset_y
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",479
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	MVO R0,var_COFY
	;[480] 	gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",480
	CALL label_PATHO
	;[481] 	gosub CollisionHARD_BALL1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",481
	CALL label_COLLISIONHARD_BALL1
	;[482] 	if TirIA=99 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",482
	MVI var_TIRIA,R0
	CMPI #99,R0
	BNE T31
	RETURN
T31:
	;[483] 	COFX=Coordx(i)-7
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",483
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #7,R0
	MVO R0,var_COFX
	;[484] 	gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",484
	CALL label_PATHO
	;[485] 	gosub CollisionHARD_BALL1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",485
	CALL label_COLLISIONHARD_BALL1
	;[486] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",486
	RETURN
	ENDP
	;[487] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",487
	;[488] D_B1: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",488
	; D_B1
label_D_B1:	PROC
	BEGIN
	;[489] 	COFX=Coordx(i)-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",489
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	DECR R0
	MVO R0,var_COFX
	;[490] 	COFY=CoordY(i)-offset_y-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",490
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	SUBI #2,R0
	MVO R0,var_COFY
	;[491] 	gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",491
	CALL label_PATHO
	;[492] 	gosub CollisionHARD_BALL1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",492
	CALL label_COLLISIONHARD_BALL1
	;[493] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",493
	RETURN
	ENDP
	;[494] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",494
	;[495] G_B1: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",495
	; G_B1
label_G_B1:	PROC
	BEGIN
	;[496] 	COFX=Coordx(i)-8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",496
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #8,R0
	MVO R0,var_COFX
	;[497] 	COFY=CoordY(i)-offset_y-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",497
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	SUBI #2,R0
	MVO R0,var_COFY
	;[498] 	gosub PATHo
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",498
	CALL label_PATHO
	;[499] 	'if RET then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",499
	;[500] 	gosub CollisionHARD_BALL1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",500
	CALL label_COLLISIONHARD_BALL1
	;[501] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",501
	RETURN
	ENDP
	;[502] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",502
	;[503] CollideObject: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",503
	; COLLIDEOBJECT
label_COLLIDEOBJECT:	PROC
	BEGIN
	;[504] 	j=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",504
	CLRR R0
	MVO R0,var_J
	;[505] 	if  #COL_TEST<>SOLHERBE and #COL_TEST<>SOL_BLANK and #COL_TEST<>PASSERELLE and #COL_TEST<>SOL_EAU and #COL_TEST<>SOL_COINEAU_BD and #COL_TEST<>COL_OmbreBatiment and #COL_TEST<>COL_HERBE1 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",505
	MVI var_&COL_TEST,R0
	CMPI #10733,R0
	MVII #65535,R0
	BNE T33
	INCR R0
T33:
	MVI var_&COL_TEST,R1
	CMPI #10579,R1
	MVII #65535,R1
	BNE T34
	INCR R1
T34:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #6464,R1
	MVII #65535,R1
	BNE T35
	INCR R1
T35:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #15017,R1
	MVII #65535,R1
	BNE T36
	INCR R1
T36:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10649,R1
	MVII #65535,R1
	BNE T37
	INCR R1
T37:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #2236,R1
	MVII #65535,R1
	BNE T38
	INCR R1
T38:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10629,R1
	MVII #65535,R1
	BNE T39
	INCR R1
T39:
	ANDR R1,R0
	BEQ T32
	;[506] 		if #COL_TEST<>SOL_COINEAU_BG and #COL_TEST<>SOL_COINEAU_HD and #COL_TEST<>SOL_COINEAU_HG and #COL_TEST<>SOL_COINEAU_BD1 and #COL_TEST<>SOL_COINEAU_BG1 and #COL_TEST<>SOL_COINEAU_HD1 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",506
	MVI var_&COL_TEST,R0
	CMPI #10657,R0
	MVII #65535,R0
	BNE T41
	INCR R0
T41:
	MVI var_&COL_TEST,R1
	CMPI #10633,R1
	MVII #65535,R1
	BNE T42
	INCR R1
T42:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10641,R1
	MVII #65535,R1
	BNE T43
	INCR R1
T43:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10681,R1
	MVII #65535,R1
	BNE T44
	INCR R1
T44:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10689,R1
	MVII #65535,R1
	BNE T45
	INCR R1
T45:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10665,R1
	MVII #65535,R1
	BNE T46
	INCR R1
T46:
	ANDR R1,R0
	BEQ T40
	;[507] 			if #COL_TEST<>SOL_COINEAU_HG1 and #COL_TEST<>SOL_BARBELE_D and #COL_TEST<>SOL_BARBELE_M and #COL_TEST<>SOL_BARBELE_G and #COL_TEST<>SOL_EXPLOSION then j=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",507
	MVI var_&COL_TEST,R0
	CMPI #10673,R0
	MVII #65535,R0
	BNE T48
	INCR R0
T48:
	MVI var_&COL_TEST,R1
	CMPI #10448,R1
	MVII #65535,R1
	BNE T49
	INCR R1
T49:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10440,R1
	MVII #65535,R1
	BNE T50
	INCR R1
T50:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10432,R1
	MVII #65535,R1
	BNE T51
	INCR R1
T51:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #10720,R1
	MVII #65535,R1
	BNE T52
	INCR R1
T52:
	ANDR R1,R0
	BEQ T47
	MVII #1,R0
	MVO R0,var_J
T47:
	;[508] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",508
T40:
	;[509] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",509
T32:
	;[510] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",510
	RETURN
	ENDP
	;[511] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",511
	;[512] CollisionHARD_BALL: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",512
	; COLLISIONHARD_BALL
label_COLLISIONHARD_BALL:	PROC
	BEGIN
	;[513] 	gosub CollideObject
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",513
	CALL label_COLLIDEOBJECT
	;[514] 	if j=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",514
	MVI var_J,R0
	CMPI #1,R0
	BNE T53
	;[515] 		TirPapi=99
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",515
	MVII #99,R0
	MVO R0,var_TIRPAPI
	;[516] 		if TypeArme=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",516
	MVI var_TYPEARME,R0
	TSTR R0
	BNE T54
	;[517] 			if #COL_TEST=CARD_IABUNKER_M then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",517
	MVI var_&COL_TEST,R0
	CMPI #6600,R0
	BNE T55
	;[518] 				d=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",518
	MVII #1,R0
	MVO R0,var_D
	;[519] 				gosub Bunker_Hit
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",519
	CALL label_BUNKER_HIT
	;[520] 				if c=0 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",520
	MVI var_C,R0
	TSTR R0
	BNE T56
	RETURN
T56:
	;[521] 				DieBunker(c)=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",521
	MVII #1,R0
	MVII #array_DIEBUNKER,R3
	ADD var_C,R3
	MVO@ R0,R3
	;[522] 				#backtab(#offset)=CARD_ImpactBoum
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",522
	MVII #10720,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[523] 				gosub SonExplosionMAX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",523
	CALL label_SONEXPLOSIONMAX
	;[524] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",524
T55:
	;[525] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",525
T54:
	;[526] 		if TypeArme=3 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",526
	MVI var_TYPEARME,R0
	CMPI #3,R0
	BNE T57
	;[527] 			'Destruction d'arbre
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",527
	;[528] 			if #COL_TEST=SOL_ROCHER then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",528
	MVI var_&COL_TEST,R0
	CMPI #10736,R0
	BNE T58
	RETURN
T58:
	;[529] 			if #COL_TEST=CARD_IABUNKER_M then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",529
	MVI var_&COL_TEST,R0
	CMPI #6600,R0
	BNE T59
	;[530] 				d=4
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",530
	MVII #4,R0
	MVO R0,var_D
	;[531] 				gosub Bunker_Hit
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",531
	CALL label_BUNKER_HIT
	;[532] 				if c=0 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",532
	MVI var_C,R0
	TSTR R0
	BNE T60
	RETURN
T60:
	;[533] 				DieBunker(c)=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",533
	MVII #1,R0
	MVII #array_DIEBUNKER,R3
	ADD var_C,R3
	MVO@ R0,R3
	;[534] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",534
T59:
	;[535] 			#backtab(#offset)=CARD_ImpactBoum
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",535
	MVII #10720,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[536] 			if #COL_TEST=SOL_TRONC then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",536
	MVI var_&COL_TEST,R0
	CMPI #10368,R0
	BNE T61
	;[537] 				#backtab(#offset)=CARD_ImpactBoum
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",537
	MVII #10720,R0
	MVO@ R0,R3
	;[538] 				#offset=COFX/8+(COFY-8)/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",538
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	SUBI #8,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[539] 				#backtab(#offset)=CARD_HERBE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",539
	MVII #10733,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[540] 				return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",540
	RETURN
	;[541] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",541
T61:
	;[542] 			if #COL_TEST=CIMEARBRE then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",542
	MVI var_&COL_TEST,R0
	CMPI #10749,R0
	BNE T62
	;[543] 				#backtab(#offset)=CARD_ImpactBoum
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",543
	MVII #10720,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[544] 				#offset=COFX/8+(COFY+8)/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",544
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	ADDI #8,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[545] 				#backtab(#offset)=CARD_HERBE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",545
	MVII #10733,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[546] 				return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",546
	RETURN
	;[547] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",547
T62:
	;[548] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",548
T57:
	;[549] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",549
T53:
	;[550] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",550
	RETURN
	ENDP
	;[551] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",551
	;[552] CollisionHARD_BALL1: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",552
	; COLLISIONHARD_BALL1
label_COLLISIONHARD_BALL1:	PROC
	BEGIN
	;[553] 	gosub CollideObject
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",553
	CALL label_COLLIDEOBJECT
	;[554] 	if j=1 then TirIA=99
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",554
	MVI var_J,R0
	CMPI #1,R0
	BNE T63
	MVII #99,R0
	MVO R0,var_TIRIA
T63:
	;[555] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",555
	RETURN
	ENDP
	;[556] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",556
	;[557] COLP: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",557
	; COLP
label_COLP:	PROC
	BEGIN
	;[558] 	gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",558
	CALL label_SONBLANK
	;[559] 	MortPapi=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",559
	MVII #1,R0
	MVO R0,var_MORTPAPI
	;[560] 	if Coordx(0)>80 then j=1 else j=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",560
	MVI array_COORDX,R0
	CMPI #80,R0
	BLE T64
	MVII #1,R0
	MVO R0,var_J
	B T65
T64:
	MVII #2,R0
	MVO R0,var_J
T65:
	;[561] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",561
	RETURN
	ENDP
	;[562] GestionCollision: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",562
	; GESTIONCOLLISION
label_GESTIONCOLLISION:	PROC
	BEGIN
	;[563] 	if MortPapi then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",563
	MVI var_MORTPAPI,R0
	TSTR R0
	BEQ T66
	RETURN
T66:
	;[564] 	'Projectile Bunker / Papi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",564
	;[565] 	'if COL0 AND HIT_SPRITE4 THEN gosub COLP:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",565
	;[566] 	'if COL1 AND HIT_SPRITE4 THEN gosub COLP:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",566
	;[567] 	'if COL2 AND HIT_SPRITE4 THEN gosub COLP:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",567
	;[568] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",568
	;[569] 	'IA / PAPi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",569
	;[570] 	'if (COL0+COL1+COL2) AND (HIT_SPRITE5+HIT_SPRITE6+HIT_SPRITE7) THEN gosub COLP:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",570
	;[571] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",571
	;[572] 	'Armes / Decor
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",572
	;[573] 	if TirPapi then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",573
	MVI var_TIRPAPI,R0
	TSTR R0
	BEQ T67
	;[574] 		i=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",574
	MVII #1,R0
	MVO R0,var_I
	;[575] 		if TypeArme=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",575
	MVI var_TYPEARME,R0
	TSTR R0
	BNE T68
	;[576] 			if TirPapi=99 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",576
	MVI var_TIRPAPI,R0
	CMPI #99,R0
	BNE T69
	RETURN
T69:
	;[577] 			gosub GestionCOL_Papi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",577
	CALL label_GESTIONCOL_PAPI
	;[578] 		elseif TypeArme=3 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",578
	B T70
T68:
	MVI var_TYPEARME,R0
	CMPI #3,R0
	BNE T71
	;[579] 			if TirPapi=99 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",579
	MVI var_TIRPAPI,R0
	CMPI #99,R0
	BNE T72
	RETURN
T72:
	;[580] 			gosub GestionCOL_Papi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",580
	CALL label_GESTIONCOL_PAPI
	;[581] 		elseif TypeArme=4 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",581
	B T70
T71:
	MVI var_TYPEARME,R0
	CMPI #4,R0
	BNE T73
	;[582] 			return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",582
	RETURN
	;[583] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",583
T70:
T73:
	;[584] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",584
T67:
	;[585] 	'Tir Bunker
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",585
	;[586] 	if TirIA then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",586
	MVI var_TIRIA,R0
	TSTR R0
	BEQ T74
	;[587] 		i=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",587
	MVII #2,R0
	MVO R0,var_I
	;[588] 		'Tir couvert
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",588
	;[589] 		Tempo(2)=Tempo(2)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",589
	MVI array_TEMPO+2,R0
	INCR R0
	MVO R0,array_TEMPO+2
	;[590] 		if Tempo(2)>12 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",590
	MVI array_TEMPO+2,R0
	CMPI #12,R0
	BLE T75
	;[591] 			Tempo(2)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",591
	CLRR R0
	MVO R0,array_TEMPO+2
	;[592] 			if SensTirBunker(ID)=0 then gosub B_B1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",592
	MVII #array_SENSTIRBUNKER,R3
	ADD var_ID,R3
	MVI@ R3,R0
	TSTR R0
	BNE T76
	CALL label_B_B1
T76:
	;[593] 			if SensTirBunker(ID)=2 then gosub D_B1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",593
	MVII #array_SENSTIRBUNKER,R3
	ADD var_ID,R3
	MVI@ R3,R0
	CMPI #2,R0
	BNE T77
	CALL label_D_B1
T77:
	;[594] 			if SensTirBunker(ID)=1 then gosub G_B1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",594
	MVII #array_SENSTIRBUNKER,R3
	ADD var_ID,R3
	MVI@ R3,R0
	CMPI #1,R0
	BNE T78
	CALL label_G_B1
T78:
	;[595] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",595
T75:
	;[596] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",596
T74:
	;[597] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",597
	RETURN
	ENDP
	;[598] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",598
	;[599] GestionCOL_Papi: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",599
	; GESTIONCOL_PAPI
label_GESTIONCOL_PAPI:	PROC
	BEGIN
	;[600] 	if SensTir=1 then Gosub H_B
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",600
	MVI var_SENSTIR,R0
	CMPI #1,R0
	BNE T79
	CALL label_H_B
T79:
	;[601] 	if SensTir=2 then gosub B_B
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",601
	MVI var_SENSTIR,R0
	CMPI #2,R0
	BNE T80
	CALL label_B_B
T80:
	;[602] 	if SensTIR=3 then gosub D_B
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",602
	MVI var_SENSTIR,R0
	CMPI #3,R0
	BNE T81
	CALL label_D_B
T81:
	;[603] 	if SensTIR=4 then gosub G_B
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",603
	MVI var_SENSTIR,R0
	CMPI #4,R0
	BNE T82
	CALL label_G_B
T82:
	;[604] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",604
	RETURN
	ENDP
	;[605] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",605
	;[606] '///// Routine Animation Tile /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",606
	;[607] AnimTile: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",607
	; ANIMTILE
label_ANIMTILE:	PROC
	BEGIN
	;[608] 	TempoTile=TempoTile+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",608
	MVI var_TEMPOTILE,R0
	INCR R0
	MVO R0,var_TEMPOTILE
	;[609] 	if TempoTile=45 then TempoTile=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",609
	MVI var_TEMPOTILE,R0
	CMPI #45,R0
	BNE T83
	CLRR R0
	MVO R0,var_TEMPOTILE
T83:
	;[610] 	'EAU / Herbe
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",610
	;[611] 	if TempoTile=5 then DEFINE DEF21,1,EAU_1:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",611
	MVI var_TEMPOTILE,R0
	CMPI #5,R0
	BNE T84
	MVII #21,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_EAU_1,R0
	MVO R0,_gram_bitmap
	RETURN
T84:
	;[612] 	if TempoTile=10 then DEFINE DEF21,1,EAU_2:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",612
	MVI var_TEMPOTILE,R0
	CMPI #10,R0
	BNE T85
	MVII #21,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_EAU_2,R0
	MVO R0,_gram_bitmap
	RETURN
T85:
	;[613] 	if TempoTile=6 then DEFINE DEF42,1,BLANK:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",613
	MVI var_TEMPOTILE,R0
	CMPI #6,R0
	BNE T86
	MVII #42,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BLANK,R0
	MVO R0,_gram_bitmap
	RETURN
T86:
	;[614] 	if TempoTile=15 then DEFINE DEF21,1,EAU_3:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",614
	MVI var_TEMPOTILE,R0
	CMPI #15,R0
	BNE T87
	MVII #21,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_EAU_3,R0
	MVO R0,_gram_bitmap
	RETURN
T87:
	;[615] 	if TempoTile=20 then DEFINE DEF21,1,EAU_4:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",615
	MVI var_TEMPOTILE,R0
	CMPI #20,R0
	BNE T88
	MVII #21,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_EAU_4,R0
	MVO R0,_gram_bitmap
	RETURN
T88:
	;[616] 	if TempoTile=16 then DEFINE DEF42,1,BLANK1:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",616
	MVI var_TEMPOTILE,R0
	CMPI #16,R0
	BNE T89
	MVII #42,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BLANK1,R0
	MVO R0,_gram_bitmap
	RETURN
T89:
	;[617] 	if TempoTile=25 then DEFINE DEF21,1,EAU_5:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",617
	MVI var_TEMPOTILE,R0
	CMPI #25,R0
	BNE T90
	MVII #21,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_EAU_5,R0
	MVO R0,_gram_bitmap
	RETURN
T90:
	;[618] 	if TempoTile=30 then DEFINE DEF21,1,EAU_6:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",618
	MVI var_TEMPOTILE,R0
	CMPI #30,R0
	BNE T91
	MVII #21,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_EAU_6,R0
	MVO R0,_gram_bitmap
	RETURN
T91:
	;[619] 	if TempoTile=26 then DEFINE DEF42,1,BLANK2:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",619
	MVI var_TEMPOTILE,R0
	CMPI #26,R0
	BNE T92
	MVII #42,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BLANK2,R0
	MVO R0,_gram_bitmap
	RETURN
T92:
	;[620] 	if TempoTile=35 then DEFINE DEF21,1,EAU_7:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",620
	MVI var_TEMPOTILE,R0
	CMPI #35,R0
	BNE T93
	MVII #21,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_EAU_7,R0
	MVO R0,_gram_bitmap
	RETURN
T93:
	;[621] 	if TempoTile=40 then DEFINE DEF21,1,EAU_8:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",621
	MVI var_TEMPOTILE,R0
	CMPI #40,R0
	BNE T94
	MVII #21,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_EAU_8,R0
	MVO R0,_gram_bitmap
	RETURN
T94:
	;[622] 	if TempoTile=36 then DEFINE DEF42,1,BLANK1:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",622
	MVI var_TEMPOTILE,R0
	CMPI #36,R0
	BNE T95
	MVII #42,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BLANK1,R0
	MVO R0,_gram_bitmap
	RETURN
T95:
	;[623] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",623
	RETURN
	ENDP
	;[624] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",624
	;[625] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",625
	;[626] '///// Routines animation Sprite /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",626
	;[627] GestionANIMATION: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",627
	; GESTIONANIMATION
label_GESTIONANIMATION:	PROC
	BEGIN
	;[628] 	Tempo(0)=Tempo(0)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",628
	MVI array_TEMPO,R0
	INCR R0
	MVO R0,array_TEMPO
	;[629] 	if Tempo(0)>4 then Tempo(0)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",629
	MVI array_TEMPO,R0
	CMPI #4,R0
	BLE T96
	CLRR R0
	MVO R0,array_TEMPO
T96:
	;[630] 	'Papi 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",630
	;[631] 	i=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",631
	CLRR R0
	MVO R0,var_I
	;[632] 	if MortPapi then gosub GestionDirPapi_B:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",632
	MVI var_MORTPAPI,R0
	TSTR R0
	BEQ T97
	CALL label_GESTIONDIRPAPI_B
	RETURN
T97:
	;[633] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",633
	;[634] 	if DH then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",634
	MVI var_DH,R0
	TSTR R0
	BEQ T98
	;[635] 		Sound 1,7000,7
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",635
	MVII #7000,R0
	MVO R0,497
	SWAP R0
	MVO R0,501
	MVII #7,R0
	MVO R0,508
	;[636] 		#miroir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",636
	CLRR R0
	MVO R0,var_&MIROIR
	;[637] 		SensTir=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",637
	MVII #1,R0
	MVO R0,var_SENSTIR
	;[638] 		if Tempo(i)=1 then DEFINE DEF00, 3, Papi_HAUT_0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",638
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #1,R0
	BNE T99
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_HAUT_0,R0
	MVO R0,_gram_bitmap
T99:
	;[639] 		if Tempo(i)=2 then DEFINE DEF00, 3, Papi_HAUT_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",639
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #2,R0
	BNE T100
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_HAUT_1,R0
	MVO R0,_gram_bitmap
T100:
	;[640] 		if Tempo(i)=3 then DEFINE DEF00, 3, Papi_HAUT_0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",640
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #3,R0
	BNE T101
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_HAUT_0,R0
	MVO R0,_gram_bitmap
T101:
	;[641] 		if Tempo(i)=4 then DEFINE DEF00, 3, Papi_HAUT_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",641
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #4,R0
	BNE T102
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_HAUT_2,R0
	MVO R0,_gram_bitmap
T102:
	;[642] 	elseif DB then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",642
	B T103
T98:
	MVI var_DB,R0
	TSTR R0
	BEQ T104
	;[643] 		Sound 1,7000,7
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",643
	MVII #7000,R0
	MVO R0,497
	SWAP R0
	MVO R0,501
	MVII #7,R0
	MVO R0,508
	;[644] 		#miroir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",644
	CLRR R0
	MVO R0,var_&MIROIR
	;[645] 		SensTir=2 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",645
	MVII #2,R0
	MVO R0,var_SENSTIR
	;[646] 		gosub GestionDirPapi_B
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",646
	CALL label_GESTIONDIRPAPI_B
	;[647] 	elseif DD then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",647
	B T103
T104:
	MVI var_DD,R0
	TSTR R0
	BEQ T105
	;[648] 		Sound 1,7000,7
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",648
	MVII #7000,R0
	MVO R0,497
	SWAP R0
	MVO R0,501
	MVII #7,R0
	MVO R0,508
	;[649] 		#miroir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",649
	CLRR R0
	MVO R0,var_&MIROIR
	;[650] 		SensTir=3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",650
	MVII #3,R0
	MVO R0,var_SENSTIR
	;[651] 		Gosub GestionDirPapi_D
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",651
	CALL label_GESTIONDIRPAPI_D
	;[652] 	elseif DG then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",652
	B T103
T105:
	MVI var_DG,R0
	TSTR R0
	BEQ T106
	;[653] 		Sound 1,7000,7
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",653
	MVII #7000,R0
	MVO R0,497
	SWAP R0
	MVO R0,501
	MVII #7,R0
	MVO R0,508
	;[654] 		#Miroir=FLIPX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",654
	MVII #1024,R0
	MVO R0,var_&MIROIR
	;[655] 		SensTir=4
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",655
	MVII #4,R0
	MVO R0,var_SENSTIR
	;[656] 		Gosub GestionDirPapi_D
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",656
	CALL label_GESTIONDIRPAPI_D
	;[657] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",657
T103:
T106:
	;[658] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",658
	RETURN
	ENDP
	;[659] GestionDirPapi_D: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",659
	; GESTIONDIRPAPI_D
label_GESTIONDIRPAPI_D:	PROC
	BEGIN
	;[660] 		if Tempo(i)=1 then DEFINE DEF00, 3, Papi_DROITE_0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",660
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #1,R0
	BNE T107
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_DROITE_0,R0
	MVO R0,_gram_bitmap
T107:
	;[661] 		if Tempo(i)=2 then DEFINE DEF00, 3, Papi_DROITE_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",661
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #2,R0
	BNE T108
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_DROITE_1,R0
	MVO R0,_gram_bitmap
T108:
	;[662] 		if Tempo(i)=3 then DEFINE DEF00, 3, Papi_DROITE_0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",662
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #3,R0
	BNE T109
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_DROITE_0,R0
	MVO R0,_gram_bitmap
T109:
	;[663] 		if Tempo(i)=4 then DEFINE DEF00, 3, Papi_DROITE_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",663
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #4,R0
	BNE T110
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_DROITE_2,R0
	MVO R0,_gram_bitmap
T110:
	;[664] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",664
	RETURN
	ENDP
	;[665] GestionDirPapi_B: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",665
	; GESTIONDIRPAPI_B
label_GESTIONDIRPAPI_B:	PROC
	BEGIN
	;[666] 	if Tempo(i)=1 then DEFINE DEF00, 3, Papi_BAS_0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",666
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #1,R0
	BNE T111
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_BAS_0,R0
	MVO R0,_gram_bitmap
T111:
	;[667] 	if Tempo(i)=2 then DEFINE DEF00, 3, Papi_BAS_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",667
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #2,R0
	BNE T112
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_BAS_1,R0
	MVO R0,_gram_bitmap
T112:
	;[668] 	if Tempo(i)=3 then DEFINE DEF00, 3, Papi_BAS_0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",668
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #3,R0
	BNE T113
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_BAS_0,R0
	MVO R0,_gram_bitmap
T113:
	;[669] 	if Tempo(i)=4 then DEFINE DEF00, 3, Papi_BAS_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",669
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #4,R0
	BNE T114
	CLRR R0
	MVO R0,_gram_target
	MVII #3,R0
	MVO R0,_gram_total
	MVII #label_PAPI_BAS_2,R0
	MVO R0,_gram_bitmap
T114:
	;[670] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",670
	RETURN
	ENDP
	;[671] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",671
	;[672] '///// Gestion Sprite /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",672
	;[673] Gestion_Sprite: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",673
	; GESTION_SPRITE
label_GESTION_SPRITE:	PROC
	BEGIN
	;[674] 	'Déplacement 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",674
	;[675] 	Tempo(1)=Tempo(1)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",675
	MVI array_TEMPO+1,R0
	INCR R0
	MVO R0,array_TEMPO+1
	;[676] 	if Tempo(1)>1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",676
	MVI array_TEMPO+1,R0
	CMPI #1,R0
	BLE T115
	;[677] 		Gosub GestionANIMATION
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",677
	CALL label_GESTIONANIMATION
	;[678] 		Tempo(1)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",678
	CLRR R0
	MVO R0,array_TEMPO+1
	;[679] 		'Collision Papi / Decor
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",679
	;[680] 		if CONT then i=0:gosub DH_TEST:gosub DB_TEST:gosub DD_TEST:gosub DG_TEST
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",680
	MVI 510,R0
	XOR 511,R0
	BEQ T116
	CLRR R0
	MVO R0,var_I
	CALL label_DH_TEST
	CALL label_DB_TEST
	CALL label_DD_TEST
	CALL label_DG_TEST
T116:
	;[681] 		if DH then coordy(0)=coordy(0)-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",681
	MVI var_DH,R0
	TSTR R0
	BEQ T117
	MVI array_COORDY,R0
	DECR R0
	MVO R0,array_COORDY
T117:
	;[682] 		if DB then coordy(0)=coordy(0)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",682
	MVI var_DB,R0
	TSTR R0
	BEQ T118
	MVI array_COORDY,R0
	INCR R0
	MVO R0,array_COORDY
T118:
	;[683] 		if DD then coordx(0)=coordx(0)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",683
	MVI var_DD,R0
	TSTR R0
	BEQ T119
	MVI array_COORDX,R0
	INCR R0
	MVO R0,array_COORDX
T119:
	;[684] 		if DG then coordx(0)=coordx(0)-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",684
	MVI var_DG,R0
	TSTR R0
	BEQ T120
	MVI array_COORDX,R0
	DECR R0
	MVO R0,array_COORDX
T120:
	;[685] 		gosub ScrollingMAP
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",685
	CALL label_SCROLLINGMAP
	;[686] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",686
T115:
	;[687] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",687
	RETURN
	ENDP
	;[688] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",688
	;[689] '///// Routines PAD /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",689
	;[690] InitDirection: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",690
	; INITDIRECTION
label_INITDIRECTION:	PROC
	BEGIN
	;[691] 	DH=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",691
	CLRR R0
	MVO R0,var_DH
	;[692] 	DG=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",692
	MVO R0,var_DG
	;[693] 	DB=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",693
	NOP
	MVO R0,var_DB
	;[694] 	DD=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",694
	MVO R0,var_DD
	;[695] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",695
	RETURN
	ENDP
	;[696] GestionPAD: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",696
	; GESTIONPAD
label_GESTIONPAD:	PROC
	BEGIN
	;[697] 	'Info Item en cours ? On quitte !
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",697
	;[698] 	if tempo(10) then gosub InitDirection:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",698
	MVI array_TEMPO+10,R0
	TSTR R0
	BEQ T121
	CALL label_INITDIRECTION
	RETURN
T121:
	;[699] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",699
	;[700] 	'Si Couteau alors on quitte
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",700
	;[701] 	if TirPapi and TypeArme=4 then gosub InitDirection:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",701
	MVI var_TIRPAPI,R0
	MVI var_TYPEARME,R1
	CMPI #4,R1
	MVII #65535,R1
	BEQ T123
	INCR R1
T123:
	ANDR R1,R0
	BEQ T122
	CALL label_INITDIRECTION
	RETURN
T122:
	;[702] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",702
	;[703] 	'Mort Papi ?
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",703
	;[704] 	if MortPapi then gosub InitDirection:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",704
	MVI var_MORTPAPI,R0
	TSTR R0
	BEQ T124
	CALL label_INITDIRECTION
	RETURN
T124:
	;[705] 	'Controle Joueur
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",705
	;[706] 	'if CONT=0 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",706
	;[707] 		'if DH then DEFINE DEF00, 3, Papi_HAUT_0:#Miroir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",707
	;[708] 		'if DB then DEFINE DEF00, 3, Papi_BAS_0:#Miroir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",708
	;[709] 		'if DD then DEFINE DEF00, 3, Papi_DROITE_0:#Miroir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",709
	;[710] 		'if DG then DEFINE DEF00, 3, Papi_DROITE_0:#Miroir=FLIPX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",710
	;[711] 	'end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",711
	;[712] 	'Déplacement
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",712
	;[713] 	if CONT.UP then DH=1 else DH=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",713
	MVI 510,R0
	XOR 511,R0
	ANDI #4,R0
	BEQ T125
	MVII #1,R0
	MVO R0,var_DH
	B T126
T125:
	CLRR R0
	MVO R0,var_DH
T126:
	;[714] 	if CONT.DOWN then DB=1 else DB=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",714
	MVI 510,R0
	XOR 511,R0
	ANDI #1,R0
	BEQ T127
	MVII #1,R0
	MVO R0,var_DB
	B T128
T127:
	CLRR R0
	MVO R0,var_DB
T128:
	;[715] 	if CONT.LEFT then DG=1 else DG=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",715
	MVI 510,R0
	XOR 511,R0
	ANDI #8,R0
	BEQ T129
	MVII #1,R0
	MVO R0,var_DG
	B T130
T129:
	CLRR R0
	MVO R0,var_DG
T130:
	;[716] 	if CONT.RIGHT then DD=1 else DD=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",716
	MVI 510,R0
	XOR 511,R0
	ANDI #2,R0
	BEQ T131
	MVII #1,R0
	MVO R0,var_DD
	B T132
T131:
	CLRR R0
	MVO R0,var_DD
T132:
	;[717] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",717
	;[718] 	'Changement d'armes / Ecran Pause Choix arme et info
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",718
	;[719] 	if CONT.B2 and Start=99 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",719
	MVI 510,R0
	XOR 511,R0
	ANDI #224,R0
	CMPI #192,R0
	MVII #65535,R0
	BEQ T134
	INCR R0
T134:
	MVI var_START,R1
	CMPI #99,R1
	MVII #65535,R1
	BEQ T135
	INCR R1
T135:
	ANDR R1,R0
	BEQ T133
	;[720] 		TypeArme=TypeArme+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",720
	MVI var_TYPEARME,R0
	INCR R0
	MVO R0,var_TYPEARME
	;[721] 		'gosub SonValidation
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",721
	;[722] 		if TypeArme>4 then TypeArme=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",722
	MVI var_TYPEARME,R0
	CMPI #4,R0
	BLE T136
	CLRR R0
	MVO R0,var_TYPEARME
T136:
	;[723] 		'Pistolet
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",723
	;[724] 		if TypeArme=0 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",724
	MVI var_TYPEARME,R0
	TSTR R0
	BNE T137
	;[725] 			Start=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",725
	MVII #1,R0
	MVO R0,var_START
	;[726] 			Tempo(10)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",726
	CLRR R0
	MVO R0,array_TEMPO+10
	;[727] 			wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",727
	CALL _wait
	;[728] 			DEFINE 37,1,PistoletTile
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",728
	MVII #37,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_PISTOLETTILE,R0
	MVO R0,_gram_bitmap
	;[729] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",729
	CALL _wait
	;[730] 		'Grenade
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",730
	;[731] 		elseIf TypeArme=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",731
	B T138
T137:
	MVI var_TYPEARME,R0
	CMPI #1,R0
	BNE T139
	;[732] 			Start=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",732
	MVII #2,R0
	MVO R0,var_START
	;[733] 			Tempo(10)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",733
	CLRR R0
	MVO R0,array_TEMPO+10
	;[734] 			wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",734
	CALL _wait
	;[735] 			DEFINE 37,1,GrenadeTile
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",735
	MVII #37,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GRENADETILE,R0
	MVO R0,_gram_bitmap
	;[736] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",736
	CALL _wait
	;[737] 			DEFINE DEF47,1,Viseur
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",737
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_VISEUR,R0
	MVO R0,_gram_bitmap
	;[738] 			#ZoomOBjectY=ZOOMY2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",738
	MVII #256,R0
	MVO R0,var_&ZOOMOBJECTY
	;[739] 			#ZoomOBjectX=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",739
	CLRR R0
	MVO R0,var_&ZOOMOBJECTX
	;[740] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",740
	CALL _wait
	;[741] 		'Maxi Grenade
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",741
	;[742] 		elseIf TypeArme=2 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",742
	B T138
T139:
	MVI var_TYPEARME,R0
	CMPI #2,R0
	BNE T140
	;[743] 			Start=3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",743
	MVII #3,R0
	MVO R0,var_START
	;[744] 			Tempo(10)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",744
	CLRR R0
	MVO R0,array_TEMPO+10
	;[745] 			wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",745
	CALL _wait
	;[746] 			DEFINE 37,1,GrenadeTile
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",746
	MVII #37,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GRENADETILE,R0
	MVO R0,_gram_bitmap
	;[747] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",747
	CALL _wait
	;[748] 		'Bazooka
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",748
	;[749] 		elseIf TypeArme=3 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",749
	B T138
T140:
	MVI var_TYPEARME,R0
	CMPI #3,R0
	BNE T141
	;[750] 			Start=4
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",750
	MVII #4,R0
	MVO R0,var_START
	;[751] 			Tempo(10)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",751
	CLRR R0
	MVO R0,array_TEMPO+10
	;[752] 			wait	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",752
	CALL _wait
	;[753] 			DEFINE 37,1,BazookaTile
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",753
	MVII #37,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BAZOOKATILE,R0
	MVO R0,_gram_bitmap
	;[754] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",754
	CALL _wait
	;[755] 		'Couteau
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",755
	;[756] 		elseif TypeArme=4 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",756
	B T138
T141:
	MVI var_TYPEARME,R0
	CMPI #4,R0
	BNE T142
	;[757] 			Start=5
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",757
	MVII #5,R0
	MVO R0,var_START
	;[758] 			tempo(10)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",758
	CLRR R0
	MVO R0,array_TEMPO+10
	;[759] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",759
	CALL _wait
	;[760] 			DEFINE 37,1,Couteau
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",760
	MVII #37,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAU,R0
	MVO R0,_gram_bitmap
	;[761] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",761
	CALL _wait
	;[762] 			DEFINE DEF47,1,CouteauH_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",762
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUH_1,R0
	MVO R0,_gram_bitmap
	;[763] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",763
	CALL _wait
	;[764] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",764
T138:
T142:
	;[765] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",765
T133:
	;[766] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",766
	;[767] 	'Conditions sécurité
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",767
	;[768] 	if DH and DB then DH=0:DB=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",768
	MVI var_DH,R0
	AND var_DB,R0
	BEQ T143
	CLRR R0
	MVO R0,var_DH
	MVO R0,var_DB
T143:
	;[769] 	if DG and DD then DG=0:DD=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",769
	MVI var_DG,R0
	AND var_DD,R0
	BEQ T144
	CLRR R0
	MVO R0,var_DG
	MVO R0,var_DD
T144:
	;[770] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",770
	;[771] 	'Tir à l'arrêt
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",771
	;[772] 	'if DH=0 and DD=0 and DG=0 and DB=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",772
	;[773] 	'Tir
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",773
	;[774] 	if CONT.B0 and TirPapi=0 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",774
	MVI 510,R0
	XOR 511,R0
	ANDI #224,R0
	CMPI #160,R0
	MVII #65535,R0
	BEQ T146
	INCR R0
T146:
	MVI var_TIRPAPI,R1
	TSTR R1
	MVII #65535,R1
	BEQ T147
	INCR R1
T147:
	ANDR R1,R0
	BEQ T145
	;[775] 		if TypeArme=0 and NbBalle then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",775
	MVI var_TYPEARME,R0
	TSTR R0
	MVII #65535,R0
	BEQ T149
	INCR R0
T149:
	AND var_NBBALLE,R0
	BEQ T148
	;[776] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",776
	CALL _wait
	;[777] 			Gosub InitArmes
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",777
	CALL label_INITARMES
	;[778] 			DEFINE DEF47,1,Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",778
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
	;[779] 			VitesseArme=5
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",779
	MVII #5,R0
	MVO R0,var_VITESSEARME
	;[780] 			DistanceArme=70
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",780
	MVII #70,R0
	MVO R0,var_DISTANCEARME
	;[781] 			#Couleur=SPR_YELLOW
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",781
	MVII #6,R0
	MVO R0,var_&COULEUR
	;[782] 			#ZoomOBjectY=ZOOMY2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",782
	MVII #256,R0
	MVO R0,var_&ZOOMOBJECTY
	;[783] 			#ZoomOBjectX=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",783
	CLRR R0
	MVO R0,var_&ZOOMOBJECTX
	;[784] 			NbBalle=NbBalle-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",784
	MVI var_NBBALLE,R0
	DECR R0
	MVO R0,var_NBBALLE
	;[785] 			Gosub Sontir
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",785
	CALL label_SONTIR
	;[786] 			if NbBalle<1 then NbBalle=0:Start=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",786
	MVI var_NBBALLE,R0
	CMPI #1,R0
	BGE T150
	CLRR R0
	MVO R0,var_NBBALLE
	MVII #1,R0
	MVO R0,var_START
T150:
	;[787] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",787
	CALL _wait
	;[788] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",788
T148:
	;[789] 		'On utilise les grenades/Bazooka uniquement à l'arrêt !
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",789
	;[790] 		if DH=0 and DD=0 and DG=0 and DB=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",790
	MVI var_DH,R0
	TSTR R0
	MVII #65535,R0
	BEQ T152
	INCR R0
T152:
	MVI var_DD,R1
	TSTR R1
	MVII #65535,R1
	BEQ T153
	INCR R1
T153:
	ANDR R1,R0
	MVI var_DG,R1
	TSTR R1
	MVII #65535,R1
	BEQ T154
	INCR R1
T154:
	ANDR R1,R0
	MVI var_DB,R1
	TSTR R1
	MVII #65535,R1
	BEQ T155
	INCR R1
T155:
	ANDR R1,R0
	BEQ T151
	;[791] 			if TypeArme=1 and NbGrenade then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",791
	MVI var_TYPEARME,R0
	CMPI #1,R0
	MVII #65535,R0
	BEQ T157
	INCR R0
T157:
	AND var_NBGRENADE,R0
	BEQ T156
	;[792] 				wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",792
	CALL _wait
	;[793] 				Gosub InitArmes
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",793
	CALL label_INITARMES
	;[794] 				Gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",794
	CALL label_SONBLANK
	;[795] 				DEFINE DEF47,1,Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",795
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
	;[796] 				VitesseArme=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",796
	MVII #2,R0
	MVO R0,var_VITESSEARME
	;[797] 				DistanceArme=40
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",797
	MVII #40,R0
	MVO R0,var_DISTANCEARME
	;[798] 				#Couleur=SPR_BLACK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",798
	CLRR R0
	MVO R0,var_&COULEUR
	;[799] 				#ZoomOBjectY=ZOOMY2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",799
	MVII #256,R0
	MVO R0,var_&ZOOMOBJECTY
	;[800] 				#ZoomOBjectX=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",800
	CLRR R0
	MVO R0,var_&ZOOMOBJECTX
	;[801] 				NbGrenade=NbGrenade-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",801
	MVI var_NBGRENADE,R0
	DECR R0
	MVO R0,var_NBGRENADE
	;[802] 				if NbGrenade<1 then NbGrenade=0:Start=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",802
	MVI var_NBGRENADE,R0
	CMPI #1,R0
	BGE T158
	CLRR R0
	MVO R0,var_NBGRENADE
	MVII #2,R0
	MVO R0,var_START
T158:
	;[803] 				wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",803
	CALL _wait
	;[804] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",804
T156:
	;[805] 			if TypeArme=2 and NbGrenadeM then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",805
	MVI var_TYPEARME,R0
	CMPI #2,R0
	MVII #65535,R0
	BEQ T160
	INCR R0
T160:
	AND var_NBGRENADEM,R0
	BEQ T159
	;[806] 				wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",806
	CALL _wait
	;[807] 				Gosub InitArmes
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",807
	CALL label_INITARMES
	;[808] 				Gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",808
	CALL label_SONBLANK
	;[809] 				DEFINE DEF47,1,Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",809
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
	;[810] 				VitesseArme=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",810
	MVII #2,R0
	MVO R0,var_VITESSEARME
	;[811] 				DistanceArme=40
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",811
	MVII #40,R0
	MVO R0,var_DISTANCEARME
	;[812] 				#Couleur=SPR_RED
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",812
	MVII #2,R0
	MVO R0,var_&COULEUR
	;[813] 				#ZoomOBjectY=ZOOMY2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",813
	MVII #256,R0
	MVO R0,var_&ZOOMOBJECTY
	;[814] 				#ZoomOBjectX=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",814
	CLRR R0
	MVO R0,var_&ZOOMOBJECTX
	;[815] 				NbGrenadeM=NbGrenadeM-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",815
	MVI var_NBGRENADEM,R0
	DECR R0
	MVO R0,var_NBGRENADEM
	;[816] 				if NbGrenadeM<1 then NbGrenadeM=0:Start=3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",816
	MVI var_NBGRENADEM,R0
	CMPI #1,R0
	BGE T161
	CLRR R0
	MVO R0,var_NBGRENADEM
	MVII #3,R0
	MVO R0,var_START
T161:
	;[817] 				wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",817
	CALL _wait
	;[818] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",818
T159:
	;[819] 			if TypeArme=3 and NbRoquette then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",819
	MVI var_TYPEARME,R0
	CMPI #3,R0
	MVII #65535,R0
	BEQ T163
	INCR R0
T163:
	AND var_NBROQUETTE,R0
	BEQ T162
	;[820] 				wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",820
	CALL _wait
	;[821] 				Gosub InitArmes
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",821
	CALL label_INITARMES
	;[822] 				Gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",822
	CALL label_SONBLANK
	;[823] 				Gosub SonBazooka
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",823
	CALL label_SONBAZOOKA
	;[824] 				DEFINE DEF47,1,Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",824
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
	;[825] 				VitesseArme=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",825
	MVII #2,R0
	MVO R0,var_VITESSEARME
	;[826] 				DistanceArme=90
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",826
	MVII #90,R0
	MVO R0,var_DISTANCEARME
	;[827] 				#Couleur=SPR_RED
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",827
	MVII #2,R0
	MVO R0,var_&COULEUR
	;[828] 				#ZoomOBjectY=ZOOMY2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",828
	MVII #256,R0
	MVO R0,var_&ZOOMOBJECTY
	;[829] 				#ZoomOBjectX=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",829
	CLRR R0
	MVO R0,var_&ZOOMOBJECTX
	;[830] 				NbRoquette=NbRoquette-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",830
	MVI var_NBROQUETTE,R0
	DECR R0
	MVO R0,var_NBROQUETTE
	;[831] 				if NbRoquette<1 then NbRoquette=0:Start=4
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",831
	MVI var_NBROQUETTE,R0
	CMPI #1,R0
	BGE T164
	CLRR R0
	MVO R0,var_NBROQUETTE
	MVII #4,R0
	MVO R0,var_START
T164:
	;[832] 				wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",832
	CALL _wait
	;[833] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",833
T162:
	;[834] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",834
T151:
	;[835] 		if TypeArme=4 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",835
	MVI var_TYPEARME,R0
	CMPI #4,R0
	BNE T165
	;[836] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",836
	CALL _wait
	;[837] 			gosub InitArmes
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",837
	CALL label_INITARMES
	;[838] 			gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",838
	CALL label_SONBLANK
	;[839] 			gosub SonCouteau
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",839
	CALL label_SONCOUTEAU
	;[840] 			DEFINE DEF47,1,CouteauH_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",840
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUH_1,R0
	MVO R0,_gram_bitmap
	;[841] 			VitesseArme=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",841
	CLRR R0
	MVO R0,var_VITESSEARME
	;[842] 			DistanceArme=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",842
	MVO R0,var_DISTANCEARME
	;[843] 			#Couleur=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",843
	MVII #7,R0
	MVO R0,var_&COULEUR
	;[844] 			#ZoomOBjectY=ZOOMY2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",844
	MVII #256,R0
	MVO R0,var_&ZOOMOBJECTY
	;[845] 			#ZoomOBjectX=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",845
	CLRR R0
	MVO R0,var_&ZOOMOBJECTX
	;[846] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",846
	CALL _wait
	;[847] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",847
T165:
	;[848] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",848
T145:
	;[849] 	'end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",849
	;[850] 	i=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",850
	CLRR R0
	MVO R0,var_I
	;[851] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",851
	RETURN
	ENDP
	;[852] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",852
	;[853] '///// Init Armes /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",853
	;[854] InitArmes: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",854
	; INITARMES
label_INITARMES:	PROC
	BEGIN
	;[855] 	TirPapi=SensTIR
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",855
	MVI var_SENSTIR,R0
	MVO R0,var_TIRPAPI
	;[856] 	CouleurImpact=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",856
	CLRR R0
	MVO R0,var_COULEURIMPACT
	;[857] 	TempoTir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",857
	MVO R0,var_TEMPOTIR
	;[858] 	DeadU=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",858
	NOP
	MVO R0,var_DEADU
	;[859] 	if TypeArme=4 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",859
	MVI var_TYPEARME,R0
	CMPI #4,R0
	BNE T166
	;[860] 		if SensTIR=1 then CoordX(1)=CoordX(0):CoordY(1)=CoordY(0)-8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",860
	MVI var_SENSTIR,R0
	CMPI #1,R0
	BNE T167
	MVI array_COORDX,R0
	MVO R0,array_COORDX+1
	MVI array_COORDY,R0
	SUBI #8,R0
	MVO R0,array_COORDY+1
T167:
	;[861] 		if SensTIR=2 then CoordX(1)=CoordX(0):CoordY(1)=CoordY(0)+8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",861
	MVI var_SENSTIR,R0
	CMPI #2,R0
	BNE T168
	MVI array_COORDX,R0
	MVO R0,array_COORDX+1
	MVI array_COORDY,R0
	ADDI #8,R0
	MVO R0,array_COORDY+1
T168:
	;[862] 		if SensTIR=3 then CoordX(1)=CoordX(0)+8:CoordY(1)=CoordY(0)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",862
	MVI var_SENSTIR,R0
	CMPI #3,R0
	BNE T169
	MVI array_COORDX,R0
	ADDI #8,R0
	MVO R0,array_COORDX+1
	MVI array_COORDY,R0
	MVO R0,array_COORDY+1
T169:
	;[863] 		if SensTIR=4 then CoordX(1)=CoordX(0)-8:CoordY(1)=CoordY(0)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",863
	MVI var_SENSTIR,R0
	CMPI #4,R0
	BNE T170
	MVI array_COORDX,R0
	SUBI #8,R0
	MVO R0,array_COORDX+1
	MVI array_COORDY,R0
	MVO R0,array_COORDY+1
T170:
	;[864] 	else
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",864
	B T171
T166:
	;[865] 		CoordX(1)=CoordX(0)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",865
	MVI array_COORDX,R0
	MVO R0,array_COORDX+1
	;[866] 		CoordY(1)=CoordY(0)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",866
	MVI array_COORDY,R0
	MVO R0,array_COORDY+1
	;[867] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",867
T171:
	;[868] 	DX_Arme=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",868
	CLRR R0
	MVO R0,var_DX_ARME
	;[869] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",869
	RETURN
	ENDP
	;[870] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",870
	;[871] '///// Impact Touch /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",871
	;[872] ImpactHIT: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",872
	; IMPACTHIT
label_IMPACTHIT:	PROC
	BEGIN
	;[873] 	#couleur=SPR_RED
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",873
	MVII #2,R0
	MVO R0,var_&COULEUR
	;[874] 	if TempoTir%2=0 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",874
	MVI var_TEMPOTIR,R0
	ANDI #1,R0
	BNE T172
	;[875] 		if DeadU=0 then DEFINE DEF47, 1, Touch else DEFINE DEF47, 1, Touch2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",875
	MVI var_DEADU,R0
	TSTR R0
	BNE T173
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_TOUCH,R0
	MVO R0,_gram_bitmap
	B T174
T173:
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_TOUCH2,R0
	MVO R0,_gram_bitmap
T174:
	;[876] 	else 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",876
	B T175
T172:
	;[877] 		DEFINE DEF47, 1, Touch1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",877
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_TOUCH1,R0
	MVO R0,_gram_bitmap
	;[878] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",878
T175:
	;[879] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",879
	;[880] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",880
	RETURN
	ENDP
	;[881] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",881
	;[882] '///// Routines des sprites /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",882
	;[883] IAHitBOX: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",883
	; IAHITBOX
label_IAHITBOX:	PROC
	BEGIN
	;[884] 	TirPapi=99
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",884
	MVII #99,R0
	MVO R0,var_TIRPAPI
	;[885] 	ImpactTouch=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",885
	MVII #1,R0
	MVO R0,var_IMPACTTOUCH
	;[886] 	VieSoldat(c)=VieSoldat(c)-f
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",886
	MVII #array_VIESOLDAT,R3
	ADD var_C,R3
	MVI@ R3,R0
	SUB var_F,R0
	MVO@ R0,R3
	;[887] 	f=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",887
	CLRR R0
	MVO R0,var_F
	;[888] 	if VieSoldat(c)=0 then gosub IAV:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",888
	MVII #array_VIESOLDAT,R3
	ADD var_C,R3
	MVI@ R3,R0
	TSTR R0
	BNE T176
	CALL label_IAV
	RETURN
T176:
	;[889] 	if VieSoldat(c)>50 then gosub IAV
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",889
	MVII #array_VIESOLDAT,R3
	ADD var_C,R3
	MVI@ R3,R0
	CMPI #50,R0
	BLE T177
	CALL label_IAV
T177:
	;[890] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",890
	RETURN
	ENDP
	;[891] IAV: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",891
	; IAV
label_IAV:	PROC
	BEGIN
	;[892] 	VisibleIA(c)=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",892
	MVII #2,R0
	MVII #array_VISIBLEIA,R3
	ADD var_C,R3
	MVO@ R0,R3
	;[893] 	TempoIA(c)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",893
	CLRR R0
	ADDI #(array_TEMPOIA-array_VISIBLEIA) AND $FFFF,R3
	MVO@ R0,R3
	;[894] 	DeadU=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",894
	MVII #1,R0
	MVO R0,var_DEADU
	;[895] 	TempoTir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",895
	CLRR R0
	MVO R0,var_TEMPOTIR
	;[896] 	VelocityIA(c)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",896
	MVII #array_VELOCITYIA,R3
	ADD var_C,R3
	MVO@ R0,R3
	;[897] 	if RANDOM(100)<50 then SensIA(c)=0 else SensIA(c)=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",897
	MVII #100,R1
	CALL _next_random
	CALL qs_mpy8
	SWAP R0
	ANDI #255,R0
	CMPI #50,R0
	BGE T178
	CLRR R0
	MVII #array_SENSIA,R3
	ADD var_C,R3
	MVO@ R0,R3
	B T179
T178:
	MVII #1,R0
	MVII #array_SENSIA,R3
	ADD var_C,R3
	MVO@ R0,R3
T179:
	;[898] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",898
	RETURN
	ENDP
	;[899] IATOUCH: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",899
	; IATOUCH
label_IATOUCH:	PROC
	BEGIN
	;[900] 	if TirPapi=99 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",900
	MVI var_TIRPAPI,R0
	CMPI #99,R0
	BNE T180
	RETURN
T180:
	;[901] 	if TypeArme=0 then f=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",901
	MVI var_TYPEARME,R0
	TSTR R0
	BNE T181
	MVII #1,R0
	MVO R0,var_F
T181:
	;[902] 	if TypeArme=3 then f=4
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",902
	MVI var_TYPEARME,R0
	CMPI #3,R0
	BNE T182
	MVII #4,R0
	MVO R0,var_F
T182:
	;[903] 	if TypeArme=1 then f=3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",903
	MVI var_TYPEARME,R0
	CMPI #1,R0
	BNE T183
	MVII #3,R0
	MVO R0,var_F
T183:
	;[904] 	if TypeArme=2 then f=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",904
	MVI var_TYPEARME,R0
	CMPI #2,R0
	BNE T184
	MVII #8,R0
	MVO R0,var_F
T184:
	;[905] 	if COL3 and HIT_SPRITE5 then c=1:gosub IAHitBOX:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",905
	MVI _col3,R0
	ANDI #32,R0
	BEQ T185
	MVII #1,R0
	MVO R0,var_C
	CALL label_IAHITBOX
	RETURN
T185:
	;[906] 	if COL3 and HIT_SPRITE6 then c=2:gosub IAHitBOX:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",906
	MVI _col3,R0
	ANDI #64,R0
	BEQ T186
	MVII #2,R0
	MVO R0,var_C
	CALL label_IAHITBOX
	RETURN
T186:
	;[907] 	if COL3 and HIT_SPRITE7 then c=3:gosub IAHitBOX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",907
	MVI _col3,R0
	ANDI #128,R0
	BEQ T187
	MVII #3,R0
	MVO R0,var_C
	CALL label_IAHITBOX
T187:
	;[908] 	end	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",908
	RETURN
	ENDP
	;[909] IATOUCH1: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",909
	; IATOUCH1
label_IATOUCH1:	PROC
	BEGIN
	;[910] 	if TypeArme=1 then f=3 else f=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",910
	MVI var_TYPEARME,R0
	CMPI #1,R0
	BNE T188
	MVII #3,R0
	MVO R0,var_F
	B T189
T188:
	MVII #8,R0
	MVO R0,var_F
T189:
	;[911] 	if COL3 and HIT_SPRITE5 then c=1:gosub IAHitBOX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",911
	MVI _col3,R0
	ANDI #32,R0
	BEQ T190
	MVII #1,R0
	MVO R0,var_C
	CALL label_IAHITBOX
T190:
	;[912] 	if COL3 and HIT_SPRITE6 then c=2:gosub IAHitBOX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",912
	MVI _col3,R0
	ANDI #64,R0
	BEQ T191
	MVII #2,R0
	MVO R0,var_C
	CALL label_IAHITBOX
T191:
	;[913] 	if COL3 and HIT_SPRITE7 then c=3:gosub IAHitBOX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",913
	MVI _col3,R0
	ANDI #128,R0
	BEQ T192
	MVII #3,R0
	MVO R0,var_C
	CALL label_IAHITBOX
T192:
	;[914] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",914
	RETURN
	ENDP
	;[915] GCout: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",915
	; GCOUT
label_GCOUT:	PROC
	BEGIN
	;[916] 	f=8:gosub IATOUCH:gosub SonExplosion
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",916
	MVII #8,R0
	MVO R0,var_F
	CALL label_IATOUCH
	CALL label_SONEXPLOSION
	;[917] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",917
	RETURN
	ENDP
	;[918] RoutineSprite: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",918
	; ROUTINESPRITE
label_ROUTINESPRITE:	PROC
	BEGIN
	;[919] 	i=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",919
	CLRR R0
	MVO R0,var_I
	;[920] 	gosub LimS
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",920
	CALL label_LIMS
	;[921] 	'Mort ?
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",921
	;[922] 	if MortPapi then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",922
	MVI var_MORTPAPI,R0
	TSTR R0
	BEQ T193
	;[923] 		Sound 0,(250-Velocity*10+FRAME) and 255,10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",923
	MVII #250,R0
	MVI var_VELOCITY,R1
	MULT R1,R4,10
	SUBR R1,R0
	MVI _frame,R1
	ADDR R1,R0
	ANDI #255,R0
	MVO R0,496
	SWAP R0
	MVO R0,500
	MVII #10,R0
	MVO R0,507
	;[924] 		Sound 1,(300+Velocity*10+FRAME) and 255,10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",924
	MVI var_VELOCITY,R0
	MULT R0,R4,10
	ADDI #300,R0
	ADDR R1,R0
	ANDI #255,R0
	MVO R0,497
	SWAP R0
	MVO R0,501
	MVII #10,R0
	MVO R0,508
	;[925] 		Sound 2,(50-Velocity*2+FRAME) and 255,10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",925
	MVII #50,R0
	MVI var_VELOCITY,R1
	SLL R1,1
	SUBR R1,R0
	MVI _frame,R1
	ADDR R1,R0
	ANDI #255,R0
	MVO R0,498
	SWAP R0
	MVO R0,502
	MVII #10,R0
	MVO R0,509
	;[926] 		wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",926
	CALL _wait
	;[927] 		Velocity=Velocity+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",927
	MVI var_VELOCITY,R0
	INCR R0
	MVO R0,var_VELOCITY
	;[928] 		if j=2 then CoordX(0)=CoordX(0)+3 else CoordX(0)=CoordX(0)-3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",928
	MVI var_J,R0
	CMPI #2,R0
	BNE T194
	MVI array_COORDX,R0
	ADDI #3,R0
	MVO R0,array_COORDX
	B T195
T194:
	MVI array_COORDX,R0
	SUBI #3,R0
	MVO R0,array_COORDX
T195:
	;[929] 		CoordY(0)=CoordY(0)-(10-Velocity)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",929
	MVI array_COORDY,R0
	MVII #10,R1
	SUB var_VELOCITY,R1
	SUBR R1,R0
	MVO R0,array_COORDY
	;[930] 		if CoordY(0)>100 then Gosub InitVariable
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",930
	MVI array_COORDY,R0
	CMPI #100,R0
	BLE T196
	CALL label_INITVARIABLE
T196:
	;[931] 		if CoordX(0)>160 then Gosub InitVariable
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",931
	MVI array_COORDX,R0
	CMPI #160,R0
	BLE T197
	CALL label_INITVARIABLE
T197:
	;[932] 		if CoordX(0)<8 then Gosub InitVariable
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",932
	MVI array_COORDX,R0
	CMPI #8,R0
	BGE T198
	CALL label_INITVARIABLE
T198:
	;[933] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",933
T193:
	;[934] 	#Visible=VISIBLE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",934
	MVII #512,R0
	MVO R0,var_&VISIBLE
	;[935] 	SPRITE 0,Coordx(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir,SPR00+SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",935
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	ADDI #256,R0
	ADD var_&VISIBLE,R0
	MVO R0,_mobs
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	ADDI #256,R0
	ADD var_&MIROIR,R0
	MVO R0,_mobs+8
	MVII #2055,R0
	MVO R0,_mobs+16
	;[936] 	SPRITE 1,Coordx(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir,SPR01+SPR_BLACK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",936
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	ADDI #256,R0
	ADD var_&VISIBLE,R0
	MVO R0,_mobs+1
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	ADDI #256,R0
	ADD var_&MIROIR,R0
	MVO R0,_mobs+9
	MVII #2056,R0
	MVO R0,_mobs+17
	;[937] 	SPRITE 2,Coordx(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir,SPR02+SPR_PINK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",937
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	ADDI #256,R0
	ADD var_&VISIBLE,R0
	MVO R0,_mobs+2
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	ADDI #256,R0
	ADD var_&MIROIR,R0
	MVO R0,_mobs+10
	MVII #6164,R0
	MVO R0,_mobs+18
	;[938] 	'end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",938
	;[939] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",939
	;[940] 	'Armes Papi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",940
	;[941] 	i=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",941
	MVII #1,R0
	MVO R0,var_I
	;[942] 	#Miroir1=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",942
	CLRR R0
	MVO R0,var_&MIROIR1
	;[943] 	'Couteau ?
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",943
	;[944] 	if TypeArme=4 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",944
	MVI var_TYPEARME,R0
	CMPI #4,R0
	BNE T199
	;[945] 		if TirPapi then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",945
	MVI var_TIRPAPI,R0
	TSTR R0
	BEQ T200
	;[946] 			TempoTir=TempoTir+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",946
	MVI var_TEMPOTIR,R0
	INCR R0
	MVO R0,var_TEMPOTIR
	;[947] 			if DeadU then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",947
	MVI var_DEADU,R0
	TSTR R0
	BEQ T201
	;[948] 				gosub ImpactHIT
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",948
	CALL label_IMPACTHIT
	;[949] 				if TempoTir=12 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",949
	MVI var_TEMPOTIR,R0
	CMPI #12,R0
	BNE T202
	;[950] 					gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",950
	CALL label_SONBLANK
	;[951] 					TirPapi=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",951
	CLRR R0
	MVO R0,var_TIRPAPI
	;[952] 					TempoTir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",952
	MVO R0,var_TEMPOTIR
	;[953] 					ImpactTouch=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",953
	NOP
	MVO R0,var_IMPACTTOUCH
	;[954] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",954
T202:
	;[955] 			else
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",955
	B T203
T201:
	;[956] 				#couleur=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",956
	MVII #7,R0
	MVO R0,var_&COULEUR
	;[957] 				if SensTIR=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",957
	MVI var_SENSTIR,R0
	CMPI #1,R0
	BNE T204
	;[958] 					if TempoTir=1 then DEFINE DEF47,1,CouteauH_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",958
	MVI var_TEMPOTIR,R0
	CMPI #1,R0
	BNE T205
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUH_1,R0
	MVO R0,_gram_bitmap
T205:
	;[959] 					if TempoTir=2 then DEFINE DEF47,1,CouteauH_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",959
	MVI var_TEMPOTIR,R0
	CMPI #2,R0
	BNE T206
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUH_2,R0
	MVO R0,_gram_bitmap
T206:
	;[960] 					if TempoTir=3 then DEFINE DEF47,1,CouteauH_3:gosub GestionCouteauBunker:gosub GCout
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",960
	MVI var_TEMPOTIR,R0
	CMPI #3,R0
	BNE T207
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUH_3,R0
	MVO R0,_gram_bitmap
	CALL label_GESTIONCOUTEAUBUNKER
	CALL label_GCOUT
T207:
	;[961] 					if TempoTir=4 then DEFINE DEF47,1,CouteauH_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",961
	MVI var_TEMPOTIR,R0
	CMPI #4,R0
	BNE T208
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUH_2,R0
	MVO R0,_gram_bitmap
T208:
	;[962] 					if TempoTir=5 then DEFINE DEF47,1,CouteauH_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",962
	MVI var_TEMPOTIR,R0
	CMPI #5,R0
	BNE T209
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUH_1,R0
	MVO R0,_gram_bitmap
T209:
	;[963] 				elseif SensTIR=2 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",963
	B T210
T204:
	MVI var_SENSTIR,R0
	CMPI #2,R0
	BNE T211
	;[964] 					if TempoTir=1 then DEFINE DEF47,1,CouteauB_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",964
	MVI var_TEMPOTIR,R0
	CMPI #1,R0
	BNE T212
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUB_1,R0
	MVO R0,_gram_bitmap
T212:
	;[965] 					if TempoTir=2 then DEFINE DEF47,1,CouteauB_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",965
	MVI var_TEMPOTIR,R0
	CMPI #2,R0
	BNE T213
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUB_2,R0
	MVO R0,_gram_bitmap
T213:
	;[966] 					if TempoTir=3 then DEFINE DEF47,1,CouteauB_3:gosub GCout
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",966
	MVI var_TEMPOTIR,R0
	CMPI #3,R0
	BNE T214
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUB_3,R0
	MVO R0,_gram_bitmap
	CALL label_GCOUT
T214:
	;[967] 					if TempoTir=4 then DEFINE DEF47,1,CouteauB_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",967
	MVI var_TEMPOTIR,R0
	CMPI #4,R0
	BNE T215
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUB_2,R0
	MVO R0,_gram_bitmap
T215:
	;[968] 					if TempoTir=5 then DEFINE DEF47,1,CouteauB_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",968
	MVI var_TEMPOTIR,R0
	CMPI #5,R0
	BNE T216
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUB_1,R0
	MVO R0,_gram_bitmap
T216:
	;[969] 				elseif SensTIR=3 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",969
	B T210
T211:
	MVI var_SENSTIR,R0
	CMPI #3,R0
	BNE T217
	;[970] 					if TempoTir=1 then DEFINE DEF47,1,CouteauD_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",970
	MVI var_TEMPOTIR,R0
	CMPI #1,R0
	BNE T218
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUD_1,R0
	MVO R0,_gram_bitmap
T218:
	;[971] 					if TempoTir=2 then DEFINE DEF47,1,CouteauD_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",971
	MVI var_TEMPOTIR,R0
	CMPI #2,R0
	BNE T219
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUD_2,R0
	MVO R0,_gram_bitmap
T219:
	;[972] 					if TempoTir=3 then DEFINE DEF47,1,CouteauD_3:gosub GCout
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",972
	MVI var_TEMPOTIR,R0
	CMPI #3,R0
	BNE T220
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUD_3,R0
	MVO R0,_gram_bitmap
	CALL label_GCOUT
T220:
	;[973] 					if TempoTir=4 then DEFINE DEF47,1,CouteauD_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",973
	MVI var_TEMPOTIR,R0
	CMPI #4,R0
	BNE T221
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUD_2,R0
	MVO R0,_gram_bitmap
T221:
	;[974] 					if TempoTir=5 then DEFINE DEF47,1,CouteauD_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",974
	MVI var_TEMPOTIR,R0
	CMPI #5,R0
	BNE T222
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUD_1,R0
	MVO R0,_gram_bitmap
T222:
	;[975] 				elseif SensTIR=4 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",975
	B T210
T217:
	MVI var_SENSTIR,R0
	CMPI #4,R0
	BNE T223
	;[976] 					#Miroir1=FLIPX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",976
	MVII #1024,R0
	MVO R0,var_&MIROIR1
	;[977] 					if TempoTir=1 then DEFINE DEF47,1,CouteauD_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",977
	MVI var_TEMPOTIR,R0
	CMPI #1,R0
	BNE T224
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUD_1,R0
	MVO R0,_gram_bitmap
T224:
	;[978] 					if TempoTir=2 then DEFINE DEF47,1,CouteauD_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",978
	MVI var_TEMPOTIR,R0
	CMPI #2,R0
	BNE T225
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUD_2,R0
	MVO R0,_gram_bitmap
T225:
	;[979] 					if TempoTir=3 then DEFINE DEF47,1,CouteauD_3:gosub GCout
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",979
	MVI var_TEMPOTIR,R0
	CMPI #3,R0
	BNE T226
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUD_3,R0
	MVO R0,_gram_bitmap
	CALL label_GCOUT
T226:
	;[980] 					if TempoTir=4 then DEFINE DEF47,1,CouteauD_2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",980
	MVI var_TEMPOTIR,R0
	CMPI #4,R0
	BNE T227
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUD_2,R0
	MVO R0,_gram_bitmap
T227:
	;[981] 					if TempoTir=5 then DEFINE DEF47,1,CouteauD_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",981
	MVI var_TEMPOTIR,R0
	CMPI #5,R0
	BNE T228
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_COUTEAUD_1,R0
	MVO R0,_gram_bitmap
T228:
	;[982] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",982
T210:
T223:
	;[983] 				if TempoTir>5 then #visible=0 else #visible=VISIBLE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",983
	MVI var_TEMPOTIR,R0
	CMPI #5,R0
	BLE T229
	CLRR R0
	MVO R0,var_&VISIBLE
	B T230
T229:
	MVII #512,R0
	MVO R0,var_&VISIBLE
T230:
	;[984] 				if TempoTir=12 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",984
	MVI var_TEMPOTIR,R0
	CMPI #12,R0
	BNE T231
	;[985] 					gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",985
	CALL label_SONBLANK
	;[986] 					TirPapi=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",986
	CLRR R0
	MVO R0,var_TIRPAPI
	;[987] 					TempoTir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",987
	MVO R0,var_TEMPOTIR
	;[988] 					ImpactTouch=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",988
	NOP
	MVO R0,var_IMPACTTOUCH
	;[989] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",989
T231:
	;[990] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",990
T203:
	;[991] 		else
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",991
	B T232
T200:
	;[992] 			#Visible=0	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",992
	CLRR R0
	MVO R0,var_&VISIBLE
	;[993] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",993
T232:
	;[994] 	else
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",994
	B T233
T199:
	;[995] 		if TirPapi then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",995
	MVI var_TIRPAPI,R0
	TSTR R0
	BEQ T234
	;[996] 			d=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",996
	CLRR R0
	MVO R0,var_D
	;[997] 			#Visible=VISIBLE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",997
	MVII #512,R0
	MVO R0,var_&VISIBLE
	;[998] 			'Déplacement
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",998
	;[999] 			if TirPapi=1 then coordy(i)=Coordy(i)-VitesseArme:gosub GestionDistanceArme
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",999
	MVI var_TIRPAPI,R0
	CMPI #1,R0
	BNE T235
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_VITESSEARME,R0
	MVO@ R0,R3
	CALL label_GESTIONDISTANCEARME
T235:
	;[1000] 			if TirPapi=2 then coordy(i)=Coordy(i)+VitesseArme:gosub GestionDistanceArme
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1000
	MVI var_TIRPAPI,R0
	CMPI #2,R0
	BNE T236
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	ADD var_VITESSEARME,R0
	MVO@ R0,R3
	CALL label_GESTIONDISTANCEARME
T236:
	;[1001] 			if TirPapi=3 then coordx(i)=Coordx(i)+VitesseArme:gosub GestionDistanceArme
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1001
	MVI var_TIRPAPI,R0
	CMPI #3,R0
	BNE T237
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	ADD var_VITESSEARME,R0
	MVO@ R0,R3
	CALL label_GESTIONDISTANCEARME
T237:
	;[1002] 			if TirPapi=4 then coordx(i)=Coordx(i)-VitesseArme:gosub GestionDistanceArme
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1002
	MVI var_TIRPAPI,R0
	CMPI #4,R0
	BNE T238
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_VITESSEARME,R0
	MVO@ R0,R3
	CALL label_GESTIONDISTANCEARME
T238:
	;[1003] 			'Limite
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1003
	;[1004] 			gosub LimS
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1004
	CALL label_LIMS
	;[1005] 			if TypeArme=0 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1005
	MVI var_TYPEARME,R0
	TSTR R0
	BNE T239
	;[1006] 				'Tir / IA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1006
	;[1007] 				if TirPapi then gosub IATOUCH
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1007
	MVI var_TIRPAPI,R0
	TSTR R0
	BEQ T240
	CALL label_IATOUCH
T240:
	;[1008] 				if d=1 then TirPapi=99
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1008
	MVI var_D,R0
	CMPI #1,R0
	BNE T241
	MVII #99,R0
	MVO R0,var_TIRPAPI
T241:
	;[1009] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1009
T239:
	;[1010] 			'Grenade Papi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1010
	;[1011] 			if TypeArme=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1011
	MVI var_TYPEARME,R0
	CMPI #1,R0
	BNE T242
	;[1012] 				if d=1 then c=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1012
	MVI var_D,R0
	CMPI #1,R0
	BNE T243
	CLRR R0
	MVO R0,var_C
T243:
	;[1013] 				gosub GestionGrenade
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1013
	CALL label_GESTIONGRENADE
	;[1014] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1014
T242:
	;[1015] 			if TypeArme=2 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1015
	MVI var_TYPEARME,R0
	CMPI #2,R0
	BNE T244
	;[1016] 				if d=1 then c=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1016
	MVI var_D,R0
	CMPI #1,R0
	BNE T245
	CLRR R0
	MVO R0,var_C
T245:
	;[1017] 				gosub GestionGrenadeMaxi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1017
	CALL label_GESTIONGRENADEMAXI
	;[1018] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1018
T244:
	;[1019] 			if TypeArme=3 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1019
	MVI var_TYPEARME,R0
	CMPI #3,R0
	BNE T246
	;[1020] 				if d=1 then TirPapi=99
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1020
	MVI var_D,R0
	CMPI #1,R0
	BNE T247
	MVII #99,R0
	MVO R0,var_TIRPAPI
T247:
	;[1021] 				#Couleur=FRAME and 6
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1021
	MVI _frame,R0
	ANDI #6,R0
	MVO R0,var_&COULEUR
	;[1022] 				gosub GestionBazooka
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1022
	CALL label_GESTIONBAZOOKA
	;[1023] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1023
T246:
	;[1024] 			'Balle Papi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1024
	;[1025] 			if TypeArme=0 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1025
	MVI var_TYPEARME,R0
	TSTR R0
	BNE T248
	;[1026] 				#Couleur=SPR_YELLOW
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1026
	MVII #6,R0
	MVO R0,var_&COULEUR
	;[1027] 				'if (FRAME and 3) then DEFINE DEF47, 1, Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1027
	;[1028] 				'Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1028
	;[1029] 				if TirPapi=99 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1029
	MVI var_TIRPAPI,R0
	CMPI #99,R0
	BNE T249
	;[1030] 					TempoTir=TempoTir+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1030
	MVI var_TEMPOTIR,R0
	INCR R0
	MVO R0,var_TEMPOTIR
	;[1031] 					if ImpactTouch then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1031
	MVI var_IMPACTTOUCH,R0
	TSTR R0
	BEQ T250
	;[1032] 						if TempoTir=1 then gosub SonExplosion
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1032
	MVI var_TEMPOTIR,R0
	CMPI #1,R0
	BNE T251
	CALL label_SONEXPLOSION
T251:
	;[1033] 						gosub ImpactHIT
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1033
	CALL label_IMPACTHIT
	;[1034] 					else
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1034
	B T252
T250:
	;[1035] 						if TempoTir=1 then gosub SonImpact
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1035
	MVI var_TEMPOTIR,R0
	CMPI #1,R0
	BNE T253
	CALL label_SONIMPACT
T253:
	;[1036] 						if TempoTir=1 then DEFINE DEF47, 1, Balle:#Couleur=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1036
	MVI var_TEMPOTIR,R0
	CMPI #1,R0
	BNE T254
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
	MVII #7,R0
	MVO R0,var_&COULEUR
T254:
	;[1037] 						if TempoTir=3 then DEFINE DEF47, 1, Balle1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1037
	MVI var_TEMPOTIR,R0
	CMPI #3,R0
	BNE T255
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE1,R0
	MVO R0,_gram_bitmap
T255:
	;[1038] 						if TempoTir=5 then DEFINE DEF47, 1, Balle2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1038
	MVI var_TEMPOTIR,R0
	CMPI #5,R0
	BNE T256
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE2,R0
	MVO R0,_gram_bitmap
T256:
	;[1039] 					end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1039
T252:
	;[1040] 					
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1040
	;[1041] 					if TempoTir=7 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1041
	MVI var_TEMPOTIR,R0
	CMPI #7,R0
	BNE T257
	;[1042] 						gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1042
	CALL label_SONBLANK
	;[1043] 						TirPapi=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1043
	CLRR R0
	MVO R0,var_TIRPAPI
	;[1044] 						TempoTir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1044
	MVO R0,var_TEMPOTIR
	;[1045] 						ImpactTouch=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1045
	NOP
	MVO R0,var_IMPACTTOUCH
	;[1046] 						Sprite 3,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1046
	MVO R0,_mobs+3
	NOP
	MVO R0,_mobs+11
	;[1047] 						Coordx(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1047
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1048] 						Coordy(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1048
	ADDI #(array_COORDY-array_COORDX) AND $FFFF,R3
	MVO@ R0,R3
	;[1049] 					end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1049
T257:
	;[1050] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1050
T249:
	;[1051] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1051
T248:
	;[1052] 		else
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1052
	B T258
T234:
	;[1053] 			#Visible=0	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1053
	CLRR R0
	MVO R0,var_&VISIBLE
	;[1054] 			'Gestion viseur en mode grenade
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1054
	;[1055] 			if TypeArme and MortPapi=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1055
	MVI var_TYPEARME,R0
	MVI var_MORTPAPI,R1
	TSTR R1
	MVII #65535,R1
	BEQ T260
	INCR R1
T260:
	ANDR R1,R0
	BEQ T259
	;[1056] 				#Visible=VISIBLE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1056
	MVII #512,R0
	MVO R0,var_&VISIBLE
	;[1057] 				#Couleur=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1057
	MVII #7,R0
	MVO R0,var_&COULEUR
	;[1058] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1058
	;[1059] 				if SensTIR=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1059
	MVI var_SENSTIR,R0
	CMPI #1,R0
	BNE T261
	;[1060] 					Coordx(i)=ConvTILE(Coordx(0))
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1060
	MVI array_COORDX,R0
	ADDI #6,R0
	SLR R0,2
	SLR R0,1
	SLL R0,2
	ADDR R0,R0
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1061] 					Coordy(i)=ConvTILE2(Coordy(0))
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1061
	MVI array_COORDY,R0
	ADDI #65500,R0
	SLR R0,2
	SLR R0,1
	SLL R0,2
	ADDR R0,R0
	ADDI #(array_COORDY-array_COORDX) AND $FFFF,R3
	MVO@ R0,R3
	;[1062] 				elseif SensTIR=2 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1062
	B T262
T261:
	MVI var_SENSTIR,R0
	CMPI #2,R0
	BNE T263
	;[1063] 					Coordx(i)=ConvTILE(Coordx(0))
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1063
	MVI array_COORDX,R0
	ADDI #6,R0
	SLR R0,2
	SLR R0,1
	SLL R0,2
	ADDR R0,R0
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1064] 					Coordy(i)=ConvTILE1(Coordy(0))
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1064
	MVI array_COORDY,R0
	ADDI #36,R0
	SLR R0,2
	SLR R0,1
	SLL R0,2
	ADDR R0,R0
	ADDI #(array_COORDY-array_COORDX) AND $FFFF,R3
	MVO@ R0,R3
	;[1065] 				elseif SensTIR=3 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1065
	B T262
T263:
	MVI var_SENSTIR,R0
	CMPI #3,R0
	BNE T264
	;[1066] 					Coordx(i)=ConvTILE3(Coordx(0))
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1066
	MVI array_COORDX,R0
	ADDI #48,R0
	SLR R0,2
	SLR R0,1
	SLL R0,2
	ADDR R0,R0
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1067] 					Coordy(i)=ConvTILE(Coordy(0))
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1067
	MVI array_COORDY,R0
	ADDI #6,R0
	SLR R0,2
	SLR R0,1
	SLL R0,2
	ADDR R0,R0
	ADDI #(array_COORDY-array_COORDX) AND $FFFF,R3
	MVO@ R0,R3
	;[1068] 				elseif SensTIR=4 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1068
	B T262
T264:
	MVI var_SENSTIR,R0
	CMPI #4,R0
	BNE T265
	;[1069] 					Coordx(i)=ConvTILE2(Coordx(0))
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1069
	MVI array_COORDX,R0
	ADDI #65500,R0
	SLR R0,2
	SLR R0,1
	SLL R0,2
	ADDR R0,R0
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1070] 					Coordy(i)=ConvTILE(Coordy(0))
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1070
	MVI array_COORDY,R0
	ADDI #6,R0
	SLR R0,2
	SLR R0,1
	SLL R0,2
	ADDR R0,R0
	ADDI #(array_COORDY-array_COORDX) AND $FFFF,R3
	MVO@ R0,R3
	;[1071] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1071
T262:
T265:
	;[1072] 				'Limite
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1072
	;[1073] 				if CoordX(i)<8 then CoordX(i)=8:#Visible=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1073
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #8,R0
	BGE T266
	MVII #8,R0
	MVO@ R0,R3
	CLRR R0
	MVO R0,var_&VISIBLE
T266:
	;[1074] 				if CoordX(i)>160 then CoordX(i)=160:#Visible=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1074
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #160,R0
	BLE T267
	MVII #160,R0
	MVO@ R0,R3
	CLRR R0
	MVO R0,var_&VISIBLE
T267:
	;[1075] 				if coordy(i)>100 then coordy(i)=98:#Visible=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1075
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #100,R0
	BLE T268
	MVII #98,R0
	MVO@ R0,R3
	CLRR R0
	MVO R0,var_&VISIBLE
T268:
	;[1076] 				#ZoomOBjectY=ZOOMY2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1076
	MVII #256,R0
	MVO R0,var_&ZOOMOBJECTY
	;[1077] 				#ZoomOBjectX=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1077
	CLRR R0
	MVO R0,var_&ZOOMOBJECTX
	;[1078] 				#Couleur=FRAME and 6
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1078
	MVI _frame,R0
	ANDI #6,R0
	MVO R0,var_&COULEUR
	;[1079] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1079
T259:
	;[1080] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1080
T258:
	;[1081] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1081
T233:
	;[1082] 	gosub SPRTIR
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1082
	CALL label_SPRTIR
	;[1083] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1083
	;[1084] 	'Tir IA 1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1084
	;[1085] 	i=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1085
	MVII #2,R0
	MVO R0,var_I
	;[1086] 	if TirIA then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1086
	MVI var_TIRIA,R0
	TSTR R0
	BEQ T269
	;[1087] 		#Visible=Visible
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1087
	MVII #512,R0
	MVO R0,var_&VISIBLE
	;[1088] 		if SensTirBunker(ID)=1 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1088
	MVII #array_SENSTIRBUNKER,R3
	ADD var_ID,R3
	MVI@ R3,R0
	CMPI #1,R0
	BNE T270
	;[1089] 			CoordX(i)=CoordX(i)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1089
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	MVO@ R0,R3
	;[1090] 			CoordY(i)=CoordY(i)+2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1090
	ADDI #(array_COORDY-array_COORDX) AND $FFFF,R3
	MVI@ R3,R0
	ADDI #2,R0
	MVO@ R0,R3
	;[1091] 		elseif SensTirBunker(ID)=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1091
	B T271
T270:
	MVII #array_SENSTIRBUNKER,R3
	ADD var_ID,R3
	MVI@ R3,R0
	TSTR R0
	BNE T272
	;[1092] 			CoordY(i)=CoordY(i)+2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1092
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	ADDI #2,R0
	MVO@ R0,R3
	;[1093] 		elseif SensTirBunker(ID)=2 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1093
	B T271
T272:
	MVII #array_SENSTIRBUNKER,R3
	ADD var_ID,R3
	MVI@ R3,R0
	CMPI #2,R0
	BNE T273
	;[1094] 			CoordX(i)=CoordX(i)+2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1094
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	ADDI #2,R0
	MVO@ R0,R3
	;[1095] 			CoordY(i)=CoordY(i)+2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1095
	ADDI #(array_COORDY-array_COORDX) AND $FFFF,R3
	MVI@ R3,R0
	ADDI #2,R0
	MVO@ R0,R3
	;[1096] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1096
T271:
T273:
	;[1097] 		'Limite
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1097
	;[1098] 		gosub LimS
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1098
	CALL label_LIMS
	;[1099] 		
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1099
	;[1100] 		if TirIA=99 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1100
	MVI var_TIRIA,R0
	CMPI #99,R0
	BNE T274
	;[1101] 			gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1101
	CALL label_SONBLANK
	;[1102] 			TirIA=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1102
	CLRR R0
	MVO R0,var_TIRIA
	;[1103] 			Tempo(2)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1103
	MVO R0,array_TEMPO+2
	;[1104] 			#Visible=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1104
	NOP
	MVO R0,var_&VISIBLE
	;[1105] 			CoordX(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1105
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1106] 			CoordY(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1106
	ADDI #(array_COORDY-array_COORDX) AND $FFFF,R3
	MVO@ R0,R3
	;[1107] 			PosBunker=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1107
	MVO R0,var_POSBUNKER
	;[1108] 			Tempo(9)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1108
	NOP
	MVO R0,array_TEMPO+9
	;[1109] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1109
T274:
	;[1110] 		SPRITE 4,CoordX(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2,SPR15+SPR_YELLOW
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1110
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	ADDI #256,R0
	ADD var_&VISIBLE,R0
	MVO R0,_mobs+4
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	ADDI #256,R0
	MVO R0,_mobs+12
	MVII #2174,R0
	MVO R0,_mobs+20
	;[1111] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1111
T269:
	;[1112] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1112
	;[1113] 	'Soldat / IA 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1113
	;[1114] 	i=3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1114
	MVII #3,R0
	MVO R0,var_I
	;[1115] 	gosub RoutIA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1115
	CALL label_ROUTIA
	;[1116] 	i=4
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1116
	MVII #4,R0
	MVO R0,var_I
	;[1117] 	gosub RoutIA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1117
	CALL label_ROUTIA
	;[1118] 	i=5
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1118
	MVII #5,R0
	MVO R0,var_I
	;[1119] 	gosub RoutIA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1119
	CALL label_ROUTIA
	;[1120] 	Sound 1,,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1120
	CLRR R0
	MVO R0,508
	;[1121] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1121
	RETURN
	ENDP
	;[1122] SPRTIR: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1122
	; SPRTIR
label_SPRTIR:	PROC
	BEGIN
	;[1123] 	SPRITE 3,Coordx(i)+HIT+#Visible,CoordY(i)-offset_y+#ZoomOBjectX+#ZoomOBjectY+#Miroir1,SPR47+#couleur
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1123
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	ADDI #256,R0
	ADD var_&VISIBLE,R0
	MVO R0,_mobs+3
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	ADD var_&ZOOMOBJECTX,R0
	ADD var_&ZOOMOBJECTY,R0
	ADD var_&MIROIR1,R0
	MVO R0,_mobs+11
	MVI var_&COULEUR,R0
	ADDI #2424,R0
	MVO R0,_mobs+19
	;[1124] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1124
	RETURN
	ENDP
	;[1125] LimS: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1125
	; LIMS
label_LIMS:	PROC
	BEGIN
	;[1126] 	f=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1126
	CLRR R0
	MVO R0,var_F
	;[1127] 	'Limite MAP/Joueur ?
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1127
	;[1128] 	if position_y_zone=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1128
	MVI var_POSITION_Y_ZONE,R0
	TSTR R0
	BNE T275
	;[1129] 		if coordy(0)<16 then coordy(0)=16
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1129
	MVI array_COORDY,R0
	CMPI #16,R0
	BGE T276
	MVII #16,R0
	MVO R0,array_COORDY
T276:
	;[1130] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1130
T275:
	;[1131] 	if coordx(i)>160 then coordx(i)=160:f=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1131
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #160,R0
	BLE T277
	MVII #160,R0
	MVO@ R0,R3
	MVII #1,R0
	MVO R0,var_F
T277:
	;[1132] 	if coordx(i)<8 then coordx(i)=8:f=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1132
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #8,R0
	BGE T278
	MVII #8,R0
	MVO@ R0,R3
	MVII #1,R0
	MVO R0,var_F
T278:
	;[1133] 	if coordy(i)>98 then coordy(i)=98:f=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1133
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #98,R0
	BLE T279
	MVII #98,R0
	MVO@ R0,R3
	MVII #1,R0
	MVO R0,var_F
T279:
	;[1134] 	if i=2 and f=1 then TirIA=99
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1134
	MVI var_I,R0
	CMPI #2,R0
	MVII #65535,R0
	BEQ T281
	INCR R0
T281:
	MVI var_F,R1
	CMPI #1,R1
	MVII #65535,R1
	BEQ T282
	INCR R1
T282:
	ANDR R1,R0
	BEQ T280
	MVII #99,R0
	MVO R0,var_TIRIA
T280:
	;[1135] 	if i=1 and f=1 then d=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1135
	MVI var_I,R0
	CMPI #1,R0
	MVII #65535,R0
	BEQ T284
	INCR R0
T284:
	MVI var_F,R1
	CMPI #1,R1
	MVII #65535,R1
	BEQ T285
	INCR R1
T285:
	ANDR R1,R0
	BEQ T283
	MVII #1,R0
	MVO R0,var_D
T283:
	;[1136] 	f=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1136
	CLRR R0
	MVO R0,var_F
	;[1137] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1137
	RETURN
	ENDP
	;[1138] RoutIA: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1138
	; ROUTIA
label_ROUTIA:	PROC
	BEGIN
	;[1139] 	if i>=MaxObjet then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1139
	MVI var_I,R0
	CMP var_MAXOBJET,R0
	BLT T286
	RETURN
T286:
	;[1140] 	IDIA=i-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1140
	MVI var_I,R0
	SUBI #2,R0
	MVO R0,var_IDIA
	;[1141] 	'IA qui saute de partout !
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1141
	;[1142] 	if VisibleIA(IDIA)=2 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1142
	MVII #array_VISIBLEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	CMPI #2,R0
	BNE T287
	;[1143] 		VelocityIA(IDIA)=VelocityIA(IDIA)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1143
	ADDI #(array_VELOCITYIA-array_VISIBLEIA) AND $FFFF,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
	;[1144] 		if SensIA(IDIA) then CoordX(i)=CoordX(i)+3 else CoordX(i)=CoordX(i)-3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1144
	ADDI #(array_SENSIA-array_VELOCITYIA) AND $FFFF,R3
	MVI@ R3,R0
	TSTR R0
	BEQ T288
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	ADDI #3,R0
	MVO@ R0,R3
	B T289
T288:
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #3,R0
	MVO@ R0,R3
T289:
	;[1145] 		CoordY(i)=CoordY(i)-(7-VelocityIA(IDIA))
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1145
	MVII #7,R0
	MVII #array_VELOCITYIA,R3
	ADD var_IDIA,R3
	SUB@ R3,R0
	NEGR R0
	MVII #array_COORDY,R3
	ADD var_I,R3
	ADD@ R3,R0
	MVO@ R0,R3
	;[1146] 		if CoordY(i)>100 then VisibleIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1146
	MVI@ R3,R0
	CMPI #100,R0
	BLE T290
	CLRR R0
	MVII #array_VISIBLEIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
T290:
	;[1147] 		if CoordX(i)>160 then VisibleIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1147
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #160,R0
	BLE T291
	CLRR R0
	MVII #array_VISIBLEIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
T291:
	;[1148] 		if CoordX(i)<8 then VisibleIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1148
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #8,R0
	BGE T292
	CLRR R0
	MVII #array_VISIBLEIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
T292:
	;[1149] 		Gosub IAP
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1149
	CALL label_IAP
	;[1150] 	elseif VisibleIA(IDIA)=1 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1150
	B T293
T287:
	MVII #array_VISIBLEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	CMPI #1,R0
	BNE T294
	;[1151] 		#Visible=VISIBLE 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1151
	MVII #512,R0
	MVO R0,var_&VISIBLE
	;[1152] 		gosub IAP
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1152
	CALL label_IAP
	;[1153] 		gosub GestionIA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1153
	CALL label_GESTIONIA
	;[1154] 		gosub LimS
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1154
	CALL label_LIMS
	;[1155] 	elseif VisibleIA(IDIA)=0 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1155
	B T293
T294:
	MVII #array_VISIBLEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	TSTR R0
	BNE T295
	;[1156] 		if IDIA=1 then SPRITE 5,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1156
	MVI var_IDIA,R0
	CMPI #1,R0
	BNE T296
	CLRR R0
	MVO R0,_mobs+5
	MVO R0,_mobs+13
T296:
	;[1157] 		if IDIA=2 then SPRITE 6,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1157
	MVI var_IDIA,R0
	CMPI #2,R0
	BNE T297
	CLRR R0
	MVO R0,_mobs+6
	MVO R0,_mobs+14
T297:
	;[1158] 		if IDIA=3 then SPRITE 7,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1158
	MVI var_IDIA,R0
	CMPI #3,R0
	BNE T298
	CLRR R0
	MVO R0,_mobs+7
	MVO R0,_mobs+15
T298:
	;[1159] 		gosub UpdateIA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1159
	CALL label_UPDATEIA
	;[1160] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1160
T293:
T295:
	;[1161] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1161
	RETURN
	ENDP
	;[1162] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1162
	;[1163] '///// Traitement des différets éléments de la MAP /////	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1163
	;[1164] TraitementElementMAP: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1164
	; TRAITEMENTELEMENTMAP
label_TRAITEMENTELEMENTMAP:	PROC
	BEGIN
	;[1165] 	for #Offset=0 to (MAP_Hauteur*MAP_LARGEUR)+SCREEN_HAUTEUR
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1165
	CLRR R0
	MVO R0,var_&OFFSET
T299:
	;[1166] 		value=Zone1(MAP_LARGEUR+#Offset)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1166
	MVII #label_ZONE1+20,R3
	ADD var_&OFFSET,R3
	MVI@ R3,R0
	MVO R0,var_VALUE
	;[1167] 		'Bunker Tireur
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1167
	;[1168] 		if value=32 then NBBUNKER=NBBUNKER+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1168
	MVI var_VALUE,R0
	CMPI #32,R0
	BNE T300
	MVI var_NBBUNKER,R0
	INCR R0
	MVO R0,var_NBBUNKER
T300:
	;[1169] 	next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1169
	MVI var_&OFFSET,R0
	INCR R0
	MVO R0,var_&OFFSET
	CMPI #1452,R0
	BLE T299
	;[1170] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1170
	RETURN
	ENDP
	;[1171] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1171
	;[1172] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1172
	;[1173] '///// Distance Arme /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1173
	;[1174] GestionDistanceArme: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1174
	; GESTIONDISTANCEARME
label_GESTIONDISTANCEARME:	PROC
	BEGIN
	;[1175] 		'Distance ?
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1175
	;[1176] 		DX_Arme=DX_Arme+VitesseArme
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1176
	MVI var_DX_ARME,R0
	ADD var_VITESSEARME,R0
	MVO R0,var_DX_ARME
	;[1177] 		if DX_Arme>DistanceArme then TirPapi=99
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1177
	MVI var_DX_ARME,R0
	CMP var_DISTANCEARME,R0
	BLE T301
	MVII #99,R0
	MVO R0,var_TIRPAPI
T301:
	;[1178] 		end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1178
	RETURN
	ENDP
	;[1179] 		
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1179
	;[1180] '///// Routines d'affichage des Tiles/Scrolling. /////	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1180
	;[1181] PrintLigneHaut:	procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1181
	; PRINTLIGNEHAUT
label_PRINTLIGNEHAUT:	PROC
	BEGIN
	;[1182] 	gosub PrintLigneBas
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1182
	CALL label_PRINTLIGNEBAS
	;[1183] 	for i=0 to MAP_LARGEUR
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1183
	CLRR R0
	MVO R0,var_I
T302:
	;[1184] 		gosub PL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1184
	CALL label_PL
	;[1185] 	next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1185
	MVI var_I,R0
	INCR R0
	MVO R0,var_I
	CMPI #20,R0
	BLE T302
	;[1186] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1186
	RETURN
	ENDP
	;[1187] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1187
	;[1188] VALDATA: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1188
	; VALDATA
label_VALDATA:	PROC
	BEGIN
	;[1189] 	value=Zone1((MAP_LARGEUR*(position_y_zone))+i)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1189
	MVII #label_ZONE1,R1
	MVI var_POSITION_Y_ZONE,R2
	MULT R2,R4,20
	ADD var_I,R2
	ADDR R2,R1
	MVI@ R1,R0
	MVO R0,var_VALUE
	;[1190] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1190
	RETURN
	ENDP
	;[1191] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1191
	;[1192] D_DATA: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1192
	; D_DATA
label_D_DATA:	PROC
	BEGIN
	;[1193] 	d=(MAP_LARGEUR*(position_y_zone))+i
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1193
	MVI var_POSITION_Y_ZONE,R0
	MULT R0,R4,20
	ADD var_I,R0
	MVO R0,var_D
	;[1194] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1194
	RETURN
	ENDP
	;[1195] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1195
	;[1196] PrintLigneBas:	procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1196
	; PRINTLIGNEBAS
label_PRINTLIGNEBAS:	PROC
	BEGIN
	;[1197] 	for i=(SCREEN_HAUTEUR-1)*MAP_LARGEUR to (SCREEN_HAUTEUR*MAP_LARGEUR)-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1197
	MVII #220,R0
	MVO R0,var_I
T303:
	;[1198] 		gosub VALDATA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1198
	CALL label_VALDATA
	;[1199] 		if value=32 then AB=AB-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1199
	MVI var_VALUE,R0
	CMPI #32,R0
	BNE T304
	MVI var_AB,R0
	DECR R0
	MVO R0,var_AB
T304:
	;[1200] 	next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1200
	MVI var_I,R0
	INCR R0
	MVO R0,var_I
	CMPI #239,R0
	BLE T303
	;[1201] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1201
	RETURN
	ENDP
	;[1202] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1202
	;[1203] PL: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1203
	; PL
label_PL:	PROC
	BEGIN
	;[1204] 	gosub VALDATA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1204
	CALL label_VALDATA
	;[1205] 	gosub D_DATA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1205
	CALL label_D_DATA
	;[1206] 	'Gestion destruction Bunker MAP
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1206
	;[1207] 	if value=32 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1207
	MVI var_VALUE,R0
	CMPI #32,R0
	BNE T305
	;[1208] 		AB=AB+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1208
	MVI var_AB,R0
	INCR R0
	MVO R0,var_AB
	;[1209] 		#backtab(i)=CARD_IABUNKER_M
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1209
	MVII #6600,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1210] 		for j=1 to NBBUNKER
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1210
	MVII #1,R0
	MVO R0,var_J
T306:
	;[1211] 			if IDB(j)=0 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1211
	MVII #array_IDB,R3
	ADD var_J,R3
	MVI@ R3,R0
	TSTR R0
	BNE T307
	;[1212] 				#backtab(i)=CARD_IABUNKER_M 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1212
	MVII #6600,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1213] 				LifeBunker(j)=6
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1213
	MVII #6,R0
	MVII #array_LIFEBUNKER,R3
	ADD var_J,R3
	MVO@ R0,R3
	;[1214] 				IDB(j)=d
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1214
	MVI var_D,R0
	ADDI #(array_IDB-array_LIFEBUNKER) AND $FFFF,R3
	MVO@ R0,R3
	;[1215] 				eBunker=j
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1215
	MVI var_J,R0
	MVO R0,var_EBUNKER
	;[1216] 				IABUNKER_CX(eBunker)=(i*8)+8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1216
	MVI var_I,R0
	SLL R0,2
	ADDR R0,R0
	ADDI #8,R0
	MVII #array_IABUNKER_CX,R3
	ADD var_EBUNKER,R3
	MVO@ R0,R3
	;[1217] 				IABUNKER_CY(eBunker)=10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1217
	MVII #10,R0
	ADDI #(array_IABUNKER_CY-array_IABUNKER_CX) AND $FFFF,R3
	MVO@ R0,R3
	;[1218] 				exit for
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1218
	B T308
	;[1219] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1219
T307:
	;[1220] 		next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1220
	MVI var_J,R0
	INCR R0
	MVO R0,var_J
	CMP var_NBBUNKER,R0
	BLE T306
T308:
	;[1221] 		PosBunker=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1221
	CLRR R0
	MVO R0,var_POSBUNKER
	;[1222] 		return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1222
	RETURN
	;[1223] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1223
T305:
	;[1224] 	gosub DefTile
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1224
	CALL label_DEFTILE
	;[1225] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1225
	RETURN
	ENDP
	;[1226] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1226
	;[1227] '///// Routine principale de scrolling /////	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1227
	;[1228] ScrollingMAP: PROCEDURE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1228
	; SCROLLINGMAP
label_SCROLLINGMAP:	PROC
	BEGIN
	;[1229] 	if DH then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1229
	MVI var_DH,R0
	TSTR R0
	BEQ T309
	;[1230] 		'Limite ?
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1230
	;[1231] 		if position_y_zone=0 then gosub RoutineSprite:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1231
	MVI var_POSITION_Y_ZONE,R0
	TSTR R0
	BNE T310
	CALL label_ROUTINESPRITE
	RETURN
T310:
	;[1232] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1232
	;[1233] 		if coordy(0)<70 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1233
	MVI array_COORDY,R0
	CMPI #70,R0
	BGE T311
	;[1234] 			if position_y_zone>0 then	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1234
	MVI var_POSITION_Y_ZONE,R0
	CMPI #0,R0
	BLE T312
	;[1235] 				IF offset_y=7 THEN 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1235
	MVI var_OFFSET_Y,R0
	CMPI #7,R0
	BNE T313
	;[1236] 					offset_d=3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1236
	MVII #3,R0
	MVO R0,var_OFFSET_D
	;[1237] 					offset_y=0 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1237
	CLRR R0
	MVO R0,var_OFFSET_Y
	;[1238] 				ELSE 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1238
	B T314
T313:
	;[1239] 					offset_y=offset_y+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1239
	MVI var_OFFSET_Y,R0
	INCR R0
	MVO R0,var_OFFSET_Y
	;[1240] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1240
T314:
	;[1241] 				CoordY(0)=70
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1241
	MVII #70,R0
	MVO R0,array_COORDY
	;[1242] 				SCROLL 0,offset_y,offset_d
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1242
	CLRR R0
	MVO R0,_scroll_x
	MVI var_OFFSET_Y,R0
	MVO R0,_scroll_y
	MVI var_OFFSET_D,R0
	MVO R0,_scroll_d
	;[1243] 				for i=3 to 5
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1243
	MVII #3,R0
	MVO R0,var_I
T315:
	;[1244] 					'Offset Elements jeu
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1244
	;[1245] 					coordy(i)=coordy(i)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1245
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
	;[1246] 					if Coordy(i)>102 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1246
	MVI@ R3,R0
	CMPI #102,R0
	BLE T316
	;[1247] 						f=i-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1247
	MVI var_I,R0
	SUBI #2,R0
	MVO R0,var_F
	;[1248] 						VisibleIA(f)=0 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1248
	CLRR R0
	MVII #array_VISIBLEIA,R3
	ADD var_F,R3
	MVO@ R0,R3
	;[1249] 					else 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1249
	B T317
T316:
	;[1250] 						gosub IAP
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1250
	CALL label_IAP
	;[1251] 					end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1251
T317:
	;[1252] 				next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1252
	MVI var_I,R0
	INCR R0
	MVO R0,var_I
	CMPI #5,R0
	BLE T315
	;[1253] 				GOSUB RoutineSprite
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1253
	CALL label_ROUTINESPRITE
	;[1254] 				if TirPapi then Coordy(1)=Coordy(1)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1254
	MVI var_TIRPAPI,R0
	TSTR R0
	BEQ T318
	MVI array_COORDY+1,R0
	INCR R0
	MVO R0,array_COORDY+1
T318:
	;[1255] 				if NBBUNKER then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1255
	MVI var_NBBUNKER,R0
	TSTR R0
	BEQ T319
	;[1256] 					for j=1 to eBunker
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1256
	MVII #1,R0
	MVO R0,var_J
T320:
	;[1257] 						IABUNKER_CY(j)= IABUNKER_CY(j)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1257
	MVII #array_IABUNKER_CY,R3
	ADD var_J,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
	;[1258] 					next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1258
	MVI var_J,R0
	INCR R0
	MVO R0,var_J
	CMP var_EBUNKER,R0
	BLE T320
	;[1259] 					if TirIA then CoordY(2)=CoordY(2)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1259
	MVI var_TIRIA,R0
	TSTR R0
	BEQ T321
	MVI array_COORDY+2,R0
	INCR R0
	MVO R0,array_COORDY+2
T321:
	;[1260] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1260
T319:
	;[1261] 				offset_d=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1261
	CLRR R0
	MVO R0,var_OFFSET_D
	;[1262] 				'Raffraichissement MAP
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1262
	;[1263] 				IF offset_y=0 THEN 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1263
	MVI var_OFFSET_Y,R0
	TSTR R0
	BNE T322
	;[1264] 					position_y_zone=position_y_zone-1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1264
	MVI var_POSITION_Y_ZONE,R0
	DECR R0
	MVO R0,var_POSITION_Y_ZONE
	;[1265] 					WAIT
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1265
	CALL _wait
	;[1266] 					GOSUB PrintLigneHaut
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1266
	CALL label_PRINTLIGNEHAUT
	;[1267] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1267
T322:
	;[1268] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1268
T312:
	;[1269] 		else	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1269
	B T323
T311:
	;[1270] 			GOSUB RoutineSprite:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1270
	CALL label_ROUTINESPRITE
	RETURN
	;[1271] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1271
T323:
	;[1272] 	else
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1272
	B T324
T309:
	;[1273] 		GOSUB RoutineSprite:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1273
	CALL label_ROUTINESPRITE
	RETURN
	;[1274] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1274
T324:
	;[1275] 	end 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1275
	RETURN
	ENDP
	;[1276] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1276
	;[1277] IAP: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1277
	; IAP
label_IAP:	PROC
	BEGIN
	;[1278] 	if i=3 then SPRITE 5,CoordX(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir3,#SPR3+#couleur3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1278
	MVI var_I,R0
	CMPI #3,R0
	BNE T325
	MVII #array_COORDX,R3
	ADDR R0,R3
	MVI@ R3,R0
	ADDI #256,R0
	ADD var_&VISIBLE,R0
	MVO R0,_mobs+5
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	ADDI #256,R0
	ADD var_&MIROIR3,R0
	MVO R0,_mobs+13
	MVI var_&SPR3,R0
	ADD var_&COULEUR3,R0
	MVO R0,_mobs+21
T325:
	;[1279] 	if i=4 then SPRITE 6,CoordX(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir4,#SPR4+#couleur4
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1279
	MVI var_I,R0
	CMPI #4,R0
	BNE T326
	MVII #array_COORDX,R3
	ADDR R0,R3
	MVI@ R3,R0
	ADDI #256,R0
	ADD var_&VISIBLE,R0
	MVO R0,_mobs+6
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	ADDI #256,R0
	ADD var_&MIROIR4,R0
	MVO R0,_mobs+14
	MVI var_&SPR4,R0
	ADD var_&COULEUR4,R0
	MVO R0,_mobs+22
T326:
	;[1280] 	if i=5 then SPRITE 7,CoordX(i)+HIT+#Visible,CoordY(i)-offset_y+ZOOMY2+#Miroir5,#SPR5+#couleur5
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1280
	MVI var_I,R0
	CMPI #5,R0
	BNE T327
	MVII #array_COORDX,R3
	ADDR R0,R3
	MVI@ R3,R0
	ADDI #256,R0
	ADD var_&VISIBLE,R0
	MVO R0,_mobs+7
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	ADDI #256,R0
	ADD var_&MIROIR5,R0
	MVO R0,_mobs+15
	MVI var_&SPR5,R0
	ADD var_&COULEUR5,R0
	MVO R0,_mobs+23
T327:
	;[1281] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1281
	RETURN
	ENDP
	;[1282] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1282
	;[1283] '###########################################################
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1283
	;[1284] '############ Nouvelle Zone en ROM.RAM #####################
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1284
	;[1285] '###########################################################
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1285
	;[1286] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1286
	;[1287] ASM ORG $C040
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1287
 ORG $C040
	;[1288] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1288
	;[1289] '/////////////////////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1289
	;[1290] '///// Bank SONS /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1290
	;[1291] '/////////////////////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1291
	;[1292] SonBlank: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1292
	; SONBLANK
label_SONBLANK:	PROC
	BEGIN
	;[1293] 	SOUND 0,1,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1293
	MVII #1,R0
	MVO R0,496
	SWAP R0
	MVO R0,500
	CLRR R0
	MVO R0,507
	;[1294] 	SOUND 1,1,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1294
	MVII #1,R0
	MVO R0,497
	SWAP R0
	MVO R0,501
	CLRR R0
	MVO R0,508
	;[1295] 	SOUND 2,1,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1295
	MVII #1,R0
	MVO R0,498
	SWAP R0
	MVO R0,502
	CLRR R0
	MVO R0,509
	;[1296] 	SOUND 4,,$38
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1296
	MVII #56,R0
	MVO R0,504
	;[1297] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1297
	RETURN
	ENDP
	;[1298] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1298
	;[1299] SonCouteau: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1299
	; SONCOUTEAU
label_SONCOUTEAU:	PROC
	BEGIN
	;[1300] 	SOUND 4,$03,$0007
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1300
	MVII #3,R0
	MVO R0,505
	MVII #7,R0
	MVO R0,504
	;[1301] 	Sound 2,$00c8,$30
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1301
	MVII #200,R0
	MVO R0,498
	SWAP R0
	MVO R0,502
	MVII #48,R0
	MVO R0,509
	;[1302] 	Sound 3,$03E8,$0004
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1302
	MVII #1000,R0
	MVO R0,499
	SWAP R0
	MVO R0,503
	MVII #4,R0
	MVO R0,506
	;[1303] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1303
	RETURN
	ENDP
	;[1304] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1304
	;[1305] SonBazooka: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1305
	; SONBAZOOKA
label_SONBAZOOKA:	PROC
	BEGIN
	;[1306] 	SOUND 4,$05,$0007
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1306
	MVII #5,R0
	MVO R0,505
	MVII #7,R0
	MVO R0,504
	;[1307] 	Sound 2,$0bb8,$30
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1307
	MVII #3000,R0
	MVO R0,498
	SWAP R0
	MVO R0,502
	MVII #48,R0
	MVO R0,509
	;[1308] 	Sound 3,$0fff,$000d
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1308
	MVII #4095,R0
	MVO R0,499
	SWAP R0
	MVO R0,503
	MVII #13,R0
	MVO R0,506
	;[1309] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1309
	RETURN
	ENDP
	;[1310] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1310
	;[1311] SonImpact: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1311
	; SONIMPACT
label_SONIMPACT:	PROC
	BEGIN
	;[1312] 	SOUND 4,$05,$0007
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1312
	MVII #5,R0
	MVO R0,505
	MVII #7,R0
	MVO R0,504
	;[1313] 	Sound 2,$0FA0,$30
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1313
	MVII #4000,R0
	MVO R0,498
	SWAP R0
	MVO R0,502
	MVII #48,R0
	MVO R0,509
	;[1314] 	Sound 3,$0447,$0000
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1314
	MVII #1095,R0
	MVO R0,499
	SWAP R0
	MVO R0,503
	CLRR R0
	MVO R0,506
	;[1315] 	end 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1315
	RETURN
	ENDP
	;[1316] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1316
	;[1317] SonTir: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1317
	; SONTIR
label_SONTIR:	PROC
	BEGIN
	;[1318] 	SOUND 4,$0c,$0007
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1318
	MVII #12,R0
	MVO R0,505
	MVII #7,R0
	MVO R0,504
	;[1319] 	Sound 2,$0FA0,$30
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1319
	MVII #4000,R0
	MVO R0,498
	SWAP R0
	MVO R0,502
	MVII #48,R0
	MVO R0,509
	;[1320] 	Sound 3,$0800,$0000
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1320
	MVII #2048,R0
	MVO R0,499
	SWAP R0
	MVO R0,503
	CLRR R0
	MVO R0,506
	;[1321] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1321
	RETURN
	ENDP
	;[1322] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1322
	;[1323] SonExplosion: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1323
	; SONEXPLOSION
label_SONEXPLOSION:	PROC
	BEGIN
	;[1324] 	SOUND 4,$19,$0007
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1324
	MVII #25,R0
	MVO R0,505
	MVII #7,R0
	MVO R0,504
	;[1325] 	Sound 2,$0FA0,$30
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1325
	MVII #4000,R0
	MVO R0,498
	SWAP R0
	MVO R0,502
	MVII #48,R0
	MVO R0,509
	;[1326] 	Sound 3,$0FFF,$0000
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1326
	MVII #4095,R0
	MVO R0,499
	SWAP R0
	MVO R0,503
	CLRR R0
	MVO R0,506
	;[1327] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1327
	RETURN
	ENDP
	;[1328] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1328
	;[1329] SonExplosionMax: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1329
	; SONEXPLOSIONMAX
label_SONEXPLOSIONMAX:	PROC
	BEGIN
	;[1330] 	SOUND 4,$1E,$0000
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1330
	MVII #30,R0
	MVO R0,505
	CLRR R0
	MVO R0,504
	;[1331] 	Sound 2,$0FA0,$30
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1331
	MVII #4000,R0
	MVO R0,498
	SWAP R0
	MVO R0,502
	MVII #48,R0
	MVO R0,509
	;[1332] 	Sound 3,$0FFF,$0000
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1332
	MVII #4095,R0
	MVO R0,499
	SWAP R0
	MVO R0,503
	CLRR R0
	MVO R0,506
	;[1333] 	end	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1333
	RETURN
	ENDP
	;[1334] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1334
	;[1335] SonValidation: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1335
	; SONVALIDATION
label_SONVALIDATION:	PROC
	BEGIN
	;[1336] 	SOUND 4,$15,$0038
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1336
	MVII #21,R0
	MVO R0,505
	MVII #56,R0
	MVO R0,504
	;[1337] 	Sound 2,$00c8,$30
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1337
	MVII #200,R0
	MVO R0,498
	SWAP R0
	MVO R0,502
	MVII #48,R0
	MVO R0,509
	;[1338] 	Sound 3,$0FFF,$0000
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1338
	MVII #4095,R0
	MVO R0,499
	SWAP R0
	MVO R0,503
	CLRR R0
	MVO R0,506
	;[1339] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1339
	RETURN
	ENDP
	;[1340] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1340
	;[1341] '///// Gestion IA /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1341
	;[1342] GestionIA: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1342
	; GESTIONIA
label_GESTIONIA:	PROC
	BEGIN
	;[1343] 	'Couleur Unité / Vitesse	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1343
	;[1344] 	f=SpeedIA(IDIA)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1344
	MVII #array_SPEEDIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	MVO R0,var_F
	;[1345] 	if i=3 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1345
	MVI var_I,R0
	CMPI #3,R0
	BNE T328
	;[1346] 		if f=5 then #couleur3=SPR_TAN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1346
	MVI var_F,R0
	CMPI #5,R0
	BNE T329
	MVII #3,R0
	MVO R0,var_&COULEUR3
T329:
	;[1347] 		if f=4 then #couleur3=SPR_YELLOW 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1347
	MVI var_F,R0
	CMPI #4,R0
	BNE T330
	MVII #6,R0
	MVO R0,var_&COULEUR3
T330:
	;[1348] 		if f=3 then #couleur3=SPR_RED 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1348
	MVI var_F,R0
	CMPI #3,R0
	BNE T331
	MVII #2,R0
	MVO R0,var_&COULEUR3
T331:
	;[1349] 		if f=2 then #couleur3=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1349
	MVI var_F,R0
	CMPI #2,R0
	BNE T332
	MVII #7,R0
	MVO R0,var_&COULEUR3
T332:
	;[1350] 		if f=1 then #couleur3=SPR_BLACK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1350
	MVI var_F,R0
	CMPI #1,R0
	BNE T333
	CLRR R0
	MVO R0,var_&COULEUR3
T333:
	;[1351] 	elseif i=4 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1351
	B T334
T328:
	MVI var_I,R0
	CMPI #4,R0
	BNE T335
	;[1352] 		if f=5 then #couleur4=SPR_TAN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1352
	MVI var_F,R0
	CMPI #5,R0
	BNE T336
	MVII #3,R0
	MVO R0,var_&COULEUR4
T336:
	;[1353] 		if f=4 then #couleur4=SPR_YELLOW 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1353
	MVI var_F,R0
	CMPI #4,R0
	BNE T337
	MVII #6,R0
	MVO R0,var_&COULEUR4
T337:
	;[1354] 		if f=3 then #couleur4=SPR_RED 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1354
	MVI var_F,R0
	CMPI #3,R0
	BNE T338
	MVII #2,R0
	MVO R0,var_&COULEUR4
T338:
	;[1355] 		if f=2 then #couleur4=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1355
	MVI var_F,R0
	CMPI #2,R0
	BNE T339
	MVII #7,R0
	MVO R0,var_&COULEUR4
T339:
	;[1356] 		if f=1 then #couleur4=SPR_BLACK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1356
	MVI var_F,R0
	CMPI #1,R0
	BNE T340
	CLRR R0
	MVO R0,var_&COULEUR4
T340:
	;[1357] 	elseif i=5 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1357
	B T334
T335:
	MVI var_I,R0
	CMPI #5,R0
	BNE T341
	;[1358] 		if f=5 then #couleur5=SPR_TAN
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1358
	MVI var_F,R0
	CMPI #5,R0
	BNE T342
	MVII #3,R0
	MVO R0,var_&COULEUR5
T342:
	;[1359] 		if f=4 then #couleur5=SPR_YELLOW 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1359
	MVI var_F,R0
	CMPI #4,R0
	BNE T343
	MVII #6,R0
	MVO R0,var_&COULEUR5
T343:
	;[1360] 		if f=3 then #couleur5=SPR_RED 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1360
	MVI var_F,R0
	CMPI #3,R0
	BNE T344
	MVII #2,R0
	MVO R0,var_&COULEUR5
T344:
	;[1361] 		if f=2 then #couleur5=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1361
	MVI var_F,R0
	CMPI #2,R0
	BNE T345
	MVII #7,R0
	MVO R0,var_&COULEUR5
T345:
	;[1362] 		if f=1 then #couleur5=SPR_BLACK
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1362
	MVI var_F,R0
	CMPI #1,R0
	BNE T346
	CLRR R0
	MVO R0,var_&COULEUR5
T346:
	;[1363] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1363
T334:
T341:
	;[1364] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1364
	;[1365] 	'Robot
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1365
	;[1366] 	if TypeIA(IDIA)=1 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1366
	MVII #array_TYPEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	CMPI #1,R0
	BNE T347
	;[1367] 		'Mort ?
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1367
	;[1368] 		if VisibleIA(IDIA)=2 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1368
	ADDI #(array_VISIBLEIA-array_TYPEIA) AND $FFFF,R3
	MVI@ R3,R0
	CMPI #2,R0
	BNE T348
	;[1369] 			TempoIA(IDIA)=TempoIA(IDIA)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1369
	ADDI #(array_TEMPOIA-array_VISIBLEIA) AND $FFFF,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
	;[1370] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1370
T348:
	;[1371] 		Tempo(i)=Tempo(i)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1371
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
	;[1372] 		if Tempo(i)>=SpeedIA(IDIA) then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1372
	MVI@ R3,R0
	MVII #array_SPEEDIA,R3
	ADD var_IDIA,R3
	CMP@ R3,R0
	BLT T349
	;[1373] 			TempoIA(IDIA)=TempoIA(IDIA)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1373
	ADDI #(array_TEMPOIA-array_SPEEDIA) AND $FFFF,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
	;[1374] 			f=TempoIA(IDIA)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1374
	MVI@ R3,R0
	MVO R0,var_F
	;[1375] 			Tempo(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1375
	CLRR R0
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1376] 			if f>2 then TempoIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1376
	MVI var_F,R0
	CMPI #2,R0
	BLE T350
	CLRR R0
	MVII #array_TEMPOIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
T350:
	;[1377] 			if f=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1377
	MVI var_F,R0
	CMPI #1,R0
	BNE T351
	;[1378] 				if i=3 then #SPR3=SPR13:#Miroir3=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1378
	MVI var_I,R0
	CMPI #3,R0
	BNE T352
	MVII #2152,R0
	MVO R0,var_&SPR3
	CLRR R0
	MVO R0,var_&MIROIR3
T352:
	;[1379] 				if i=4 then #SPR4=SPR13:#Miroir4=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1379
	MVI var_I,R0
	CMPI #4,R0
	BNE T353
	MVII #2152,R0
	MVO R0,var_&SPR4
	CLRR R0
	MVO R0,var_&MIROIR4
T353:
	;[1380] 				if i=5 then #SPR5=SPR13:#Miroir5=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1380
	MVI var_I,R0
	CMPI #5,R0
	BNE T354
	MVII #2152,R0
	MVO R0,var_&SPR5
	CLRR R0
	MVO R0,var_&MIROIR5
T354:
	;[1381] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1381
T351:
	;[1382] 			if f=2 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1382
	MVI var_F,R0
	CMPI #2,R0
	BNE T355
	;[1383] 				if i=3 then #SPR3=SPR14:#Miroir3=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1383
	MVI var_I,R0
	CMPI #3,R0
	BNE T356
	MVII #2160,R0
	MVO R0,var_&SPR3
	CLRR R0
	MVO R0,var_&MIROIR3
T356:
	;[1384] 				if i=4 then #SPR4=SPR14:#Miroir4=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1384
	MVI var_I,R0
	CMPI #4,R0
	BNE T357
	MVII #2160,R0
	MVO R0,var_&SPR4
	CLRR R0
	MVO R0,var_&MIROIR4
T357:
	;[1385] 				if i=5 then #SPR5=SPR14:#Miroir5=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1385
	MVI var_I,R0
	CMPI #5,R0
	BNE T358
	MVII #2160,R0
	MVO R0,var_&SPR5
	CLRR R0
	MVO R0,var_&MIROIR5
T358:
	;[1386] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1386
T355:
	;[1387] 			gosub PATHL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1387
	CALL label_PATHL
	;[1388] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1388
T349:
	;[1389] 	'Soldat
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1389
	;[1390] 	elseif TypeIA(IDIA)=2 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1390
	B T359
T347:
	MVII #array_TYPEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	CMPI #2,R0
	BNE T360
	;[1391] 		'Animation
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1391
	;[1392] 		Tempo(i)=Tempo(i)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1392
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
	;[1393] 		if Tempo(i)>=SpeedIA(IDIA)then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1393
	MVI@ R3,R0
	MVII #array_SPEEDIA,R3
	ADD var_IDIA,R3
	CMP@ R3,R0
	BLT T361
	;[1394] 			TempoIA(IDIA)=TempoIA(IDIA)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1394
	ADDI #(array_TEMPOIA-array_SPEEDIA) AND $FFFF,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
	;[1395] 			f=TempoIA(IDIA)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1395
	MVI@ R3,R0
	MVO R0,var_F
	;[1396] 			Tempo(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1396
	CLRR R0
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1397] 			gosub PATHL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1397
	CALL label_PATHL
	;[1398] 			if SensIA(IDIA)<2 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1398
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	CMPI #2,R0
	BGE T362
	;[1399] 				if f=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1399
	MVI var_F,R0
	CMPI #1,R0
	BNE T363
	;[1400] 					if i=3 then #SPR3=SPR03
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1400
	MVI var_I,R0
	CMPI #3,R0
	BNE T364
	MVII #2072,R0
	MVO R0,var_&SPR3
T364:
	;[1401] 					if i=4 then #SPR4=SPR03
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1401
	MVI var_I,R0
	CMPI #4,R0
	BNE T365
	MVII #2072,R0
	MVO R0,var_&SPR4
T365:
	;[1402] 					if i=5 then #SPR5=SPR03
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1402
	MVI var_I,R0
	CMPI #5,R0
	BNE T366
	MVII #2072,R0
	MVO R0,var_&SPR5
T366:
	;[1403] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1403
T363:
	;[1404] 				if f=2 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1404
	MVI var_F,R0
	CMPI #2,R0
	BNE T367
	;[1405] 					if i=3 then #SPR3=SPR04
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1405
	MVI var_I,R0
	CMPI #3,R0
	BNE T368
	MVII #2080,R0
	MVO R0,var_&SPR3
T368:
	;[1406] 					if i=4 then #SPR4=SPR04
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1406
	MVI var_I,R0
	CMPI #4,R0
	BNE T369
	MVII #2080,R0
	MVO R0,var_&SPR4
T369:
	;[1407] 					if i=5 then #SPR5=SPR04
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1407
	MVI var_I,R0
	CMPI #5,R0
	BNE T370
	MVII #2080,R0
	MVO R0,var_&SPR5
T370:
	;[1408] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1408
T367:
	;[1409] 				if f=3 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1409
	MVI var_F,R0
	CMPI #3,R0
	BNE T371
	;[1410] 					if i=3 then #SPR3=SPR05
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1410
	MVI var_I,R0
	CMPI #3,R0
	BNE T372
	MVII #2088,R0
	MVO R0,var_&SPR3
T372:
	;[1411] 					if i=4 then #SPR4=SPR05
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1411
	MVI var_I,R0
	CMPI #4,R0
	BNE T373
	MVII #2088,R0
	MVO R0,var_&SPR4
T373:
	;[1412] 					if i=5 then #SPR5=SPR05
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1412
	MVI var_I,R0
	CMPI #5,R0
	BNE T374
	MVII #2088,R0
	MVO R0,var_&SPR5
T374:
	;[1413] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1413
T371:
	;[1414] 				if f=4 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1414
	MVI var_F,R0
	CMPI #4,R0
	BNE T375
	;[1415] 					if i=3 then #SPR3=SPR06
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1415
	MVI var_I,R0
	CMPI #3,R0
	BNE T376
	MVII #2096,R0
	MVO R0,var_&SPR3
T376:
	;[1416] 					if i=4 then #SPR4=SPR06
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1416
	MVI var_I,R0
	CMPI #4,R0
	BNE T377
	MVII #2096,R0
	MVO R0,var_&SPR4
T377:
	;[1417] 					if i=5 then #SPR5=SPR06
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1417
	MVI var_I,R0
	CMPI #5,R0
	BNE T378
	MVII #2096,R0
	MVO R0,var_&SPR5
T378:
	;[1418] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1418
T375:
	;[1419] 				if f>4 then TempoIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1419
	MVI var_F,R0
	CMPI #4,R0
	BLE T379
	CLRR R0
	MVII #array_TEMPOIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
T379:
	;[1420] 				if SensIA(IDIA)=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1420
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	CMPI #1,R0
	BNE T380
	;[1421] 					if i=3 then #miroir3=FLIPX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1421
	MVI var_I,R0
	CMPI #3,R0
	BNE T381
	MVII #1024,R0
	MVO R0,var_&MIROIR3
T381:
	;[1422] 					if i=4 then #miroir4=FLIPX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1422
	MVI var_I,R0
	CMPI #4,R0
	BNE T382
	MVII #1024,R0
	MVO R0,var_&MIROIR4
T382:
	;[1423] 					if i=5 then #miroir5=FLIPX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1423
	MVI var_I,R0
	CMPI #5,R0
	BNE T383
	MVII #1024,R0
	MVO R0,var_&MIROIR5
T383:
	;[1424] 				else 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1424
	B T384
T380:
	;[1425] 					if i=3 then #miroir3=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1425
	MVI var_I,R0
	CMPI #3,R0
	BNE T385
	CLRR R0
	MVO R0,var_&MIROIR3
T385:
	;[1426] 					if i=4 then #miroir4=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1426
	MVI var_I,R0
	CMPI #4,R0
	BNE T386
	CLRR R0
	MVO R0,var_&MIROIR4
T386:
	;[1427] 					if i=5 then #miroir5=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1427
	MVI var_I,R0
	CMPI #5,R0
	BNE T387
	CLRR R0
	MVO R0,var_&MIROIR5
T387:
	;[1428] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1428
T384:
	;[1429] 			elseif SensIA(IDIA)=3 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1429
	B T388
T362:
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	CMPI #3,R0
	BNE T389
	;[1430] 				if f=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1430
	MVI var_F,R0
	CMPI #1,R0
	BNE T390
	;[1431] 					if i=3 then #SPR3=SPR07
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1431
	MVI var_I,R0
	CMPI #3,R0
	BNE T391
	MVII #2104,R0
	MVO R0,var_&SPR3
T391:
	;[1432] 					if i=4 then #SPR4=SPR07
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1432
	MVI var_I,R0
	CMPI #4,R0
	BNE T392
	MVII #2104,R0
	MVO R0,var_&SPR4
T392:
	;[1433] 					if i=5 then #SPR5=SPR07
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1433
	MVI var_I,R0
	CMPI #5,R0
	BNE T393
	MVII #2104,R0
	MVO R0,var_&SPR5
T393:
	;[1434] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1434
T390:
	;[1435] 				if f=2 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1435
	MVI var_F,R0
	CMPI #2,R0
	BNE T394
	;[1436] 					if i=3 then #SPR3=SPR08
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1436
	MVI var_I,R0
	CMPI #3,R0
	BNE T395
	MVII #2112,R0
	MVO R0,var_&SPR3
T395:
	;[1437] 					if i=4 then #SPR4=SPR08
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1437
	MVI var_I,R0
	CMPI #4,R0
	BNE T396
	MVII #2112,R0
	MVO R0,var_&SPR4
T396:
	;[1438] 					if i=5 then #SPR5=SPR08
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1438
	MVI var_I,R0
	CMPI #5,R0
	BNE T397
	MVII #2112,R0
	MVO R0,var_&SPR5
T397:
	;[1439] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1439
T394:
	;[1440] 				if f=3 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1440
	MVI var_F,R0
	CMPI #3,R0
	BNE T398
	;[1441] 					if i=3 then #SPR3=SPR09
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1441
	MVI var_I,R0
	CMPI #3,R0
	BNE T399
	MVII #2120,R0
	MVO R0,var_&SPR3
T399:
	;[1442] 					if i=4 then #SPR4=SPR09
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1442
	MVI var_I,R0
	CMPI #4,R0
	BNE T400
	MVII #2120,R0
	MVO R0,var_&SPR4
T400:
	;[1443] 					if i=5 then #SPR5=SPR09
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1443
	MVI var_I,R0
	CMPI #5,R0
	BNE T401
	MVII #2120,R0
	MVO R0,var_&SPR5
T401:
	;[1444] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1444
T398:
	;[1445] 				if f>3 then TempoIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1445
	MVI var_F,R0
	CMPI #3,R0
	BLE T402
	CLRR R0
	MVII #array_TEMPOIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
T402:
	;[1446] 				if i=3 then #miroir3=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1446
	MVI var_I,R0
	CMPI #3,R0
	BNE T403
	CLRR R0
	MVO R0,var_&MIROIR3
T403:
	;[1447] 				if i=4 then #miroir4=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1447
	MVI var_I,R0
	CMPI #4,R0
	BNE T404
	CLRR R0
	MVO R0,var_&MIROIR4
T404:
	;[1448] 				if i=5 then #miroir5=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1448
	MVI var_I,R0
	CMPI #5,R0
	BNE T405
	CLRR R0
	MVO R0,var_&MIROIR5
T405:
	;[1449] 			elseif SensIA(IDIA)=2 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1449
	B T388
T389:
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	CMPI #2,R0
	BNE T406
	;[1450] 				if f=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1450
	MVI var_F,R0
	CMPI #1,R0
	BNE T407
	;[1451] 					if i=3 then #SPR3=SPR10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1451
	MVI var_I,R0
	CMPI #3,R0
	BNE T408
	MVII #2128,R0
	MVO R0,var_&SPR3
T408:
	;[1452] 					if i=4 then #SPR4=SPR10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1452
	MVI var_I,R0
	CMPI #4,R0
	BNE T409
	MVII #2128,R0
	MVO R0,var_&SPR4
T409:
	;[1453] 					if i=5 then #SPR5=SPR10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1453
	MVI var_I,R0
	CMPI #5,R0
	BNE T410
	MVII #2128,R0
	MVO R0,var_&SPR5
T410:
	;[1454] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1454
T407:
	;[1455] 				if f=2 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1455
	MVI var_F,R0
	CMPI #2,R0
	BNE T411
	;[1456] 					if i=3 then #SPR3=SPR11
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1456
	MVI var_I,R0
	CMPI #3,R0
	BNE T412
	MVII #2136,R0
	MVO R0,var_&SPR3
T412:
	;[1457] 					if i=4 then #SPR4=SPR11
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1457
	MVI var_I,R0
	CMPI #4,R0
	BNE T413
	MVII #2136,R0
	MVO R0,var_&SPR4
T413:
	;[1458] 					if i=5 then #SPR5=SPR11
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1458
	MVI var_I,R0
	CMPI #5,R0
	BNE T414
	MVII #2136,R0
	MVO R0,var_&SPR5
T414:
	;[1459] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1459
T411:
	;[1460] 				if f=3 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1460
	MVI var_F,R0
	CMPI #3,R0
	BNE T415
	;[1461] 					if i=3 then #SPR3=SPR12
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1461
	MVI var_I,R0
	CMPI #3,R0
	BNE T416
	MVII #2144,R0
	MVO R0,var_&SPR3
T416:
	;[1462] 					if i=4 then #SPR4=SPR12
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1462
	MVI var_I,R0
	CMPI #4,R0
	BNE T417
	MVII #2144,R0
	MVO R0,var_&SPR4
T417:
	;[1463] 					if i=5 then #SPR5=SPR12
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1463
	MVI var_I,R0
	CMPI #5,R0
	BNE T418
	MVII #2144,R0
	MVO R0,var_&SPR5
T418:
	;[1464] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1464
T415:
	;[1465] 				if f>3 then TempoIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1465
	MVI var_F,R0
	CMPI #3,R0
	BLE T419
	CLRR R0
	MVII #array_TEMPOIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
T419:
	;[1466] 				if i=3 then #miroir3=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1466
	MVI var_I,R0
	CMPI #3,R0
	BNE T420
	CLRR R0
	MVO R0,var_&MIROIR3
T420:
	;[1467] 				if i=4 then #miroir4=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1467
	MVI var_I,R0
	CMPI #4,R0
	BNE T421
	CLRR R0
	MVO R0,var_&MIROIR4
T421:
	;[1468] 				if i=5 then #miroir5=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1468
	MVI var_I,R0
	CMPI #5,R0
	BNE T422
	CLRR R0
	MVO R0,var_&MIROIR5
T422:
	;[1469] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1469
T388:
T406:
	;[1470] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1470
T361:
	;[1471] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1471
T359:
T360:
	;[1472] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1472
	RETURN
	ENDP
	;[1473] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1473
	;[1474] PATHL: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1474
	; PATHL
label_PATHL:	PROC
	BEGIN
	;[1475] 	if StartGame and MortPapi=0 then gosub PathIA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1475
	MVI var_STARTGAME,R0
	MVI var_MORTPAPI,R1
	TSTR R1
	MVII #65535,R1
	BEQ T424
	INCR R1
T424:
	ANDR R1,R0
	BEQ T423
	CALL label_PATHIA
T423:
	;[1476] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1476
	RETURN
	ENDP
	;[1477] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1477
	;[1478] IATri: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1478
	; IATRI
label_IATRI:	PROC
	BEGIN
	;[1479] 	VieSoldat(IDIA)=RANDOM(5):if VieSoldat(IDIA)=0 then VieSoldat(IDIA)=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1479
	MVII #5,R1
	CALL _next_random
	CALL qs_mpy8
	SWAP R0
	MVII #array_VIESOLDAT,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
	MVI@ R3,R0
	TSTR R0
	BNE T425
	MVII #1,R0
	MVO@ R0,R3
T425:
	;[1480] 	SpeedIA(IDIA)=VieSoldat(IDIA):if SpeedIA(IDIA)=0 then SpeedIA(IDIA)=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1480
	MVII #array_VIESOLDAT,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	ADDI #(array_SPEEDIA-array_VIESOLDAT) AND $FFFF,R3
	MVO@ R0,R3
	MVI@ R3,R0
	TSTR R0
	BNE T426
	MVII #1,R0
	MVO@ R0,R3
T426:
	;[1481] 	TypeIA(IDIA)=RANDOM(3):if TypeIA(IDIA)=0 then TypeIA(IDIA)=1:if TypeIA(IDIA)>2 then TypeIA(IDIA)=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1481
	MVII #3,R1
	CALL _next_random
	CALL qs_mpy8
	SWAP R0
	MVII #array_TYPEIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
	MVI@ R3,R0
	TSTR R0
	BNE T427
	MVII #1,R0
	MVO@ R0,R3
	MVI@ R3,R0
	CMPI #2,R0
	BLE T428
	MVII #2,R0
	MVO@ R0,R3
T428:
T427:
	;[1482] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1482
	RETURN
	ENDP
	;[1483] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1483
	;[1484] '///// Update MAP Information ITEM /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1484
	;[1485] UIA: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1485
	; UIA
label_UIA:	PROC
	BEGIN
	;[1486] 	CoordY(i)=offset_y
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1486
	MVI var_OFFSET_Y,R0
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1487] 	Coordx(i)=c*8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1487
	MVI var_C,R0
	SLL R0,2
	ADDR R0,R0
	ADDI #(array_COORDX-array_COORDY) AND $FFFF,R3
	MVO@ R0,R3
	;[1488] 	TempoIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1488
	CLRR R0
	MVII #array_TEMPOIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
	;[1489] 	Tempo(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1489
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1490] 	#Visible=Visible
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1490
	MVII #512,R0
	MVO R0,var_&VISIBLE
	;[1491] 	c=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1491
	CLRR R0
	MVO R0,var_C
	;[1492] 	Gosub IATri
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1492
	CALL label_IATRI
	;[1493] 	VisibleIA(IDIA)=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1493
	MVII #1,R0
	MVII #array_VISIBLEIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
	;[1494] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1494
	RETURN
	ENDP
	;[1495] UpdateIA: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1495
	; UPDATEIA
label_UPDATEIA:	PROC
	BEGIN
	;[1496] 	Tempo(i)=Tempo(i)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1496
	MVII #array_TEMPO,R3
	ADD var_I,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
	;[1497] 	if Tempo(i)>50 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1497
	MVI@ R3,R0
	CMPI #50,R0
	BLE T429
	;[1498] 		Tempo(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1498
	CLRR R0
	MVO@ R0,R3
	;[1499] 		c=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1499
	MVO R0,var_C
	;[1500] 		for j=0 to MAP_LARGEUR
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1500
	NOP
	MVO R0,var_J
T430:
	;[1501] 			c=c+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1501
	MVI var_C,R0
	INCR R0
	MVO R0,var_C
	;[1502] 			value=Zone1((MAP_LARGEUR*(position_y_zone))+j)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1502
	MVII #label_ZONE1,R1
	MVI var_POSITION_Y_ZONE,R2
	MULT R2,R4,20
	ADD var_J,R2
	ADDR R2,R1
	MVI@ R1,R0
	MVO R0,var_VALUE
	;[1503] 			if value=27 and RANDOM(100)<75  then gosub UIA:exit for
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1503
	MVII #100,R1
	CALL _next_random
	CALL qs_mpy8
	SWAP R0
	ANDI #255,R0
	CMPI #75,R0
	MVII #65535,R0
	BLT T432
	INCR R0
T432:
	MVI var_VALUE,R1
	CMPI #27,R1
	MVII #65535,R1
	BEQ T433
	INCR R1
T433:
	ANDR R1,R0
	BEQ T431
	CALL label_UIA
	B T434
T431:
	;[1504] 			if value=0 and RANDOM(100)<75 then gosub UIA:exit for
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1504
	MVII #100,R1
	CALL _next_random
	CALL qs_mpy8
	SWAP R0
	ANDI #255,R0
	CMPI #75,R0
	MVII #65535,R0
	BLT T436
	INCR R0
T436:
	MVI var_VALUE,R1
	TSTR R1
	MVII #65535,R1
	BEQ T437
	INCR R1
T437:
	ANDR R1,R0
	BEQ T435
	CALL label_UIA
	B T434
T435:
	;[1505] 		next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1505
	MVI var_J,R0
	INCR R0
	MVO R0,var_J
	CMPI #20,R0
	BLE T430
T434:
	;[1506] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1506
T429:
	;[1507] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1507
	RETURN
	ENDP
	;[1508] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1508
	;[1509] PATHO: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1509
	; PATHO
label_PATHO:	PROC
	BEGIN
	;[1510] 	#offset = COFX/8+COFY/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1510
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1511] 	#COL_TEST = PEEK($0200+#offset)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1511
	MOVR R0,R1
	ADDI #512,R1
	MVI@ R1,R0
	MVO R0,var_&COL_TEST
	;[1512] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1512
	RETURN
	ENDP
	;[1513] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1513
	;[1514] PathIA: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1514
	; PATHIA
label_PATHIA:	PROC
	BEGIN
	;[1515] 	IF (Coordx(i) AND 7)+((Coordy(i)-offset_y) AND 7) THEN GOTO loop_moving2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1515
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	ANDI #7,R0
	ADDI #(array_COORDY-array_COORDX) AND $FFFF,R3
	MVI@ R3,R1
	SUB var_OFFSET_Y,R1
	ANDI #7,R1
	ADDR R1,R0
	BNE label_LOOP_MOVING2
	;[1516] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1516
	;[1517] 	if (FRAME and RANDOM(32))=0 then gosub SEARCHIA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1517
	CALL _next_random
	ANDI #31,R0
	MVI _frame,R1
	ANDR R1,R0
	BNE T439
	CALL label_SEARCHIA
T439:
	;[1518] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1518
	;[1519] 	MoveIA(IDIA)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1519
	CLRR R0
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
	;[1520] 	'D
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1520
	;[1521] 	COFX=Coordx(i)-1+8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1521
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	ADDI #7,R0
	MVO R0,var_COFX
	;[1522] 	COFY=CoordY(i)-offset_y-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1522
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	SUBI #2,R0
	MVO R0,var_COFY
	;[1523] 	gosub PATHO
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1523
	CALL label_PATHO
	;[1524] 	gosub TestCOL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1524
	CALL label_TESTCOL
	;[1525] 	if d=0 then MoveIA(IDIA)=MoveIA(IDIA)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1525
	MVI var_D,R0
	TSTR R0
	BNE T440
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
T440:
	;[1526] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1526
	;[1527] 	'G
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1527
	;[1528] 	COFX=Coordx(i)-16
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1528
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #16,R0
	MVO R0,var_COFX
	;[1529] 	gosub PATHO
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1529
	CALL label_PATHO
	;[1530] 	gosub TestCOL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1530
	CALL label_TESTCOL
	;[1531] 	if d=0 then MoveIA(IDIA)=MoveIA(IDIA)+2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1531
	MVI var_D,R0
	TSTR R0
	BNE T441
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	ADDI #2,R0
	MVO@ R0,R3
T441:
	;[1532] 	'B
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1532
	;[1533] 	COFX=Coordx(i)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1533
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	MVO R0,var_COFX
	;[1534] 	COFY=CoordY(i)-offset_y-16
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1534
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_OFFSET_Y,R0
	SUBI #16,R0
	MVO R0,var_COFY
	;[1535] 	gosub PATHO
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1535
	CALL label_PATHO
	;[1536] 	gosub TestCOL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1536
	CALL label_TESTCOL
	;[1537] 	if d=0 then MoveIA(IDIA)=MoveIA(IDIA)+4
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1537
	MVI var_D,R0
	TSTR R0
	BNE T442
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	ADDI #4,R0
	MVO@ R0,R3
T442:
	;[1538] 	'H
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1538
	;[1539] 	COFX=Coordx(i)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1539
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	MVO R0,var_COFX
	;[1540] 	COFY=CoordY(i)-3-offset_y+8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1540
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #3,R0
	SUB var_OFFSET_Y,R0
	ADDI #8,R0
	MVO R0,var_COFY
	;[1541] 	gosub PATHO
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1541
	CALL label_PATHO
	;[1542] 	gosub TestCOL
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1542
	CALL label_TESTCOL
	;[1543] 	if d=0 then MoveIA(IDIA)=MoveIA(IDIA)+8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1543
	MVI var_D,R0
	TSTR R0
	BNE T443
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	ADDI #8,R0
	MVO@ R0,R3
T443:
	;[1544] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1544
	;[1545] 	ON SensIA(IDIA) GOTO loop_moving_horiz,loop_moving_horiz,loop_moving_vert,loop_moving_vert
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1545
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R1
	CMPI #4,R1
	BC T445
	ADDI #T444,R1
	MVI@ R1,PC
T444:
	DECLE label_LOOP_MOVING_HORIZ
	DECLE label_LOOP_MOVING_HORIZ
	DECLE label_LOOP_MOVING_VERT
	DECLE label_LOOP_MOVING_VERT
T445:
	;[1546] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1546
	;[1547] 	' If it was moving horizontally, go now for vertical
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1547
	;[1548] loop_moving_horiz:
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1548
	; LOOP_MOVING_HORIZ
label_LOOP_MOVING_HORIZ:	;[1549] 	IF abs(Coordy(0)-Coordy(i)-offset_y)<=RANDOM(96) AND (MoveIA(IDIA) AND 3) = 0 THEN GOTO loop_moving2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1549
	MVII #96,R1
	CALL _next_random
	CALL qs_mpy8
	SWAP R0
	ANDI #255,R0
	MVI array_COORDY,R1
	MVII #array_COORDY,R3
	ADD var_I,R3
	SUB@ R3,R1
	SUB var_OFFSET_Y,R1
	BPL T447
	NEGR R1
T447:
	CMPR R1,R0
	MVII #65535,R0
	BGE T448
	INCR R0
T448:
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R1
	ANDI #3,R1
	MVII #65535,R1
	BEQ T449
	INCR R1
T449:
	ANDR R1,R0
	BNE label_LOOP_MOVING2
	;[1550] 	IF Coordy(0) < Coordy(i)-offset_y THEN GOTO loop_moving_horiz_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1550
	MVI array_COORDY,R0
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R1
	SUB var_OFFSET_Y,R1
	CMPR R1,R0
	BLT label_LOOP_MOVING_HORIZ_1
	;[1551] 	IF (MoveIA(IDIA) AND 8) = 0 THEN SensIA(IDIA) = 3:GOTO loop_moving2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1551
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	ANDI #8,R0
	BNE T451
	MVII #3,R0
	ADDI #(array_SENSIA-array_MOVEIA) AND $FFFF,R3
	MVO@ R0,R3
	B label_LOOP_MOVING2
T451:
	;[1552] loop_moving_horiz_1:
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1552
	; LOOP_MOVING_HORIZ_1
label_LOOP_MOVING_HORIZ_1:	;[1553] 	IF (MoveIA(IDIA) AND 4) = 0 THEN SensIA(IDIA) = 2:GOTO loop_moving2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1553
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	ANDI #4,R0
	BNE T452
	MVII #2,R0
	ADDI #(array_SENSIA-array_MOVEIA) AND $FFFF,R3
	MVO@ R0,R3
	B label_LOOP_MOVING2
T452:
	;[1554] 	IF (MoveIA(IDIA) AND 8) = 0 THEN SensIA(IDIA) = 3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1554
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	ANDI #8,R0
	BNE T453
	MVII #3,R0
	ADDI #(array_SENSIA-array_MOVEIA) AND $FFFF,R3
	MVO@ R0,R3
T453:
	;[1555] 	GOTO loop_moving2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1555
	B label_LOOP_MOVING2
	;[1556] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1556
	;[1557] 	' If it was moving vertically, go now for horizontal
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1557
	;[1558] loop_moving_vert:
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1558
	; LOOP_MOVING_VERT
label_LOOP_MOVING_VERT:	;[1559] 	IF abs(Coordx(0) - Coordx(i))<=RANDOM(96) AND (MoveIA(IDIA) AND 12) = 0 THEN GOTO loop_moving2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1559
	MVII #96,R1
	CALL _next_random
	CALL qs_mpy8
	SWAP R0
	ANDI #255,R0
	MVI array_COORDX,R1
	MVII #array_COORDX,R3
	ADD var_I,R3
	SUB@ R3,R1
	BPL T455
	NEGR R1
T455:
	CMPR R1,R0
	MVII #65535,R0
	BGE T456
	INCR R0
T456:
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R1
	ANDI #12,R1
	MVII #65535,R1
	BEQ T457
	INCR R1
T457:
	ANDR R1,R0
	BNE label_LOOP_MOVING2
	;[1560] 	IF Coordx(0) <  Coordx(i) THEN GOTO loop_moving_vert_1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1560
	MVI array_COORDX,R0
	MVII #array_COORDX,R3
	ADD var_I,R3
	CMP@ R3,R0
	BLT label_LOOP_MOVING_VERT_1
	;[1561] 	IF (MoveIA(IDIA) AND 1) = 0 THEN SensIA(IDIA) = 0:GOTO loop_moving2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1561
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	ANDI #1,R0
	BNE T459
	ADDI #(array_SENSIA-array_MOVEIA) AND $FFFF,R3
	MVO@ R0,R3
	B label_LOOP_MOVING2
T459:
	;[1562] loop_moving_vert_1:
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1562
	; LOOP_MOVING_VERT_1
label_LOOP_MOVING_VERT_1:	;[1563] 	IF (MoveIA(IDIA) AND 2) = 0 THEN SensIA(IDIA) = 1:GOTO loop_moving2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1563
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	ANDI #2,R0
	BNE T460
	MVII #1,R0
	ADDI #(array_SENSIA-array_MOVEIA) AND $FFFF,R3
	MVO@ R0,R3
	B label_LOOP_MOVING2
T460:
	;[1564] 	IF (MoveIA(IDIA) AND 1) = 0 THEN SensIA(IDIA) = 0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1564
	MVII #array_MOVEIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	ANDI #1,R0
	BNE T461
	ADDI #(array_SENSIA-array_MOVEIA) AND $FFFF,R3
	MVO@ R0,R3
T461:
	;[1565] 	GOTO loop_moving2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1565
	B label_LOOP_MOVING2
	;[1566] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1566
	;[1567] loop_moving2:
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1567
	; LOOP_MOVING2
label_LOOP_MOVING2:	;[1568] 		if Coordy(i)>100-offset_y then VisibleIA(IDIA)=0:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1568
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	MVII #100,R1
	SUB var_OFFSET_Y,R1
	CMPR R1,R0
	BLE T462
	CLRR R0
	MVII #array_VISIBLEIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
	RETURN
T462:
	;[1569] 		IF SensIA(IDIA) = 0 THEN Coordx(i) = Coordx(i) +1'D
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1569
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	TSTR R0
	BNE T463
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
T463:
	;[1570] 		IF SensIA(IDIA) = 1 THEN Coordx(i) = Coordx(i) - 1 'G
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1570
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	CMPI #1,R0
	BNE T464
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	DECR R0
	MVO@ R0,R3
T464:
	;[1571] 		IF SensIA(IDIA)= 2 THEN Coordy(i) = Coordy(i) - 1  'b
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1571
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	CMPI #2,R0
	BNE T465
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	DECR R0
	MVO@ R0,R3
T465:
	;[1572] 		IF SensIA(IDIA) = 3 THEN Coordy(i) = Coordy(i) +1 'h
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1572
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVI@ R3,R0
	CMPI #3,R0
	BNE T466
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	INCR R0
	MVO@ R0,R3
T466:
	;[1573] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1573
	RETURN
	ENDP
	;[1574] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1574
	;[1575] SEARCHIA: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1575
	; SEARCHIA
label_SEARCHIA:	PROC
	BEGIN
	;[1576] 	if abs(Coordx(0)-Coordx(i))<RANDOM(48) then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1576
	MVII #48,R1
	CALL _next_random
	CALL qs_mpy8
	SWAP R0
	ANDI #255,R0
	MVI array_COORDX,R1
	MVII #array_COORDX,R3
	ADD var_I,R3
	SUB@ R3,R1
	BPL T468
	NEGR R1
T468:
	CMPR R1,R0
	BLE T467
	;[1577] 		if Coordy(0)>=Coordy(i) then SensIA(IDIA)=3:return else SensIA(IDIA)=2:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1577
	MVI array_COORDY,R0
	MVII #array_COORDY,R3
	ADD var_I,R3
	CMP@ R3,R0
	BLT T469
	MVII #3,R0
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
	RETURN
	B T470
T469:
	MVII #2,R0
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
	RETURN
T470:
	;[1578] 	elseif abs(Coordy(0)-Coordy(i))<RANDOM(48) then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1578
	B T471
T467:
	MVII #48,R1
	CALL _next_random
	CALL qs_mpy8
	SWAP R0
	ANDI #255,R0
	MVI array_COORDY,R1
	MVII #array_COORDY,R3
	ADD var_I,R3
	SUB@ R3,R1
	BPL T473
	NEGR R1
T473:
	CMPR R1,R0
	BLE T472
	;[1579] 		if Coordx(0)>=Coordx(i) then SensIA(IDIA)=0:return else SensIA(IDIA)=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1579
	MVI array_COORDX,R0
	MVII #array_COORDX,R3
	ADD var_I,R3
	CMP@ R3,R0
	BLT T474
	CLRR R0
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
	RETURN
	B T475
T474:
	MVII #1,R0
	MVII #array_SENSIA,R3
	ADD var_IDIA,R3
	MVO@ R0,R3
T475:
	;[1580] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1580
T471:
T472:
	;[1581] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1581
	RETURN
	ENDP
	;[1582] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1582
	;[1583] '///// Gestion Bunker /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1583
	;[1584] GestionBunker: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1584
	; GESTIONBUNKER
label_GESTIONBUNKER:	PROC
	BEGIN
	;[1585] 	if MortPapi then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1585
	MVI var_MORTPAPI,R0
	TSTR R0
	BEQ T476
	RETURN
T476:
	;[1586] 	if TirIA then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1586
	MVI var_TIRIA,R0
	TSTR R0
	BEQ T477
	RETURN
T477:
	;[1587] 	f=CoordY(0)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1587
	MVI array_COORDY,R0
	MVO R0,var_F
	;[1588] 	e=CoordX(0)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1588
	MVI array_COORDX,R0
	MVO R0,var_E
	;[1589] 	'Gestion des différents Bunker rencontrés.
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1589
	;[1590] 	if NBBUNKER then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1590
	MVI var_NBBUNKER,R0
	TSTR R0
	BEQ T478
	;[1591] 		for i=1 to eBunker
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1591
	MVII #1,R0
	MVO R0,var_I
T479:
	;[1592] 			if (f>IABUNKER_CY(i)+8) and IDB(i) and DieBunker(i)=0 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1592
	MVI var_F,R0
	MVII #array_IABUNKER_CY,R3
	ADD var_I,R3
	MVI@ R3,R1
	ADDI #8,R1
	CMPR R1,R0
	MVII #65535,R0
	BGT T481
	INCR R0
T481:
	MVII #array_IDB,R3
	ADD var_I,R3
	AND@ R3,R0
	ADDI #(array_DIEBUNKER-array_IDB) AND $FFFF,R3
	MVI@ R3,R1
	TSTR R1
	MVII #65535,R1
	BEQ T482
	INCR R1
T482:
	ANDR R1,R0
	BEQ T480
	;[1593] 				if abs(f-IABUNKER_CY(i))<55 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1593
	MVI var_F,R0
	MVII #array_IABUNKER_CY,R3
	ADD var_I,R3
	SUB@ R3,R0
	BPL T484
	NEGR R0
T484:
	CMPI #55,R0
	BGE T483
	;[1594] 					if abs(e-IABUNKER_CX(i))<40 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1594
	MVI var_E,R0
	MVII #array_IABUNKER_CX,R3
	ADD var_I,R3
	SUB@ R3,R0
	BPL T486
	NEGR R0
T486:
	CMPI #40,R0
	BGE T485
	;[1595] 						ID=i
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1595
	MVI var_I,R0
	MVO R0,var_ID
	;[1596] 						exit for
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1596
	B T487
	;[1597] 					end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1597
T485:
	;[1598] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1598
T483:
	;[1599] 			else
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1599
	B T488
T480:
	;[1600] 				ID=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1600
	CLRR R0
	MVO R0,var_ID
	;[1601] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1601
T488:
	;[1602] 		next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1602
	MVI var_I,R0
	INCR R0
	MVO R0,var_I
	CMP var_EBUNKER,R0
	BLE T479
T487:
	;[1603] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1603
T478:
	;[1604] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1604
	;[1605] 	if ID and bT=0 then DEFINE DEF57,1,IABUNKER_M:wait:bT=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1605
	MVI var_ID,R0
	MVI var_BT,R1
	TSTR R1
	MVII #65535,R1
	BEQ T490
	INCR R1
T490:
	ANDR R1,R0
	BEQ T489
	MVII #57,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_IABUNKER_M,R0
	MVO R0,_gram_bitmap
	CALL _wait
	MVII #1,R0
	MVO R0,var_BT
T489:
	;[1606] 	if ID=0 and bT=1 then DEFINE DEF57,1,BUNKER_M:wait:bT=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1606
	MVI var_ID,R0
	TSTR R0
	MVII #65535,R0
	BEQ T492
	INCR R0
T492:
	MVI var_BT,R1
	CMPI #1,R1
	MVII #65535,R1
	BEQ T493
	INCR R1
T493:
	ANDR R1,R0
	BEQ T491
	MVII #57,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BUNKER_M,R0
	MVO R0,_gram_bitmap
	CALL _wait
	CLRR R0
	MVO R0,var_BT
T491:
	;[1607] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1607
	;[1608] 	if ID=0 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1608
	MVI var_ID,R0
	TSTR R0
	BNE T494
	RETURN
T494:
	;[1609] 	if DieBunker(ID) then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1609
	MVII #array_DIEBUNKER,R3
	ADD var_ID,R3
	MVI@ R3,R0
	TSTR R0
	BEQ T495
	RETURN
T495:
	;[1610] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1610
	;[1611] 	'Pos X du Tireur / Ciblage
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1611
	;[1612] 	Tempo(9)=Tempo(9)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1612
	MVI array_TEMPO+9,R0
	INCR R0
	MVO R0,array_TEMPO+9
	;[1613] 	if Tempo(9)>30 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1613
	MVI array_TEMPO+9,R0
	CMPI #30,R0
	BLE T496
	;[1614] 		Tempo(9)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1614
	CLRR R0
	MVO R0,array_TEMPO+9
	;[1615] 		PosBunker=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1615
	MVO R0,var_POSBUNKER
	;[1616] 		'Animation du Ciblage & Direction Tir IA.
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1616
	;[1617] 		if e<(IABUNKER_CX(ID)-8) then SensTirBunker(ID)=1:PosBunker=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1617
	MVI var_E,R0
	MVII #array_IABUNKER_CX,R3
	ADD var_ID,R3
	MVI@ R3,R1
	SUBI #8,R1
	CMPR R1,R0
	BGE T497
	MVII #1,R0
	ADDI #(array_SENSTIRBUNKER-array_IABUNKER_CX) AND $FFFF,R3
	MVO@ R0,R3
	MVO R0,var_POSBUNKER
T497:
	;[1618] 		if (e>=(IABUNKER_CX(ID)-8) and e<=IABUNKER_CX(ID)+8) then SensTirBunker(ID)=0:PosBunker=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1618
	MVI var_E,R0
	MVII #array_IABUNKER_CX,R3
	ADD var_ID,R3
	MVI@ R3,R1
	SUBI #8,R1
	CMPR R1,R0
	MVII #65535,R0
	BGE T499
	INCR R0
T499:
	MVI var_E,R1
	MVII #array_IABUNKER_CX,R3
	ADD var_ID,R3
	MVI@ R3,R2
	ADDI #8,R2
	CMPR R2,R1
	MVII #65535,R1
	BLE T500
	INCR R1
T500:
	ANDR R1,R0
	BEQ T498
	CLRR R0
	MVII #array_SENSTIRBUNKER,R3
	ADD var_ID,R3
	MVO@ R0,R3
	MVII #1,R0
	MVO R0,var_POSBUNKER
T498:
	;[1619] 		if e>IABUNKER_CX(ID)+8 then SensTirBunker(ID)=2:PosBunker=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1619
	MVI var_E,R0
	MVII #array_IABUNKER_CX,R3
	ADD var_ID,R3
	MVI@ R3,R1
	ADDI #8,R1
	CMPR R1,R0
	BLE T501
	MVII #2,R0
	ADDI #(array_SENSTIRBUNKER-array_IABUNKER_CX) AND $FFFF,R3
	MVO@ R0,R3
	MVII #1,R0
	MVO R0,var_POSBUNKER
T501:
	;[1620] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1620
T496:
	;[1621] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1621
	;[1622] 	if PosBunker then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1622
	MVI var_POSBUNKER,R0
	TSTR R0
	BEQ T502
	;[1623] 		'Choix du Tir IA 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1623
	;[1624] 		#Visible=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1624
	CLRR R0
	MVO R0,var_&VISIBLE
	;[1625] 		if (abs(f-IABUNKER_CY(ID)))<60 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1625
	MVI var_F,R0
	MVII #array_IABUNKER_CY,R3
	ADD var_ID,R3
	SUB@ R3,R0
	BPL T504
	NEGR R0
T504:
	CMPI #60,R0
	BGE T503
	;[1626] 			if (abs(e-IABUNKER_CX(ID)))<55 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1626
	MVI var_E,R0
	MVII #array_IABUNKER_CX,R3
	ADD var_ID,R3
	SUB@ R3,R0
	BPL T506
	NEGR R0
T506:
	CMPI #55,R0
	BGE T505
	;[1627] 				TirIA=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1627
	MVII #1,R0
	MVO R0,var_TIRIA
	;[1628] 				Gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1628
	CALL label_SONBLANK
	;[1629] 				gosub SonTir
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1629
	CALL label_SONTIR
	;[1630] 				CoordX(2)=IABUNKER_CX(ID)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1630
	MVII #array_IABUNKER_CX,R3
	ADD var_ID,R3
	MVI@ R3,R0
	MVO R0,array_COORDX+2
	;[1631] 				CoordY(2)=IABUNKER_CY(ID)+4
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1631
	MVII #array_IABUNKER_CY,R3
	ADD var_ID,R3
	MVI@ R3,R0
	ADDI #4,R0
	MVO R0,array_COORDY+2
	;[1632] 				if SensTirBunker(ID)=1 then DEFINE DEF57,1,IABUNKER_G:wait:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1632
	MVII #array_SENSTIRBUNKER,R3
	ADD var_ID,R3
	MVI@ R3,R0
	CMPI #1,R0
	BNE T507
	MVII #57,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_IABUNKER_G,R0
	MVO R0,_gram_bitmap
	CALL _wait
	RETURN
T507:
	;[1633] 				if SensTirBunker(ID)=0 then DEFINE DEF57,1,IABUNKER_M:wait:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1633
	MVII #array_SENSTIRBUNKER,R3
	ADD var_ID,R3
	MVI@ R3,R0
	TSTR R0
	BNE T508
	MVII #57,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_IABUNKER_M,R0
	MVO R0,_gram_bitmap
	CALL _wait
	RETURN
T508:
	;[1634] 				if SensTirBunker(ID)=2 then DEFINE DEF57,1,IABUNKER_D:wait:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1634
	MVII #array_SENSTIRBUNKER,R3
	ADD var_ID,R3
	MVI@ R3,R0
	CMPI #2,R0
	BNE T509
	MVII #57,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_IABUNKER_D,R0
	MVO R0,_gram_bitmap
	CALL _wait
	RETURN
T509:
	;[1635] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1635
T505:
	;[1636] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1636
T503:
	;[1637] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1637
T502:
	;[1638] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1638
	;[1639] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1639
	RETURN
	ENDP
	;[1640] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1640
	;[1641] '///// Affichage de la Zone de jeu sur l'écran complet /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1641
	;[1642] DisplayZoneScreen: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1642
	; DISPLAYZONESCREEN
label_DISPLAYZONESCREEN:	PROC
	BEGIN
	;[1643] 	for i=0 to 239
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1643
	CLRR R0
	MVO R0,var_I
T510:
	;[1644] 		gosub VALDATA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1644
	CALL label_VALDATA
	;[1645] 		gosub DefTile
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1645
	CALL label_DEFTILE
	;[1646] 	next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1646
	MVI var_I,R0
	INCR R0
	MVO R0,var_I
	CMPI #239,R0
	BLE T510
	;[1647] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1647
	RETURN
	ENDP
	;[1648] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1648
	;[1649] '///// Gestion Bazooka /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1649
	;[1650] GestionBazooka: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1650
	; GESTIONBAZOOKA
label_GESTIONBAZOOKA:	PROC
	BEGIN
	;[1651] 	'Tir / IA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1651
	;[1652] 	if TirPapi then gosub IATOUCH
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1652
	MVI var_TIRPAPI,R0
	TSTR R0
	BEQ T511
	CALL label_IATOUCH
T511:
	;[1653] 	if TirPapi=99 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1653
	MVI var_TIRPAPI,R0
	CMPI #99,R0
	BNE T512
	;[1654] 		Sound 0,,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1654
	CLRR R0
	MVO R0,507
	;[1655] 		TempoTir=TempoTir+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1655
	MVI var_TEMPOTIR,R0
	INCR R0
	MVO R0,var_TEMPOTIR
	;[1656] 		if TempoTir=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1656
	MVI var_TEMPOTIR,R0
	CMPI #1,R0
	BNE T513
	;[1657] 			gosub SonExplosion
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1657
	CALL label_SONEXPLOSION
	;[1658] 			#Couleur=SPR_RED 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1658
	MVII #2,R0
	MVO R0,var_&COULEUR
	;[1659] 			DEFINE DEF47, 1, Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1659
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
	;[1660] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1660
T513:
	;[1661] 		if ImpactTouch then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1661
	MVI var_IMPACTTOUCH,R0
	TSTR R0
	BEQ T514
	;[1662] 			#Couleur=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1662
	MVII #7,R0
	MVO R0,var_&COULEUR
	;[1663] 			gosub ImpactHIT
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1663
	CALL label_IMPACTHIT
	;[1664] 		else
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1664
	B T515
T514:
	;[1665] 			#Couleur=SPR_RED 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1665
	MVII #2,R0
	MVO R0,var_&COULEUR
	;[1666] 			if TempoTir=4 then DEFINE DEF47, 1, Balle1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1666
	MVI var_TEMPOTIR,R0
	CMPI #4,R0
	BNE T516
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE1,R0
	MVO R0,_gram_bitmap
T516:
	;[1667] 			if TempoTir=7 then DEFINE DEF47, 1, Balle2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1667
	MVI var_TEMPOTIR,R0
	CMPI #7,R0
	BNE T517
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE2,R0
	MVO R0,_gram_bitmap
T517:
	;[1668] 			if TempoTir=10 then DEFINE DEF47, 1, Balle3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1668
	MVI var_TEMPOTIR,R0
	CMPI #10,R0
	BNE T518
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE3,R0
	MVO R0,_gram_bitmap
T518:
	;[1669] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1669
T515:
	;[1670] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1670
	;[1671] 		if TempoTir=13 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1671
	MVI var_TEMPOTIR,R0
	CMPI #13,R0
	BNE T519
	;[1672] 			gosub RAC
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1672
	CALL label_RAC
	;[1673] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1673
T519:
	;[1674] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1674
T512:
	;[1675] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1675
	RETURN
	ENDP
	;[1676] RAC: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1676
	; RAC
label_RAC:	PROC
	BEGIN
	;[1677] 	gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1677
	CALL label_SONBLANK
	;[1678] 	TirPapi=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1678
	CLRR R0
	MVO R0,var_TIRPAPI
	;[1679] 	TempoTir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1679
	MVO R0,var_TEMPOTIR
	;[1680] 	ImpactTouch=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1680
	NOP
	MVO R0,var_IMPACTTOUCH
	;[1681] 	wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1681
	CALL _wait
	;[1682] 	j=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1682
	CLRR R0
	MVO R0,var_J
	;[1683] 	Coordx(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1683
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVO@ R0,R3
	;[1684] 	Coordy(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1684
	ADDI #(array_COORDY-array_COORDX) AND $FFFF,R3
	MVO@ R0,R3
	;[1685] 	DEFINE DEF47, 1, viseur
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1685
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_VISEUR,R0
	MVO R0,_gram_bitmap
	;[1686] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1686
	RETURN
	ENDP
	;[1687] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1687
	;[1688] '///// Gestion Grenade /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1688
	;[1689] GestionGrenade: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1689
	; GESTIONGRENADE
label_GESTIONGRENADE:	PROC
	BEGIN
	;[1690] 	gosub GestionZoomGrenade
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1690
	CALL label_GESTIONZOOMGRENADE
	;[1691] 	if TirPapi then Sound 0,100-DX_Arme,10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1691
	MVI var_TIRPAPI,R0
	TSTR R0
	BEQ T520
	MVII #100,R0
	SUB var_DX_ARME,R0
	MVO R0,496
	SWAP R0
	MVO R0,500
	MVII #10,R0
	MVO R0,507
T520:
	;[1692] 	if TirPapi=99 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1692
	MVI var_TIRPAPI,R0
	CMPI #99,R0
	BNE T521
	;[1693] 		Sound 0,,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1693
	CLRR R0
	MVO R0,507
	;[1694] 		gosub GestionCollisionExplosif
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1694
	CALL label_GESTIONCOLLISIONEXPLOSIF
	;[1695] 		TempoTir=TempoTir+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1695
	MVI var_TEMPOTIR,R0
	INCR R0
	MVO R0,var_TEMPOTIR
	;[1696] 		if TempoTir=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1696
	MVI var_TEMPOTIR,R0
	CMPI #1,R0
	BNE T522
	;[1697] 			gosub SonExplosion
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1697
	CALL label_SONEXPLOSION
	;[1698] 			'#ZoomOBjectY=ZOOMY2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1698
	;[1699] 			if CouleurImpact=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1699
	MVI var_COULEURIMPACT,R0
	CMPI #1,R0
	BNE T523
	;[1700] 				#Couleur=SPR_RED 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1700
	MVII #2,R0
	MVO R0,var_&COULEUR
	;[1701] 				DEFINE DEF47, 1, Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1701
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
	;[1702] 			elseif CouleurImpact=2 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1702
	B T524
T523:
	MVI var_COULEURIMPACT,R0
	CMPI #2,R0
	BNE T525
	;[1703] 				#Couleur=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1703
	MVII #7,R0
	MVO R0,var_&COULEUR
	;[1704] 				DEFINE DEF47, 1, Gerbe
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1704
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GERBE,R0
	MVO R0,_gram_bitmap
	;[1705] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1705
T524:
T525:
	;[1706] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1706
	;[1707] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1707
T522:
	;[1708] 		if ImpactTouch then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1708
	MVI var_IMPACTTOUCH,R0
	TSTR R0
	BEQ T526
	;[1709] 			#Couleur=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1709
	MVII #7,R0
	MVO R0,var_&COULEUR
	;[1710] 			gosub ImpactHIT
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1710
	CALL label_IMPACTHIT
	;[1711] 		else
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1711
	B T527
T526:
	;[1712] 			gosub A1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1712
	CALL label_A1
	;[1713] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1713
T527:
	;[1714] 		
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1714
	;[1715] 		if TempoTir=13 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1715
	MVI var_TEMPOTIR,R0
	CMPI #13,R0
	BNE T528
	;[1716] 			gosub RAC
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1716
	CALL label_RAC
	;[1717] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1717
T528:
	;[1718] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1718
T521:
	;[1719] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1719
	RETURN
	ENDP
	;[1720] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1720
	;[1721] '///// ZOOM Grenade /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1721
	;[1722] GestionZoomGrenade: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1722
	; GESTIONZOOMGRENADE
label_GESTIONZOOMGRENADE:	PROC
	BEGIN
	;[1723] 	if DX_Arme=1 then DEFINE DEF47, 1, Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1723
	MVI var_DX_ARME,R0
	CMPI #1,R0
	BNE T529
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
T529:
	;[1724] 	if DX_Arme=7 then DEFINE DEF47, 1, Grenade
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1724
	MVI var_DX_ARME,R0
	CMPI #7,R0
	BNE T530
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GRENADE,R0
	MVO R0,_gram_bitmap
T530:
	;[1725] 	if DX_Arme=12 then DEFINE DEF47, 1, Grenade1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1725
	MVI var_DX_ARME,R0
	CMPI #12,R0
	BNE T531
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GRENADE1,R0
	MVO R0,_gram_bitmap
T531:
	;[1726] 	if DX_Arme=20 then DEFINE DEF47, 1, Grenade2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1726
	MVI var_DX_ARME,R0
	CMPI #20,R0
	BNE T532
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GRENADE2,R0
	MVO R0,_gram_bitmap
T532:
	;[1727] 	if DX_Arme=28 then DEFINE DEF47, 1, Grenade1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1727
	MVI var_DX_ARME,R0
	CMPI #28,R0
	BNE T533
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GRENADE1,R0
	MVO R0,_gram_bitmap
T533:
	;[1728] 	if DX_Arme=33 then DEFINE DEF47, 1, Grenade1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1728
	MVI var_DX_ARME,R0
	CMPI #33,R0
	BNE T534
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GRENADE1,R0
	MVO R0,_gram_bitmap
T534:
	;[1729] 	if DX_Arme=DistanceArme then wait:DEFINE DEF47, 1, Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1729
	MVI var_DX_ARME,R0
	CMP var_DISTANCEARME,R0
	BNE T535
	CALL _wait
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
T535:
	;[1730] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1730
	RETURN
	ENDP
	;[1731] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1731
	;[1732] GestionGrenadeMaxi: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1732
	; GESTIONGRENADEMAXI
label_GESTIONGRENADEMAXI:	PROC
	BEGIN
	;[1733] 	gosub GestionZoomGrenade
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1733
	CALL label_GESTIONZOOMGRENADE
	;[1734] 	if TirPapi then Sound 0,100-DX_Arme,10
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1734
	MVI var_TIRPAPI,R0
	TSTR R0
	BEQ T536
	MVII #100,R0
	SUB var_DX_ARME,R0
	MVO R0,496
	SWAP R0
	MVO R0,500
	MVII #10,R0
	MVO R0,507
T536:
	;[1735] 	if TirPapi=99 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1735
	MVI var_TIRPAPI,R0
	CMPI #99,R0
	BNE T537
	;[1736] 		sound 0,,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1736
	CLRR R0
	MVO R0,507
	;[1737] 		TempoTir=TempoTir+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1737
	MVI var_TEMPOTIR,R0
	INCR R0
	MVO R0,var_TEMPOTIR
	;[1738] 		if TempoTir=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1738
	MVI var_TEMPOTIR,R0
	CMPI #1,R0
	BNE T538
	;[1739] 			gosub GestionCollisionExplosifMaxi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1739
	CALL label_GESTIONCOLLISIONEXPLOSIFMAXI
	;[1740] 			gosub SonExplosionMAX
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1740
	CALL label_SONEXPLOSIONMAX
	;[1741] 			if CouleurImpact=1 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1741
	MVI var_COULEURIMPACT,R0
	CMPI #1,R0
	BNE T539
	;[1742] 				#ZoomOBjectX=ZOOMX2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1742
	MVII #1024,R0
	MVO R0,var_&ZOOMOBJECTX
	;[1743] 				#ZoomOBjectY=ZOOMY4
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1743
	MVII #512,R0
	MVO R0,var_&ZOOMOBJECTY
	;[1744] 				#Couleur=SPR_RED 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1744
	MVII #2,R0
	MVO R0,var_&COULEUR
	;[1745] 				DEFINE DEF47, 1, Balle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1745
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE,R0
	MVO R0,_gram_bitmap
	;[1746] 			elseif CouleurImpact=2 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1746
	B T540
T539:
	MVI var_COULEURIMPACT,R0
	CMPI #2,R0
	BNE T541
	;[1747] 				#Couleur=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1747
	MVII #7,R0
	MVO R0,var_&COULEUR
	;[1748] 				#ZoomOBjectX=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1748
	CLRR R0
	MVO R0,var_&ZOOMOBJECTX
	;[1749] 				#ZoomOBjectY=ZOOMY2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1749
	MVII #256,R0
	MVO R0,var_&ZOOMOBJECTY
	;[1750] 				DEFINE DEF47, 1, Gerbe
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1750
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GERBE,R0
	MVO R0,_gram_bitmap
	;[1751] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1751
T540:
T541:
	;[1752] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1752
T538:
	;[1753] 		gosub A1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1753
	CALL label_A1
	;[1754] 		if TempoTir=13 then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1754
	MVI var_TEMPOTIR,R0
	CMPI #13,R0
	BNE T542
	;[1755] 			gosub SonBlank
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1755
	CALL label_SONBLANK
	;[1756] 			TirPapi=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1756
	CLRR R0
	MVO R0,var_TIRPAPI
	;[1757] 			TempoTir=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1757
	MVO R0,var_TEMPOTIR
	;[1758] 			wait
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1758
	CALL _wait
	;[1759] 			j=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1759
	CLRR R0
	MVO R0,var_J
	;[1760] 			DEFINE DEF47, 1, viseur
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1760
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_VISEUR,R0
	MVO R0,_gram_bitmap
	;[1761] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1761
T542:
	;[1762] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1762
T537:
	;[1763] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1763
	RETURN
	ENDP
	;[1764] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1764
	;[1765] A1: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1765
	; A1
label_A1:	PROC
	BEGIN
	;[1766] 	if CouleurImpact=1 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1766
	MVI var_COULEURIMPACT,R0
	CMPI #1,R0
	BNE T543
	;[1767] 		#Couleur=SPR_RED 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1767
	MVII #2,R0
	MVO R0,var_&COULEUR
	;[1768] 		if TempoTir=4 then DEFINE DEF47, 1, Balle1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1768
	MVI var_TEMPOTIR,R0
	CMPI #4,R0
	BNE T544
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE1,R0
	MVO R0,_gram_bitmap
T544:
	;[1769] 		if TempoTir=7 then DEFINE DEF47, 1, Balle2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1769
	MVI var_TEMPOTIR,R0
	CMPI #7,R0
	BNE T545
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE2,R0
	MVO R0,_gram_bitmap
T545:
	;[1770] 		if TempoTir=10 then DEFINE DEF47, 1, Balle3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1770
	MVI var_TEMPOTIR,R0
	CMPI #10,R0
	BNE T546
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_BALLE3,R0
	MVO R0,_gram_bitmap
T546:
	;[1771] 	elseif CouleurImpact=2 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1771
	B T547
T543:
	MVI var_COULEURIMPACT,R0
	CMPI #2,R0
	BNE T548
	;[1772] 		#Couleur=SPR_WHITE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1772
	MVII #7,R0
	MVO R0,var_&COULEUR
	;[1773] 		if TempoTir=4 then DEFINE DEF47, 1, Gerbe1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1773
	MVI var_TEMPOTIR,R0
	CMPI #4,R0
	BNE T549
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GERBE1,R0
	MVO R0,_gram_bitmap
T549:
	;[1774] 		if TempoTir=7 then DEFINE DEF47, 1, Gerbe2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1774
	MVI var_TEMPOTIR,R0
	CMPI #7,R0
	BNE T550
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GERBE2,R0
	MVO R0,_gram_bitmap
T550:
	;[1775] 		if TempoTir=10 then DEFINE DEF47, 1, Gerbe3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1775
	MVI var_TEMPOTIR,R0
	CMPI #10,R0
	BNE T551
	MVII #47,R0
	MVO R0,_gram_target
	MVII #1,R0
	MVO R0,_gram_total
	MVII #label_GERBE3,R0
	MVO R0,_gram_bitmap
T551:
	;[1776] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1776
T547:
T548:
	;[1777] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1777
	RETURN
	ENDP
	;[1778] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1778
	;[1779] '///// Affichage Informations /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1779
	;[1780] DisplayInfo: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1780
	; DISPLAYINFO
label_DISPLAYINFO:	PROC
	BEGIN
	;[1781] 	if Start=99 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1781
	MVI var_START,R0
	CMPI #99,R0
	BNE T552
	RETURN
T552:
	;[1782] 	Gosub DisplayItem
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1782
	CALL label_DISPLAYITEM
	;[1783] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1783
	RETURN
	ENDP
	;[1784] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1784
	;[1785] '///// Init Sprite //////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1785
	;[1786] InitSprite: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1786
	; INITSPRITE
label_INITSPRITE:	PROC
	BEGIN
	;[1787] 	'INIT Sprite
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1787
	;[1788] 	SPRITE 0,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1788
	CLRR R0
	MVO R0,_mobs
	MVO R0,_mobs+8
	;[1789] 	SPRITE 1,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1789
	NOP
	MVO R0,_mobs+1
	MVO R0,_mobs+9
	;[1790] 	SPRITE 2,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1790
	NOP
	MVO R0,_mobs+2
	MVO R0,_mobs+10
	;[1791] 	SPRITE 3,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1791
	NOP
	MVO R0,_mobs+3
	MVO R0,_mobs+11
	;[1792] 	SPRITE 4,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1792
	NOP
	MVO R0,_mobs+4
	MVO R0,_mobs+12
	;[1793] 	SPRITE 5,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1793
	NOP
	MVO R0,_mobs+5
	MVO R0,_mobs+13
	;[1794] 	SPRITE 6,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1794
	NOP
	MVO R0,_mobs+6
	MVO R0,_mobs+14
	;[1795] 	SPRITE 7,0,0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1795
	NOP
	MVO R0,_mobs+7
	MVO R0,_mobs+15
	;[1796] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1796
	RETURN
	ENDP
	;[1797] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1797
	;[1798] '///// Affichage Vies /////	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1798
	;[1799] DisplayLife: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1799
	; DISPLAYLIFE
label_DISPLAYLIFE:	PROC
	BEGIN
	;[1800] 	Tempo(10)=Tempo(10)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1800
	MVI array_TEMPO+10,R0
	INCR R0
	MVO R0,array_TEMPO+10
	;[1801] 	Print AT SCREENPOS(0,10) COLOR 7,"LIFES "
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1801
	MVII #712,R0
	MVO R0,_screen
	MVII #7,R0
	MVO R0,_color
	MVI _screen,R4
	MVII #352,R0
	XOR _color,R0
	MVO@ R0,R4
	XORI #40,R0
	MVO@ R0,R4
	XORI #120,R0
	MVO@ R0,R4
	XORI #24,R0
	MVO@ R0,R4
	XORI #176,R0
	MVO@ R0,R4
	XORI #408,R0
	MVO@ R0,R4
	MVO R4,_screen
	;[1802] 	for i=1 to LifePapi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1802
	MVII #1,R0
	MVO R0,var_I
T553:
	;[1803] 		PRINT AT SCREENPOS(5+i,10) COLOR CS_YELLOW,CARD_VIES
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1803
	MVI var_I,R0
	ADDI #717,R0
	MVO R0,_screen
	MVII #6,R0
	MVO R0,_color
	MVII #2338,R0
	MVI _screen,R4
	MVO@ R0,R4
	MVO R4,_screen
	;[1804] 	next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1804
	MVI var_I,R0
	INCR R0
	MVO R0,var_I
	CMP var_LIFEPAPI,R0
	BLE T553
	;[1805] 	if Tempo(10)>25 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1805
	MVI array_TEMPO+10,R0
	CMPI #25,R0
	BLE T554
	;[1806] 		gosub UpdateMAPITEM
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1806
	CALL label_UPDATEMAPITEM
	;[1807] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1807
T554:
	;[1808] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1808
	RETURN
	ENDP
	;[1809] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1809
	;[1810] '///// Update ITEM /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1810
	;[1811] DisplayItem: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1811
	; DISPLAYITEM
label_DISPLAYITEM:	PROC
	BEGIN
	;[1812] 	Tempo(10)=Tempo(10)+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1812
	MVI array_TEMPO+10,R0
	INCR R0
	MVO R0,array_TEMPO+10
	;[1813] 	if Start=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1813
	MVI var_START,R0
	TSTR R0
	BNE T555
	;[1814] 		Print AT SCREENPOS(0,10) COLOR 7,"LIFES "
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1814
	MVII #712,R0
	MVO R0,_screen
	MVII #7,R0
	MVO R0,_color
	MVI _screen,R4
	MVII #352,R0
	XOR _color,R0
	MVO@ R0,R4
	XORI #40,R0
	MVO@ R0,R4
	XORI #120,R0
	MVO@ R0,R4
	XORI #24,R0
	MVO@ R0,R4
	XORI #176,R0
	MVO@ R0,R4
	XORI #408,R0
	MVO@ R0,R4
	MVO R4,_screen
	;[1815] 		for i=1 to LifePapi
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1815
	MVII #1,R0
	MVO R0,var_I
T556:
	;[1816] 			PRINT AT SCREENPOS(5+i,10) COLOR CS_RED,CARD_VIES
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1816
	MVI var_I,R0
	ADDI #717,R0
	MVO R0,_screen
	MVII #2,R0
	MVO R0,_color
	MVII #2338,R0
	MVI _screen,R4
	MVO@ R0,R4
	MVO R4,_screen
	;[1817] 		next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1817
	MVI var_I,R0
	INCR R0
	MVO R0,var_I
	CMP var_LIFEPAPI,R0
	BLE T556
	;[1818] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1818
T555:
	;[1819] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1819
	;[1820] 	if Start=1 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1820
	MVI var_START,R0
	CMPI #1,R0
	BNE T557
	;[1821] 		c=NbBalle
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1821
	MVI var_NBBALLE,R0
	MVO R0,var_C
	;[1822] 		gosub DI
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1822
	CALL label_DI
	;[1823] 	elseif Start=2 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1823
	B T558
T557:
	MVI var_START,R0
	CMPI #2,R0
	BNE T559
	;[1824] 		c=NbGrenade
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1824
	MVI var_NBGRENADE,R0
	MVO R0,var_C
	;[1825] 		Print AT SCREENPOS(0,10) COLOR CS_YELLOW,CARD_ITEM
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1825
	MVII #712,R0
	MVO R0,_screen
	MVII #6,R0
	MVO R0,_color
	MVII #2350,R0
	MVI _screen,R4
	MVO@ R0,R4
	MVO R4,_screen
	;[1826] 		if NbGrenade then PRINT AT SCREENPOS(1,10) COLOR 7,<2>NbGrenade else PRINT AT SCREENPOS(1,10) COLOR 7,"NO AMMO"
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1826
	MVI var_NBGRENADE,R0
	TSTR R0
	BEQ T560
	MVII #713,R0
	MVO R0,_screen
	MVII #7,R0
	MVO R0,_color
	MVI var_NBGRENADE,R0
	MVII #2,R2
	MVI _color,R3
	MVI _screen,R4
	CALL PRNUM16.z
	MVO R4,_screen
	B T561
T560:
	MVII #713,R0
	MVO R0,_screen
	MVII #7,R0
	MVO R0,_color
	MVI _screen,R4
	MVII #368,R0
	XOR _color,R0
	MVO@ R0,R4
	XORI #8,R0
	MVO@ R0,R4
	XORI #376,R0
	MVO@ R0,R4
	XORI #264,R0
	MVO@ R0,R4
	XORI #96,R0
	MVO@ R0,R4
	MVO@ R0,R4
	XORI #16,R0
	MVO@ R0,R4
	MVO R4,_screen
T561:
	;[1827] 	elseif Start=3 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1827
	B T558
T559:
	MVI var_START,R0
	CMPI #3,R0
	BNE T562
	;[1828] 		c=NbGrenadeM
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1828
	MVI var_NBGRENADEM,R0
	MVO R0,var_C
	;[1829] 		gosub DI1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1829
	CALL label_DI1
	;[1830] 	elseif  Start=4 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1830
	B T558
T562:
	MVI var_START,R0
	CMPI #4,R0
	BNE T563
	;[1831] 		c=NbRoquette
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1831
	MVI var_NBROQUETTE,R0
	MVO R0,var_C
	;[1832] 		gosub DI
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1832
	CALL label_DI
	;[1833] 	elseif Start=5 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1833
	B T558
T563:
	MVI var_START,R0
	CMPI #5,R0
	BNE T564
	;[1834] 		Print AT SCREENPOS(0,10) COLOR CS_YELLOW,CARD_ITEM
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1834
	MVII #712,R0
	MVO R0,_screen
	MVII #6,R0
	MVO R0,_color
	MVII #2350,R0
	MVI _screen,R4
	MVO@ R0,R4
	MVO R4,_screen
	;[1835] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1835
T558:
T564:
	;[1836] 	if Tempo(10)>30 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1836
	MVI array_TEMPO+10,R0
	CMPI #30,R0
	BLE T565
	;[1837] 		gosub UpdateMAPITEM
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1837
	CALL label_UPDATEMAPITEM
	;[1838] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1838
T565:
	;[1839] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1839
	RETURN
	ENDP
	;[1840] DI1: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1840
	; DI1
label_DI1:	PROC
	BEGIN
	;[1841] 		Print AT SCREENPOS(0,10) COLOR CS_YELLOW,CARD_ITEM1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1841
	MVII #712,R0
	MVO R0,_screen
	MVII #6,R0
	MVO R0,_color
	MVII #2514,R0
	MVI _screen,R4
	MVO@ R0,R4
	MVO R4,_screen
	;[1842] 		if c then PRINT AT SCREENPOS(1,10) COLOR 7,<2>c else PRINT AT SCREENPOS(1,10) COLOR 7,"NO AMMO"
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1842
	MVI var_C,R0
	TSTR R0
	BEQ T566
	MVII #713,R0
	MVO R0,_screen
	MVII #7,R0
	MVO R0,_color
	MVI var_C,R0
	MVII #2,R2
	MVI _color,R3
	MVI _screen,R4
	CALL PRNUM16.z
	MVO R4,_screen
	B T567
T566:
	MVII #713,R0
	MVO R0,_screen
	MVII #7,R0
	MVO R0,_color
	MVI _screen,R4
	MVII #368,R0
	XOR _color,R0
	MVO@ R0,R4
	XORI #8,R0
	MVO@ R0,R4
	XORI #376,R0
	MVO@ R0,R4
	XORI #264,R0
	MVO@ R0,R4
	XORI #96,R0
	MVO@ R0,R4
	MVO@ R0,R4
	XORI #16,R0
	MVO@ R0,R4
	MVO R4,_screen
T567:
	;[1843] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1843
	RETURN
	ENDP
	;[1844] DI: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1844
	; DI
label_DI:	PROC
	BEGIN
	;[1845] 		Print AT SCREENPOS(0,10) COLOR CS_YELLOW,CARD_ITEM
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1845
	MVII #712,R0
	MVO R0,_screen
	MVII #6,R0
	MVO R0,_color
	MVII #2350,R0
	MVI _screen,R4
	MVO@ R0,R4
	MVO R4,_screen
	;[1846] 		if c then PRINT AT SCREENPOS(1,10) COLOR 7,<2>c else PRINT AT SCREENPOS(1,10) COLOR 7,"NO AMMO"
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1846
	MVI var_C,R0
	TSTR R0
	BEQ T568
	MVII #713,R0
	MVO R0,_screen
	MVII #7,R0
	MVO R0,_color
	MVI var_C,R0
	MVII #2,R2
	MVI _color,R3
	MVI _screen,R4
	CALL PRNUM16.z
	MVO R4,_screen
	B T569
T568:
	MVII #713,R0
	MVO R0,_screen
	MVII #7,R0
	MVO R0,_color
	MVI _screen,R4
	MVII #368,R0
	XOR _color,R0
	MVO@ R0,R4
	XORI #8,R0
	MVO@ R0,R4
	XORI #376,R0
	MVO@ R0,R4
	XORI #264,R0
	MVO@ R0,R4
	XORI #96,R0
	MVO@ R0,R4
	MVO@ R0,R4
	XORI #16,R0
	MVO@ R0,R4
	MVO R4,_screen
T569:
	;[1847] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1847
	RETURN
	ENDP
	;[1848] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1848
	;[1849] '///// Update MAP Information ITEM /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1849
	;[1850] UpdateMAPITEM: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1850
	; UPDATEMAPITEM
label_UPDATEMAPITEM:	PROC
	BEGIN
	;[1851] 	Start=99
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1851
	MVII #99,R0
	MVO R0,var_START
	;[1852] 	Tempo(10)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1852
	CLRR R0
	MVO R0,array_TEMPO+10
	;[1853] 	c=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1853
	MVO R0,var_C
	;[1854] 	for i=(SCREEN_HAUTEUR-2)*(MAP_LARGEUR) to (SCREEN_HAUTEUR*MAP_LARGEUR)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1854
	MVII #200,R0
	MVO R0,var_I
T570:
	;[1855] 		c=c+1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1855
	MVI var_C,R0
	INCR R0
	MVO R0,var_C
	;[1856] 		gosub VALDATA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1856
	CALL label_VALDATA
	;[1857] 		gosub D_DATA
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1857
	CALL label_D_DATA
	;[1858] 		gosub DefTile
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1858
	CALL label_DEFTILE
	;[1859] 		if c>8 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1859
	MVI var_C,R0
	CMPI #8,R0
	BLE T571
	RETURN
T571:
	;[1860] 	next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1860
	MVI var_I,R0
	INCR R0
	MVO R0,var_I
	CMPI #238,R0
	BLE T570
	;[1861] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1861
	RETURN
	ENDP
	;[1862] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1862
	;[1863] '//////////////////////////////////////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1863
	;[1864] '///// Collision Grenade/Explosif /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1864
	;[1865] '//////////////////////////////////////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1865
	;[1866] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1866
	;[1867] '///// Grenades normales
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1867
	;[1868] GestionCollisionExplosif: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1868
	; GESTIONCOLLISIONEXPLOSIF
label_GESTIONCOLLISIONEXPLOSIF:	PROC
	BEGIN
	;[1869] 		if CouleurImpact=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1869
	MVI var_COULEURIMPACT,R0
	TSTR R0
	BNE T572
	;[1870] 			COFX=Coordx(i)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1870
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	MVO R0,var_COFX
	;[1871] 			COFY=(CoordY(i)-2)-offset_y
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1871
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	SUB var_OFFSET_Y,R0
	MVO R0,var_COFY
	;[1872] 			#offset=COFX/8+COFY/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1872
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1873] 			#COL_TEST = PEEK($0200+#offset)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1873
	MOVR R0,R1
	ADDI #512,R1
	MVI@ R1,R0
	MVO R0,var_&COL_TEST
	;[1874] 			CouleurImpact=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1874
	MVII #1,R0
	MVO R0,var_COULEURIMPACT
	;[1875] 			'Destruction d'arbre
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1875
	;[1876] 			if #COL_TEST=SOL_TRONC then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1876
	MVI var_&COL_TEST,R0
	CMPI #10368,R0
	BNE T573
	;[1877] 				#backtab(#offset)=CARD_ImpactBoum
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1877
	MVII #10720,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[1878] 				#offset=COFX/8+(COFY-8)/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1878
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	SUBI #8,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1879] 				#backtab(#offset)=CARD_HERBE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1879
	MVII #10733,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[1880] 				return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1880
	RETURN
	;[1881] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1881
T573:
	;[1882] 			if #COL_TEST=CIMEARBRE then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1882
	MVI var_&COL_TEST,R0
	CMPI #10749,R0
	BNE T574
	;[1883] 				#backtab(#offset)=CARD_ImpactBoum
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1883
	MVII #10720,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[1884] 				#offset=COFX/8+(COFY+8)/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1884
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	ADDI #8,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1885] 				#backtab(#offset)=CARD_HERBE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1885
	MVII #10733,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[1886] 				return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1886
	RETURN
	;[1887] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1887
T574:
	;[1888] 			if #COL_TEST=CARD_IABUNKER_M then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1888
	MVI var_&COL_TEST,R0
	CMPI #6600,R0
	BNE T575
	;[1889] 				d=3
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1889
	MVII #3,R0
	MVO R0,var_D
	;[1890] 				gosub Bunker_Hit
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1890
	CALL label_BUNKER_HIT
	;[1891] 				if c=0 then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1891
	MVI var_C,R0
	TSTR R0
	BNE T576
	RETURN
T576:
	;[1892] 				DieBunker(c)=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1892
	MVII #1,R0
	MVII #array_DIEBUNKER,R3
	ADD var_C,R3
	MVO@ R0,R3
	;[1893] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1893
T575:
	;[1894] 			gosub IATOUCH1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1894
	CALL label_IATOUCH1
	;[1895] 			if #COL_TEST=SOL_ROCHER then return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1895
	MVI var_&COL_TEST,R0
	CMPI #10736,R0
	BNE T577
	RETURN
T577:
	;[1896] 			if #COL_TEST<>SOL_EAU then #backtab(#offset)=CARD_ImpactBoum
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1896
	MVI var_&COL_TEST,R0
	CMPI #15017,R0
	BEQ T578
	MVII #10720,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
T578:
	;[1897] 			if #COL_TEST=PASSERELLE then #backtab(#offset)=CARD_EAU1:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1897
	MVI var_&COL_TEST,R0
	CMPI #6464,R0
	BNE T579
	MVII #15017,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	RETURN
T579:
	;[1898] 			if #COL_TEST=SOL_EAU then CouleurImpact=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1898
	MVI var_&COL_TEST,R0
	CMPI #15017,R0
	BNE T580
	MVII #2,R0
	MVO R0,var_COULEURIMPACT
T580:
	;[1899] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1899
T572:
	;[1900] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1900
	RETURN
	ENDP
	;[1901] '////// Grenades fragmentation /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1901
	;[1902] GestionCollisionExplosifMaxi: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1902
	; GESTIONCOLLISIONEXPLOSIFMAXI
label_GESTIONCOLLISIONEXPLOSIFMAXI:	PROC
	BEGIN
	;[1903] 	if CouleurImpact=0 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1903
	MVI var_COULEURIMPACT,R0
	TSTR R0
	BNE T581
	;[1904] 		if Coordy(i)>90 then c=8 else c=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1904
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	CMPI #90,R0
	BLE T582
	MVII #8,R0
	MVO R0,var_C
	B T583
T582:
	CLRR R0
	MVO R0,var_C
T583:
	;[1905] 		COFX=Coordx(i)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1905
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	MVO R0,var_COFX
	;[1906] 		COFY=(CoordY(i)-2)-offset_y
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1906
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	SUB var_OFFSET_Y,R0
	MVO R0,var_COFY
	;[1907] 		#offset=COFX/8+COFY/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1907
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1908] 		#COL_TEST = PEEK($0200+#offset)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1908
	MOVR R0,R1
	ADDI #512,R1
	MVI@ R1,R0
	MVO R0,var_&COL_TEST
	;[1909] 		CouleurImpact=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1909
	MVII #1,R0
	MVO R0,var_COULEURIMPACT
	;[1910] 		d=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1910
	MVII #8,R0
	MVO R0,var_D
	;[1911] 		gosub Bunker_Hit
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1911
	CALL label_BUNKER_HIT
	;[1912] 		gosub IATOUCH1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1912
	CALL label_IATOUCH1
	;[1913] 		'Destruction d'arbre
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1913
	;[1914] 		if #COL_TEST=SOL_TRONC then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1914
	MVI var_&COL_TEST,R0
	CMPI #10368,R0
	BNE T584
	;[1915] 			#backtab(#offset)=CARD_ImpactBoum
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1915
	MVII #10720,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[1916] 			#offset=COFX/8+(COFY-8)/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1916
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	SUBI #8,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1917] 			#backtab(#offset)=CARD_HERBE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1917
	MVII #10733,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[1918] 			return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1918
	RETURN
	;[1919] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1919
T584:
	;[1920] 		if #COL_TEST=CIMEARBRE then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1920
	MVI var_&COL_TEST,R0
	CMPI #10749,R0
	BNE T585
	;[1921] 			#backtab(#offset)=CARD_ImpactBoum
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1921
	MVII #10720,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[1922] 			#offset=COFX/8+(COFY+8-c)/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1922
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	ADDI #8,R1
	SUB var_C,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1923] 			#backtab(#offset)=CARD_HERBE
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1923
	MVII #10733,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[1924] 			return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1924
	RETURN
	;[1925] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1925
T585:
	;[1926] 		#offset=COFX/8+COFY/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1926
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1927] 		#COL_TEST = PEEK($0200+#offset)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1927
	MOVR R0,R1
	ADDI #512,R1
	MVI@ R1,R0
	MVO R0,var_&COL_TEST
	;[1928] 		CouleurImpact=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1928
	MVII #1,R0
	MVO R0,var_COULEURIMPACT
	;[1929] 		d=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1929
	MVII #8,R0
	MVO R0,var_D
	;[1930] 		gosub Bunker_Hit
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1930
	CALL label_BUNKER_HIT
	;[1931] 		gosub IATOUCH1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1931
	CALL label_IATOUCH1
	;[1932] 		gosub CM
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1932
	CALL label_CM
	;[1933] 		
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1933
	;[1934] 		#offset=COFX/8+(COFY-8)/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1934
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	SUBI #8,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1935] 		#COL_TEST = PEEK($0200+#offset)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1935
	MOVR R0,R1
	ADDI #512,R1
	MVI@ R1,R0
	MVO R0,var_&COL_TEST
	;[1936] 		d=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1936
	MVII #8,R0
	MVO R0,var_D
	;[1937] 		gosub Bunker_Hit
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1937
	CALL label_BUNKER_HIT
	;[1938] 		gosub IATOUCH1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1938
	CALL label_IATOUCH1
	;[1939] 		gosub CM
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1939
	CALL label_CM
	;[1940] 		
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1940
	;[1941] 		#offset=COFX/8+(COFY+8-c)/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1941
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	ADDI #8,R1
	SUB var_C,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1942] 		#COL_TEST = PEEK($0200+#offset)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1942
	MOVR R0,R1
	ADDI #512,R1
	MVI@ R1,R0
	MVO R0,var_&COL_TEST
	;[1943] 		d=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1943
	MVII #8,R0
	MVO R0,var_D
	;[1944] 		gosub Bunker_Hit
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1944
	CALL label_BUNKER_HIT
	;[1945] 		gosub IATOUCH1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1945
	CALL label_IATOUCH1
	;[1946] 		gosub CM
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1946
	CALL label_CM
	;[1947] 		
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1947
	;[1948] 		#offset=(COFX+8)/8+(COFY)/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1948
	MVI var_COFX,R0
	ADDI #8,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1949] 		#COL_TEST = PEEK($0200+#offset)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1949
	MOVR R0,R1
	ADDI #512,R1
	MVI@ R1,R0
	MVO R0,var_&COL_TEST
	;[1950] 		d=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1950
	MVII #8,R0
	MVO R0,var_D
	;[1951] 		gosub Bunker_Hit
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1951
	CALL label_BUNKER_HIT
	;[1952] 		gosub IATOUCH1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1952
	CALL label_IATOUCH1
	;[1953] 		gosub CM
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1953
	CALL label_CM
	;[1954] 		
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1954
	;[1955] 		#offset=(COFX-8)/8+(COFY)/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1955
	MVI var_COFX,R0
	SUBI #8,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1956] 		#COL_TEST = PEEK($0200+#offset)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1956
	MOVR R0,R1
	ADDI #512,R1
	MVI@ R1,R0
	MVO R0,var_&COL_TEST
	;[1957] 		d=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1957
	MVII #8,R0
	MVO R0,var_D
	;[1958] 		gosub Bunker_Hit
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1958
	CALL label_BUNKER_HIT
	;[1959] 		gosub IATOUCH1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1959
	CALL label_IATOUCH1
	;[1960] 		gosub CM
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1960
	CALL label_CM
	;[1961] 		if #COL_TEST=SOL_EAU then CouleurImpact=2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1961
	MVI var_&COL_TEST,R0
	CMPI #15017,R0
	BNE T586
	MVII #2,R0
	MVO R0,var_COULEURIMPACT
T586:
	;[1962] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1962
T581:
	;[1963] end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1963
	RETURN
	ENDP
	;[1964] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1964
	;[1965] IAT2: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1965
	; IAT2
label_IAT2:	PROC
	BEGIN
	;[1966] 	VisibleIA(c)=2:VelocityIA(c)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1966
	MVII #2,R0
	MVII #array_VISIBLEIA,R3
	ADD var_C,R3
	MVO@ R0,R3
	CLRR R0
	ADDI #(array_VELOCITYIA-array_VISIBLEIA) AND $FFFF,R3
	MVO@ R0,R3
	;[1967] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1967
	RETURN
	ENDP
	;[1968] CM: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1968
	; CM
label_CM:	PROC
	BEGIN
	;[1969] 		if #COL_TEST<>SOL_EAU and #COL_TEST<>SOL_ROCHER and #COL_TEST<>CARD_IABUNKER_M then 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1969
	MVI var_&COL_TEST,R0
	CMPI #15017,R0
	MVII #65535,R0
	BNE T588
	INCR R0
T588:
	MVI var_&COL_TEST,R1
	CMPI #10736,R1
	MVII #65535,R1
	BNE T589
	INCR R1
T589:
	ANDR R1,R0
	MVI var_&COL_TEST,R1
	CMPI #6600,R1
	MVII #65535,R1
	BNE T590
	INCR R1
T590:
	ANDR R1,R0
	BEQ T587
	;[1970] 			#backtab(#offset)=CARD_ImpactBoum
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1970
	MVII #10720,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	;[1971] 			if #COL_TEST=PASSERELLE then #backtab(#offset)=CARD_EAU1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1971
	MVI var_&COL_TEST,R0
	CMPI #6464,R0
	BNE T591
	MVII #15017,R0
	MVO@ R0,R3
T591:
	;[1972] 		end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1972
T587:
	;[1973] 		if ImpactTouch then #backtab(#offset)=CARD_ImpactBoum
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1973
	MVI var_IMPACTTOUCH,R0
	TSTR R0
	BEQ T592
	MVII #10720,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
T592:
	;[1974] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1974
	RETURN
	ENDP
	;[1975] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1975
	;[1976] '///// Couteau vs Bunker /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1976
	;[1977] GestionCouteauBunker: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1977
	; GESTIONCOUTEAUBUNKER
label_GESTIONCOUTEAUBUNKER:	PROC
	BEGIN
	;[1978] 	COFX=Coordx(i)-2
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1978
	MVII #array_COORDX,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	MVO R0,var_COFX
	;[1979] 	COFY=(CoordY(i)-2)-offset_y
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1979
	MVII #array_COORDY,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUBI #2,R0
	SUB var_OFFSET_Y,R0
	MVO R0,var_COFY
	;[1980] 	#offset=COFX/8+COFY/8*20
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1980
	MVI var_COFX,R0
	SLR R0,2
	SLR R0,1
	MVI var_COFY,R1
	SLR R1,2
	SLR R1,1
	MULT R1,R4,20
	ADDR R1,R0
	MVO R0,var_&OFFSET
	;[1981] 	#COL_TEST = PEEK($0200+#offset)
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1981
	MOVR R0,R1
	ADDI #512,R1
	MVI@ R1,R0
	MVO R0,var_&COL_TEST
	;[1982] 	d=8
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1982
	MVII #8,R0
	MVO R0,var_D
	;[1983] 	gosub Bunker_Hit
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1983
	CALL label_BUNKER_HIT
	;[1984] 	if ImpactTouch then #backtab(#offset)=CARD_ImpactBoum:gosub SonExplosion
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1984
	MVI var_IMPACTTOUCH,R0
	TSTR R0
	BEQ T593
	MVII #10720,R0
	MVII #Q2,R3
	ADD var_&OFFSET,R3
	MVO@ R0,R3
	CALL label_SONEXPLOSION
T593:
	;[1985] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1985
	RETURN
	ENDP
	;[1986] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1986
	;[1987] '///// Bunker touché ! //////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1987
	;[1988] Bunker_Hit: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1988
	; BUNKER_HIT
label_BUNKER_HIT:	PROC
	BEGIN
	;[1989] 	if #COL_TEST=CARD_IABUNKER_M and bT then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1989
	MVI var_&COL_TEST,R0
	CMPI #6600,R0
	MVII #65535,R0
	BEQ T595
	INCR R0
T595:
	AND var_BT,R0
	BEQ T594
	;[1990] 		c=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1990
	CLRR R0
	MVO R0,var_C
	;[1991] 		for i=1 to NBBUNKER
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1991
	MVII #1,R0
	MVO R0,var_I
T596:
	;[1992] 			if abs(coordy(1)-IABUNKER_CY(i))<16 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1992
	MVI array_COORDY+1,R0
	MVII #array_IABUNKER_CY,R3
	ADD var_I,R3
	SUB@ R3,R0
	BPL T598
	NEGR R0
T598:
	CMPI #16,R0
	BGE T597
	;[1993] 				if abs(Coordx(1)-IABUNKER_CX(i))<16 then
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1993
	MVI array_COORDX+1,R0
	MVII #array_IABUNKER_CX,R3
	ADD var_I,R3
	SUB@ R3,R0
	BPL T600
	NEGR R0
T600:
	CMPI #16,R0
	BGE T599
	;[1994] 					LifeBunker(i)=LifeBunker(i)-d
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1994
	MVII #array_LIFEBUNKER,R3
	ADD var_I,R3
	MVI@ R3,R0
	SUB var_D,R0
	MVO@ R0,R3
	;[1995] 					if LifeBunker(i)>50 then LifeBunker(i)=0
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1995
	MVI@ R3,R0
	CMPI #50,R0
	BLE T601
	CLRR R0
	MVO@ R0,R3
T601:
	;[1996] 					if LifeBunker(i)=0 then DieBunker(i)=1:c=i
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1996
	MVII #array_LIFEBUNKER,R3
	ADD var_I,R3
	MVI@ R3,R0
	TSTR R0
	BNE T602
	MVII #1,R0
	ADDI #(array_DIEBUNKER-array_LIFEBUNKER) AND $FFFF,R3
	MVO@ R0,R3
	MVI var_I,R0
	MVO R0,var_C
T602:
	;[1997] 					ImpactTouch=1
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1997
	MVII #1,R0
	MVO R0,var_IMPACTTOUCH
	;[1998] 					exit for
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1998
	B T603
	;[1999] 				end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",1999
T599:
	;[2000] 			end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2000
T597:
	;[2001] 		next
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2001
	MVI var_I,R0
	INCR R0
	MVO R0,var_I
	CMP var_NBBUNKER,R0
	BLE T596
T603:
	;[2002] 	end if
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2002
T594:
	;[2003] 	end
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2003
	RETURN
	ENDP
	;[2004] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2004
	;[2005] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2005
	;[2006] '///// Elaboration MAP /////	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2006
	;[2007] DefTile: procedure
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2007
	; DEFTILE
label_DEFTILE:	PROC
	BEGIN
	;[2008] 	if value=0 then #backtab(i)=CARD_HERBE:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2008
	MVI var_VALUE,R0
	TSTR R0
	BNE T604
	MVII #10733,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T604:
	;[2009] 	if value=1 then #backtab(i)=CARD_ROCHER:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2009
	MVI var_VALUE,R0
	CMPI #1,R0
	BNE T605
	MVII #10736,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T605:
	;[2010] 	if value=2 then #backtab(i)=CARD_ARBRECIME:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2010
	MVI var_VALUE,R0
	CMPI #2,R0
	BNE T606
	MVII #10749,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T606:
	;[2011] 	if value=3 then #backtab(i)=CARD_ARBRETRONC:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2011
	MVI var_VALUE,R0
	CMPI #3,R0
	BNE T607
	MVII #10368,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T607:
	;[2012] 	if value=4 then #backtab(i)=CARD_SACG:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2012
	MVI var_VALUE,R0
	CMPI #4,R0
	BNE T608
	MVII #11400,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T608:
	;[2013] 	if value=5 then #backtab(i)=CARD_SACM:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2013
	MVI var_VALUE,R0
	CMPI #5,R0
	BNE T609
	MVII #11408,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T609:
	;[2014] 	if value=6 then #backtab(i)=CARD_SACD:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2014
	MVI var_VALUE,R0
	CMPI #6,R0
	BNE T610
	MVII #11416,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T610:
	;[2015] 	if value=7 then #backtab(i)=CARD_SAC:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2015
	MVI var_VALUE,R0
	CMPI #7,R0
	BNE T611
	MVII #11424,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T611:
	;[2016] 	if value=8 then #backtab(i)=CARD_EAU1:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2016
	MVI var_VALUE,R0
	CMPI #8,R0
	BNE T612
	MVII #15017,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T612:
	;[2017] 	if value=10 then #backtab(i)=CARD_MURAVANT:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2017
	MVI var_VALUE,R0
	CMPI #10,R0
	BNE T613
	MVII #6320,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T613:
	;[2018] 	if value=11 then #backtab(i)=CARD_FrontWALL:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2018
	MVI var_VALUE,R0
	CMPI #11,R0
	BNE T614
	MVII #6472,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T614:
	;[2019] 	if value=12 then #backtab(i)=CARD_BARBELEG:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2019
	MVI var_VALUE,R0
	CMPI #12,R0
	BNE T615
	MVII #10432,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T615:
	;[2020] 	if value=13 then #backtab(i)=CARD_BARBELEM:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2020
	MVI var_VALUE,R0
	CMPI #13,R0
	BNE T616
	MVII #10440,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T616:
	;[2021] 	if value=14 then #backtab(i)=CARD_BARBELED:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2021
	MVI var_VALUE,R0
	CMPI #14,R0
	BNE T617
	MVII #10448,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T617:
	;[2022] 	if value=15 then #backtab(i)=CARD_TOITURE:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2022
	MVI var_VALUE,R0
	CMPI #15,R0
	BNE T618
	MVII #3288,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T618:
	;[2023] 	if value=16 then #backtab(i)=CARD_MURMAISON1:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2023
	MVI var_VALUE,R0
	CMPI #16,R0
	BNE T619
	MVII #6368,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T619:
	;[2024] 	if value=17 then #backtab(i)=CARD_MURMAISONENTREE:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2024
	MVI var_VALUE,R0
	CMPI #17,R0
	BNE T620
	MVII #6376,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T620:
	;[2025] 	if value=18 then #backtab(i)=CARD_MURMAISON2:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2025
	MVI var_VALUE,R0
	CMPI #18,R0
	BNE T621
	MVII #6384,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T621:
	;[2026] 	if value=19 then #backtab(i)=CARD_TOITBUNKER:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2026
	MVI var_VALUE,R0
	CMPI #19,R0
	BNE T622
	MVII #6392,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T622:
	;[2027] 	if value=20 then #backtab(i)=CARD_BUNKERG
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2027
	MVI var_VALUE,R0
	CMPI #20,R0
	BNE T623
	MVII #6400,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
T623:
	;[2028] 	if value=21 then #backtab(i)=CARD_BUNKERD
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2028
	MVI var_VALUE,R0
	CMPI #21,R0
	BNE T624
	MVII #6408,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
T624:
	;[2029] 	if value=22 then #backtab(i)=CARD_BUNKERM:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2029
	MVI var_VALUE,R0
	CMPI #22,R0
	BNE T625
	MVII #6416,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T625:
	;[2030] 	if value=23 then #backtab(i)=CARD_PASSERELLE:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2030
	MVI var_VALUE,R0
	CMPI #23,R0
	BNE T626
	MVII #6464,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T626:
	;[2031] 	if value=24 then #backtab(i)=CARD_MurMaison11:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2031
	MVI var_VALUE,R0
	CMPI #24,R0
	BNE T627
	MVII #6448,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T627:
	;[2032] 	if value=25 then #backtab(i)=CARD_MurMaison22:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2032
	MVI var_VALUE,R0
	CMPI #25,R0
	BNE T628
	MVII #6456,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T628:
	;[2033] 	if value=26 then #backtab(i)=CARD_OmbreBatiment:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2033
	MVI var_VALUE,R0
	CMPI #26,R0
	BNE T629
	MVII #2236,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T629:
	;[2034] 	if value=27 then #backtab(i)=CARD_HERBE1:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2034
	MVI var_VALUE,R0
	CMPI #27,R0
	BNE T630
	MVII #10629,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T630:
	;[2035] 	if value=29 then #backtab(i)=CARD_TOITURE:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2035
	MVI var_VALUE,R0
	CMPI #29,R0
	BNE T631
	MVII #3288,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T631:
	;[2036] 	if value=30 then #backtab(i)=CARD_BLANK:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2036
	MVI var_VALUE,R0
	CMPI #30,R0
	BNE T632
	MVII #10579,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T632:
	;[2037] 	if value=31 then #backtab(i)=CARD_MURBRIQUE:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2037
	MVI var_VALUE,R0
	CMPI #31,R0
	BNE T633
	MVII #6488,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T633:
	;[2038] 	if value=49 then #backtab(i)=CARD_HautPont:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2038
	MVI var_VALUE,R0
	CMPI #49,R0
	BNE T634
	MVII #10712,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T634:
	;[2039] 	if value=50 then #backtab(i)=CARD_BLACK:return
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2039
	MVI var_VALUE,R0
	CMPI #50,R0
	BNE T635
	MVII #2328,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T635:
	;[2040] 	if value=51 then #backtab(i)=CARD_PONT_2:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2040
	MVI var_VALUE,R0
	CMPI #51,R0
	BNE T636
	MVII #6512,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T636:
	;[2041] 	if value=52 then #backtab(i)=CARD_MURBRIQUE_1:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2041
	MVI var_VALUE,R0
	CMPI #52,R0
	BNE T637
	MVII #6496,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T637:
	;[2042] 	if value=53 then #backtab(i)=CARD_COINEAU_BG1:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2042
	MVI var_VALUE,R0
	CMPI #53,R0
	BNE T638
	MVII #10689,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T638:
	;[2043] 	if value=54 then #backtab(i)=CARD_COINEAU_BD1:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2043
	MVI var_VALUE,R0
	CMPI #54,R0
	BNE T639
	MVII #10681,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T639:
	;[2044] 	if value=55 then #backtab(i)=CARD_COINEAU_HG1:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2044
	MVI var_VALUE,R0
	CMPI #55,R0
	BNE T640
	MVII #10673,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T640:
	;[2045] 	if value=56 then #backtab(i)=CARD_COINEAU_HD1:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2045
	MVI var_VALUE,R0
	CMPI #56,R0
	BNE T641
	MVII #10665,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T641:
	;[2046] 	if value=57 then #backtab(i)=CARD_COINEAU_BG:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2046
	MVI var_VALUE,R0
	CMPI #57,R0
	BNE T642
	MVII #10657,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T642:
	;[2047] 	if value=58 then #backtab(i)=CARD_COINEAU_BD:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2047
	MVI var_VALUE,R0
	CMPI #58,R0
	BNE T643
	MVII #10649,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T643:
	;[2048] 	if value=59 then #backtab(i)=CARD_COINEAU_HG:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2048
	MVI var_VALUE,R0
	CMPI #59,R0
	BNE T644
	MVII #10641,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T644:
	;[2049] 	if value=60 then #backtab(i)=CARD_COINEAU_HD:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2049
	MVI var_VALUE,R0
	CMPI #60,R0
	BNE T645
	MVII #10633,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T645:
	;[2050] 	if value=63 then #backtab(i)=CARD_PONT_1:return 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2050
	MVI var_VALUE,R0
	CMPI #63,R0
	BNE T646
	MVII #6504,R0
	MVII #Q2,R3
	ADD var_I,R3
	MVO@ R0,R3
	RETURN
T646:
	;[2051] 	end	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2051
	RETURN
	ENDP
	;[2052] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2052
	;[2053] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2053
	;[2054] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2054
	;[2055] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2055
	;[2056] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2056
	;[2057] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2057
	;[2058] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2058
	;[2059] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2059
	;[2060] '######################
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2060
	;[2061] '####  Zones Jeu   ####
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2061
	;[2062] '######################
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2062
	;[2063] 	include "ZoneJeu.bas"
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2063
	;FILE ZoneJeu.bas
	;[1] '//////////////////////////
	SRCFILE "ZoneJeu.bas",1
	;[2] '////   Zones de jeu  /////
	SRCFILE "ZoneJeu.bas",2
	;[3] '//////////////////////////
	SRCFILE "ZoneJeu.bas",3
	;[4] 
	SRCFILE "ZoneJeu.bas",4
	;[5] 
	SRCFILE "ZoneJeu.bas",5
	;[6] '//// Zone 1 /////
	SRCFILE "ZoneJeu.bas",6
	;[7] '//// 12 x 72
	SRCFILE "ZoneJeu.bas",7
	;[8] Zone1:
	SRCFILE "ZoneJeu.bas",8
	; ZONE1
label_ZONE1:	;[9]     Data 8,8,8,8,8,8,8,8,27,0,2,0,27,27,27,27,27,27,0,0
	SRCFILE "ZoneJeu.bas",9
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 27
	DECLE 0
	DECLE 2
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 0
	;[10]     Data 8,8,8,8,8,8,8,57,0,2,3,0,27,30,0,0,27,27,2,2
	SRCFILE "ZoneJeu.bas",10
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 57
	DECLE 0
	DECLE 2
	DECLE 3
	DECLE 0
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 2
	DECLE 2
	;[11]     Data 8,8,8,8,8,8,57,0,2,2,0,30,0,2,0,27,0,0,3,3
	SRCFILE "ZoneJeu.bas",11
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 57
	DECLE 0
	DECLE 2
	DECLE 2
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 2
	DECLE 0
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 3
	DECLE 3
	;[12]     Data 8,8,8,8,57,30,27,27,3,3,27,0,2,2,2,2,2,2,2,2
	SRCFILE "ZoneJeu.bas",12
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 57
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 3
	DECLE 3
	DECLE 27
	DECLE 0
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 2
	;[13]     Data 27,27,0,30,0,0,0,27,27,27,27,0,3,3,3,3,2,2,2,2
	SRCFILE "ZoneJeu.bas",13
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 3
	DECLE 3
	DECLE 3
	DECLE 3
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 2
	;[14]     Data 27,27,0,0,0,2,2,2,0,27,0,2,2,0,30,0,2,2,2,2
	SRCFILE "ZoneJeu.bas",14
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 0
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 0
	DECLE 27
	DECLE 0
	DECLE 2
	DECLE 2
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 2
	;[15]     Data 2,2,2,0,2,2,3,3,30,27,27,3,3,0,0,0,3,3,3,3
	SRCFILE "ZoneJeu.bas",15
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 0
	DECLE 2
	DECLE 2
	DECLE 3
	DECLE 3
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 3
	DECLE 3
	DECLE 0
	DECLE 0
	DECLE 0
	DECLE 3
	DECLE 3
	DECLE 3
	DECLE 3
	;[16]     Data 3,3,3,0,3,3,0,0,0,0,30,0,27,27,27,27,0,30,0,27
	SRCFILE "ZoneJeu.bas",16
	DECLE 3
	DECLE 3
	DECLE 3
	DECLE 0
	DECLE 3
	DECLE 3
	DECLE 0
	DECLE 0
	DECLE 0
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 27
	;[17]     Data 0,0,30,0,30,0,30,0,2,2,2,19,19,19,0,30,19,19,19,27
	SRCFILE "ZoneJeu.bas",17
	DECLE 0
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 0
	DECLE 30
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 27
	;[18]     Data 19,19,19,19,19,19,30,0,3,3,3,20,22,21,0,27,20,22,21,0
	SRCFILE "ZoneJeu.bas",18
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 30
	DECLE 0
	DECLE 3
	DECLE 3
	DECLE 3
	DECLE 20
	DECLE 22
	DECLE 21
	DECLE 0
	DECLE 27
	DECLE 20
	DECLE 22
	DECLE 21
	DECLE 0
	;[19]     Data 31,31,31,31,31,31,0,27,0,27,27,12,13,14,30,0,12,13,14,30
	SRCFILE "ZoneJeu.bas",19
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 0
	DECLE 27
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 12
	DECLE 13
	DECLE 14
	DECLE 30
	DECLE 0
	DECLE 12
	DECLE 13
	DECLE 14
	DECLE 30
	;[20]     Data 26,26,26,26,26,26,27,27,30,0,27,27,30,0,27,27,0,27,27,27
	SRCFILE "ZoneJeu.bas",20
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 27
	;[21]     Data 15,15,15,30,0,11,0,27,30,0,30,30,0,0,30,27,27,27,27,27
	SRCFILE "ZoneJeu.bas",21
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 30
	DECLE 0
	DECLE 11
	DECLE 0
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 30
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	;[22]     Data 15,15,15,27,0,11,27,0,11,10,10,11,27,0,0,27,27,0,0,27
	SRCFILE "ZoneJeu.bas",22
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 27
	DECLE 0
	DECLE 11
	DECLE 27
	DECLE 0
	DECLE 11
	DECLE 10
	DECLE 10
	DECLE 11
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 27
	;[23]     Data 50,50,24,27,30,10,10,10,10,26,26,11,0,27,27,27,0,0,27,0
	SRCFILE "ZoneJeu.bas",23
	DECLE 50
	DECLE 50
	DECLE 24
	DECLE 27
	DECLE 30
	DECLE 10
	DECLE 10
	DECLE 10
	DECLE 10
	DECLE 26
	DECLE 26
	DECLE 11
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 27
	DECLE 0
	;[24]     Data 26,26,26,0,30,26,26,26,26,27,0,11,0,27,27,1,27,27,27,0
	SRCFILE "ZoneJeu.bas",24
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 0
	DECLE 30
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 27
	DECLE 0
	DECLE 11
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 1
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 0
	;[25]     Data 19,19,19,19,19,27,27,27,27,19,19,19,19,19,27,27,27,27,19,19
	SRCFILE "ZoneJeu.bas",25
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 19
	DECLE 19
	;[26]     Data 31,31,31,31,31,27,0,27,27,31,31,31,31,31,7,0,27,7,31,31
	SRCFILE "ZoneJeu.bas",26
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 27
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 7
	DECLE 0
	DECLE 27
	DECLE 7
	DECLE 31
	DECLE 31
	;[27]     Data 26,26,26,26,26,30,0,27,30,26,26,26,26,26,26,0,27,26,26,26
	SRCFILE "ZoneJeu.bas",27
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 30
	DECLE 0
	DECLE 27
	DECLE 30
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 0
	DECLE 27
	DECLE 26
	DECLE 26
	DECLE 26
	;[28]     Data 2,2,2,2,2,0,27,30,0,0,2,2,2,2,2,0,27,2,2,2
	SRCFILE "ZoneJeu.bas",28
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 0
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 2
	DECLE 0
	DECLE 27
	DECLE 2
	DECLE 2
	DECLE 2
	;[29]     Data 3,3,3,3,3,2,27,27,30,0,3,2,2,3,3,27,27,3,2,2
	SRCFILE "ZoneJeu.bas",29
	DECLE 3
	DECLE 3
	DECLE 3
	DECLE 3
	DECLE 3
	DECLE 2
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 3
	DECLE 2
	DECLE 2
	DECLE 3
	DECLE 3
	DECLE 27
	DECLE 27
	DECLE 3
	DECLE 2
	DECLE 2
	;[30]     Data 0,30,27,27,27,3,0,30,0,2,2,3,3,0,0,27,27,0,3,3
	SRCFILE "ZoneJeu.bas",30
	DECLE 0
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 3
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 2
	DECLE 2
	DECLE 3
	DECLE 3
	DECLE 0
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 3
	DECLE 3
	;[31]     Data 8,8,60,30,27,27,30,30,0,3,2,0,0,30,0,27,0,0,30,0
	SRCFILE "ZoneJeu.bas",31
	DECLE 8
	DECLE 8
	DECLE 60
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 30
	DECLE 0
	DECLE 3
	DECLE 2
	DECLE 0
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 30
	DECLE 0
	;[32]     Data 8,8,8,8,60,27,0,27,27,27,3,27,59,8,8,23,23,8,8,8
	SRCFILE "ZoneJeu.bas",32
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 60
	DECLE 27
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 3
	DECLE 27
	DECLE 59
	DECLE 8
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	;[33]     Data 8,8,8,8,57,27,0,27,27,27,0,30,8,8,8,23,23,8,8,8
	SRCFILE "ZoneJeu.bas",33
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 57
	DECLE 27
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 30
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	;[34]     Data 8,8,57,30,27,27,2,0,0,2,27,0,58,8,8,23,23,8,8,8
	SRCFILE "ZoneJeu.bas",34
	DECLE 8
	DECLE 8
	DECLE 57
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 2
	DECLE 0
	DECLE 0
	DECLE 2
	DECLE 27
	DECLE 0
	DECLE 58
	DECLE 8
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	;[35]     Data 0,30,27,27,0,2,3,0,0,3,27,0,27,0,0,27,27,27,30,0
	SRCFILE "ZoneJeu.bas",35
	DECLE 0
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 2
	DECLE 3
	DECLE 0
	DECLE 0
	DECLE 3
	DECLE 27
	DECLE 0
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 0
	;[36]     Data 51,50,51,50,51,50,51,50,27,27,50,51,50,51,50,51,50,51,50,51
	SRCFILE "ZoneJeu.bas",36
	DECLE 51
	DECLE 50
	DECLE 51
	DECLE 50
	DECLE 51
	DECLE 50
	DECLE 51
	DECLE 50
	DECLE 27
	DECLE 27
	DECLE 50
	DECLE 51
	DECLE 50
	DECLE 51
	DECLE 50
	DECLE 51
	DECLE 50
	DECLE 51
	DECLE 50
	DECLE 51
	;[37]     Data 63,63,63,63,63,63,63,63,27,13,63,63,63,63,63,63,63,63,63,63
	SRCFILE "ZoneJeu.bas",37
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 27
	DECLE 13
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	;[38]     Data 31,31,31,31,31,31,31,31,30,0,31,31,31,31,31,31,31,31,31,31
	SRCFILE "ZoneJeu.bas",38
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 30
	DECLE 0
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	;[39]     Data 31,31,31,31,31,31,31,31,13,27,31,31,31,31,31,31,31,31,31,31
	SRCFILE "ZoneJeu.bas",39
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 13
	DECLE 27
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	;[40]     Data 26,26,26,26,26,26,26,26,0,30,26,26,26,26,26,26,26,26,26,26
	SRCFILE "ZoneJeu.bas",40
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 0
	DECLE 30
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	;[41]     Data 27,27,15,15,15,15,15,27,2,0,27,27,10,10,10,10,10,27,27,0
	SRCFILE "ZoneJeu.bas",41
	DECLE 27
	DECLE 27
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 27
	DECLE 2
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 10
	DECLE 10
	DECLE 10
	DECLE 10
	DECLE 10
	DECLE 27
	DECLE 27
	DECLE 0
	;[42]     Data 0,2,15,15,15,15,15,27,3,30,30,0,31,31,31,31,31,2,27,0
	SRCFILE "ZoneJeu.bas",42
	DECLE 0
	DECLE 2
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 27
	DECLE 3
	DECLE 30
	DECLE 30
	DECLE 0
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 2
	DECLE 27
	DECLE 0
	;[43]     Data 27,3,25,25,25,25,25,27,0,27,0,0,7,19,19,19,7,3,27,27
	SRCFILE "ZoneJeu.bas",43
	DECLE 27
	DECLE 3
	DECLE 25
	DECLE 25
	DECLE 25
	DECLE 25
	DECLE 25
	DECLE 27
	DECLE 0
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 7
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 7
	DECLE 3
	DECLE 27
	DECLE 27
	;[44]     Data 27,0,24,22,22,32,24,27,0,0,27,27,7,20,32,21,7,0,27,0
	SRCFILE "ZoneJeu.bas",44
	DECLE 27
	DECLE 0
	DECLE 24
	DECLE 22
	DECLE 22
	DECLE 32
	DECLE 24
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 7
	DECLE 20
	DECLE 32
	DECLE 21
	DECLE 7
	DECLE 0
	DECLE 27
	DECLE 0
	;[45]     Data 30,27,24,24,24,24,24,27,0,30,0,0,27,12,13,14,27,0,0,30
	SRCFILE "ZoneJeu.bas",45
	DECLE 30
	DECLE 27
	DECLE 24
	DECLE 24
	DECLE 24
	DECLE 24
	DECLE 24
	DECLE 27
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 27
	DECLE 12
	DECLE 13
	DECLE 14
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 30
	;[46]     Data 23,60,26,26,26,26,26,27,59,23,23,60,30,0,30,30,0,30,59,8
	SRCFILE "ZoneJeu.bas",46
	DECLE 23
	DECLE 60
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 27
	DECLE 59
	DECLE 23
	DECLE 23
	DECLE 60
	DECLE 30
	DECLE 0
	DECLE 30
	DECLE 30
	DECLE 0
	DECLE 30
	DECLE 59
	DECLE 8
	;[47]     Data 23,8,60,0,30,0,30,59,8,23,23,8,60,27,27,27,30,59,8,8
	SRCFILE "ZoneJeu.bas",47
	DECLE 23
	DECLE 8
	DECLE 60
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 30
	DECLE 59
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 60
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 59
	DECLE 8
	DECLE 8
	;[48]     Data 27,8,8,54,0,0,53,8,8,23,23,8,8,8,23,23,8,8,8,8
	SRCFILE "ZoneJeu.bas",48
	DECLE 27
	DECLE 8
	DECLE 8
	DECLE 54
	DECLE 0
	DECLE 0
	DECLE 53
	DECLE 8
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	;[49]     Data 30,56,8,8,23,23,8,8,8,23,23,8,8,8,23,23,8,8,8,57
	SRCFILE "ZoneJeu.bas",49
	DECLE 30
	DECLE 56
	DECLE 8
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 57
	;[50]     Data 30,0,27,30,27,27,0,30,0,0,0,58,57,27,27,30,27,27,30,27
	SRCFILE "ZoneJeu.bas",50
	DECLE 30
	DECLE 0
	DECLE 27
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 0
	DECLE 58
	DECLE 57
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 27
	;[51]     Data 51,50,51,50,51,50,51,27,27,27,27,27,27,2,27,51,50,51,50,51
	SRCFILE "ZoneJeu.bas",51
	DECLE 51
	DECLE 50
	DECLE 51
	DECLE 50
	DECLE 51
	DECLE 50
	DECLE 51
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 2
	DECLE 27
	DECLE 51
	DECLE 50
	DECLE 51
	DECLE 50
	DECLE 51
	;[52]     Data 63,63,63,63,63,63,63,2,27,2,0,27,0,3,27,63,63,63,63,63
	SRCFILE "ZoneJeu.bas",52
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 2
	DECLE 27
	DECLE 2
	DECLE 0
	DECLE 27
	DECLE 0
	DECLE 3
	DECLE 27
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	DECLE 63
	;[53]     Data 31,31,31,31,31,32,31,3,30,3,0,0,0,0,27,31,32,31,31,31
	SRCFILE "ZoneJeu.bas",53
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 32
	DECLE 31
	DECLE 3
	DECLE 30
	DECLE 3
	DECLE 0
	DECLE 0
	DECLE 0
	DECLE 0
	DECLE 27
	DECLE 31
	DECLE 32
	DECLE 31
	DECLE 31
	DECLE 31
	;[54]     Data 31,31,31,31,31,31,31,27,27,27,27,1,2,30,0,31,31,31,31,31
	SRCFILE "ZoneJeu.bas",54
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 1
	DECLE 2
	DECLE 30
	DECLE 0
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	DECLE 31
	;[55]     Data 26,26,26,26,26,26,26,0,0,2,27,30,3,27,27,26,26,26,26,26
	SRCFILE "ZoneJeu.bas",55
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 0
	DECLE 0
	DECLE 2
	DECLE 27
	DECLE 30
	DECLE 3
	DECLE 27
	DECLE 27
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	;[56]     Data 30,19,19,19,19,19,2,27,0,3,0,27,0,27,0,27,27,0,0,30
	SRCFILE "ZoneJeu.bas",56
	DECLE 30
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 2
	DECLE 27
	DECLE 0
	DECLE 3
	DECLE 0
	DECLE 27
	DECLE 0
	DECLE 27
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 30
	;[57]     Data 27,11,20,32,21,11,3,0,30,30,0,30,27,27,30,0,27,30,27,27
	SRCFILE "ZoneJeu.bas",57
	DECLE 27
	DECLE 11
	DECLE 20
	DECLE 32
	DECLE 21
	DECLE 11
	DECLE 3
	DECLE 0
	DECLE 30
	DECLE 30
	DECLE 0
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 27
	DECLE 30
	DECLE 27
	DECLE 27
	;[58]     Data 27,12,13,13,13,14,27,30,2,30,0,15,15,15,15,0,27,27,1,27
	SRCFILE "ZoneJeu.bas",58
	DECLE 27
	DECLE 12
	DECLE 13
	DECLE 13
	DECLE 13
	DECLE 14
	DECLE 27
	DECLE 30
	DECLE 2
	DECLE 30
	DECLE 0
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 1
	DECLE 27
	;[59]     Data 27,27,30,30,27,30,0,0,3,19,19,15,15,15,15,19,19,0,0,27
	SRCFILE "ZoneJeu.bas",59
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 30
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 3
	DECLE 19
	DECLE 19
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 19
	DECLE 19
	DECLE 0
	DECLE 0
	DECLE 27
	;[60]     Data 0,30,27,27,30,0,0,4,5,31,31,16,50,50,16,31,31,5,6,0
	SRCFILE "ZoneJeu.bas",60
	DECLE 0
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 4
	DECLE 5
	DECLE 31
	DECLE 31
	DECLE 16
	DECLE 50
	DECLE 50
	DECLE 16
	DECLE 31
	DECLE 31
	DECLE 5
	DECLE 6
	DECLE 0
	;[61]     Data 30,0,27,27,27,0,0,26,26,26,26,26,26,26,26,26,26,26,26,0
	SRCFILE "ZoneJeu.bas",61
	DECLE 30
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 0
	;[62]     Data 19,19,19,0,27,59,8,8,8,8,8,8,23,23,8,8,8,60,30,30
	SRCFILE "ZoneJeu.bas",62
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 0
	DECLE 27
	DECLE 59
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 60
	DECLE 30
	DECLE 30
	;[63]     Data 20,32,21,27,30,8,8,8,19,19,19,8,23,23,8,8,8,8,30,2
	SRCFILE "ZoneJeu.bas",63
	DECLE 20
	DECLE 32
	DECLE 21
	DECLE 27
	DECLE 30
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 19
	DECLE 19
	DECLE 19
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 30
	DECLE 2
	;[64]     Data 26,26,26,27,0,8,55,12,20,32,21,14,0,27,30,56,8,8,0,3
	SRCFILE "ZoneJeu.bas",64
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 27
	DECLE 0
	DECLE 8
	DECLE 55
	DECLE 12
	DECLE 20
	DECLE 32
	DECLE 21
	DECLE 14
	DECLE 0
	DECLE 27
	DECLE 30
	DECLE 56
	DECLE 8
	DECLE 8
	DECLE 0
	DECLE 3
	;[65]     Data 27,27,0,30,0,8,30,12,13,13,13,14,27,30,2,30,8,8,0,27
	SRCFILE "ZoneJeu.bas",65
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 8
	DECLE 30
	DECLE 12
	DECLE 13
	DECLE 13
	DECLE 13
	DECLE 14
	DECLE 27
	DECLE 30
	DECLE 2
	DECLE 30
	DECLE 8
	DECLE 8
	DECLE 0
	DECLE 27
	;[66]     Data 30,27,27,27,53,8,54,0,27,30,27,27,30,0,3,53,8,8,54,0
	SRCFILE "ZoneJeu.bas",66
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 53
	DECLE 8
	DECLE 54
	DECLE 0
	DECLE 27
	DECLE 30
	DECLE 27
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 3
	DECLE 53
	DECLE 8
	DECLE 8
	DECLE 54
	DECLE 0
	;[67]     Data 8,23,23,8,8,8,8,8,8,23,23,8,8,8,8,8,8,8,8,8
	SRCFILE "ZoneJeu.bas",67
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	;[68]     Data 8,23,23,8,8,8,8,8,8,23,23,8,8,8,8,8,15,15,15,15
	SRCFILE "ZoneJeu.bas",68
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 23
	DECLE 23
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 8
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	;[69]     Data 27,30,0,27,0,27,30,0,27,27,27,27,27,0,0,27,15,15,15,15
	SRCFILE "ZoneJeu.bas",69
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 27
	DECLE 0
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 27
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	;[70]     Data 2,15,15,15,27,11,10,10,30,27,0,0,10,10,11,0,25,50,50,25
	SRCFILE "ZoneJeu.bas",70
	DECLE 2
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 27
	DECLE 11
	DECLE 10
	DECLE 10
	DECLE 30
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 10
	DECLE 10
	DECLE 11
	DECLE 0
	DECLE 25
	DECLE 50
	DECLE 50
	DECLE 25
	;[71]     Data 3,15,15,15,27,11,31,31,27,30,27,0,31,31,11,0,26,26,26,26
	SRCFILE "ZoneJeu.bas",71
	DECLE 3
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 27
	DECLE 11
	DECLE 31
	DECLE 31
	DECLE 27
	DECLE 30
	DECLE 27
	DECLE 0
	DECLE 31
	DECLE 31
	DECLE 11
	DECLE 0
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	;[72]     Data 0,16,17,18,27,11,26,26,27,4,6,27,26,26,10,10,10,0,0,10
	SRCFILE "ZoneJeu.bas",72
	DECLE 0
	DECLE 16
	DECLE 17
	DECLE 18
	DECLE 27
	DECLE 11
	DECLE 26
	DECLE 26
	DECLE 27
	DECLE 4
	DECLE 6
	DECLE 27
	DECLE 26
	DECLE 26
	DECLE 10
	DECLE 10
	DECLE 10
	DECLE 0
	DECLE 0
	DECLE 10
	;[73]     Data 27,26,26,26,27,11,0,27,30,26,26,0,0,0,26,26,26,0,27,26
	SRCFILE "ZoneJeu.bas",73
	DECLE 27
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 27
	DECLE 11
	DECLE 0
	DECLE 27
	DECLE 30
	DECLE 26
	DECLE 26
	DECLE 0
	DECLE 0
	DECLE 0
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 0
	DECLE 27
	DECLE 26
	;[74]     Data 27,27,0,10,10,10,0,27,27,0,0,15,15,15,15,15,15,27,30,2
	SRCFILE "ZoneJeu.bas",74
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 10
	DECLE 10
	DECLE 10
	DECLE 0
	DECLE 27
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 27
	DECLE 30
	DECLE 2
	;[75]     Data 1,27,0,26,26,26,0,2,30,27,30,15,15,15,15,15,15,0,0,3
	SRCFILE "ZoneJeu.bas",75
	DECLE 1
	DECLE 27
	DECLE 0
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 0
	DECLE 2
	DECLE 30
	DECLE 27
	DECLE 30
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 15
	DECLE 0
	DECLE 0
	DECLE 3
	;[76]     Data 0,0,27,1,0,30,0,3,0,0,0,25,25,25,25,25,25,2,30,0
	SRCFILE "ZoneJeu.bas",76
	DECLE 0
	DECLE 0
	DECLE 27
	DECLE 1
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 3
	DECLE 0
	DECLE 0
	DECLE 0
	DECLE 25
	DECLE 25
	DECLE 25
	DECLE 25
	DECLE 25
	DECLE 25
	DECLE 2
	DECLE 30
	DECLE 0
	;[77]     Data 27,30,0,0,30,2,30,27,0,1,0,24,22,24,24,22,24,3,0,30
	SRCFILE "ZoneJeu.bas",77
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 30
	DECLE 2
	DECLE 30
	DECLE 27
	DECLE 0
	DECLE 1
	DECLE 0
	DECLE 24
	DECLE 22
	DECLE 24
	DECLE 24
	DECLE 22
	DECLE 24
	DECLE 3
	DECLE 0
	DECLE 30
	;[78]     Data 30,2,30,30,0,3,27,30,0,0,30,24,24,24,24,24,24,0,0,2
	SRCFILE "ZoneJeu.bas",78
	DECLE 30
	DECLE 2
	DECLE 30
	DECLE 30
	DECLE 0
	DECLE 3
	DECLE 27
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 30
	DECLE 24
	DECLE 24
	DECLE 24
	DECLE 24
	DECLE 24
	DECLE 24
	DECLE 0
	DECLE 0
	DECLE 2
	;[79]     Data 0,3,30,2,30,0,30,2,30,0,30,26,26,26,26,26,26,0,27,3
	SRCFILE "ZoneJeu.bas",79
	DECLE 0
	DECLE 3
	DECLE 30
	DECLE 2
	DECLE 30
	DECLE 0
	DECLE 30
	DECLE 2
	DECLE 30
	DECLE 0
	DECLE 30
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 26
	DECLE 0
	DECLE 27
	DECLE 3
	;[80]     Data 27,0,0,3,0,27,0,3,0,0,30,0,0,30,0,0,30,0,0,0
	SRCFILE "ZoneJeu.bas",80
	DECLE 27
	DECLE 0
	DECLE 0
	DECLE 3
	DECLE 0
	DECLE 27
	DECLE 0
	DECLE 3
	DECLE 0
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 30
	DECLE 0
	DECLE 0
	DECLE 0
	;ENDFILE
	;FILE C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas
	;[2064] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2064
	;[2065] 	
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2065
	;[2066] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2066
	;[2067] '#################################
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2067
	;[2068] '##### Ressources Graphiques #####
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2068
	;[2069] '#################################
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2069
	;[2070] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2070
	;[2071] '//////////////////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2071
	;[2072] '//// TileSet /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2072
	;[2073] '//////////////////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2073
	;[2074] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2074
	;[2075] 	include "TileSet.bas"
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2075
	;FILE TileSet.bas
	;[1] '/////////////////////////
	SRCFILE "TileSet.bas",1
	;[2] '//// TileSet Divers /////
	SRCFILE "TileSet.bas",2
	;[3] '/////////////////////////
	SRCFILE "TileSet.bas",3
	;[4] 
	SRCFILE "TileSet.bas",4
	;[5] '///// Couteau /////
	SRCFILE "TileSet.bas",5
	;[6] Couteau:
	SRCFILE "TileSet.bas",6
	; COUTEAU
label_COUTEAU:	;[7]     BITMAP "........"
	SRCFILE "TileSet.bas",7
	;[8]     BITMAP "..#....."
	SRCFILE "TileSet.bas",8
	DECLE 8192
	;[9]     BITMAP ".#......"
	SRCFILE "TileSet.bas",9
	;[10]     BITMAP "##.####."
	SRCFILE "TileSet.bas",10
	DECLE 56896
	;[11]     BITMAP "##..####"
	SRCFILE "TileSet.bas",11
	;[12]     BITMAP "##.####."
	SRCFILE "TileSet.bas",12
	DECLE 57039
	;[13]     BITMAP ".#......"
	SRCFILE "TileSet.bas",13
	;[14]     BITMAP "..#....."
	SRCFILE "TileSet.bas",14
	DECLE 8256
	;[15] 	
	SRCFILE "TileSet.bas",15
	;[16] '///// Bazooka /////
	SRCFILE "TileSet.bas",16
	;[17] BazookaTILE:
	SRCFILE "TileSet.bas",17
	; BAZOOKATILE
label_BAZOOKATILE:	;[18] 	BITMAP "........"
	SRCFILE "TileSet.bas",18
	;[19] 	BITMAP "#......#"
	SRCFILE "TileSet.bas",19
	DECLE 33024
	;[20] 	BITMAP "########"
	SRCFILE "TileSet.bas",20
	;[21] 	BITMAP "#......#"
	SRCFILE "TileSet.bas",21
	DECLE 33279
	;[22] 	BITMAP "########"
	SRCFILE "TileSet.bas",22
	;[23] 	BITMAP "########"
	SRCFILE "TileSet.bas",23
	DECLE 65535
	;[24] 	BITMAP "#.###..#"
	SRCFILE "TileSet.bas",24
	;[25] 	BITMAP "..#....."
	SRCFILE "TileSet.bas",25
	DECLE 8377
	;[26] 
	SRCFILE "TileSet.bas",26
	;[27] '///// Impact Explosion /////
	SRCFILE "TileSet.bas",27
	;[28] ImpactExplosion:
	SRCFILE "TileSet.bas",28
	; IMPACTEXPLOSION
label_IMPACTEXPLOSION:	;[29] 	BITMAP "..####.."
	SRCFILE "TileSet.bas",29
	;[30] 	BITMAP ".######."
	SRCFILE "TileSet.bas",30
	DECLE 32316
	;[31] 	BITMAP "########"
	SRCFILE "TileSet.bas",31
	;[32] 	BITMAP "##....##"
	SRCFILE "TileSet.bas",32
	DECLE 50175
	;[33] 	BITMAP "#..#...#"
	SRCFILE "TileSet.bas",33
	;[34] 	BITMAP "....#..."
	SRCFILE "TileSet.bas",34
	DECLE 2193
	;[35] 	BITMAP ".#....#."
	SRCFILE "TileSet.bas",35
	;[36] 	BITMAP "..####.."
	SRCFILE "TileSet.bas",36
	DECLE 15426
	;[37] 	
	SRCFILE "TileSet.bas",37
	;[38] '///// Grenades /////
	SRCFILE "TileSet.bas",38
	;[39] GrenadeTILE:
	SRCFILE "TileSet.bas",39
	; GRENADETILE
label_GRENADETILE:	;[40] 	BITMAP "...##.#."
	SRCFILE "TileSet.bas",40
	;[41] 	BITMAP "..####.."
	SRCFILE "TileSet.bas",41
	DECLE 15386
	;[42] 	BITMAP ".###..#."
	SRCFILE "TileSet.bas",42
	;[43] 	BITMAP ".####.#."
	SRCFILE "TileSet.bas",43
	DECLE 31346
	;[44] 	BITMAP ".######."
	SRCFILE "TileSet.bas",44
	;[45] 	BITMAP ".######."
	SRCFILE "TileSet.bas",45
	DECLE 32382
	;[46] 	BITMAP ".######."
	SRCFILE "TileSet.bas",46
	;[47] 	BITMAP "..####.."
	SRCFILE "TileSet.bas",47
	DECLE 15486
	;[48] 
	SRCFILE "TileSet.bas",48
	;[49] '///// Pistolet /////
	SRCFILE "TileSet.bas",49
	;[50] PistoletTILE:
	SRCFILE "TileSet.bas",50
	; PISTOLETTILE
label_PISTOLETTILE:	;[51] 	BITMAP "......#."
	SRCFILE "TileSet.bas",51
	;[52] 	BITMAP ".######."
	SRCFILE "TileSet.bas",52
	DECLE 32258
	;[53] 	BITMAP ".######."
	SRCFILE "TileSet.bas",53
	;[54] 	BITMAP ".##.#..."
	SRCFILE "TileSet.bas",54
	DECLE 26750
	;[55] 	BITMAP ".###...."
	SRCFILE "TileSet.bas",55
	;[56] 	BITMAP ".##....."
	SRCFILE "TileSet.bas",56
	DECLE 24688
	;[57] 	BITMAP ".##....."
	SRCFILE "TileSet.bas",57
	;[58] 	BITMAP "........"
	SRCFILE "TileSet.bas",58
	DECLE 96
	;[59] 
	SRCFILE "TileSet.bas",59
	;[60] '///// Vies Papi /////
	SRCFILE "TileSet.bas",60
	;[61] ViePapi:
	SRCFILE "TileSet.bas",61
	; VIEPAPI
label_VIEPAPI:	;[62] 	BITMAP ".##..##."
	SRCFILE "TileSet.bas",62
	;[63] 	BITMAP "#####..#"
	SRCFILE "TileSet.bas",63
	DECLE 63846
	;[64] 	BITMAP "######.#"
	SRCFILE "TileSet.bas",64
	;[65] 	BITMAP "########"
	SRCFILE "TileSet.bas",65
	DECLE 65533
	;[66] 	BITMAP "########"
	SRCFILE "TileSet.bas",66
	;[67] 	BITMAP ".######."
	SRCFILE "TileSet.bas",67
	DECLE 32511
	;[68] 	BITMAP "..####.."
	SRCFILE "TileSet.bas",68
	;[69] 	BITMAP "...##..."
	SRCFILE "TileSet.bas",69
	DECLE 6204
	;[70] 
	SRCFILE "TileSet.bas",70
	;[71] '//// Sol Herbe ////
	SRCFILE "TileSet.bas",71
	;[72] SOL_HERBE:
	SRCFILE "TileSet.bas",72
	; SOL_HERBE
label_SOL_HERBE:	;[73] 	BITMAP "......#."
	SRCFILE "TileSet.bas",73
	;[74] 	BITMAP ".#...#.."
	SRCFILE "TileSet.bas",74
	DECLE 17410
	;[75] 	BITMAP "#....#.."
	SRCFILE "TileSet.bas",75
	;[76] 	BITMAP "#....##."
	SRCFILE "TileSet.bas",76
	DECLE 34436
	;[77] 	BITMAP "..#....."
	SRCFILE "TileSet.bas",77
	;[78] 	BITMAP "..#.#..."
	SRCFILE "TileSet.bas",78
	DECLE 10272
	;[79] 	BITMAP ".#...#.."
	SRCFILE "TileSet.bas",79
	;[80] 	BITMAP ".##....."
	SRCFILE "TileSet.bas",80
	DECLE 24644
	;[81] 	
	SRCFILE "TileSet.bas",81
	;[82] HERBE1:
	SRCFILE "TileSet.bas",82
	; HERBE1
label_HERBE1:	;[83]     BITMAP "........"
	SRCFILE "TileSet.bas",83
	;[84]     BITMAP "..#...#."
	SRCFILE "TileSet.bas",84
	DECLE 8704
	;[85]     BITMAP ".#......"
	SRCFILE "TileSet.bas",85
	;[86]     BITMAP "....#..."
	SRCFILE "TileSet.bas",86
	DECLE 2112
	;[87]     BITMAP "...#...."
	SRCFILE "TileSet.bas",87
	;[88]     BITMAP "...##..."
	SRCFILE "TileSet.bas",88
	DECLE 6160
	;[89]     BITMAP "........"
	SRCFILE "TileSet.bas",89
	;[90]     BITMAP "........"
	SRCFILE "TileSet.bas",90
	DECLE 0
	;[91] 	
	SRCFILE "TileSet.bas",91
	;[92] '//// Rocher ////
	SRCFILE "TileSet.bas",92
	;[93] ROCHER:
	SRCFILE "TileSet.bas",93
	; ROCHER
label_ROCHER:	;[94] 	BITMAP "........"
	SRCFILE "TileSet.bas",94
	;[95] 	BITMAP "..#####."
	SRCFILE "TileSet.bas",95
	DECLE 15872
	;[96] 	BITMAP ".##.####"
	SRCFILE "TileSet.bas",96
	;[97] 	BITMAP "########"
	SRCFILE "TileSet.bas",97
	DECLE 65391
	;[98] 	BITMAP "#.######"
	SRCFILE "TileSet.bas",98
	;[99] 	BITMAP "#.######"
	SRCFILE "TileSet.bas",99
	DECLE 49087
	;[100] 	BITMAP ".#.#####"
	SRCFILE "TileSet.bas",100
	;[101] 	BITMAP "#######."
	SRCFILE "TileSet.bas",101
	DECLE 65119
	;[102] 
	SRCFILE "TileSet.bas",102
	;[103] '//// Arbre ////
	SRCFILE "TileSet.bas",103
	;[104] ARBRE_CIME:
	SRCFILE "TileSet.bas",104
	; ARBRE_CIME
label_ARBRE_CIME:	;[105] 	BITMAP "...###.#"
	SRCFILE "TileSet.bas",105
	;[106] 	BITMAP "#.#####."
	SRCFILE "TileSet.bas",106
	DECLE 48669
	;[107] 	BITMAP ".#######"
	SRCFILE "TileSet.bas",107
	;[108] 	BITMAP "########"
	SRCFILE "TileSet.bas",108
	DECLE 65407
	;[109] 	BITMAP ".#######"
	SRCFILE "TileSet.bas",109
	;[110] 	BITMAP "##.#####"
	SRCFILE "TileSet.bas",110
	DECLE 57215
	;[111] 	BITMAP "####.###"
	SRCFILE "TileSet.bas",111
	;[112] 	BITMAP "..#####."
	SRCFILE "TileSet.bas",112
	DECLE 16119
	;[113] 
	SRCFILE "TileSet.bas",113
	;[114] ARBRE_TRONC:
	SRCFILE "TileSet.bas",114
	; ARBRE_TRONC
label_ARBRE_TRONC:	;[115] 	BITMAP "...###.."	'BG	:	DarkGreen/FG : Black
	SRCFILE "TileSet.bas",115
	;[116] 	BITMAP "...##..."
	SRCFILE "TileSet.bas",116
	DECLE 6172
	;[117] 	BITMAP ".*.##..."
	SRCFILE "TileSet.bas",117
	;[118] 	BITMAP "..*##..."
	SRCFILE "TileSet.bas",118
	DECLE 14424
	;[119] 	BITMAP "...##..."
	SRCFILE "TileSet.bas",119
	;[120] 	BITMAP "...##..."
	SRCFILE "TileSet.bas",120
	DECLE 6168
	;[121] 	BITMAP "...##*.."
	SRCFILE "TileSet.bas",121
	;[122] 	BITMAP ".**....."
	SRCFILE "TileSet.bas",122
	DECLE 24604
	;[123] 	
	SRCFILE "TileSet.bas",123
	;[124] '///// Sacs /////
	SRCFILE "TileSet.bas",124
	;[125] SAC_G:
	SRCFILE "TileSet.bas",125
	; SAC_G
label_SAC_G:	;[126] 	BITMAP ".#######"
	SRCFILE "TileSet.bas",126
	;[127] 	BITMAP "#...*..."
	SRCFILE "TileSet.bas",127
	DECLE 34943
	;[128] 	BITMAP "#......."
	SRCFILE "TileSet.bas",128
	;[129] 	BITMAP "#......."
	SRCFILE "TileSet.bas",129
	DECLE 32896
	;[130] 	BITMAP "#......."
	SRCFILE "TileSet.bas",130
	;[131] 	BITMAP "##.#.#.#"
	SRCFILE "TileSet.bas",131
	DECLE 54656
	;[132] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",132
	;[133] 	BITMAP ".#######"
	SRCFILE "TileSet.bas",133
	DECLE 32682
	;[134] 
	SRCFILE "TileSet.bas",134
	;[135] SAC_M:
	SRCFILE "TileSet.bas",135
	; SAC_M
label_SAC_M:	;[136] 	BITMAP "*#######"
	SRCFILE "TileSet.bas",136
	;[137] 	BITMAP "....*..."
	SRCFILE "TileSet.bas",137
	DECLE 2303
	;[138] 	BITMAP "........"
	SRCFILE "TileSet.bas",138
	;[139] 	BITMAP "........"
	SRCFILE "TileSet.bas",139
	DECLE 0
	;[140] 	BITMAP "........"
	SRCFILE "TileSet.bas",140
	;[141] 	BITMAP ".#.###.#"
	SRCFILE "TileSet.bas",141
	DECLE 23808
	;[142] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",142
	;[143] 	BITMAP "########"
	SRCFILE "TileSet.bas",143
	DECLE 65450
	;[144] 	
	SRCFILE "TileSet.bas",144
	;[145] SAC_D:
	SRCFILE "TileSet.bas",145
	; SAC_D
label_SAC_D:	;[146] 	BITMAP "#######."
	SRCFILE "TileSet.bas",146
	;[147] 	BITMAP "...*...#"
	SRCFILE "TileSet.bas",147
	DECLE 4606
	;[148] 	BITMAP ".......#"
	SRCFILE "TileSet.bas",148
	;[149] 	BITMAP ".......#"
	SRCFILE "TileSet.bas",149
	DECLE 257
	;[150] 	BITMAP ".......#"
	SRCFILE "TileSet.bas",150
	;[151] 	BITMAP "*#.#.#.#"
	SRCFILE "TileSet.bas",151
	DECLE 54529
	;[152] 	BITMAP "#.#.#.##"
	SRCFILE "TileSet.bas",152
	;[153] 	BITMAP "#######."
	SRCFILE "TileSet.bas",153
	DECLE 65195
	;[154] 
	SRCFILE "TileSet.bas",154
	;[155] SAC:
	SRCFILE "TileSet.bas",155
	; SAC
label_SAC:	;[156] 	BITMAP "*######*"
	SRCFILE "TileSet.bas",156
	;[157] 	BITMAP "#......#"
	SRCFILE "TileSet.bas",157
	DECLE 33279
	;[158] 	BITMAP "#......#"
	SRCFILE "TileSet.bas",158
	;[159] 	BITMAP "#......#"
	SRCFILE "TileSet.bas",159
	DECLE 33153
	;[160] 	BITMAP "#......#"
	SRCFILE "TileSet.bas",160
	;[161] 	BITMAP "##.#.#.#"
	SRCFILE "TileSet.bas",161
	DECLE 54657
	;[162] 	BITMAP "#.#.#.##"
	SRCFILE "TileSet.bas",162
	;[163] 	BITMAP "########"
	SRCFILE "TileSet.bas",163
	DECLE 65451
	;[164] 	
	SRCFILE "TileSet.bas",164
	;[165] '///// Eau /////
	SRCFILE "TileSet.bas",165
	;[166] EAU_1:
	SRCFILE "TileSet.bas",166
	; EAU_1
label_EAU_1:	;[167] 	BITMAP "########"	'BG	:	LIGHTBLUE/FG : BLUE
	SRCFILE "TileSet.bas",167
	;[168] 	BITMAP "#...####"
	SRCFILE "TileSet.bas",168
	DECLE 36863
	;[169] 	BITMAP "########"
	SRCFILE "TileSet.bas",169
	;[170] 	BITMAP "####..##"
	SRCFILE "TileSet.bas",170
	DECLE 62463
	;[171] 	BITMAP "########"
	SRCFILE "TileSet.bas",171
	;[172] 	BITMAP "#..#####"
	SRCFILE "TileSet.bas",172
	DECLE 40959
	;[173] 	BITMAP "#####..#"
	SRCFILE "TileSet.bas",173
	;[174] 	BITMAP "########"
	SRCFILE "TileSet.bas",174
	DECLE 65529
	;[175] 
	SRCFILE "TileSet.bas",175
	;[176] EAU_2:
	SRCFILE "TileSet.bas",176
	; EAU_2
label_EAU_2:	;[177] 	BITMAP "########"
	SRCFILE "TileSet.bas",177
	;[178] 	BITMAP "...####."
	SRCFILE "TileSet.bas",178
	DECLE 7935
	;[179] 	BITMAP "########"
	SRCFILE "TileSet.bas",179
	;[180] 	BITMAP "###..###"
	SRCFILE "TileSet.bas",180
	DECLE 59391
	;[181] 	BITMAP "########"
	SRCFILE "TileSet.bas",181
	;[182] 	BITMAP "..######"
	SRCFILE "TileSet.bas",182
	DECLE 16383
	;[183] 	BITMAP "####..##"
	SRCFILE "TileSet.bas",183
	;[184] 	BITMAP "########"
	SRCFILE "TileSet.bas",184
	DECLE 65523
	;[185] 	
	SRCFILE "TileSet.bas",185
	;[186] EAU_3:
	SRCFILE "TileSet.bas",186
	; EAU_3
label_EAU_3:	;[187] 	BITMAP "########"
	SRCFILE "TileSet.bas",187
	;[188] 	BITMAP "..####.."
	SRCFILE "TileSet.bas",188
	DECLE 15615
	;[189] 	BITMAP "########"
	SRCFILE "TileSet.bas",189
	;[190] 	BITMAP "##..####"
	SRCFILE "TileSet.bas",190
	DECLE 53247
	;[191] 	BITMAP "########"
	SRCFILE "TileSet.bas",191
	;[192] 	BITMAP ".######."
	SRCFILE "TileSet.bas",192
	DECLE 32511
	;[193] 	BITMAP "###..###"
	SRCFILE "TileSet.bas",193
	;[194] 	BITMAP "########"
	SRCFILE "TileSet.bas",194
	DECLE 65511
	;[195] 	
	SRCFILE "TileSet.bas",195
	;[196] EAU_4:
	SRCFILE "TileSet.bas",196
	; EAU_4
label_EAU_4:	;[197] 	BITMAP "########"
	SRCFILE "TileSet.bas",197
	;[198] 	BITMAP ".####..."
	SRCFILE "TileSet.bas",198
	DECLE 30975
	;[199] 	BITMAP "########"
	SRCFILE "TileSet.bas",199
	;[200] 	BITMAP "#..#####"
	SRCFILE "TileSet.bas",200
	DECLE 40959
	;[201] 	BITMAP "########"
	SRCFILE "TileSet.bas",201
	;[202] 	BITMAP "######.."
	SRCFILE "TileSet.bas",202
	DECLE 64767
	;[203] 	BITMAP "##..###*"
	SRCFILE "TileSet.bas",203
	;[204] 	BITMAP "########"
	SRCFILE "TileSet.bas",204
	DECLE 65487
	;[205] 	
	SRCFILE "TileSet.bas",205
	;[206] EAU_5:
	SRCFILE "TileSet.bas",206
	; EAU_5
label_EAU_5:	;[207] 	BITMAP "########"
	SRCFILE "TileSet.bas",207
	;[208] 	BITMAP "####...."
	SRCFILE "TileSet.bas",208
	DECLE 61695
	;[209] 	BITMAP "########"
	SRCFILE "TileSet.bas",209
	;[210] 	BITMAP "..#####*"
	SRCFILE "TileSet.bas",210
	DECLE 16383
	;[211] 	BITMAP "########"
	SRCFILE "TileSet.bas",211
	;[212] 	BITMAP "#####..*"
	SRCFILE "TileSet.bas",212
	DECLE 63999
	;[213] 	BITMAP "#..###**"
	SRCFILE "TileSet.bas",213
	;[214] 	BITMAP "########"
	SRCFILE "TileSet.bas",214
	DECLE 65439
	;[215] 	
	SRCFILE "TileSet.bas",215
	;[216] EAU_6:
	SRCFILE "TileSet.bas",216
	; EAU_6
label_EAU_6:	;[217] 	BITMAP "########"
	SRCFILE "TileSet.bas",217
	;[218] 	BITMAP "###....*"
	SRCFILE "TileSet.bas",218
	DECLE 57855
	;[219] 	BITMAP "########"
	SRCFILE "TileSet.bas",219
	;[220] 	BITMAP ".#####*."
	SRCFILE "TileSet.bas",220
	DECLE 32511
	;[221] 	BITMAP "########"
	SRCFILE "TileSet.bas",221
	;[222] 	BITMAP "####..**"
	SRCFILE "TileSet.bas",222
	DECLE 62463
	;[223] 	BITMAP "..###***"
	SRCFILE "TileSet.bas",223
	;[224] 	BITMAP "########"
	SRCFILE "TileSet.bas",224
	DECLE 65343
	;[225] 	
	SRCFILE "TileSet.bas",225
	;[226] EAU_7:
	SRCFILE "TileSet.bas",226
	; EAU_7
label_EAU_7:	;[227] 	BITMAP "########"
	SRCFILE "TileSet.bas",227
	;[228] 	BITMAP "##....**"
	SRCFILE "TileSet.bas",228
	DECLE 50175
	;[229] 	BITMAP "########"
	SRCFILE "TileSet.bas",229
	;[230] 	BITMAP "#####*.."
	SRCFILE "TileSet.bas",230
	DECLE 64767
	;[231] 	BITMAP "########"
	SRCFILE "TileSet.bas",231
	;[232] 	BITMAP "###..***"
	SRCFILE "TileSet.bas",232
	DECLE 59391
	;[233] 	BITMAP ".###***."
	SRCFILE "TileSet.bas",233
	;[234] 	BITMAP "########"
	SRCFILE "TileSet.bas",234
	DECLE 65406
	;[235] 	
	SRCFILE "TileSet.bas",235
	;[236] EAU_8:
	SRCFILE "TileSet.bas",236
	; EAU_8
label_EAU_8:	;[237] 	BITMAP "########"
	SRCFILE "TileSet.bas",237
	;[238] 	BITMAP "#....***"
	SRCFILE "TileSet.bas",238
	DECLE 34815
	;[239] 	BITMAP "########"
	SRCFILE "TileSet.bas",239
	;[240] 	BITMAP "#####..*"
	SRCFILE "TileSet.bas",240
	DECLE 63999
	;[241] 	BITMAP "########"
	SRCFILE "TileSet.bas",241
	;[242] 	BITMAP "##..****"
	SRCFILE "TileSet.bas",242
	DECLE 53247
	;[243] 	BITMAP "###***.."
	SRCFILE "TileSet.bas",243
	;[244] 	BITMAP "########"
	SRCFILE "TileSet.bas",244
	DECLE 65532
	;[245] 	
	SRCFILE "TileSet.bas",245
	;[246] 	
	SRCFILE "TileSet.bas",246
	;[247] 	
	SRCFILE "TileSet.bas",247
	;[248] '///// Murs /////
	SRCFILE "TileSet.bas",248
	;[249] FrontWALL:
	SRCFILE "TileSet.bas",249
	; FRONTWALL
label_FRONTWALL:	;[250]     BITMAP "........"
	SRCFILE "TileSet.bas",250
	;[251]     BITMAP "........"
	SRCFILE "TileSet.bas",251
	DECLE 0
	;[252]     BITMAP "........"
	SRCFILE "TileSet.bas",252
	;[253]     BITMAP "........"
	SRCFILE "TileSet.bas",253
	DECLE 0
	;[254]     BITMAP "........"
	SRCFILE "TileSet.bas",254
	;[255]     BITMAP "........"
	SRCFILE "TileSet.bas",255
	DECLE 0
	;[256]     BITMAP "........"
	SRCFILE "TileSet.bas",256
	;[257]     BITMAP "........"
	SRCFILE "TileSet.bas",257
	DECLE 0
	;[258] 	
	SRCFILE "TileSet.bas",258
	;[259] MUR_AVANT:
	SRCFILE "TileSet.bas",259
	; MUR_AVANT
label_MUR_AVANT:	;[260] 	BITMAP "........"	'BG	:	GREY/FG : BLACK
	SRCFILE "TileSet.bas",260
	;[261] 	BITMAP "........"
	SRCFILE "TileSet.bas",261
	DECLE 0
	;[262] 	BITMAP "........"
	SRCFILE "TileSet.bas",262
	;[263] 	BITMAP "........"
	SRCFILE "TileSet.bas",263
	DECLE 0
	;[264] 	BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",264
	;[265] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",265
	DECLE 43605
	;[266] 	BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",266
	;[267] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",267
	DECLE 43605
	;[268] 
	SRCFILE "TileSet.bas",268
	;[269] '///// Barbelés /////	'BG :	DarkGreen/FG : BLACK
	SRCFILE "TileSet.bas",269
	;[270] BARBELE_G:
	SRCFILE "TileSet.bas",270
	; BARBELE_G
label_BARBELE_G:	;[271] 	BITMAP "........"
	SRCFILE "TileSet.bas",271
	;[272] 	BITMAP "........"
	SRCFILE "TileSet.bas",272
	DECLE 0
	;[273] 	BITMAP ".....#.#"
	SRCFILE "TileSet.bas",273
	;[274] 	BITMAP "...#..#."
	SRCFILE "TileSet.bas",274
	DECLE 4613
	;[275] 	BITMAP "....##.#"
	SRCFILE "TileSet.bas",275
	;[276] 	BITMAP "...#..#."
	SRCFILE "TileSet.bas",276
	DECLE 4621
	;[277] 	BITMAP "...#####"
	SRCFILE "TileSet.bas",277
	;[278] 	BITMAP "..#.#.#."
	SRCFILE "TileSet.bas",278
	DECLE 10783
	;[279] 
	SRCFILE "TileSet.bas",279
	;[280] BARBELE_M:
	SRCFILE "TileSet.bas",280
	; BARBELE_M
label_BARBELE_M:	;[281] 	BITMAP "........"
	SRCFILE "TileSet.bas",281
	;[282] 	BITMAP "........"
	SRCFILE "TileSet.bas",282
	DECLE 0
	;[283] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",283
	;[284] 	BITMAP ".#...#.."
	SRCFILE "TileSet.bas",284
	DECLE 17578
	;[285] 	BITMAP "###.###."
	SRCFILE "TileSet.bas",285
	;[286] 	BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",286
	DECLE 21998
	;[287] 	BITMAP "########"
	SRCFILE "TileSet.bas",287
	;[288] 	BITMAP ".#...#.."
	SRCFILE "TileSet.bas",288
	DECLE 17663
	;[289] 	
	SRCFILE "TileSet.bas",289
	;[290] BARBELE_D:
	SRCFILE "TileSet.bas",290
	; BARBELE_D
label_BARBELE_D:	;[291] 	BITMAP "........"
	SRCFILE "TileSet.bas",291
	;[292] 	BITMAP "........"
	SRCFILE "TileSet.bas",292
	DECLE 0
	;[293] 	BITMAP "#.#....."
	SRCFILE "TileSet.bas",293
	;[294] 	BITMAP ".#..#..."
	SRCFILE "TileSet.bas",294
	DECLE 18592
	;[295] 	BITMAP "####...."
	SRCFILE "TileSet.bas",295
	;[296] 	BITMAP ".#..#..."
	SRCFILE "TileSet.bas",296
	DECLE 18672
	;[297] 	BITMAP "#####..."
	SRCFILE "TileSet.bas",297
	;[298] 	BITMAP ".#.#.#.."
	SRCFILE "TileSet.bas",298
	DECLE 21752
	;[299] 
	SRCFILE "TileSet.bas",299
	;[300] '///// Toiture /////	'BG :	RED/FG : BLACK
	SRCFILE "TileSet.bas",300
	;[301] TOITURE:
	SRCFILE "TileSet.bas",301
	; TOITURE
label_TOITURE:	;[302] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",302
	;[303] 	BITMAP "###.###."
	SRCFILE "TileSet.bas",303
	DECLE 61098
	;[304] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",304
	;[305] 	BITMAP "#.###.##"
	SRCFILE "TileSet.bas",305
	DECLE 48042
	;[306] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",306
	;[307] 	BITMAP "###.###."
	SRCFILE "TileSet.bas",307
	DECLE 61098
	;[308] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",308
	;[309] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",309
	DECLE 43690
	;[310] 	
	SRCFILE "TileSet.bas",310
	;[311] '///// Mur Maison /////	  'BG : GREY/FG : BLACK
	SRCFILE "TileSet.bas",311
	;[312] MURMAISON_1:
	SRCFILE "TileSet.bas",312
	; MURMAISON_1
label_MURMAISON_1:	;[313] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",313
	;[314] 	BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",314
	DECLE 21930
	;[315] 	BITMAP "........"
	SRCFILE "TileSet.bas",315
	;[316] 	BITMAP "..###..."
	SRCFILE "TileSet.bas",316
	DECLE 14336
	;[317] 	BITMAP ".#.*.#.."
	SRCFILE "TileSet.bas",317
	;[318] 	BITMAP ".#.*.#.."
	SRCFILE "TileSet.bas",318
	DECLE 21588
	;[319] 	BITMAP ".#####.."
	SRCFILE "TileSet.bas",319
	;[320] 	BITMAP "........"
	SRCFILE "TileSet.bas",320
	DECLE 124
	;[321] 	
	SRCFILE "TileSet.bas",321
	;[322] MurMaison1:
	SRCFILE "TileSet.bas",322
	; MURMAISON1
label_MURMAISON1:	;[323]     BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",323
	;[324]     BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",324
	DECLE 43690
	;[325]     BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",325
	;[326]     BITMAP "#.###.##"
	SRCFILE "TileSet.bas",326
	DECLE 48042
	;[327]     BITMAP "###.###."
	SRCFILE "TileSet.bas",327
	;[328]     BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",328
	DECLE 43758
	;[329]     BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",329
	;[330]     BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",330
	DECLE 43690
	;[331] 	
	SRCFILE "TileSet.bas",331
	;[332] MurMaison2:
	SRCFILE "TileSet.bas",332
	; MURMAISON2
label_MURMAISON2:	;[333]     BITMAP "########"
	SRCFILE "TileSet.bas",333
	;[334]     BITMAP "########"
	SRCFILE "TileSet.bas",334
	DECLE 65535
	;[335]     BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",335
	;[336]     BITMAP "#.###.##"
	SRCFILE "TileSet.bas",336
	DECLE 48042
	;[337]     BITMAP "###.###."
	SRCFILE "TileSet.bas",337
	;[338]     BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",338
	DECLE 43758
	;[339]     BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",339
	;[340]     BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",340
	DECLE 43690
	;[341] 	
	SRCFILE "TileSet.bas",341
	;[342] EntreeNoire:
	SRCFILE "TileSet.bas",342
	; ENTREENOIRE
label_ENTREENOIRE:	;[343]     BITMAP "........"
	SRCFILE "TileSet.bas",343
	;[344]     BITMAP "........"
	SRCFILE "TileSet.bas",344
	DECLE 0
	;[345]     BITMAP "........"
	SRCFILE "TileSet.bas",345
	;[346]     BITMAP "........"
	SRCFILE "TileSet.bas",346
	DECLE 0
	;[347]     BITMAP "........"
	SRCFILE "TileSet.bas",347
	;[348]     BITMAP "........"
	SRCFILE "TileSet.bas",348
	DECLE 0
	;[349]     BITMAP "........"
	SRCFILE "TileSet.bas",349
	;[350]     BITMAP "........"
	SRCFILE "TileSet.bas",350
	DECLE 0
	;[351] 	
	SRCFILE "TileSet.bas",351
	;[352] MURMAISON_ENTREE:
	SRCFILE "TileSet.bas",352
	; MURMAISON_ENTREE
label_MURMAISON_ENTREE:	;[353] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",353
	;[354] 	BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",354
	DECLE 21930
	;[355] 	BITMAP "........"
	SRCFILE "TileSet.bas",355
	;[356] 	BITMAP "........"
	SRCFILE "TileSet.bas",356
	DECLE 0
	;[357] 	BITMAP "..####.."
	SRCFILE "TileSet.bas",357
	;[358] 	BITMAP ".######."
	SRCFILE "TileSet.bas",358
	DECLE 32316
	;[359] 	BITMAP ".####.#."
	SRCFILE "TileSet.bas",359
	;[360] 	BITMAP ".######."
	SRCFILE "TileSet.bas",360
	DECLE 32378
	;[361] MURMAISON_2:
	SRCFILE "TileSet.bas",361
	; MURMAISON_2
label_MURMAISON_2:	;[362] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",362
	;[363] 	BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",363
	DECLE 21930
	;[364] 	BITMAP "........"
	SRCFILE "TileSet.bas",364
	;[365] 	BITMAP "........"
	SRCFILE "TileSet.bas",365
	DECLE 0
	;[366] 	BITMAP "........"
	SRCFILE "TileSet.bas",366
	;[367] 	BITMAP "........"
	SRCFILE "TileSet.bas",367
	DECLE 0
	;[368] 	BITMAP "........"
	SRCFILE "TileSet.bas",368
	;[369] 	BITMAP "........"
	SRCFILE "TileSet.bas",369
	DECLE 0
	;[370] 	
	SRCFILE "TileSet.bas",370
	;[371] '///// Bunkers /////	'BG : GREY/FG : BLACK
	SRCFILE "TileSet.bas",371
	;[372] TOITBUNKER:
	SRCFILE "TileSet.bas",372
	; TOITBUNKER
label_TOITBUNKER:	;[373] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",373
	;[374] 	BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",374
	DECLE 21930
	;[375] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",375
	;[376] 	BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",376
	DECLE 21930
	;[377] 	BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",377
	;[378] 	BITMAP "........"
	SRCFILE "TileSet.bas",378
	DECLE 170
	;[379] 	BITMAP "........"
	SRCFILE "TileSet.bas",379
	;[380] 	BITMAP "........"
	SRCFILE "TileSet.bas",380
	DECLE 0
	;[381] 	
	SRCFILE "TileSet.bas",381
	;[382] BUNKER_G:
	SRCFILE "TileSet.bas",382
	; BUNKER_G
label_BUNKER_G:	;[383] 	BITMAP "........"
	SRCFILE "TileSet.bas",383
	;[384] 	BITMAP "...#####"
	SRCFILE "TileSet.bas",384
	DECLE 7936
	;[385] 	BITMAP "..#....."
	SRCFILE "TileSet.bas",385
	;[386] 	BITMAP "..#.####"
	SRCFILE "TileSet.bas",386
	DECLE 12064
	;[387] 	BITMAP "..#.####"
	SRCFILE "TileSet.bas",387
	;[388] 	BITMAP "..#.####"
	SRCFILE "TileSet.bas",388
	DECLE 12079
	;[389] 	BITMAP "..#.####"
	SRCFILE "TileSet.bas",389
	;[390] 	BITMAP ".#.#...."
	SRCFILE "TileSet.bas",390
	DECLE 20527
	;[391] 
	SRCFILE "TileSet.bas",391
	;[392] BUNKER_D:
	SRCFILE "TileSet.bas",392
	; BUNKER_D
label_BUNKER_D:	;[393] 	BITMAP "........"
	SRCFILE "TileSet.bas",393
	;[394] 	BITMAP "#####..."
	SRCFILE "TileSet.bas",394
	DECLE 63488
	;[395] 	BITMAP ".....#.."
	SRCFILE "TileSet.bas",395
	;[396] 	BITMAP "####.#.."
	SRCFILE "TileSet.bas",396
	DECLE 62468
	;[397] 	BITMAP "####.#.."
	SRCFILE "TileSet.bas",397
	;[398] 	BITMAP "####.#.."
	SRCFILE "TileSet.bas",398
	DECLE 62708
	;[399] 	BITMAP "####.#.."
	SRCFILE "TileSet.bas",399
	;[400] 	BITMAP "....#.#."
	SRCFILE "TileSet.bas",400
	DECLE 2804
	;[401] 	
	SRCFILE "TileSet.bas",401
	;[402] BUNKER_M:
	SRCFILE "TileSet.bas",402
	; BUNKER_M
label_BUNKER_M:	;[403] 	BITMAP "........"
	SRCFILE "TileSet.bas",403
	;[404] 	BITMAP "########"
	SRCFILE "TileSet.bas",404
	DECLE 65280
	;[405] 	BITMAP "........"
	SRCFILE "TileSet.bas",405
	;[406] 	BITMAP "########"
	SRCFILE "TileSet.bas",406
	DECLE 65280
	;[407] 	BITMAP "########"
	SRCFILE "TileSet.bas",407
	;[408] 	BITMAP "########"
	SRCFILE "TileSet.bas",408
	DECLE 65535
	;[409] 	BITMAP "########"
	SRCFILE "TileSet.bas",409
	;[410] 	BITMAP "........"
	SRCFILE "TileSet.bas",410
	DECLE 255
	;[411] 	
	SRCFILE "TileSet.bas",411
	;[412] '///// Ombre Batiment ///// BG : Brown / FG : DARKGREEN
	SRCFILE "TileSet.bas",412
	;[413] OmbreBatiment:
	SRCFILE "TileSet.bas",413
	; OMBREBATIMENT
label_OMBREBATIMENT:	;[414]     BITMAP "........"
	SRCFILE "TileSet.bas",414
	;[415]     BITMAP "........"
	SRCFILE "TileSet.bas",415
	DECLE 0
	;[416]     BITMAP "#.#.#.#."
	SRCFILE "TileSet.bas",416
	;[417]     BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",417
	DECLE 21930
	;[418]     BITMAP "########"
	SRCFILE "TileSet.bas",418
	;[419]     BITMAP "########"
	SRCFILE "TileSet.bas",419
	DECLE 65535
	;[420]     BITMAP "########"
	SRCFILE "TileSet.bas",420
	;[421]     BITMAP "########"
	SRCFILE "TileSet.bas",421
	DECLE 65535
	;[422] 
	SRCFILE "TileSet.bas",422
	;[423] '///// Passerelle /////		BG : YELLOW/FG : TAN
	SRCFILE "TileSet.bas",423
	;[424] PASSERELLE:
	SRCFILE "TileSet.bas",424
	; PASSERELLE
label_PASSERELLE:	;[425]     BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",425
	;[426]     BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",426
	DECLE 21845
	;[427]     BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",427
	;[428]     BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",428
	DECLE 21845
	;[429]     BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",429
	;[430]     BITMAP ".#.###.#"
	SRCFILE "TileSet.bas",430
	DECLE 23893
	;[431]     BITMAP ".###.###"
	SRCFILE "TileSet.bas",431
	;[432]     BITMAP ".#.#.#.#"
	SRCFILE "TileSet.bas",432
	DECLE 21879
	;[433] 
	SRCFILE "TileSet.bas",433
	;[434] '///// BLANK /////
	SRCFILE "TileSet.bas",434
	;[435] BLANK:
	SRCFILE "TileSet.bas",435
	; BLANK
label_BLANK:	;[436] 	BITMAP "........"
	SRCFILE "TileSet.bas",436
	;[437] 	BITMAP "........"
	SRCFILE "TileSet.bas",437
	DECLE 0
	;[438] 	BITMAP "........"
	SRCFILE "TileSet.bas",438
	;[439] 	BITMAP "........"
	SRCFILE "TileSet.bas",439
	DECLE 0
	;[440] 	BITMAP "...*.#.."
	SRCFILE "TileSet.bas",440
	;[441] 	BITMAP "....#..."
	SRCFILE "TileSet.bas",441
	DECLE 2068
	;[442] 	BITMAP "....#..*"
	SRCFILE "TileSet.bas",442
	;[443] 	BITMAP "...#..*."
	SRCFILE "TileSet.bas",443
	DECLE 4617
	;[444] 
	SRCFILE "TileSet.bas",444
	;[445] 
	SRCFILE "TileSet.bas",445
	;[446] 	
	SRCFILE "TileSet.bas",446
	;[447] BLANK1:	
	SRCFILE "TileSet.bas",447
	; BLANK1
label_BLANK1:	;[448] 	BITMAP "........"
	SRCFILE "TileSet.bas",448
	;[449] 	BITMAP "........"
	SRCFILE "TileSet.bas",449
	DECLE 0
	;[450] 	BITMAP "........"
	SRCFILE "TileSet.bas",450
	;[451] 	BITMAP "........"
	SRCFILE "TileSet.bas",451
	DECLE 0
	;[452] 	BITMAP ".*.#...."
	SRCFILE "TileSet.bas",452
	;[453] 	BITMAP "..*#...."
	SRCFILE "TileSet.bas",453
	DECLE 12368
	;[454] 	BITMAP "...#..*."
	SRCFILE "TileSet.bas",454
	;[455] 	BITMAP "...#..*."
	SRCFILE "TileSet.bas",455
	DECLE 4626
	;[456] 
	SRCFILE "TileSet.bas",456
	;[457] 
	SRCFILE "TileSet.bas",457
	;[458] 	
	SRCFILE "TileSet.bas",458
	;[459] BLANK2:	
	SRCFILE "TileSet.bas",459
	; BLANK2
label_BLANK2:	;[460] 	BITMAP "........"
	SRCFILE "TileSet.bas",460
	;[461] 	BITMAP "........"
	SRCFILE "TileSet.bas",461
	DECLE 0
	;[462] 	BITMAP "........"
	SRCFILE "TileSet.bas",462
	;[463] 	BITMAP "........"
	SRCFILE "TileSet.bas",463
	DECLE 0
	;[464] 	BITMAP "*.*....."
	SRCFILE "TileSet.bas",464
	;[465] 	BITMAP ".*#....."
	SRCFILE "TileSet.bas",465
	DECLE 24736
	;[466] 	BITMAP "..#...*."
	SRCFILE "TileSet.bas",466
	;[467] 	BITMAP "...#..*."
	SRCFILE "TileSet.bas",467
	DECLE 4642
	;[468] 
	SRCFILE "TileSet.bas",468
	;[469] 
	SRCFILE "TileSet.bas",469
	;[470] 	
	SRCFILE "TileSet.bas",470
	;[471] '///// Mur Brique /////		'BG : GREY/FG : BLACK
	SRCFILE "TileSet.bas",471
	;[472] MURBRIQUE:
	SRCFILE "TileSet.bas",472
	; MURBRIQUE
label_MURBRIQUE:	;[473] 	BITMAP "########"
	SRCFILE "TileSet.bas",473
	;[474] 	BITMAP "..#...#."
	SRCFILE "TileSet.bas",474
	DECLE 8959
	;[475] 	BITMAP "########"
	SRCFILE "TileSet.bas",475
	;[476] 	BITMAP "#...#..."
	SRCFILE "TileSet.bas",476
	DECLE 35071
	;[477] 	BITMAP "########"
	SRCFILE "TileSet.bas",477
	;[478] 	BITMAP "..#...#."
	SRCFILE "TileSet.bas",478
	DECLE 8959
	;[479] 	BITMAP "########"
	SRCFILE "TileSet.bas",479
	;[480] 	BITMAP "#...#..."
	SRCFILE "TileSet.bas",480
	DECLE 35071
	;[481] 	
	SRCFILE "TileSet.bas",481
	;[482] MURBRIQUE_1:
	SRCFILE "TileSet.bas",482
	; MURBRIQUE_1
label_MURBRIQUE_1:	;[483] 	BITMAP "........"
	SRCFILE "TileSet.bas",483
	;[484] 	BITMAP "........"
	SRCFILE "TileSet.bas",484
	DECLE 0
	;[485] 	BITMAP "........"
	SRCFILE "TileSet.bas",485
	;[486] 	BITMAP "........"
	SRCFILE "TileSet.bas",486
	DECLE 0
	;[487] 	BITMAP "########"
	SRCFILE "TileSet.bas",487
	;[488] 	BITMAP "..#...#."
	SRCFILE "TileSet.bas",488
	DECLE 8959
	;[489] 	BITMAP "########"
	SRCFILE "TileSet.bas",489
	;[490] 	BITMAP "#...#..."
	SRCFILE "TileSet.bas",490
	DECLE 35071
	;[491] 	
	SRCFILE "TileSet.bas",491
	;[492] '///// PONT DETAIL /////	'BG : GREY/FG : BLACK
	SRCFILE "TileSet.bas",492
	;[493] PONT_1:
	SRCFILE "TileSet.bas",493
	; PONT_1
label_PONT_1:	;[494] 	BITMAP "########"
	SRCFILE "TileSet.bas",494
	;[495] 	BITMAP "########"
	SRCFILE "TileSet.bas",495
	DECLE 65535
	;[496] 	BITMAP "########"
	SRCFILE "TileSet.bas",496
	;[497] 	BITMAP "########"
	SRCFILE "TileSet.bas",497
	DECLE 65535
	;[498] 	BITMAP "########"
	SRCFILE "TileSet.bas",498
	;[499] 	BITMAP "########"
	SRCFILE "TileSet.bas",499
	DECLE 65535
	;[500] 	BITMAP "########"
	SRCFILE "TileSet.bas",500
	;[501] 	BITMAP "........"
	SRCFILE "TileSet.bas",501
	DECLE 255
	;[502] 	
	SRCFILE "TileSet.bas",502
	;[503] PONT_2:
	SRCFILE "TileSet.bas",503
	; PONT_2
label_PONT_2:	;[504] 	BITMAP "########"
	SRCFILE "TileSet.bas",504
	;[505] 	BITMAP "########"
	SRCFILE "TileSet.bas",505
	DECLE 65535
	;[506] 	BITMAP "########"
	SRCFILE "TileSet.bas",506
	;[507] 	BITMAP "########"
	SRCFILE "TileSet.bas",507
	DECLE 65535
	;[508] 	BITMAP "........"
	SRCFILE "TileSet.bas",508
	;[509] 	BITMAP "########"
	SRCFILE "TileSet.bas",509
	DECLE 65280
	;[510] 	BITMAP "########"
	SRCFILE "TileSet.bas",510
	;[511] 	BITMAP "########"
	SRCFILE "TileSet.bas",511
	DECLE 65535
	;[512] 	
	SRCFILE "TileSet.bas",512
	;[513] HautPont:
	SRCFILE "TileSet.bas",513
	; HAUTPONT
label_HAUTPONT:	;[514]     BITMAP "........"
	SRCFILE "TileSet.bas",514
	;[515]     BITMAP "########"
	SRCFILE "TileSet.bas",515
	DECLE 65280
	;[516]     BITMAP "########"
	SRCFILE "TileSet.bas",516
	;[517]     BITMAP "########"
	SRCFILE "TileSet.bas",517
	DECLE 65535
	;[518]     BITMAP "########"
	SRCFILE "TileSet.bas",518
	;[519]     BITMAP "########"
	SRCFILE "TileSet.bas",519
	DECLE 65535
	;[520]     BITMAP "########"
	SRCFILE "TileSet.bas",520
	;[521]     BITMAP "########"
	SRCFILE "TileSet.bas",521
	DECLE 65535
	;[522] 	
	SRCFILE "TileSet.bas",522
	;[523] 
	SRCFILE "TileSet.bas",523
	;[524] '///// Coin EAU /////	'BG : DARKGREEN/FG : BLUE
	SRCFILE "TileSet.bas",524
	;[525] COINEAU_HD:
	SRCFILE "TileSet.bas",525
	; COINEAU_HD
label_COINEAU_HD:	;[526] 	BITMAP "##......"
	SRCFILE "TileSet.bas",526
	;[527] 	BITMAP "####...."
	SRCFILE "TileSet.bas",527
	DECLE 61632
	;[528] 	BITMAP "#####..."
	SRCFILE "TileSet.bas",528
	;[529] 	BITMAP "######.."
	SRCFILE "TileSet.bas",529
	DECLE 64760
	;[530] 	BITMAP "######.."
	SRCFILE "TileSet.bas",530
	;[531] 	BITMAP "#######."
	SRCFILE "TileSet.bas",531
	DECLE 65276
	;[532] 	BITMAP "#######."
	SRCFILE "TileSet.bas",532
	;[533] 	BITMAP "########"
	SRCFILE "TileSet.bas",533
	DECLE 65534
	;[534] 
	SRCFILE "TileSet.bas",534
	;[535] COINEAU_HG:
	SRCFILE "TileSet.bas",535
	; COINEAU_HG
label_COINEAU_HG:	;[536] 	BITMAP "......##"
	SRCFILE "TileSet.bas",536
	;[537] 	BITMAP "....####"
	SRCFILE "TileSet.bas",537
	DECLE 3843
	;[538] 	BITMAP "...#####"
	SRCFILE "TileSet.bas",538
	;[539] 	BITMAP "..######"
	SRCFILE "TileSet.bas",539
	DECLE 16159
	;[540] 	BITMAP "..######"
	SRCFILE "TileSet.bas",540
	;[541] 	BITMAP ".#######"
	SRCFILE "TileSet.bas",541
	DECLE 32575
	;[542] 	BITMAP ".#######"
	SRCFILE "TileSet.bas",542
	;[543] 	BITMAP "########"
	SRCFILE "TileSet.bas",543
	DECLE 65407
	;[544] 
	SRCFILE "TileSet.bas",544
	;[545] COINEAU_BD:
	SRCFILE "TileSet.bas",545
	; COINEAU_BD
label_COINEAU_BD:	;[546] 	BITMAP "########"
	SRCFILE "TileSet.bas",546
	;[547] 	BITMAP ".#######"
	SRCFILE "TileSet.bas",547
	DECLE 32767
	;[548] 	BITMAP ".#######"
	SRCFILE "TileSet.bas",548
	;[549] 	BITMAP "..######"
	SRCFILE "TileSet.bas",549
	DECLE 16255
	;[550] 	BITMAP "..######"
	SRCFILE "TileSet.bas",550
	;[551] 	BITMAP "...#####"
	SRCFILE "TileSet.bas",551
	DECLE 7999
	;[552] 	BITMAP "....####"
	SRCFILE "TileSet.bas",552
	;[553] 	BITMAP "......##"
	SRCFILE "TileSet.bas",553
	DECLE 783
	;[554] 	
	SRCFILE "TileSet.bas",554
	;[555] COINEAU_BG:
	SRCFILE "TileSet.bas",555
	; COINEAU_BG
label_COINEAU_BG:	;[556] 	BITMAP "########"
	SRCFILE "TileSet.bas",556
	;[557] 	BITMAP "#######."
	SRCFILE "TileSet.bas",557
	DECLE 65279
	;[558] 	BITMAP "#######."
	SRCFILE "TileSet.bas",558
	;[559] 	BITMAP "######.."
	SRCFILE "TileSet.bas",559
	DECLE 64766
	;[560] 	BITMAP "######.."
	SRCFILE "TileSet.bas",560
	;[561] 	BITMAP "#####..."
	SRCFILE "TileSet.bas",561
	DECLE 63740
	;[562] 	BITMAP "####...."
	SRCFILE "TileSet.bas",562
	;[563] 	BITMAP "##......"
	SRCFILE "TileSet.bas",563
	DECLE 49392
	;[564] 	
	SRCFILE "TileSet.bas",564
	;[565] COINEAU_HD1:
	SRCFILE "TileSet.bas",565
	; COINEAU_HD1
label_COINEAU_HD1:	;[566] 	BITMAP "..######"
	SRCFILE "TileSet.bas",566
	;[567] 	BITMAP "....####"
	SRCFILE "TileSet.bas",567
	DECLE 3903
	;[568] 	BITMAP ".....###"
	SRCFILE "TileSet.bas",568
	;[569] 	BITMAP "......##"
	SRCFILE "TileSet.bas",569
	DECLE 775
	;[570] 	BITMAP "......##"
	SRCFILE "TileSet.bas",570
	;[571] 	BITMAP ".......#"
	SRCFILE "TileSet.bas",571
	DECLE 259
	;[572] 	BITMAP ".......#"
	SRCFILE "TileSet.bas",572
	;[573] 	BITMAP "........"
	SRCFILE "TileSet.bas",573
	DECLE 1
	;[574] 	
	SRCFILE "TileSet.bas",574
	;[575] COINEAU_HG1:
	SRCFILE "TileSet.bas",575
	; COINEAU_HG1
label_COINEAU_HG1:	;[576] 	BITMAP "######.."
	SRCFILE "TileSet.bas",576
	;[577] 	BITMAP "####...."
	SRCFILE "TileSet.bas",577
	DECLE 61692
	;[578] 	BITMAP "###....."
	SRCFILE "TileSet.bas",578
	;[579] 	BITMAP "##......"
	SRCFILE "TileSet.bas",579
	DECLE 49376
	;[580] 	BITMAP "##......"
	SRCFILE "TileSet.bas",580
	;[581] 	BITMAP "#......."
	SRCFILE "TileSet.bas",581
	DECLE 32960
	;[582] 	BITMAP "#......."
	SRCFILE "TileSet.bas",582
	;[583] 	BITMAP "........"
	SRCFILE "TileSet.bas",583
	DECLE 128
	;[584] 
	SRCFILE "TileSet.bas",584
	;[585] COINTEAU_BD1:
	SRCFILE "TileSet.bas",585
	; COINTEAU_BD1
label_COINTEAU_BD1:	;[586] 	BITMAP "........"
	SRCFILE "TileSet.bas",586
	;[587] 	BITMAP "#......."
	SRCFILE "TileSet.bas",587
	DECLE 32768
	;[588] 	BITMAP "#......."
	SRCFILE "TileSet.bas",588
	;[589] 	BITMAP "##......"
	SRCFILE "TileSet.bas",589
	DECLE 49280
	;[590] 	BITMAP "##......"
	SRCFILE "TileSet.bas",590
	;[591] 	BITMAP "###....."
	SRCFILE "TileSet.bas",591
	DECLE 57536
	;[592] 	BITMAP "####...."
	SRCFILE "TileSet.bas",592
	;[593] 	BITMAP "######.."
	SRCFILE "TileSet.bas",593
	DECLE 64752
	;[594] 
	SRCFILE "TileSet.bas",594
	;[595] COINEAU_BG1:
	SRCFILE "TileSet.bas",595
	; COINEAU_BG1
label_COINEAU_BG1:	;[596] 	BITMAP "........"
	SRCFILE "TileSet.bas",596
	;[597] 	BITMAP ".......#"
	SRCFILE "TileSet.bas",597
	DECLE 256
	;[598] 	BITMAP ".......#"
	SRCFILE "TileSet.bas",598
	;[599] 	BITMAP "......##"
	SRCFILE "TileSet.bas",599
	DECLE 769
	;[600] 	BITMAP "......##"
	SRCFILE "TileSet.bas",600
	;[601] 	BITMAP ".....###"
	SRCFILE "TileSet.bas",601
	DECLE 1795
	;[602] 	BITMAP "....####"
	SRCFILE "TileSet.bas",602
	;[603] 	BITMAP "..######"
	SRCFILE "TileSet.bas",603
	DECLE 16143
	;[604] 	
	SRCFILE "TileSet.bas",604
	;[605] '///// ENNEMI BUNKER /////	BG : GREY/FG : BLACK
	SRCFILE "TileSet.bas",605
	;[606] IABUNKER_M:
	SRCFILE "TileSet.bas",606
	; IABUNKER_M
label_IABUNKER_M:	;[607] 	BITMAP "........"
	SRCFILE "TileSet.bas",607
	;[608] 	BITMAP "########"
	SRCFILE "TileSet.bas",608
	DECLE 65280
	;[609] 	BITMAP "........"
	SRCFILE "TileSet.bas",609
	;[610] 	BITMAP "##...###"
	SRCFILE "TileSet.bas",610
	DECLE 50944
	;[611] 	BITMAP "#.###.##"
	SRCFILE "TileSet.bas",611
	;[612] 	BITMAP "##.#.###"
	SRCFILE "TileSet.bas",612
	DECLE 55227
	;[613] 	BITMAP "###.####"
	SRCFILE "TileSet.bas",613
	;[614] 	BITMAP "..#.#..."
	SRCFILE "TileSet.bas",614
	DECLE 10479
	;[615] 
	SRCFILE "TileSet.bas",615
	;[616] IABUNKER_D:
	SRCFILE "TileSet.bas",616
	; IABUNKER_D
label_IABUNKER_D:	;[617] 	BITMAP "........"
	SRCFILE "TileSet.bas",617
	;[618] 	BITMAP "########"
	SRCFILE "TileSet.bas",618
	DECLE 65280
	;[619] 	BITMAP "........"
	SRCFILE "TileSet.bas",619
	;[620] 	BITMAP "#..#####"
	SRCFILE "TileSet.bas",620
	DECLE 40704
	;[621] 	BITMAP ".##.####"
	SRCFILE "TileSet.bas",621
	;[622] 	BITMAP "#.#.####"
	SRCFILE "TileSet.bas",622
	DECLE 44911
	;[623] 	BITMAP "##.#####"
	SRCFILE "TileSet.bas",623
	;[624] 	BITMAP "..#.#..."
	SRCFILE "TileSet.bas",624
	DECLE 10463
	;[625] 
	SRCFILE "TileSet.bas",625
	;[626] IABUNKER_G:
	SRCFILE "TileSet.bas",626
	; IABUNKER_G
label_IABUNKER_G:	;[627] 	BITMAP "........"
	SRCFILE "TileSet.bas",627
	;[628] 	BITMAP "########"
	SRCFILE "TileSet.bas",628
	DECLE 65280
	;[629] 	BITMAP "........"
	SRCFILE "TileSet.bas",629
	;[630] 	BITMAP "#####..#"
	SRCFILE "TileSet.bas",630
	DECLE 63744
	;[631] 	BITMAP "####.##."
	SRCFILE "TileSet.bas",631
	;[632] 	BITMAP "####.#.#"
	SRCFILE "TileSet.bas",632
	DECLE 62966
	;[633] 	BITMAP "#####.##"
	SRCFILE "TileSet.bas",633
	;[634] 	BITMAP "...#.#.."
	SRCFILE "TileSet.bas",634
	DECLE 5371
	;[635] 	
	SRCFILE "TileSet.bas",635
	;[636] 
	SRCFILE "TileSet.bas",636
	;[637] 
	SRCFILE "TileSet.bas",637
	;[638] 
	SRCFILE "TileSet.bas",638
	;[639] 
	SRCFILE "TileSet.bas",639
	;[640] 	
	SRCFILE "TileSet.bas",640
	;[641] 
	SRCFILE "TileSet.bas",641
	;[642] 
	SRCFILE "TileSet.bas",642
	;ENDFILE
	;FILE C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas
	;[2076] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2076
	;[2077] '//////////////////////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2077
	;[2078] '//// SpriteSheet /////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2078
	;[2079] '//////////////////////
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2079
	;[2080] 
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2080
	;[2081] 	include "SpriteSheet.bas"
	SRCFILE "C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas",2081
	;FILE SpriteSheet.bas
	;[1] '//////////////////////////
	SRCFILE "SpriteSheet.bas",1
	;[2] '//// SpriteSheet jeu /////
	SRCFILE "SpriteSheet.bas",2
	;[3] '//////////////////////////
	SRCFILE "SpriteSheet.bas",3
	;[4] 
	SRCFILE "SpriteSheet.bas",4
	;[5] '///// Couteau /////
	SRCFILE "SpriteSheet.bas",5
	;[6] 'Haut
	SRCFILE "SpriteSheet.bas",6
	;[7] CouteauH_1:
	SRCFILE "SpriteSheet.bas",7
	; COUTEAUH_1
label_COUTEAUH_1:	;[8]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",8
	;[9]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",9
	DECLE 0
	;[10]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",10
	;[11]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",11
	DECLE 0
	;[12]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",12
	;[13]     BITMAP "...#...."
	SRCFILE "SpriteSheet.bas",13
	DECLE 4096
	;[14]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",14
	;[15]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",15
	DECLE 14392
	;[16] CouteauH_2:
	SRCFILE "SpriteSheet.bas",16
	; COUTEAUH_2
label_COUTEAUH_2:	;[17]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",17
	;[18]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",18
	DECLE 0
	;[19]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",19
	;[20]     BITMAP "...#...."
	SRCFILE "SpriteSheet.bas",20
	DECLE 4096
	;[21]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",21
	;[22]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",22
	DECLE 14392
	;[23]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",23
	;[24]     BITMAP "........"	
	SRCFILE "SpriteSheet.bas",24
	DECLE 56
	;[25] CouteauH_3:
	SRCFILE "SpriteSheet.bas",25
	; COUTEAUH_3
label_COUTEAUH_3:	;[26]     BITMAP "...#...."
	SRCFILE "SpriteSheet.bas",26
	;[27]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",27
	DECLE 14352
	;[28]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",28
	;[29]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",29
	DECLE 14392
	;[30]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",30
	;[31]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",31
	DECLE 56
	;[32]     BITMAP ".#####.."
	SRCFILE "SpriteSheet.bas",32
	;[33]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",33
	DECLE 14460
	;[34] 'Bas	
	SRCFILE "SpriteSheet.bas",34
	;[35] CouteauB_1:
	SRCFILE "SpriteSheet.bas",35
	; COUTEAUB_1
label_COUTEAUB_1:	;[36]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",36
	;[37]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",37
	DECLE 14392
	;[38]     BITMAP "...#...."
	SRCFILE "SpriteSheet.bas",38
	;[39]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",39
	DECLE 16
	;[40]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",40
	;[41]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",41
	DECLE 0
	;[42]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",42
	;[43]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",43
	DECLE 0
	;[44] CouteauB_2:
	SRCFILE "SpriteSheet.bas",44
	; COUTEAUB_2
label_COUTEAUB_2:	;[45]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",45
	;[46]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",46
	DECLE 14336
	;[47]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",47
	;[48]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",48
	DECLE 14392
	;[49]     BITMAP "...#...."
	SRCFILE "SpriteSheet.bas",49
	;[50]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",50
	DECLE 16
	;[51]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",51
	;[52]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",52
	DECLE 0
	;[53] CouteauB_3:
	SRCFILE "SpriteSheet.bas",53
	; COUTEAUB_3
label_COUTEAUB_3:	;[54]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",54
	;[55]     BITMAP ".#####.."
	SRCFILE "SpriteSheet.bas",55
	DECLE 31800
	;[56]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",56
	;[57]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",57
	DECLE 14336
	;[58]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",58
	;[59]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",59
	DECLE 14392
	;[60]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",60
	;[61]     BITMAP "...#...."
	SRCFILE "SpriteSheet.bas",61
	DECLE 4152
	;[62] 'Droit
	SRCFILE "SpriteSheet.bas",62
	;[63] CouteauD_1:
	SRCFILE "SpriteSheet.bas",63
	; COUTEAUD_1
label_COUTEAUD_1:	;[64]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",64
	;[65]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",65
	DECLE 0
	;[66]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",66
	;[67]     BITMAP "##......"
	SRCFILE "SpriteSheet.bas",67
	DECLE 49152
	;[68]     BITMAP "###....."
	SRCFILE "SpriteSheet.bas",68
	;[69]     BITMAP "##......"
	SRCFILE "SpriteSheet.bas",69
	DECLE 49376
	;[70]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",70
	;[71]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",71
	DECLE 0
	;[72] CouteauD_2:
	SRCFILE "SpriteSheet.bas",72
	; COUTEAUD_2
label_COUTEAUD_2:	;[73]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",73
	;[74]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",74
	DECLE 0
	;[75]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",75
	;[76]     BITMAP ".###...."
	SRCFILE "SpriteSheet.bas",76
	DECLE 28672
	;[77]     BITMAP ".####..."
	SRCFILE "SpriteSheet.bas",77
	;[78]     BITMAP ".###...."
	SRCFILE "SpriteSheet.bas",78
	DECLE 28792
	;[79]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",79
	;[80]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",80
	DECLE 0
	;[81] CouteauD_3:
	SRCFILE "SpriteSheet.bas",81
	; COUTEAUD_3
label_COUTEAUD_3:	;[82]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",82
	;[83]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",83
	DECLE 0
	;[84]     BITMAP ".#......"
	SRCFILE "SpriteSheet.bas",84
	;[85]     BITMAP "##.####."
	SRCFILE "SpriteSheet.bas",85
	DECLE 56896
	;[86]     BITMAP "##.#####"
	SRCFILE "SpriteSheet.bas",86
	;[87]     BITMAP "##.####."
	SRCFILE "SpriteSheet.bas",87
	DECLE 57055
	;[88]     BITMAP ".#......"
	SRCFILE "SpriteSheet.bas",88
	;[89]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",89
	DECLE 64
	;[90] 	
	SRCFILE "SpriteSheet.bas",90
	;[91] '///// Viseur ////
	SRCFILE "SpriteSheet.bas",91
	;[92] Viseur:
	SRCFILE "SpriteSheet.bas",92
	; VISEUR
label_VISEUR:	;[93] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",93
	;[94] 	BITMAP "..#....#"
	SRCFILE "SpriteSheet.bas",94
	DECLE 8448
	;[95] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",95
	;[96] 	BITMAP "....##.."
	SRCFILE "SpriteSheet.bas",96
	DECLE 3072
	;[97] 	BITMAP "....##.."
	SRCFILE "SpriteSheet.bas",97
	;[98] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",98
	DECLE 12
	;[99] 	BITMAP "..#....#"
	SRCFILE "SpriteSheet.bas",99
	;[100] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",100
	DECLE 33
	;[101] 	
	SRCFILE "SpriteSheet.bas",101
	;[102] '///// Gerbe Eau /////
	SRCFILE "SpriteSheet.bas",102
	;[103] Gerbe:
	SRCFILE "SpriteSheet.bas",103
	; GERBE
label_GERBE:	;[104] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",104
	;[105] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",105
	DECLE 0
	;[106] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",106
	;[107] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",107
	DECLE 0
	;[108] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",108
	;[109] 	BITMAP "...#...."
	SRCFILE "SpriteSheet.bas",109
	DECLE 4096
	;[110] 	BITMAP "...#...."
	SRCFILE "SpriteSheet.bas",110
	;[111] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",111
	DECLE 16
	;[112] 	
	SRCFILE "SpriteSheet.bas",112
	;[113] Gerbe1:
	SRCFILE "SpriteSheet.bas",113
	; GERBE1
label_GERBE1:	;[114] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",114
	;[115] 	BITMAP ".#...#.."
	SRCFILE "SpriteSheet.bas",115
	DECLE 17408
	;[116] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",116
	;[117] 	BITMAP "...#...."
	SRCFILE "SpriteSheet.bas",117
	DECLE 4096
	;[118] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",118
	;[119] 	BITMAP "...#...."
	SRCFILE "SpriteSheet.bas",119
	DECLE 4096
	;[120] 	BITMAP "...#...."
	SRCFILE "SpriteSheet.bas",120
	;[121] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",121
	DECLE 16
	;[122] 	
	SRCFILE "SpriteSheet.bas",122
	;[123] Gerbe2:
	SRCFILE "SpriteSheet.bas",123
	; GERBE2
label_GERBE2:	;[124] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",124
	;[125] 	BITMAP ".#...#.."
	SRCFILE "SpriteSheet.bas",125
	DECLE 17408
	;[126] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",126
	;[127] 	BITMAP "#.....#."
	SRCFILE "SpriteSheet.bas",127
	DECLE 33280
	;[128] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",128
	;[129] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",129
	DECLE 0
	;[130] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",130
	;[131] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",131
	DECLE 0
	;[132] 	
	SRCFILE "SpriteSheet.bas",132
	;[133] Gerbe3:
	SRCFILE "SpriteSheet.bas",133
	; GERBE3
label_GERBE3:	;[134] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",134
	;[135] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",135
	DECLE 0
	;[136] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",136
	;[137] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",137
	DECLE 0
	;[138] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",138
	;[139] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",139
	DECLE 0
	;[140] 	BITMAP "#.....#."
	SRCFILE "SpriteSheet.bas",140
	;[141] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",141
	DECLE 130
	;[142] 
	SRCFILE "SpriteSheet.bas",142
	;[143] '///// Grenad /////
	SRCFILE "SpriteSheet.bas",143
	;[144] Grenade:
	SRCFILE "SpriteSheet.bas",144
	; GRENADE
label_GRENADE:	;[145] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",145
	;[146] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",146
	DECLE 0
	;[147] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",147
	;[148] 	BITMAP "..##.#.."
	SRCFILE "SpriteSheet.bas",148
	DECLE 13336
	;[149] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",149
	;[150] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",150
	DECLE 6204
	;[151] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",151
	;[152] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",152
	DECLE 0
	;[153] 	
	SRCFILE "SpriteSheet.bas",153
	;[154] Grenade1:
	SRCFILE "SpriteSheet.bas",154
	; GRENADE1
label_GRENADE1:	;[155] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",155
	;[156] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",156
	DECLE 15360
	;[157] 	BITMAP ".####.#."
	SRCFILE "SpriteSheet.bas",157
	;[158] 	BITMAP ".######."
	SRCFILE "SpriteSheet.bas",158
	DECLE 32378
	;[159] 	BITMAP ".######."
	SRCFILE "SpriteSheet.bas",159
	;[160] 	BITMAP ".######."
	SRCFILE "SpriteSheet.bas",160
	DECLE 32382
	;[161] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",161
	;[162] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",162
	DECLE 60
	;[163] 
	SRCFILE "SpriteSheet.bas",163
	;[164] Grenade2:
	SRCFILE "SpriteSheet.bas",164
	; GRENADE2
label_GRENADE2:	;[165] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",165
	;[166] 	BITMAP ".####.#."
	SRCFILE "SpriteSheet.bas",166
	DECLE 31292
	;[167] 	BITMAP "########"
	SRCFILE "SpriteSheet.bas",167
	;[168] 	BITMAP "########"
	SRCFILE "SpriteSheet.bas",168
	DECLE 65535
	;[169] 	BITMAP "########"
	SRCFILE "SpriteSheet.bas",169
	;[170] 	BITMAP "########"
	SRCFILE "SpriteSheet.bas",170
	DECLE 65535
	;[171] 	BITMAP ".######."
	SRCFILE "SpriteSheet.bas",171
	;[172] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",172
	DECLE 15486
	;[173] 
	SRCFILE "SpriteSheet.bas",173
	;[174] '///// Balle /////
	SRCFILE "SpriteSheet.bas",174
	;[175] Touch:
	SRCFILE "SpriteSheet.bas",175
	; TOUCH
label_TOUCH:	;[176] 	BITMAP ".######."
	SRCFILE "SpriteSheet.bas",176
	;[177] 	BITMAP "########"
	SRCFILE "SpriteSheet.bas",177
	DECLE 65406
	;[178] 	BITMAP "########"
	SRCFILE "SpriteSheet.bas",178
	;[179] 	BITMAP "########"
	SRCFILE "SpriteSheet.bas",179
	DECLE 65535
	;[180] 	BITMAP "########"
	SRCFILE "SpriteSheet.bas",180
	;[181] 	BITMAP "########"
	SRCFILE "SpriteSheet.bas",181
	DECLE 65535
	;[182] 	BITMAP "########"
	SRCFILE "SpriteSheet.bas",182
	;[183] 	BITMAP ".######."
	SRCFILE "SpriteSheet.bas",183
	DECLE 32511
	;[184] Touch1:
	SRCFILE "SpriteSheet.bas",184
	; TOUCH1
label_TOUCH1:	;[185] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",185
	;[186] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",186
	DECLE 0
	;[187] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",187
	;[188] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",188
	DECLE 0
	;[189] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",189
	;[190] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",190
	DECLE 0
	;[191] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",191
	;[192] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",192
	DECLE 0
	;[193] 	
	SRCFILE "SpriteSheet.bas",193
	;[194] Touch2:
	SRCFILE "SpriteSheet.bas",194
	; TOUCH2
label_TOUCH2:	;[195]     BITMAP "#...#..#"
	SRCFILE "SpriteSheet.bas",195
	;[196]     BITMAP ".#..#.#."
	SRCFILE "SpriteSheet.bas",196
	DECLE 19081
	;[197]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",197
	;[198]     BITMAP "......##"
	SRCFILE "SpriteSheet.bas",198
	DECLE 768
	;[199]     BITMAP "##......"
	SRCFILE "SpriteSheet.bas",199
	;[200]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",200
	DECLE 192
	;[201]     BITMAP ".#.#..#."
	SRCFILE "SpriteSheet.bas",201
	;[202]     BITMAP "#..#...#"
	SRCFILE "SpriteSheet.bas",202
	DECLE 37202
	;[203] 	
	SRCFILE "SpriteSheet.bas",203
	;[204] Balle:
	SRCFILE "SpriteSheet.bas",204
	; BALLE
label_BALLE:	;[205] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",205
	;[206] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",206
	DECLE 0
	;[207] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",207
	;[208] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",208
	DECLE 6144
	;[209] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",209
	;[210] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",210
	DECLE 24
	;[211] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",211
	;[212] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",212
	DECLE 0
	;[213] 	
	SRCFILE "SpriteSheet.bas",213
	;[214] Balle1:
	SRCFILE "SpriteSheet.bas",214
	; BALLE1
label_BALLE1:	;[215] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",215
	;[216] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",216
	DECLE 0
	;[217] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",217
	;[218] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",218
	DECLE 36
	;[219] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",219
	;[220] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",220
	DECLE 9216
	;[221] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",221
	;[222] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",222
	DECLE 0
	;[223] 
	SRCFILE "SpriteSheet.bas",223
	;[224] Balle2:
	SRCFILE "SpriteSheet.bas",224
	; BALLE2
label_BALLE2:	;[225] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",225
	;[226] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",226
	DECLE 16896
	;[227] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",227
	;[228] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",228
	DECLE 0
	;[229] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",229
	;[230] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",230
	DECLE 0
	;[231] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",231
	;[232] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",232
	DECLE 66
	;[233] 	
	SRCFILE "SpriteSheet.bas",233
	;[234] Balle3:
	SRCFILE "SpriteSheet.bas",234
	; BALLE3
label_BALLE3:	;[235] 	BITMAP "#......#"
	SRCFILE "SpriteSheet.bas",235
	;[236] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",236
	DECLE 129
	;[237] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",237
	;[238] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",238
	DECLE 0
	;[239] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",239
	;[240] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",240
	DECLE 0
	;[241] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",241
	;[242] 	BITMAP "#......#"
	SRCFILE "SpriteSheet.bas",242
	DECLE 33024
	;[243] 
	SRCFILE "SpriteSheet.bas",243
	;[244] '///// Robot IA /////
	SRCFILE "SpriteSheet.bas",244
	;[245] Robot1:
	SRCFILE "SpriteSheet.bas",245
	; ROBOT1
label_ROBOT1:	;[246]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",246
	;[247]     BITMAP ".######."
	SRCFILE "SpriteSheet.bas",247
	DECLE 32316
	;[248]     BITMAP "########"
	SRCFILE "SpriteSheet.bas",248
	;[249]     BITMAP ".#.#.#.#"
	SRCFILE "SpriteSheet.bas",249
	DECLE 22015
	;[250]     BITMAP "########"
	SRCFILE "SpriteSheet.bas",250
	;[251]     BITMAP ".######."
	SRCFILE "SpriteSheet.bas",251
	DECLE 32511
	;[252]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",252
	;[253]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",253
	DECLE 60
	;[254] 
	SRCFILE "SpriteSheet.bas",254
	;[255]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",255
	;[256]     BITMAP ".######."
	SRCFILE "SpriteSheet.bas",256
	DECLE 32316
	;[257]     BITMAP "########"
	SRCFILE "SpriteSheet.bas",257
	;[258]     BITMAP "#.#.#.#."
	SRCFILE "SpriteSheet.bas",258
	DECLE 43775
	;[259]     BITMAP "########"
	SRCFILE "SpriteSheet.bas",259
	;[260]     BITMAP ".######."
	SRCFILE "SpriteSheet.bas",260
	DECLE 32511
	;[261]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",261
	;[262]     BITMAP "........"
	SRCFILE "SpriteSheet.bas",262
	DECLE 60
	;[263] 
	SRCFILE "SpriteSheet.bas",263
	;[264] '///// Ennemi SOLDAT /////
	SRCFILE "SpriteSheet.bas",264
	;[265] ' BAS
	SRCFILE "SpriteSheet.bas",265
	;[266] Soldat_1B:
	SRCFILE "SpriteSheet.bas",266
	; SOLDAT_1B
label_SOLDAT_1B:	;[267]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",267
	;[268]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",268
	DECLE 15416
	;[269]     BITMAP "..##...."
	SRCFILE "SpriteSheet.bas",269
	;[270]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",270
	DECLE 6192
	;[271]     BITMAP "..##.#.."
	SRCFILE "SpriteSheet.bas",271
	;[272]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",272
	DECLE 6196
	;[273]     BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",273
	;[274]     BITMAP ".##..##."
	SRCFILE "SpriteSheet.bas",274
	DECLE 26148
	;[275] 
	SRCFILE "SpriteSheet.bas",275
	;[276]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",276
	;[277]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",277
	DECLE 15416
	;[278]     BITMAP "..##...."
	SRCFILE "SpriteSheet.bas",278
	;[279]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",279
	DECLE 6192
	;[280]     BITMAP "..##.#.."
	SRCFILE "SpriteSheet.bas",280
	;[281]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",281
	DECLE 14388
	;[282]     BITMAP ".##..#.."
	SRCFILE "SpriteSheet.bas",282
	;[283]     BITMAP ".....##."
	SRCFILE "SpriteSheet.bas",283
	DECLE 1636
	;[284] 
	SRCFILE "SpriteSheet.bas",284
	;[285]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",285
	;[286]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",286
	DECLE 15416
	;[287]     BITMAP "..##...."
	SRCFILE "SpriteSheet.bas",287
	;[288]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",288
	DECLE 6192
	;[289]     BITMAP "..##.#.."
	SRCFILE "SpriteSheet.bas",289
	;[290]     BITMAP "...###.."
	SRCFILE "SpriteSheet.bas",290
	DECLE 7220
	;[291]     BITMAP "..#..##."
	SRCFILE "SpriteSheet.bas",291
	;[292]     BITMAP ".##....."
	SRCFILE "SpriteSheet.bas",292
	DECLE 24614
	;[293] 
	SRCFILE "SpriteSheet.bas",293
	;[294] 'Haut
	SRCFILE "SpriteSheet.bas",294
	;[295] Soldat_1H:
	SRCFILE "SpriteSheet.bas",295
	; SOLDAT_1H
label_SOLDAT_1H:	;[296]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",296
	;[297]     BITMAP "..#####."
	SRCFILE "SpriteSheet.bas",297
	DECLE 15932
	;[298]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",298
	;[299]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",299
	DECLE 6204
	;[300]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",300
	;[301]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",301
	DECLE 6204
	;[302]     BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",302
	;[303]     BITMAP ".##..##."
	SRCFILE "SpriteSheet.bas",303
	DECLE 26148
	;[304] 	
	SRCFILE "SpriteSheet.bas",304
	;[305]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",305
	;[306]     BITMAP "..#####."
	SRCFILE "SpriteSheet.bas",306
	DECLE 15932
	;[307]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",307
	;[308]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",308
	DECLE 6204
	;[309]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",309
	;[310]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",310
	DECLE 14396
	;[311]     BITMAP ".##..#.."
	SRCFILE "SpriteSheet.bas",311
	;[312]     BITMAP ".....##."
	SRCFILE "SpriteSheet.bas",312
	DECLE 1636
	;[313] 
	SRCFILE "SpriteSheet.bas",313
	;[314]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",314
	;[315]     BITMAP "..#####."
	SRCFILE "SpriteSheet.bas",315
	DECLE 15932
	;[316]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",316
	;[317]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",317
	DECLE 6204
	;[318]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",318
	;[319]     BITMAP "...###.."
	SRCFILE "SpriteSheet.bas",319
	DECLE 7228
	;[320]     BITMAP "..#..##."
	SRCFILE "SpriteSheet.bas",320
	;[321]     BITMAP ".##....."
	SRCFILE "SpriteSheet.bas",321
	DECLE 24614
	;[322] 
	SRCFILE "SpriteSheet.bas",322
	;[323] 'Droit/Gauche
	SRCFILE "SpriteSheet.bas",323
	;[324] Soldat_1D:
	SRCFILE "SpriteSheet.bas",324
	; SOLDAT_1D
label_SOLDAT_1D:	;[325]     BITMAP "...###.."
	SRCFILE "SpriteSheet.bas",325
	;[326]     BITMAP "...####."
	SRCFILE "SpriteSheet.bas",326
	DECLE 7708
	;[327]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",327
	;[328]     BITMAP "..##.#.."
	SRCFILE "SpriteSheet.bas",328
	DECLE 13336
	;[329]     BITMAP "..#####."
	SRCFILE "SpriteSheet.bas",329
	;[330]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",330
	DECLE 6206
	;[331]     BITMAP "..##.#.."
	SRCFILE "SpriteSheet.bas",331
	;[332]     BITMAP ".#...##."
	SRCFILE "SpriteSheet.bas",332
	DECLE 17972
	;[333] 
	SRCFILE "SpriteSheet.bas",333
	;[334]     BITMAP "...###.."
	SRCFILE "SpriteSheet.bas",334
	;[335]     BITMAP "...####."
	SRCFILE "SpriteSheet.bas",335
	DECLE 7708
	;[336]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",336
	;[337]     BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",337
	DECLE 15384
	;[338]     BITMAP "..#####."
	SRCFILE "SpriteSheet.bas",338
	;[339]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",339
	DECLE 6206
	;[340]     BITMAP "..###..."
	SRCFILE "SpriteSheet.bas",340
	;[341]     BITMAP "..#.##.."
	SRCFILE "SpriteSheet.bas",341
	DECLE 11320
	;[342] 	
	SRCFILE "SpriteSheet.bas",342
	;[343]     BITMAP "...###.."
	SRCFILE "SpriteSheet.bas",343
	;[344]     BITMAP "...####."
	SRCFILE "SpriteSheet.bas",344
	DECLE 7708
	;[345]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",345
	;[346]     BITMAP "...###.."
	SRCFILE "SpriteSheet.bas",346
	DECLE 7192
	;[347]     BITMAP "...####."
	SRCFILE "SpriteSheet.bas",347
	;[348]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",348
	DECLE 6174
	;[349]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",349
	;[350]     BITMAP "...#.#.."
	SRCFILE "SpriteSheet.bas",350
	DECLE 5144
	;[351] 
	SRCFILE "SpriteSheet.bas",351
	;[352]     BITMAP "...###.."
	SRCFILE "SpriteSheet.bas",352
	;[353]     BITMAP "...####."
	SRCFILE "SpriteSheet.bas",353
	DECLE 7708
	;[354]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",354
	;[355]     BITMAP "...###.."
	SRCFILE "SpriteSheet.bas",355
	DECLE 7192
	;[356]     BITMAP "...####."
	SRCFILE "SpriteSheet.bas",356
	;[357]     BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",357
	DECLE 6174
	;[358]     BITMAP "..##.#.."
	SRCFILE "SpriteSheet.bas",358
	;[359]     BITMAP "..#....."	
	SRCFILE "SpriteSheet.bas",359
	DECLE 8244
	;[360] 	
	SRCFILE "SpriteSheet.bas",360
	;[361] '///// Papi Commando /////
	SRCFILE "SpriteSheet.bas",361
	;[362] PAPI_BAS_0:
	SRCFILE "SpriteSheet.bas",362
	; PAPI_BAS_0
label_PAPI_BAS_0:	;[363] 'WHITE
	SRCFILE "SpriteSheet.bas",363
	;[364] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",364
	;[365] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",365
	DECLE 16896
	;[366] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",366
	;[367] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",367
	DECLE 16896
	;[368] 	BITMAP ".#.##.#."
	SRCFILE "SpriteSheet.bas",368
	;[369] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",369
	DECLE 15450
	;[370] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",370
	;[371] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",371
	DECLE 0
	;[372] 'BLACK
	SRCFILE "SpriteSheet.bas",372
	;[373] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",373
	;[374] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",374
	DECLE 0
	;[375] 	BITMAP ".######."
	SRCFILE "SpriteSheet.bas",375
	;[376] 	BITMAP "#.#..#.."
	SRCFILE "SpriteSheet.bas",376
	DECLE 42110
	;[377] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",377
	;[378] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",378
	DECLE 0
	;[379] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",379
	;[380] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",380
	DECLE 9240
	;[381] 'PINK
	SRCFILE "SpriteSheet.bas",381
	;[382] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",382
	;[383] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",383
	DECLE 15360
	;[384] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",384
	;[385] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",385
	DECLE 6144
	;[386] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",386
	;[387] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",387
	DECLE 36
	;[388] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",388
	;[389] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",389
	DECLE 6180
	;[390] 	
	SRCFILE "SpriteSheet.bas",390
	;[391] PAPI_BAS_1:
	SRCFILE "SpriteSheet.bas",391
	; PAPI_BAS_1
label_PAPI_BAS_1:	;[392] 'WHITE
	SRCFILE "SpriteSheet.bas",392
	;[393] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",393
	;[394] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",394
	DECLE 9282
	;[395] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",395
	;[396] 	BITMAP ".#.##.#."
	SRCFILE "SpriteSheet.bas",396
	DECLE 23106
	;[397] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",397
	;[398] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",398
	DECLE 60
	;[399] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",399
	;[400] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",400
	DECLE 0
	;[401] 'BLACK
	SRCFILE "SpriteSheet.bas",401
	;[402] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",402
	;[403] 	BITMAP "##.##.#."
	SRCFILE "SpriteSheet.bas",403
	DECLE 55808
	;[404] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",404
	;[405] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",405
	DECLE 36
	;[406] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",406
	;[407] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",407
	DECLE 6144
	;[408] 	BITMAP ".##..#.."
	SRCFILE "SpriteSheet.bas",408
	;[409] 	BITMAP ".....##."
	SRCFILE "SpriteSheet.bas",409
	DECLE 1636
	;[410] 'PINK
	SRCFILE "SpriteSheet.bas",410
	;[411] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",411
	;[412] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",412
	DECLE 60
	;[413] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",413
	;[414] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",414
	DECLE 9240
	;[415] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",415
	;[416] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",416
	DECLE 9216
	;[417] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",417
	;[418] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",418
	DECLE 24
	;[419] 	
	SRCFILE "SpriteSheet.bas",419
	;[420] PAPI_BAS_2:
	SRCFILE "SpriteSheet.bas",420
	; PAPI_BAS_2
label_PAPI_BAS_2:	;[421] 'WHITE
	SRCFILE "SpriteSheet.bas",421
	;[422] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",422
	;[423] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",423
	DECLE 9282
	;[424] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",424
	;[425] 	BITMAP ".#.##.#."
	SRCFILE "SpriteSheet.bas",425
	DECLE 23106
	;[426] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",426
	;[427] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",427
	DECLE 60
	;[428] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",428
	;[429] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",429
	DECLE 0
	;[430] 'BLACK
	SRCFILE "SpriteSheet.bas",430
	;[431] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",431
	;[432] 	BITMAP "##.##.#."
	SRCFILE "SpriteSheet.bas",432
	DECLE 55808
	;[433] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",433
	;[434] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",434
	DECLE 36
	;[435] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",435
	;[436] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",436
	DECLE 6144
	;[437] 	BITMAP "..#..##."
	SRCFILE "SpriteSheet.bas",437
	;[438] 	BITMAP ".##....."
	SRCFILE "SpriteSheet.bas",438
	DECLE 24614
	;[439] 'PINK
	SRCFILE "SpriteSheet.bas",439
	;[440] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",440
	;[441] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",441
	DECLE 60
	;[442] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",442
	;[443] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",443
	DECLE 9240
	;[444] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",444
	;[445] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",445
	DECLE 9216
	;[446] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",446
	;[447] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",447
	DECLE 24
	;[448] 	
	SRCFILE "SpriteSheet.bas",448
	;[449] PAPI_DROITE_0:
	SRCFILE "SpriteSheet.bas",449
	; PAPI_DROITE_0
label_PAPI_DROITE_0:	;[450] 'WHITE
	SRCFILE "SpriteSheet.bas",450
	;[451] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",451
	;[452] 	BITMAP ".#......"
	SRCFILE "SpriteSheet.bas",452
	DECLE 16384
	;[453] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",453
	;[454] 	BITMAP ".#......"
	SRCFILE "SpriteSheet.bas",454
	DECLE 16384
	;[455] 	BITMAP ".##.#.#."
	SRCFILE "SpriteSheet.bas",455
	;[456] 	BITMAP "...###.."
	SRCFILE "SpriteSheet.bas",456
	DECLE 7274
	;[457] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",457
	;[458] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",458
	DECLE 0
	;[459] 'BLACK
	SRCFILE "SpriteSheet.bas",459
	;[460] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",460
	;[461] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",461
	DECLE 0
	;[462] 	BITMAP ".######."
	SRCFILE "SpriteSheet.bas",462
	;[463] 	BITMAP "#..#.#.."
	SRCFILE "SpriteSheet.bas",463
	DECLE 38014
	;[464] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",464
	;[465] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",465
	DECLE 0
	;[466] 	BITMAP "..##...."
	SRCFILE "SpriteSheet.bas",466
	;[467] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",467
	DECLE 15408
	;[468] 'PINK
	SRCFILE "SpriteSheet.bas",468
	;[469] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",469
	;[470] 	BITMAP "..#####."
	SRCFILE "SpriteSheet.bas",470
	DECLE 15872
	;[471] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",471
	;[472] 	BITMAP "..#.#.#."
	SRCFILE "SpriteSheet.bas",472
	DECLE 10752
	;[473] 	BITMAP "...#.#.."
	SRCFILE "SpriteSheet.bas",473
	;[474] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",474
	DECLE 20
	;[475] 	BITMAP "....###."
	SRCFILE "SpriteSheet.bas",475
	;[476] 	BITMAP "........"	
	SRCFILE "SpriteSheet.bas",476
	DECLE 14
	;[477] 
	SRCFILE "SpriteSheet.bas",477
	;[478] PAPI_DROITE_1:
	SRCFILE "SpriteSheet.bas",478
	; PAPI_DROITE_1
label_PAPI_DROITE_1:	;[479] 'WHITE
	SRCFILE "SpriteSheet.bas",479
	;[480] 	BITMAP ".#......"
	SRCFILE "SpriteSheet.bas",480
	;[481] 	BITMAP "..##.##."
	SRCFILE "SpriteSheet.bas",481
	DECLE 13888
	;[482] 	BITMAP ".#......"
	SRCFILE "SpriteSheet.bas",482
	;[483] 	BITMAP ".##.#.#."
	SRCFILE "SpriteSheet.bas",483
	DECLE 27200
	;[484] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",484
	;[485] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",485
	DECLE 60
	;[486] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",486
	;[487] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",487
	DECLE 0
	;[488] 'BLACK
	SRCFILE "SpriteSheet.bas",488
	;[489] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",489
	;[490] 	BITMAP "##..#..."
	SRCFILE "SpriteSheet.bas",490
	DECLE 51200
	;[491] 	BITMAP "...#.#.."
	SRCFILE "SpriteSheet.bas",491
	;[492] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",492
	DECLE 20
	;[493] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",493
	;[494] 	BITMAP "..#....."
	SRCFILE "SpriteSheet.bas",494
	DECLE 8192
	;[495] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",495
	;[496] 	BITMAP "..#..##."
	SRCFILE "SpriteSheet.bas",496
	DECLE 9788
	;[497] 'PINK
	SRCFILE "SpriteSheet.bas",497
	;[498] 	BITMAP "..#####."
	SRCFILE "SpriteSheet.bas",498
	;[499] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",499
	DECLE 62
	;[500] 	BITMAP "..#.#.#."
	SRCFILE "SpriteSheet.bas",500
	;[501] 	BITMAP "...#.#.."
	SRCFILE "SpriteSheet.bas",501
	DECLE 5162
	;[502] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",502
	;[503] 	BITMAP "...####."
	SRCFILE "SpriteSheet.bas",503
	DECLE 7680
	;[504] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",504
	;[505] 	BITMAP "........"	
	SRCFILE "SpriteSheet.bas",505
	DECLE 0
	;[506] 	
	SRCFILE "SpriteSheet.bas",506
	;[507] 	
	SRCFILE "SpriteSheet.bas",507
	;[508] 	
	SRCFILE "SpriteSheet.bas",508
	;[509] PAPI_DROITE_2:
	SRCFILE "SpriteSheet.bas",509
	; PAPI_DROITE_2
label_PAPI_DROITE_2:	;[510] 'WHITE
	SRCFILE "SpriteSheet.bas",510
	;[511] 	BITMAP ".#......"
	SRCFILE "SpriteSheet.bas",511
	;[512] 	BITMAP "..##.##."
	SRCFILE "SpriteSheet.bas",512
	DECLE 13888
	;[513] 	BITMAP ".#......"
	SRCFILE "SpriteSheet.bas",513
	;[514] 	BITMAP ".##.#.#."
	SRCFILE "SpriteSheet.bas",514
	DECLE 27200
	;[515] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",515
	;[516] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",516
	DECLE 60
	;[517] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",517
	;[518] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",518
	DECLE 0
	;[519] 'BLACK
	SRCFILE "SpriteSheet.bas",519
	;[520] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",520
	;[521] 	BITMAP "##..#..."
	SRCFILE "SpriteSheet.bas",521
	DECLE 51200
	;[522] 	BITMAP "...#.#.."
	SRCFILE "SpriteSheet.bas",522
	;[523] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",523
	DECLE 20
	;[524] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",524
	;[525] 	BITMAP "..##...."
	SRCFILE "SpriteSheet.bas",525
	DECLE 12288
	;[526] 	BITMAP "..#####."
	SRCFILE "SpriteSheet.bas",526
	;[527] 	BITMAP ".#...#.."
	SRCFILE "SpriteSheet.bas",527
	DECLE 17470
	;[528] 'PINK
	SRCFILE "SpriteSheet.bas",528
	;[529] 	BITMAP "..#####."
	SRCFILE "SpriteSheet.bas",529
	;[530] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",530
	DECLE 62
	;[531] 	BITMAP "..#.#.#."
	SRCFILE "SpriteSheet.bas",531
	;[532] 	BITMAP "...#.#.."
	SRCFILE "SpriteSheet.bas",532
	DECLE 5162
	;[533] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",533
	;[534] 	BITMAP "....###."
	SRCFILE "SpriteSheet.bas",534
	DECLE 3584
	;[535] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",535
	;[536] 	BITMAP "........"	
	SRCFILE "SpriteSheet.bas",536
	DECLE 0
	;[537] 	
	SRCFILE "SpriteSheet.bas",537
	;[538] PAPI_HAUT_0:
	SRCFILE "SpriteSheet.bas",538
	; PAPI_HAUT_0
label_PAPI_HAUT_0:	;[539] 'WHITE
	SRCFILE "SpriteSheet.bas",539
	;[540] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",540
	;[541] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",541
	DECLE 16896
	;[542] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",542
	;[543] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",543
	DECLE 16896
	;[544] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",544
	;[545] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",545
	DECLE 36
	;[546] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",546
	;[547] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",547
	DECLE 0
	;[548] 'BLACK
	SRCFILE "SpriteSheet.bas",548
	;[549] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",549
	;[550] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",550
	DECLE 0
	;[551] 	BITMAP ".######."
	SRCFILE "SpriteSheet.bas",551
	;[552] 	BITMAP "#......."
	SRCFILE "SpriteSheet.bas",552
	DECLE 32894
	;[553] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",553
	;[554] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",554
	DECLE 6144
	;[555] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",555
	;[556] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",556
	DECLE 15420
	;[557] 'PINK
	SRCFILE "SpriteSheet.bas",557
	;[558] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",558
	;[559] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",559
	DECLE 15360
	;[560] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",560
	;[561] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",561
	DECLE 15360
	;[562] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",562
	;[563] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",563
	DECLE 9240
	;[564] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",564
	;[565] 	BITMAP "........"	
	SRCFILE "SpriteSheet.bas",565
	DECLE 0
	;[566] 		
	SRCFILE "SpriteSheet.bas",566
	;[567] 	
	SRCFILE "SpriteSheet.bas",567
	;[568] PAPI_HAUT_1:
	SRCFILE "SpriteSheet.bas",568
	; PAPI_HAUT_1
label_PAPI_HAUT_1:	;[569] 'WHITE
	SRCFILE "SpriteSheet.bas",569
	;[570] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",570
	;[571] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",571
	DECLE 66
	;[572] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",572
	;[573] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",573
	DECLE 9282
	;[574] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",574
	;[575] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",575
	DECLE 0
	;[576] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",576
	;[577] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",577
	DECLE 0
	;[578] 'BLACK
	SRCFILE "SpriteSheet.bas",578
	;[579] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",579
	;[580] 	BITMAP "#######."
	SRCFILE "SpriteSheet.bas",580
	DECLE 65024
	;[581] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",581
	;[582] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",582
	DECLE 0
	;[583] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",583
	;[584] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",584
	DECLE 15384
	;[585] 	BITMAP ".##.##.."
	SRCFILE "SpriteSheet.bas",585
	;[586] 	BITMAP ".....##."
	SRCFILE "SpriteSheet.bas",586
	DECLE 1644
	;[587] 'PINK
	SRCFILE "SpriteSheet.bas",587
	;[588] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",588
	;[589] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",589
	DECLE 60
	;[590] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",590
	;[591] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",591
	DECLE 6204
	;[592] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",592
	;[593] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",593
	DECLE 36
	;[594] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",594
	;[595] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",595
	DECLE 0
	;[596] 	
	SRCFILE "SpriteSheet.bas",596
	;[597] PAPI_HAUT_2:
	SRCFILE "SpriteSheet.bas",597
	; PAPI_HAUT_2
label_PAPI_HAUT_2:	;[598] 'WHITE
	SRCFILE "SpriteSheet.bas",598
	;[599] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",599
	;[600] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",600
	DECLE 66
	;[601] 	BITMAP ".#....#."
	SRCFILE "SpriteSheet.bas",601
	;[602] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",602
	DECLE 9282
	;[603] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",603
	;[604] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",604
	DECLE 0
	;[605] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",605
	;[606] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",606
	DECLE 0
	;[607] 'BLACK
	SRCFILE "SpriteSheet.bas",607
	;[608] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",608
	;[609] 	BITMAP "#######."
	SRCFILE "SpriteSheet.bas",609
	DECLE 65024
	;[610] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",610
	;[611] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",611
	DECLE 0
	;[612] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",612
	;[613] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",613
	DECLE 15384
	;[614] 	BITMAP "..##.##."
	SRCFILE "SpriteSheet.bas",614
	;[615] 	BITMAP ".##....."
	SRCFILE "SpriteSheet.bas",615
	DECLE 24630
	;[616] 'PINK
	SRCFILE "SpriteSheet.bas",616
	;[617] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",617
	;[618] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",618
	DECLE 60
	;[619] 	BITMAP "..####.."
	SRCFILE "SpriteSheet.bas",619
	;[620] 	BITMAP "...##..."
	SRCFILE "SpriteSheet.bas",620
	DECLE 6204
	;[621] 	BITMAP "..#..#.."
	SRCFILE "SpriteSheet.bas",621
	;[622] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",622
	DECLE 36
	;[623] 	BITMAP "........"
	SRCFILE "SpriteSheet.bas",623
	;[624] 	BITMAP "........"	
	SRCFILE "SpriteSheet.bas",624
	DECLE 0
	;[625] 	
	SRCFILE "SpriteSheet.bas",625
	;[626] 	
	SRCFILE "SpriteSheet.bas",626
	;[627] 	
	SRCFILE "SpriteSheet.bas",627
	;[628] 	
	SRCFILE "SpriteSheet.bas",628
	;[629] 	
	SRCFILE "SpriteSheet.bas",629
	;[630] 	
	SRCFILE "SpriteSheet.bas",630
	;[631] 	
	SRCFILE "SpriteSheet.bas",631
	;ENDFILE
	;FILE C:\Users\Alain\Documents\GitHub\Papi Commando Intellivision\papi-commando-intellivision\PapiCommando.bas
	;ENDFILE
	SRCFILE "",0
intybasic_scroll:	equ 1	; Forces to include scroll library
intybasic_stack:	equ 1	; Forces to include stack overflow checking
intybasic_numbers:	equ 1	; Forces to include numbers library
intybasic_fastmult:	equ 1	; Forces to include fast multiplication
        ;
        ; Epilogue for IntyBASIC programs
        ; by Oscar Toledo G.  http://nanochess.org/
        ;
        ; Revision: Jan/30/2014. Moved GRAM code below MOB updates.
        ;                        Added comments.
        ; Revision: Feb/26/2014. Optimized access to collision registers
        ;                        per DZ-Jay suggestion. Added scrolling
        ;                        routines with optimization per intvnut
        ;                        suggestion. Added border/mask support.
        ; Revision: Apr/02/2014. Added support to set MODE (color stack
        ;                        or foreground/background), added support
        ;                        for SCREEN statement.
        ; Revision: Aug/19/2014. Solved bug in bottom scroll, moved an
        ;                        extra unneeded line.
        ; Revision: Aug/26/2014. Integrated music player and NTSC/PAL
        ;                        detection.
        ; Revision: Oct/24/2014. Adjust in some comments.
        ; Revision: Nov/13/2014. Integrated Joseph Zbiciak's routines
        ;                        for printing numbers.
        ; Revision: Nov/17/2014. Redesigned MODE support to use a single
        ;                        variable.
        ; Revision: Nov/21/2014. Added Intellivoice support routines made
        ;                        by Joseph Zbiciak.
	; Revision: Dec/11/2014. Optimized keypad decode routines.
	; Revision: Jan/25/2015. Added marker for insertion of ON FRAME GOSUB
	; Revision: Feb/17/2015. Allows to deactivate music player (PLAY NONE)
	; Revision: Apr/21/2015. Accelerates common case of keypad not pressed.
	;                        Added ECS ROM disable code.
	; Revision: Apr/22/2015. Added Joseph Zbiciak accelerated multiplication
	;                        routines.
	; Revision: Jun/04/2015. Optimized play_music (per GroovyBee suggestion)
	; Revision: Jul/25/2015. Added infinite loop at start to avoid crashing
	;                        with empty programs. Solved bug where _color
	;                        didn't started with white.
	; Revision: Aug/20/2015. Moved ECS mapper disable code so nothing gets
	;                        after it (GroovyBee 42K sample code)
	; Revision: Aug/21/2015. Added Joseph Zbiciak routines for JLP Flash
	;                        handling.
	; Revision: Aug/31/2015. Added CPYBLK2 for SCREEN fifth argument.
	; Revision: Sep/01/2015. Defined labels Q1 and Q2 as alias.
	; Revision: Jan/22/2016. Music player allows not to use noise channel
	;                        for drums. Allows setting music volume.
	; Revision: Jan/23/2016. Added jump inside of music (for MUSIC JUMP)
	; Revision: May/03/2016. Preserves current mode in bit 0 of _mode_select

	;
	; Avoids empty programs to crash
	; 
stuck:	B stuck

	;
	; Copy screen helper for SCREEN wide statement
	;

CPYBLK2:	PROC
	MOVR R0,R3		; Offset
	MOVR R5,R2
	PULR R0
	PULR R1
	PULR R5
	PULR R4
	PSHR R2
	SUBR R1,R3

@@1:    PSHR R3
	MOVR R1,R3              ; Init line copy
@@2:    MVI@ R4,R2              ; Copy line
        MVO@ R2,R5
        DECR R3
        BNE @@2
        PULR R3                 ; Add offset to start in next line
        ADDR R3,R4
	SUBR R1,R5
        ADDI #20,R5
        DECR R0                 ; Count lines
        BNE @@1

	RETURN
	ENDP

        ;
        ; Copy screen helper for SCREEN statement
        ;
CPYBLK: PROC
        BEGIN
        MOVR R3,R4
        MOVR R2,R5

@@1:    MOVR R1,R3              ; Init line copy
@@2:    MVI@ R4,R2              ; Copy line
        MVO@ R2,R5
        DECR R3
        BNE @@2
        MVII #20,R3             ; Add offset to start in next line
        SUBR R1,R3
        ADDR R3,R4
        ADDR R3,R5
        DECR R0                 ; Count lines
        BNE @@1
	RETURN
        ENDP

        ;
        ; Wait for interruption
        ;
_wait:  PROC

    IF DEFINED intybasic_keypad
        MVI $01FF,R0
        COMR R0
        ANDI #$FF,R0
        CMP _cnt1_p0,R0
        BNE @@2
        CMP _cnt1_p1,R0
        BNE @@2
	TSTR R0		; Accelerates common case of key not pressed
	MVII #_keypad_table+13,R4
	BEQ @@4
        MVII #_keypad_table,R4
    REPEAT 6
        CMP@ R4,R0
        BEQ @@4
	CMP@ R4,R0
        BEQ @@4
    ENDR
	INCR R4
@@4:    SUBI #_keypad_table+1,R4
	MVO R4,_cnt1_key

@@2:    MVI _cnt1_p1,R1
        MVO R1,_cnt1_p0
        MVO R0,_cnt1_p1

        MVI $01FE,R0
        COMR R0
        ANDI #$FF,R0
        CMP _cnt2_p0,R0
        BNE @@5
        CMP _cnt2_p1,R0
        BNE @@5
	TSTR R0		; Accelerates common case of key not pressed
	MVII #_keypad_table+13,R4
	BEQ @@7
        MVII #_keypad_table,R4
    REPEAT 6
        CMP@ R4,R0
        BEQ @@7
	CMP@ R4,R0
	BEQ @@7
    ENDR

	INCR R4
@@7:    SUBI #_keypad_table+1,R4
	MVO R4,_cnt2_key

@@5:    MVI _cnt2_p1,R1
        MVO R1,_cnt2_p0
        MVO R0,_cnt2_p1
    ENDI

        CLRR    R0
        MVO     R0,_int         ; Clears waiting flag
@@1:    CMP     _int,  R0       ; Waits for change
        BEQ     @@1
        JR      R5              ; Returns
        ENDP

        ;
        ; Keypad table
        ;
_keypad_table:          PROC
        DECLE $48,$81,$41,$21,$82,$42,$22,$84,$44,$24,$88,$28
        ENDP

_pal1_vector:    PROC
        MVII #_pal2_vector,R0
        MVO R0,ISRVEC
        SWAP R0
        MVO R0,ISRVEC+1
        MVII #3,R0
        MVO R0,_ntsc
        JR R5
        ENDP

_pal2_vector:    PROC
        MVII #_int_vector,R0     ; Point to "real" interruption handler
        MVO R0,ISRVEC
        SWAP R0
        MVO R0,ISRVEC+1
        MVII #4,R0
        MVO R0,_ntsc
	CLRR R0
	CLRR R4
	MVII #$18,R1
@@1:	MVO@ R0,R4
	DECR R1
	BNE @@1
        JR R5
        ENDP

        ;
        ; Interruption routine
        ;
_int_vector:     PROC
        BEGIN

        MVO     R0,     $20     ; Activates display

    IF DEFINED intybasic_stack
	CMPI #$308,R6
	BNC @@vs
	MVI $21,R0	; Activates Color Stack mode
	CLRR R0
	MVO R0,$28
	MVO R0,$29
	MVO R0,$2A
	MVO R0,$2B
	MVII #@@vs1,R4
	MVII #$200,R5
	MVII #20,R1
@@vs2:	MVI@ R4,R0
	MVO@ R0,R5
	DECR R1
	BNE @@vs2
	RETURN

	; Stack Overflow message
@@vs1:	DECLE 0,0,0,$33*8+7,$54*8+7,$41*8+7,$43*8+7,$4B*8+7,$00*8+7
	DECLE $4F*8+7,$56*8+7,$45*8+7,$52*8+7,$46*8+7,$4C*8+7
	DECLE $4F*8+7,$57*8+7,0,0,0

@@vs:
    ENDI
        MVII    #1,     R0
        MVO     R0,     _int    ; Indicates interrupt happened

        MVI _mode_select,R0
        SARC R0,2
        BNOV @@vi0
	CLRR R1
        BNC @@vi14
        MVO R0,$21  ; Activates Foreground/Background mode
        INCR R1
	B @@vi15

@@vi14: MVI $21,R0  ; Activates Color Stack mode
        MVI _color,R0
        MVO R0,$28
        SWAP R0
        MVO R0,$29
        SLR R0,2
        SLR R0,2
        MVO R0,$2A
        SWAP R0
        MVO R0,$2B
@@vi15: MVO R1,_mode_select
	MVII #7,R0
        MVO R0,_color           ; Default color for PRINT "string"
@@vi0:
        MVI _border_color,R0
        MVO     R0,     $2C     ; Border color
        MVI _border_mask,R0
        MVO     R0,     $32     ; Border mask
        ;
        ; Save collision registers for further use and clear them
        ;
        MVII #$18,R4
        MVII #_col0,R5
        MVI@ R4,R0
        MVO@ R0,R5  ; _col0
        MVI@ R4,R0
        MVO@ R0,R5  ; _col1
        MVI@ R4,R0
        MVO@ R0,R5  ; _col2
        MVI@ R4,R0
        MVO@ R0,R5  ; _col3
        MVI@ R4,R0
        MVO@ R0,R5  ; _col4
        MVI@ R4,R0
        MVO@ R0,R5  ; _col5
        MVI@ R4,R0
        MVO@ R0,R5  ; _col6
        MVI@ R4,R0
        MVO@ R0,R5  ; _col7
        MVII #$18,R5
        CLRR R0
        MVO@ R0,R5
        MVO@ R0,R5
        MVO@ R0,R5
        MVO@ R0,R5
        MVO@ R0,R5
        MVO@ R0,R5
        MVO@ R0,R5
        MVO@ R0,R5
        
    IF DEFINED intybasic_scroll

        ;
        ; Scrolling things
        ;
        MVI _scroll_x,R0
        MVO R0,$30
        MVI _scroll_y,R0
        MVO R0,$31
    ENDI

        ;
        ; Updates sprites (MOBs)
        ;
        MVII #_mobs,R4
        MVII #$0,R5     ; X-coordinates
        MVII #8,R1
@@vi2:  MVI@ R4,R0
        MVO@ R0,R5
        MVI@ R4,R0
        MVO@ R0,R5
        MVI@ R4,R0
        MVO@ R0,R5
        DECR R1
        BNE @@vi2

    IF DEFINED intybasic_music
     	MVI _ntsc,R0
        TSTR R0         ; PAL?
        BEQ @@vo97      ; Yes, always emit sound
	MVI _music_frame,R0
	INCR R0
	CMPI #6,R0
	BNE @@vo14
	CLRR R0
@@vo14:	MVO R0,_music_frame
	BEQ @@vo15
@@vo97:	CALL _emit_sound
@@vo15:
    ENDI

        ;
        ; Detect GRAM definition
        ;
        MVI _gram_bitmap,R4
        TSTR R4
        BEQ @@vi1
        MVI _gram_target,R1
        SLL R1,2
        SLL R1,1
        ADDI #$3800,R1
        MOVR R1,R5
        MVI _gram_total,R0
@@vi3:
        MVI@    R4,     R1
        MVO@    R1,     R5
        SWAP    R1
        MVO@    R1,     R5
        MVI@    R4,     R1
        MVO@    R1,     R5
        SWAP    R1
        MVO@    R1,     R5
        MVI@    R4,     R1
        MVO@    R1,     R5
        SWAP    R1
        MVO@    R1,     R5
        MVI@    R4,     R1
        MVO@    R1,     R5
        SWAP    R1
        MVO@    R1,     R5
        DECR R0
        BNE @@vi3
        MVO R0,_gram_bitmap
@@vi1:
        MVI _gram2_bitmap,R4
        TSTR R4
        BEQ @@vii1
        MVI _gram2_target,R1
        SLL R1,2
        SLL R1,1
        ADDI #$3800,R1
        MOVR R1,R5
        MVI _gram2_total,R0
@@vii3:
        MVI@    R4,     R1
        MVO@    R1,     R5
        SWAP    R1
        MVO@    R1,     R5
        MVI@    R4,     R1
        MVO@    R1,     R5
        SWAP    R1
        MVO@    R1,     R5
        MVI@    R4,     R1
        MVO@    R1,     R5
        SWAP    R1
        MVO@    R1,     R5
        MVI@    R4,     R1
        MVO@    R1,     R5
        SWAP    R1
        MVO@    R1,     R5
        DECR R0
        BNE @@vii3
        MVO R0,_gram2_bitmap
@@vii1:

    IF DEFINED intybasic_scroll
        ;
        ; Frame scroll support
        ;
        MVI _scroll_d,R0
        TSTR R0
        BEQ @@vi4
        CLRR R1
        MVO R1,_scroll_d
        DECR R0     ; Left
        BEQ @@vi5
        DECR R0     ; Right
        BEQ @@vi6
        DECR R0     ; Top
        BEQ @@vi7
        DECR R0     ; Bottom
        BEQ @@vi8
        B @@vi4

@@vi5:  MVII #$0200,R4
        MOVR R4,R5
        INCR R5
        MVII #12,R1
@@vi12: MVI@ R4,R2
        MVI@ R4,R3
        REPEAT 8
        MVO@ R2,R5
        MVI@ R4,R2
        MVO@ R3,R5
        MVI@ R4,R3
        ENDR
        MVO@ R2,R5
        MVI@ R4,R2
        MVO@ R3,R5
        MVO@ R2,R5
        INCR R4
        INCR R5
        DECR R1
        BNE @@vi12
        B @@vi4

@@vi6:  MVII #$0201,R4
        MVII #$0200,R5
        MVII #12,R1
@@vi11:
        REPEAT 19
        MVI@ R4,R0
        MVO@ R0,R5
        ENDR
        INCR R4
        INCR R5
        DECR R1
        BNE @@vi11
        B @@vi4
    
        ;
        ; Complex routine to be ahead of STIC display
        ; Moves first the top 6 lines, saves intermediate line
        ; Then moves the bottom 6 lines and restores intermediate line
        ;
@@vi7:  MVII #$0264,R4
        MVII #5,R1
        MVII #_scroll_buffer,R5
        REPEAT 20
        MVI@ R4,R0
        MVO@ R0,R5
        ENDR
        SUBI #40,R4
        MOVR R4,R5
        ADDI #20,R5
@@vi10:
        REPEAT 20
        MVI@ R4,R0
        MVO@ R0,R5
        ENDR
        SUBI #40,R4
        SUBI #40,R5
        DECR R1
        BNE @@vi10
        MVII #$02C8,R4
        MVII #$02DC,R5
        MVII #5,R1
@@vi13:
        REPEAT 20
        MVI@ R4,R0
        MVO@ R0,R5
        ENDR
        SUBI #40,R4
        SUBI #40,R5
        DECR R1
        BNE @@vi13
        MVII #_scroll_buffer,R4
        REPEAT 20
        MVI@ R4,R0
        MVO@ R0,R5
        ENDR
        B @@vi4

@@vi8:  MVII #$0214,R4
        MVII #$0200,R5
        MVII #$DC/4,R1
@@vi9:  
        REPEAT 4
        MVI@ R4,R0
        MVO@ R0,R5
        ENDR
        DECR R1
        BNE @@vi9
        B @@vi4

@@vi4:
    ENDI

    IF DEFINED intybasic_voice
        ;
        ; Intellivoice support
        ;
        CALL IV_ISR
    ENDI

        ;
        ; Random number generator
        ;
	CALL _next_random

    IF DEFINED intybasic_music
	; Generate sound for next frame
       	MVI _ntsc,R0
        TSTR R0         ; PAL?
        BEQ @@vo98      ; Yes, always generate sound
	MVI _music_frame,R0
	TSTR R0
	BEQ @@vo16
@@vo98: CALL _generate_music
@@vo16:
    ENDI

        ; Increase frame number
        MVI _frame,R0
        INCR R0
        MVO R0,_frame

	; This mark is for ON FRAME GOSUB support

        RETURN
        ENDP

	;
	; Generates the next random number
	;
_next_random:	PROC

MACRO _ROR
	RRC R0,1
	MOVR R0,R2
	SLR R2,2
	SLR R2,2
	ANDI #$0800,R2
	SLR R2,2
	SLR R2,2
	ANDI #$007F,R0
	XORR R2,R0
ENDM
        MVI _rand,R0
        SETC
        _ROR
        XOR _frame,R0
        _ROR
        XOR _rand,R0
        _ROR
        XORI #9,R0
        MVO R0,_rand
	JR R5
	ENDP

    IF DEFINED intybasic_music

        ;
        ; Music player, comes from my game Princess Quest for Intellivision
        ; so it's a practical tracker used in a real game ;) and with enough
        ; features.
        ;

        ; NTSC frequency for notes (based on 3.579545 mhz)
ntsc_note_table:    PROC
        ; Silence - 0
        DECLE 0
        ; Octave 2 - 1
        DECLE 1721,1621,1532,1434,1364,1286,1216,1141,1076,1017,956,909
        ; Octave 3 - 13
        DECLE 854,805,761,717,678,639,605,571,538,508,480,453
        ; Octave 4 - 25
        DECLE 427,404,380,360,339,321,302,285,270,254,240,226
        ; Octave 5 - 37
        DECLE 214,202,191,180,170,160,151,143,135,127,120,113
        ; Octave 6 - 49
        DECLE 107,101,95,90,85,80,76,71,67,64,60,57
        ; Octave 7 - 61
        ; Space for two notes more
	ENDP

        ; PAL frequency for notes (based on 4 mhz)
pal_note_table:    PROC
        ; Silence - 0
        DECLE 0
        ; Octava 2 - 1
        DECLE 1923,1812,1712,1603,1524,1437,1359,1276,1202,1136,1068,1016
        ; Octava 3 - 13
        DECLE 954,899,850,801,758,714,676,638,601,568,536,506
        ; Octava 4 - 25
        DECLE 477,451,425,402,379,358,338,319,301,284,268,253
        ; Octava 5 - 37
        DECLE 239,226,213,201,190,179,169,159,150,142,134,127
        ; Octava 6 - 49
        DECLE 120,113,106,100,95,89,84,80,75,71,67,63
        ; Octava 7 - 61
        ; Space for two notes more
	ENDP
    ENDI

        ;
        ; Music tracker init
        ;
_init_music:	PROC
    IF DEFINED intybasic_music
        MVI _ntsc,R0
        CMPI #1,R0
        MVII #ntsc_note_table,R0
        BEQ @@0
        MVII #pal_note_table,R0
@@0:    MVO R0,_music_table
        MVII #$38,R0	; $B8 blocks controllers o.O!
	MVO R0,_music_mix
        CLRR R0
    ELSE
	JR R5
    ENDI
	ENDP

    IF DEFINED intybasic_music
        ;
        ; Start music
        ; R0 = Pointer to music
        ;
_play_music:	PROC
	MOVR R0,R2
        MVII #1,R0
	MOVR R0,R3
	TSTR R2
	BEQ @@1
	MVI@ R2,R3
	INCR R2
@@1:	MVO R2,_music_start
	MVO R2,_music_p
	MVO R3,_music_t
	MVO R0,_music_tc
        JR R5

	ENDP

        ;
        ; Generate music
        ;
_generate_music:	PROC
	BEGIN
	MVI _music_mix,R0
	ANDI #$C0,R0
	XORI #$38,R0
	MVO R0,_music_mix
	CLRR R1			; Turn off volume for the three sound channels
	MVO R1,_music_vol1
	MVO R1,_music_vol2
	NOP
	MVO R1,_music_vol3
	MVI _music_tc,R3
	DECR R3
	MVO R3,_music_tc
	BNE @@6
	; R3 is zero from here up to @@6
	MVI _music_p,R4
@@15:	TSTR R4		; Silence?
	BEQ @@000	; Keep quiet
	MVI@ R4,R0
	MVI@ R4,R1
	MVI _music_t,R2
        CMPI #$FE,R0	; The end?
	BEQ @@001       ; Keep quiet
	CMPI #$FD,R0	; Repeat?
	BNE @@00
	MVI _music_start,R4
	B @@15

@@001:	MOVR R1,R4	; Jump, zero will make it quiet
	B @@15

@@000:  MVII #1,R0
        MVO R0,_music_tc
        B @@0
        
@@00: 	MVO R2,_music_tc    ; Restart note time
     	MVO R4,_music_p
     	
	MOVR R0,R2
	ANDI #$FF,R2
	CMPI #$3F,R2	; Sustain note?
	BEQ @@1
	MOVR R2,R4
	ANDI #$3F,R4
	MVO R4,_music_n1	; Note
	MVO R3,_music_s1	; Waveform
	ANDI #$C0,R2
	MVO R2,_music_i1	; Instrument
	
@@1:	MOVR R0,R2
	SWAP R2
	ANDI #$FF,R2
	CMPI #$3F,R2	; Sustain note?
	BEQ @@2
	MOVR R2,R4
	ANDI #$3F,R4
	MVO R4,_music_n2	; Note
	MVO R3,_music_s2	; Waveform
	ANDI #$C0,R2
	MVO R2,_music_i2	; Instrument
	
@@2:	MOVR R1,R2
	ANDI #$FF,R2
	CMPI #$3F,R2	; Sustain note?
	BEQ @@3
	MOVR R2,R4
	ANDI #$3F,R4
	MVO R4,_music_n3	; Note
	MVO R3,_music_s3	; Waveform
	ANDI #$C0,R2
	MVO R2,_music_i3	; Instrument
	
@@3:	MOVR R1,R2
	SWAP R2
	MVO R2,_music_n4
	MVO R3,_music_s4
	
        ;
        ; Construct main voice
        ;
@@6:	MVI _music_n1,R3	; Read note
	TSTR R3		; There is note?
	BEQ @@7		; No, jump
	MVI _music_s1,R1
	MVI _music_i1,R2
	MOVR R1,R0
	CALL _note2freq
	MVO R3,_music_freq10	; Note in voice A
	SWAP R3
	MVO R3,_music_freq11
	MVO R1,_music_vol1
        ; Increase time for instrument waveform
	INCR R0
	CMPI #$18,R0
	BNE @@20
	SUBI #$08,R0
@@20:	MVO R0,_music_s1

@@7:	MVI _music_n2,R3	; Read note
	TSTR R3		; There is note?
	BEQ @@8		; No, jump
	MVI _music_s2,R1
	MVI _music_i2,R2
	MOVR R1,R0
	CALL _note2freq
	MVO R3,_music_freq20	; Note in voice B
	SWAP R3
	MVO R3,_music_freq21
	MVO R1,_music_vol2
        ; Increase time for instrument waveform
	INCR R0
	CMPI #$18,R0
	BNE @@21
	SUBI #$08,R0
@@21:	MVO R0,_music_s2

@@8:	MVI _music_n3,R3	; Read note
	TSTR R3		; There is note?
	BEQ @@9		; No, jump
	MVI _music_s3,R1
	MVI _music_i3,R2
	MOVR R1,R0
	CALL _note2freq
	MVO R3,_music_freq30	; Note in voice C
	SWAP R3
	MVO R3,_music_freq31
	MVO R1,_music_vol3
        ; Increase time for instrument waveform
	INCR R0
	CMPI #$18,R0
	BNE @@22
	SUBI #$08,R0
@@22:	MVO R0,_music_s3

@@9:	MVI _music_n4,R0	; Read drum
	DECR R0		; There is drum?
	BMI @@4		; No, jump
	MVI _music_s4,R1
	       		; 1 - Strong
	BNE @@5
	CMPI #3,R1
	BGE @@12
@@10:	MVII #5,R0
	MVO R0,_music_noise
	CALL _activate_drum
	B @@12

@@5:	DECR R0		;2 - Short
	BNE @@11
	TSTR R1
	BNE @@12
	MVII #8,R0
	MVO R0,_music_noise
	CALL _activate_drum
	B @@12

@@11:	;DECR R0	; 3 - Rolling
	;BNE @@12
	CMPI #2,R1
	BLT @@10
	MVI _music_t,R0
	SLR R0,1
	CMPR R0,R1
	BLT @@12
        ADDI #2,R0
	CMPR R0,R1
	BLT @@10
        ; Increase time for drum waveform
@@12:   INCR R1
	MVO R1,_music_s4
@@4:
@@0:	RETURN
	ENDP

        ;
	; Translates note number to frequency
        ; R3 = Note
        ; R1 = Position in waveform for instrument
        ; R2 = Instrument
        ;
_note2freq:	PROC
        ADD _music_table,R3
	MVI@ R3,R3
        SWAP R2
	BEQ _piano_instrument
	RLC R2,1
	BNC _clarinet_instrument
	BPL _flute_instrument
;	BMI _bass_instrument
	ENDP

        ;
        ; Generates a bass
        ;
_bass_instrument:	PROC
	SLL R3,2	; Lower 2 octaves
	ADDI #_bass_volume,R1
	MVI@ R1,R1	; Bass effect
    IF DEFINED intybasic_music_volume
	B _global_volume
    ELSE
	JR R5
    ENDI
	ENDP

_bass_volume:	PROC
        DECLE 12,13,14,14,13,12,12,12
        DECLE 11,11,12,12,11,11,12,12
	DECLE 11,11,12,12,11,11,12,12
	ENDP

        ;
        ; Generates a piano
        ; R3 = Frequency
        ; R1 = Waveform position
        ;
        ; Output:
        ; R3 = Frequency.
        ; R1 = Volume.
        ;
_piano_instrument:	PROC
	ADDI #_piano_volume,R1
	MVI@ R1,R1
    IF DEFINED intybasic_music_volume
	B _global_volume
    ELSE
	JR R5
    ENDI
	ENDP

_piano_volume:	PROC
        DECLE 14,13,13,12,12,11,11,10
        DECLE 10,9,9,8,8,7,7,6
        DECLE 6,6,7,7,6,6,5,5
	ENDP

        ;
        ; Generate a clarinet
        ; R3 = Frequency
        ; R1 = Waveform position
        ;
        ; Output:
        ; R3 = Frequency
        ; R1 = Volume
        ;
_clarinet_instrument:	PROC
	ADDI #_clarinet_vibrato,R1
	ADD@ R1,R3
	CLRC
	RRC R3,1	; Duplicates frequency
	ADCR R3
        ADDI #_clarinet_volume-_clarinet_vibrato,R1
	MVI@ R1,R1
    IF DEFINED intybasic_music_volume
	B _global_volume
    ELSE
	JR R5
    ENDI
	ENDP

_clarinet_vibrato:	PROC
        DECLE 0,0,0,0
        DECLE -2,-4,-2,0
        DECLE 2,4,2,0
        DECLE -2,-4,-2,0
        DECLE 2,4,2,0
        DECLE -2,-4,-2,0
	ENDP

_clarinet_volume:	PROC
        DECLE 13,14,14,13,13,12,12,12
        DECLE 11,11,11,11,12,12,12,12
        DECLE 11,11,11,11,12,12,12,12
	ENDP

        ;
        ; Generates a flute
        ; R3 = Frequency
        ; R1 = Waveform position
        ;
        ; Output:
        ; R3 = Frequency
        ; R1 = Volume
        ;
_flute_instrument:	PROC
	ADDI #_flute_vibrato,R1
	ADD@ R1,R3
	ADDI #_flute_volume-_flute_vibrato,R1
	MVI@ R1,R1
    IF DEFINED intybasic_music_volume
	B _global_volume
    ELSE
	JR R5
    ENDI
	ENDP

_flute_vibrato:	PROC
        DECLE 0,0,0,0
        DECLE 0,1,2,1
        DECLE 0,1,2,1
        DECLE 0,1,2,1
        DECLE 0,1,2,1
        DECLE 0,1,2,1
	ENDP
                 
_flute_volume:	PROC
        DECLE 10,12,13,13,12,12,12,12
        DECLE 11,11,11,11,10,10,10,10
        DECLE 11,11,11,11,10,10,10,10
	ENDP

    IF DEFINED intybasic_music_volume

_global_volume:	PROC
	MVI _music_vol,R2
	ANDI #$0F,R2
	SLL R2,2
	SLL R2,2
	ADDR R1,R2
	ADDI #@@table,R2
	MVI@ R2,R1
	JR R5

@@table:
	DECLE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	DECLE 0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1
	DECLE 0,0,0,0,1,1,1,1,1,1,1,2,2,2,2,2
	DECLE 0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3
	DECLE 0,0,1,1,1,1,2,2,2,2,3,3,3,4,4,4
	DECLE 0,0,1,1,1,2,2,2,3,3,3,4,4,4,5,5
	DECLE 0,0,1,1,2,2,2,3,3,4,4,4,5,5,6,6
	DECLE 0,1,1,1,2,2,3,3,4,4,5,5,6,6,7,7
	DECLE 0,1,1,2,2,3,3,4,4,5,5,6,6,7,8,8
	DECLE 0,1,1,2,2,3,4,4,5,5,6,7,7,8,8,9
	DECLE 0,1,1,2,3,3,4,5,5,6,7,7,8,9,9,10
	DECLE 0,1,2,2,3,4,4,5,6,7,7,8,9,10,10,11
	DECLE 0,1,2,2,3,4,5,6,6,7,8,9,10,10,11,12
	DECLE 0,1,2,3,4,4,5,6,7,8,9,10,10,11,12,13
	DECLE 0,1,2,3,4,5,6,7,8,8,9,10,11,12,13,14
	DECLE 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15

	ENDP

    ENDI

        ;
        ; Emits sound
        ;
_emit_sound:	PROC
        MOVR R5,R1
	MVI _music_mode,R2
	SARC R2,1
	BEQ @@6
	MVII #_music_freq10,R4
	MVII #$01F0,R5
        MVI@ R4,R0
	MVO@ R0,R5	; $01F0 - Channel A Period (Low 8 bits of 12)
        MVI@ R4,R0
	MVO@ R0,R5	; $01F1 - Channel B Period (Low 8 bits of 12)
	DECR R2
	BEQ @@1
        MVI@ R4,R0	
	MVO@ R0,R5	; $01F2 - Channel C Period (Low 8 bits of 12)
	INCR R5		; Avoid $01F3 - Enveloped Period (Low 8 bits of 16)
        MVI@ R4,R0
	MVO@ R0,R5	; $01F4 - Channel A Period (High 4 bits of 12)
        MVI@ R4,R0
	MVO@ R0,R5	; $01F5 - Channel B Period (High 4 bits of 12)
        MVI@ R4,R0
	MVO@ R0,R5	; $01F6 - Channel C Period (High 4 bits of 12)
	INCR R5		; Avoid $01F7 - Envelope Period (High 8 bits of 16)
	BC @@2		; Jump if playing with drums
	ADDI #2,R4
	ADDI #3,R5
	B @@3

@@2:	MVI@ R4,R0
	MVO@ R0,R5	; $01F8 - Enable Noise/Tone (bits 3-5 Noise : 0-2 Tone)
        MVI@ R4,R0	
	MVO@ R0,R5	; $01F9 - Noise Period (5 bits)
	INCR R5		; Avoid $01FA - Envelope Type (4 bits)
@@3:    MVI@ R4,R0
	MVO@ R0,R5	; $01FB - Channel A Volume
        MVI@ R4,R0
	MVO@ R0,R5	; $01FC - Channel B Volume
        MVI@ R4,R0
	MVO@ R0,R5	; $01FD - Channel C Volume
        JR R1

@@1:	INCR R4		
	ADDI #2,R5	; Avoid $01F2 and $01F3
        MVI@ R4,R0
	MVO@ R0,R5	; $01F4
        MVI@ R4,R0
	MVO@ R0,R5	; $01F5
	INCR R4
	ADDI #2,R5	; Avoid $01F6 and $01F7
	BC @@4		; Jump if playing with drums
	ADDI #2,R4
	ADDI #3,R5
	B @@5

@@4:	MVI@ R4,R0
	MVO@ R0,R5	; $01F8
        MVI@ R4,R0
	MVO@ R0,R5	; $01F9
	INCR R5		; Avoid $01FA
@@5:    MVI@ R4,R0
	MVO@ R0,R5	; $01FB
        MVI@ R4,R0
	MVO@ R0,R5	; $01FD
@@6:    JR R1
	ENDP

        ;
        ; Activates drum
        ;
_activate_drum:	PROC
    IF DEFINED intybasic_music_volume
	BEGIN
    ENDI
	MVI _music_mode,R2
	SARC R2,1	; PLAY NO DRUMS?
	BNC @@0		; Yes, jump
	MVI _music_vol1,R0
	TSTR R0
	BNE @@1
        MVII #11,R1
    IF DEFINED intybasic_music_volume
	CALL _global_volume
    ENDI
	MVO R1,_music_vol1
	MVI _music_mix,R0
	ANDI #$F6,R0
	XORI #$01,R0
	MVO R0,_music_mix
    IF DEFINED intybasic_music_volume
	RETURN
    ELSE
	JR R5
    ENDI

@@1:    MVI _music_vol2,R0
	TSTR R0
	BNE @@2
        MVII #11,R1
    IF DEFINED intybasic_music_volume
	CALL _global_volume
    ENDI
	MVO R1,_music_vol2
	MVI _music_mix,R0
	ANDI #$ED,R0
	XORI #$02,R0
	MVO R0,_music_mix
    IF DEFINED intybasic_music_volume
	RETURN
    ELSE
	JR R5
    ENDI

@@2:    DECR R2		; PLAY SIMPLE?
        BEQ @@3		; Yes, jump
        MVI _music_vol3,R0
	TSTR R0
	BNE @@3
        MVII #11,R1
    IF DEFINED intybasic_music_volume
	CALL _global_volume
    ENDI
	MVO R1,_music_vol3
	MVI _music_mix,R0
	ANDI #$DB,R0
	XORI #$04,R0
	MVO R0,_music_mix
    IF DEFINED intybasic_music_volume
	RETURN
    ELSE
	JR R5
    ENDI

@@3:    MVI _music_mix,R0
        ANDI #$EF,R0
	MVO R0,_music_mix
@@0:	
    IF DEFINED intybasic_music_volume
	RETURN
    ELSE
	JR R5
    ENDI

	ENDP

    ENDI
    
    IF DEFINED intybasic_numbers

	;
	; Following code from as1600 libraries, prnum16.asm
        ; Public domain by Joseph Zbiciak
	;

;* ======================================================================== *;
;*  These routines are placed into the public domain by their author.  All  *;
;*  copyright rights are hereby relinquished on the routines and data in    *;
;*  this file.  -- Joseph Zbiciak, 2008                                     *;
;* ======================================================================== *;

;; ======================================================================== ;;
;;  _PW10                                                                   ;;
;;      Lookup table holding the first 5 powers of 10 (1 thru 10000) as     ;;
;;      16-bit numbers.                                                     ;;
;; ======================================================================== ;;
_PW10   PROC    ; 0 thru 10000
        DECLE   10000, 1000, 100, 10, 1, 0
        ENDP

;; ======================================================================== ;;
;;  PRNUM16.l     -- Print an unsigned 16-bit number left-justified.        ;;
;;  PRNUM16.b     -- Print an unsigned 16-bit number with leading blanks.   ;;
;;  PRNUM16.z     -- Print an unsigned 16-bit number with leading zeros.    ;;
;;                                                                          ;;
;;  AUTHOR                                                                  ;;
;;      Joseph Zbiciak  <im14u2c AT globalcrossing DOT net>                 ;;
;;                                                                          ;;
;;  REVISION HISTORY                                                        ;;
;;      30-Mar-2003 Initial complete revision                               ;;
;;                                                                          ;;
;;  INPUTS for all variants                                                 ;;
;;      R0  Number to print.                                                ;;
;;      R2  Width of field.  Ignored by PRNUM16.l.                          ;;
;;      R3  Format word, added to digits to set the color.                  ;;
;;          Note:  Bit 15 MUST be cleared when building with PRNUM32.       ;;
;;      R4  Pointer to location on screen to print number                   ;;
;;                                                                          ;;
;;  OUTPUTS                                                                 ;;
;;      R0  Zeroed                                                          ;;
;;      R1  Unmodified                                                      ;;
;;      R2  Unmodified                                                      ;;
;;      R3  Unmodified                                                      ;;
;;      R4  Points to first character after field.                          ;;
;;                                                                          ;;
;;  DESCRIPTION                                                             ;;
;;      These routines print unsigned 16-bit numbers in a field up to 5     ;;
;;      positions wide.  The number is printed either in left-justified     ;;
;;      or right-justified format.  Right-justified numbers are padded      ;;
;;      with leading blanks or leading zeros.  Left-justified numbers       ;;
;;      are not padded on the right.                                        ;;
;;                                                                          ;;
;;      This code handles fields wider than 5 characters, padding with      ;;
;;      zeros or blanks as necessary.                                       ;;
;;                                                                          ;;
;;              Routine      Value(hex)     Field        Output             ;;
;;              ----------   ----------   ----------   ----------           ;;
;;              PRNUM16.l      $0045         n/a        "69"                ;;
;;              PRNUM16.b      $0045          4         "  69"              ;;
;;              PRNUM16.b      $0045          6         "    69"            ;;
;;              PRNUM16.z      $0045          4         "0069"              ;;
;;              PRNUM16.z      $0045          6         "000069"            ;;
;;                                                                          ;;
;;  TECHNIQUES                                                              ;;
;;      This routine uses repeated subtraction to divide the number         ;;
;;      to display by various powers of 10.  This is cheaper than a         ;;
;;      full divide, at least when the input number is large.  It's         ;;
;;      also easier to get right.  :-)                                      ;;
;;                                                                          ;;
;;      The printing routine first pads out fields wider than 5 spaces      ;;
;;      with zeros or blanks as requested.  It then scans the power-of-10   ;;
;;      table looking for the first power of 10 that is <= the number to    ;;
;;      display.  While scanning for this power of 10, it outputs leading   ;;
;;      blanks or zeros, if requested.  This eliminates "leading digit"     ;;
;;      logic from the main digit loop.                                     ;;
;;                                                                          ;;
;;      Once in the main digit loop, we discover the value of each digit    ;;
;;      by repeated subtraction.  We build up our digit value while         ;;
;;      subtracting the power-of-10 repeatedly.  We iterate until we go     ;;
;;      a step too far, and then we add back on power-of-10 to restore      ;;
;;      the remainder.                                                      ;;
;;                                                                          ;;
;;  NOTES                                                                   ;;
;;      The left-justified variant ignores field width.                     ;;
;;                                                                          ;;
;;      The code is fully reentrant.                                        ;;
;;                                                                          ;;
;;      This code does not handle numbers which are too large to be         ;;
;;      displayed in the provided field.  If the number is too large,       ;;
;;      non-digit characters will be displayed in the initial digit         ;;
;;      position.  Also, the run time of this routine may get excessively   ;;
;;      large, depending on the magnitude of the overflow.                  ;;
;;                                                                          ;;
;;      When using with PRNUM32, one must either include PRNUM32 before     ;;
;;      this function, or define the symbol _WITH_PRNUM32.  PRNUM32         ;;
;;      needs a tiny bit of support from PRNUM16 to handle numbers in       ;;
;;      the range 65536...99999 correctly.                                  ;;
;;                                                                          ;;
;;  CODESIZE                                                                ;;
;;      73 words, including power-of-10 table                               ;;
;;      80 words, if compiled with PRNUM32.                                 ;;
;;                                                                          ;;
;;      To save code size, you can define the following symbols to omit     ;;
;;      some variants:                                                      ;;
;;                                                                          ;;
;;          _NO_PRNUM16.l:   Disables PRNUM16.l.  Saves 10 words            ;;
;;          _NO_PRNUM16.b:   Disables PRNUM16.b.  Saves 3 words.            ;;
;;                                                                          ;;
;;      Defining both symbols saves 17 words total, because it omits        ;;
;;      some code shared by both routines.                                  ;;
;;                                                                          ;;
;;  STACK USAGE                                                             ;;
;;      This function uses up to 4 words of stack space.                    ;;
;; ======================================================================== ;;

PRNUM16 PROC

    
        ;; ---------------------------------------------------------------- ;;
        ;;  PRNUM16.l:  Print unsigned, left-justified.                     ;;
        ;; ---------------------------------------------------------------- ;;
@@l:    PSHR    R5              ; save return address
@@l1:   MVII    #$1,    R5      ; set R5 to 1 to counteract screen ptr update
                                ; in the 'find initial power of 10' loop
        PSHR    R2
        MVII    #5,     R2      ; force effective field width to 5.
        B       @@z2

        ;; ---------------------------------------------------------------- ;;
        ;;  PRNUM16.b:  Print unsigned with leading blanks.                 ;;
        ;; ---------------------------------------------------------------- ;;
@@b:    PSHR    R5
@@b1:   CLRR    R5              ; let the blank loop do its thing
        INCR    PC              ; skip the PSHR R5

        ;; ---------------------------------------------------------------- ;;
        ;;  PRNUM16.z:  Print unsigned with leading zeros.                  ;;
        ;; ---------------------------------------------------------------- ;;
@@z:    PSHR    R5
@@z1:   PSHR    R2
@@z2:   PSHR    R1

        ;; ---------------------------------------------------------------- ;;
        ;;  Find the initial power of 10 to use for display.                ;;
        ;;  Note:  For fields wider than 5, fill the extra spots above 5    ;;
        ;;  with blanks or zeros as needed.                                 ;;
        ;; ---------------------------------------------------------------- ;;
        MVII    #_PW10+5,R1     ; Point to end of power-of-10 table
        SUBR    R2,     R1      ; Subtract the field width to get right power
        PSHR    R3              ; save format word

        CMPI    #2,     R5      ; are we leading with zeros?
        BNC     @@lblnk         ; no:  then do the loop w/ blanks

        CLRR    R5              ; force R5==0
        ADDI    #$80,   R3      ; yes: do the loop with zeros
        B       @@lblnk
    

@@llp   MVO@    R3,     R4      ; print a blank/zero

        SUBR    R5,     R4      ; rewind pointer if needed.

        INCR    R1              ; get next power of 10
@@lblnk DECR    R2              ; decrement available digits
        BEQ     @@ldone
        CMPI    #5,     R2      ; field too wide?
        BGE     @@llp           ; just force blanks/zeros 'till we're narrower.
        CMP@    R1,     R0      ; Is this power of 10 too big?
        BNC     @@llp           ; Yes:  Put a blank and go to next

@@ldone PULR    R3              ; restore format word

        ;; ---------------------------------------------------------------- ;;
        ;;  The digit loop prints at least one digit.  It discovers digits  ;;
        ;;  by repeated subtraction.                                        ;;
        ;; ---------------------------------------------------------------- ;;
@@digit TSTR    R0              ; If the number is zero, print zero and leave
        BNEQ    @@dig1          ; no: print the number

        MOVR    R3,     R5      ;\    
        ADDI    #$80,   R5      ; |-- print a 0 there.
        MVO@    R5,     R4      ;/    
        B       @@done

@@dig1:
    
@@nxdig MOVR    R3,     R5      ; save display format word
@@cont: ADDI    #$80-8, R5      ; start our digit as one just before '0'
@@spcl:
 
        ;; ---------------------------------------------------------------- ;;
        ;;  Divide by repeated subtraction.  This divide is constructed     ;;
        ;;  to go "one step too far" and then back up.                      ;;
        ;; ---------------------------------------------------------------- ;;
@@div:  ADDI    #8,     R5      ; increment our digit
        SUB@    R1,     R0      ; subtract power of 10
        BC      @@div           ; loop until we go too far
        ADD@    R1,     R0      ; add back the extra power of 10.

        MVO@    R5,     R4      ; display the digit.

        INCR    R1              ; point to next power of 10
        DECR    R2              ; any room left in field?
        BPL     @@nxdig         ; keep going until R2 < 0.

@@done: PULR    R1              ; restore R1
        PULR    R2              ; restore R2
        PULR    PC              ; return

        ENDP
        
    ENDI

    IF DEFINED intybasic_voice
;;==========================================================================;;
;;  SP0256-AL2 Allophones                                                   ;;
;;                                                                          ;;
;;  This file contains the allophone set that was obtained from an          ;;
;;  SP0256-AL2.  It is being provided for your convenience.                 ;;
;;                                                                          ;;
;;  The directory "al2" contains a series of assembly files, each one       ;;
;;  containing a single allophone.  This series of files may be useful in   ;;
;;  situations where space is at a premium.                                 ;;
;;                                                                          ;;
;;  Consult the Archer SP0256-AL2 documentation (under doc/programming)     ;;
;;  for more information about SP0256-AL2's allophone library.              ;;
;;                                                                          ;;
;; ------------------------------------------------------------------------ ;;
;;                                                                          ;;
;;  Copyright information:                                                  ;;
;;                                                                          ;;
;;  The allophone data below was extracted from the SP0256-AL2 ROM image.   ;;
;;  The SP0256-AL2 allophones are NOT in the public domain, nor are they    ;;
;;  placed under the GNU General Public License.  This program is           ;;
;;  distributed in the hope that it will be useful, but WITHOUT ANY         ;;
;;  WARRANTY; without even the implied warranty of MERCHANTABILITY or       ;;
;;  FITNESS FOR A PARTICULAR PURPOSE.                                       ;;
;;                                                                          ;;
;;  Microchip, Inc. retains the copyright to the data and algorithms        ;;
;;  contained in the SP0256-AL2.  This speech data is distributed with      ;;
;;  explicit permission from Microchip, Inc.  All such redistributions      ;;
;;  must retain this notice of copyright.                                   ;;
;;                                                                          ;;
;;  No copyright claims are made on this data by the author(s) of SDK1600.  ;;
;;  Please see http://spatula-city.org/~im14u2c/sp0256-al2/ for details.    ;;
;;                                                                          ;;
;;==========================================================================;;

;; ------------------------------------------------------------------------ ;;
_AA:
    DECLE   _AA.end - _AA - 1
    DECLE   $0318, $014C, $016F, $02CE, $03AF, $015F, $01B1, $008E
    DECLE   $0088, $0392, $01EA, $024B, $03AA, $039B, $000F, $0000
_AA.end:  ; 16 decles
;; ------------------------------------------------------------------------ ;;
_AE1:
    DECLE   _AE1.end - _AE1 - 1
    DECLE   $0118, $038E, $016E, $01FC, $0149, $0043, $026F, $036E
    DECLE   $01CC, $0005, $0000
_AE1.end:  ; 11 decles
;; ------------------------------------------------------------------------ ;;
_AO:
    DECLE   _AO.end - _AO - 1
    DECLE   $0018, $010E, $016F, $0225, $00C6, $02C4, $030F, $0160
    DECLE   $024B, $0005, $0000
_AO.end:  ; 11 decles
;; ------------------------------------------------------------------------ ;;
_AR:
    DECLE   _AR.end - _AR - 1
    DECLE   $0218, $010C, $016E, $001E, $000B, $0091, $032F, $00DE
    DECLE   $018B, $0095, $0003, $0238, $0027, $01E0, $03E8, $0090
    DECLE   $0003, $01C7, $0020, $03DE, $0100, $0190, $01CA, $02AB
    DECLE   $00B7, $004A, $0386, $0100, $0144, $02B6, $0024, $0320
    DECLE   $0011, $0041, $01DF, $0316, $014C, $016E, $001E, $00C4
    DECLE   $02B2, $031E, $0264, $02AA, $019D, $01BE, $000B, $00F0
    DECLE   $006A, $01CE, $00D6, $015B, $03B5, $03E4, $0000, $0380
    DECLE   $0007, $0312, $03E8, $030C, $016D, $02EE, $0085, $03C2
    DECLE   $03EC, $0283, $024A, $0005, $0000
_AR.end:  ; 69 decles
;; ------------------------------------------------------------------------ ;;
_AW:
    DECLE   _AW.end - _AW - 1
    DECLE   $0010, $01CE, $016E, $02BE, $0375, $034F, $0220, $0290
    DECLE   $008A, $026D, $013F, $01D5, $0316, $029F, $02E2, $018A
    DECLE   $0170, $0035, $00BD, $0000, $0000
_AW.end:  ; 21 decles
;; ------------------------------------------------------------------------ ;;
_AX:
    DECLE   _AX.end - _AX - 1
    DECLE   $0218, $02CD, $016F, $02F5, $0386, $00C2, $00CD, $0094
    DECLE   $010C, $0005, $0000
_AX.end:  ; 11 decles
;; ------------------------------------------------------------------------ ;;
_AY:
    DECLE   _AY.end - _AY - 1
    DECLE   $0110, $038C, $016E, $03B7, $03B3, $02AF, $0221, $009E
    DECLE   $01AA, $01B3, $00BF, $02E7, $025B, $0354, $00DA, $017F
    DECLE   $018A, $03F3, $00AF, $02D5, $0356, $027F, $017A, $01FB
    DECLE   $011E, $01B9, $03E5, $029F, $025A, $0076, $0148, $0124
    DECLE   $003D, $0000
_AY.end:  ; 34 decles
;; ------------------------------------------------------------------------ ;;
_BB1:
    DECLE   _BB1.end - _BB1 - 1
    DECLE   $0318, $004C, $016C, $00FB, $00C7, $0144, $002E, $030C
    DECLE   $010E, $018C, $01DC, $00AB, $00C9, $0268, $01F7, $021D
    DECLE   $01B3, $0098, $0000
_BB1.end:  ; 19 decles
;; ------------------------------------------------------------------------ ;;
_BB2:
    DECLE   _BB2.end - _BB2 - 1
    DECLE   $00F4, $0046, $0062, $0200, $0221, $03E4, $0087, $016F
    DECLE   $02A6, $02B7, $0212, $0326, $0368, $01BF, $0338, $0196
    DECLE   $0002
_BB2.end:  ; 17 decles
;; ------------------------------------------------------------------------ ;;
_CH:
    DECLE   _CH.end - _CH - 1
    DECLE   $00F5, $0146, $0052, $0000, $032A, $0049, $0032, $02F2
    DECLE   $02A5, $0000, $026D, $0119, $0124, $00F6, $0000
_CH.end:  ; 15 decles
;; ------------------------------------------------------------------------ ;;
_DD1:
    DECLE   _DD1.end - _DD1 - 1
    DECLE   $0318, $034C, $016E, $0397, $01B9, $0020, $02B1, $008E
    DECLE   $0349, $0291, $01D8, $0072, $0000
_DD1.end:  ; 13 decles
;; ------------------------------------------------------------------------ ;;
_DD2:
    DECLE   _DD2.end - _DD2 - 1
    DECLE   $00F4, $00C6, $00F2, $0000, $0129, $00A6, $0246, $01F3
    DECLE   $02C6, $02B7, $028E, $0064, $0362, $01CF, $0379, $01D5
    DECLE   $0002
_DD2.end:  ; 17 decles
;; ------------------------------------------------------------------------ ;;
_DH1:
    DECLE   _DH1.end - _DH1 - 1
    DECLE   $0018, $034F, $016D, $030B, $0306, $0363, $017E, $006A
    DECLE   $0164, $019E, $01DA, $00CB, $00E8, $027A, $03E8, $01D7
    DECLE   $0173, $00A1, $0000
_DH1.end:  ; 19 decles
;; ------------------------------------------------------------------------ ;;
_DH2:
    DECLE   _DH2.end - _DH2 - 1
    DECLE   $0119, $034C, $016D, $030B, $0306, $0363, $017E, $006A
    DECLE   $0164, $019E, $01DA, $00CB, $00E8, $027A, $03E8, $01D7
    DECLE   $0173, $00A1, $0000
_DH2.end:  ; 19 decles
;; ------------------------------------------------------------------------ ;;
_EH:
    DECLE   _EH.end - _EH - 1
    DECLE   $0218, $02CD, $016F, $0105, $014B, $0224, $02CF, $0274
    DECLE   $014C, $0005, $0000
_EH.end:  ; 11 decles
;; ------------------------------------------------------------------------ ;;
_EL:
    DECLE   _EL.end - _EL - 1
    DECLE   $0118, $038D, $016E, $011C, $008B, $03D2, $030F, $0262
    DECLE   $006C, $019D, $01CC, $022B, $0170, $0078, $03FE, $0018
    DECLE   $0183, $03A3, $010D, $016E, $012E, $00C6, $00C3, $0300
    DECLE   $0060, $000D, $0005, $0000
_EL.end:  ; 28 decles
;; ------------------------------------------------------------------------ ;;
_ER1:
    DECLE   _ER1.end - _ER1 - 1
    DECLE   $0118, $034C, $016E, $001C, $0089, $01C3, $034E, $03E6
    DECLE   $00AB, $0095, $0001, $0000, $03FC, $0381, $0000, $0188
    DECLE   $01DA, $00CB, $00E7, $0048, $03A6, $0244, $016C, $01A8
    DECLE   $03E4, $0000, $0002, $0001, $00FC, $01DA, $02E4, $0000
    DECLE   $0002, $0008, $0200, $0217, $0164, $0000, $000E, $0038
    DECLE   $0014, $01EA, $0264, $0000, $0002, $0048, $01EC, $02F1
    DECLE   $03CC, $016D, $021E, $0048, $00C2, $034E, $036A, $000D
    DECLE   $008D, $000B, $0200, $0047, $0022, $03A8, $0000, $0000
_ER1.end:  ; 64 decles
;; ------------------------------------------------------------------------ ;;
_ER2:
    DECLE   _ER2.end - _ER2 - 1
    DECLE   $0218, $034C, $016E, $001C, $0089, $01C3, $034E, $03E6
    DECLE   $00AB, $0095, $0001, $0000, $03FC, $0381, $0000, $0190
    DECLE   $01D8, $00CB, $00E7, $0058, $01A6, $0244, $0164, $02A9
    DECLE   $0024, $0000, $0000, $0007, $0201, $02F8, $02E4, $0000
    DECLE   $0002, $0001, $00FC, $02DA, $0024, $0000, $0002, $0008
    DECLE   $0200, $0217, $0024, $0000, $000E, $0038, $0014, $03EA
    DECLE   $03A4, $0000, $0002, $0048, $01EC, $03F1, $038C, $016D
    DECLE   $021E, $0048, $00C2, $034E, $036A, $000D, $009D, $0003
    DECLE   $0200, $0047, $0022, $03A8, $0000, $0000
_ER2.end:  ; 70 decles
;; ------------------------------------------------------------------------ ;;
_EY:
    DECLE   _EY.end - _EY - 1
    DECLE   $0310, $038C, $016E, $02A7, $00BB, $0160, $0290, $0094
    DECLE   $01CA, $03A9, $00C1, $02D7, $015B, $01D4, $03CE, $02FF
    DECLE   $00EA, $03E7, $0041, $0277, $025B, $0355, $03C9, $0103
    DECLE   $02EA, $03E4, $003F, $0000
_EY.end:  ; 28 decles
;; ------------------------------------------------------------------------ ;;
_FF:
    DECLE   _FF.end - _FF - 1
    DECLE   $0119, $03C8, $0000, $00A7, $0094, $0138, $01C6, $0000
_FF.end:  ; 8 decles
;; ------------------------------------------------------------------------ ;;
_GG1:
    DECLE   _GG1.end - _GG1 - 1
    DECLE   $00F4, $00C6, $00C2, $0200, $0015, $03FE, $0283, $01FD
    DECLE   $01E6, $00B7, $030A, $0364, $0331, $017F, $033D, $0215
    DECLE   $0002
_GG1.end:  ; 17 decles
;; ------------------------------------------------------------------------ ;;
_GG2:
    DECLE   _GG2.end - _GG2 - 1
    DECLE   $00F4, $0106, $0072, $0300, $0021, $0308, $0039, $0173
    DECLE   $00C6, $00B7, $037E, $03A3, $0319, $0177, $0036, $0217
    DECLE   $0002
_GG2.end:  ; 17 decles
;; ------------------------------------------------------------------------ ;;
_GG3:
    DECLE   _GG3.end - _GG3 - 1
    DECLE   $00F8, $0146, $00F2, $0100, $0132, $03A8, $0055, $01F5
    DECLE   $00A6, $02B7, $0291, $0326, $0368, $0167, $023A, $01C6
    DECLE   $0002
_GG3.end:  ; 17 decles
;; ------------------------------------------------------------------------ ;;
_HH1:
    DECLE   _HH1.end - _HH1 - 1
    DECLE   $0218, $01C9, $0000, $0095, $0127, $0060, $01D6, $0213
    DECLE   $0002, $01AE, $033E, $01A0, $03C4, $0122, $0001, $0218
    DECLE   $01E4, $03FD, $0019, $0000
_HH1.end:  ; 20 decles
;; ------------------------------------------------------------------------ ;;
_HH2:
    DECLE   _HH2.end - _HH2 - 1
    DECLE   $0218, $00CB, $0000, $0086, $000F, $0240, $0182, $031A
    DECLE   $02DB, $0008, $0293, $0067, $00BD, $01E0, $0092, $000C
    DECLE   $0000
_HH2.end:  ; 17 decles
;; ------------------------------------------------------------------------ ;;
_IH:
    DECLE   _IH.end - _IH - 1
    DECLE   $0118, $02CD, $016F, $0205, $0144, $02C3, $00FE, $031A
    DECLE   $000D, $0005, $0000
_IH.end:  ; 11 decles
;; ------------------------------------------------------------------------ ;;
_IY:
    DECLE   _IY.end - _IY - 1
    DECLE   $0318, $02CC, $016F, $0008, $030B, $01C3, $0330, $0178
    DECLE   $002B, $019D, $01F6, $018B, $01E1, $0010, $020D, $0358
    DECLE   $015F, $02A4, $02CC, $016F, $0109, $030B, $0193, $0320
    DECLE   $017A, $034C, $009C, $0017, $0001, $0200, $03C1, $0020
    DECLE   $00A7, $001D, $0001, $0104, $003D, $0040, $01A7, $01CA
    DECLE   $018B, $0160, $0078, $01F6, $0343, $01C7, $0090, $0000
_IY.end:  ; 48 decles
;; ------------------------------------------------------------------------ ;;
_JH:
    DECLE   _JH.end - _JH - 1
    DECLE   $0018, $0149, $0001, $00A4, $0321, $0180, $01F4, $039A
    DECLE   $02DC, $023C, $011A, $0047, $0200, $0001, $018E, $034E
    DECLE   $0394, $0356, $02C1, $010C, $03FD, $0129, $00B7, $01BA
    DECLE   $0000
_JH.end:  ; 25 decles
;; ------------------------------------------------------------------------ ;;
_KK1:
    DECLE   _KK1.end - _KK1 - 1
    DECLE   $00F4, $00C6, $00D2, $0000, $023A, $03E0, $02D1, $02E5
    DECLE   $0184, $0200, $0041, $0210, $0188, $00C5, $0000
_KK1.end:  ; 15 decles
;; ------------------------------------------------------------------------ ;;
_KK2:
    DECLE   _KK2.end - _KK2 - 1
    DECLE   $021D, $023C, $0211, $003C, $0180, $024D, $0008, $032B
    DECLE   $025B, $002D, $01DC, $01E3, $007A, $0000
_KK2.end:  ; 14 decles
;; ------------------------------------------------------------------------ ;;
_KK3:
    DECLE   _KK3.end - _KK3 - 1
    DECLE   $00F7, $0046, $01D2, $0300, $0131, $006C, $006E, $00F1
    DECLE   $00E4, $0000, $025A, $010D, $0110, $01F9, $014A, $0001
    DECLE   $00B5, $01A2, $00D8, $01CE, $0000
_KK3.end:  ; 21 decles
;; ------------------------------------------------------------------------ ;;
_LL:
    DECLE   _LL.end - _LL - 1
    DECLE   $0318, $038C, $016D, $029E, $0333, $0260, $0221, $0294
    DECLE   $01C4, $0299, $025A, $00E6, $014C, $012C, $0031, $0000
_LL.end:  ; 16 decles
;; ------------------------------------------------------------------------ ;;
_MM:
    DECLE   _MM.end - _MM - 1
    DECLE   $0210, $034D, $016D, $03F5, $00B0, $002E, $0220, $0290
    DECLE   $03CE, $02B6, $03AA, $00F3, $00CF, $015D, $016E, $0000
_MM.end:  ; 16 decles
;; ------------------------------------------------------------------------ ;;
_NG1:
    DECLE   _NG1.end - _NG1 - 1
    DECLE   $0118, $03CD, $016E, $00DC, $032F, $01BF, $01E0, $0116
    DECLE   $02AB, $029A, $0358, $01DB, $015B, $01A7, $02FD, $02B1
    DECLE   $03D2, $0356, $0000
_NG1.end:  ; 19 decles
;; ------------------------------------------------------------------------ ;;
_NN1:
    DECLE   _NN1.end - _NN1 - 1
    DECLE   $0318, $03CD, $016C, $0203, $0306, $03C3, $015F, $0270
    DECLE   $002A, $009D, $000D, $0248, $01B4, $0120, $01E1, $00C8
    DECLE   $0003, $0040, $0000, $0080, $015F, $0006, $0000
_NN1.end:  ; 23 decles
;; ------------------------------------------------------------------------ ;;
_NN2:
    DECLE   _NN2.end - _NN2 - 1
    DECLE   $0018, $034D, $016D, $0203, $0306, $03C3, $015F, $0270
    DECLE   $002A, $0095, $0003, $0248, $01B4, $0120, $01E1, $0090
    DECLE   $000B, $0040, $0000, $0080, $015F, $019E, $01F6, $028B
    DECLE   $00E0, $0266, $03F6, $01D8, $0143, $01A8, $0024, $00C0
    DECLE   $0080, $0000, $01E6, $0321, $0024, $0260, $000A, $0008
    DECLE   $03FE, $0000, $0000
_NN2.end:  ; 43 decles
;; ------------------------------------------------------------------------ ;;
_OR2:
    DECLE   _OR2.end - _OR2 - 1
    DECLE   $0218, $018C, $016D, $02A6, $03AB, $004F, $0301, $0390
    DECLE   $02EA, $0289, $0228, $0356, $01CF, $02D5, $0135, $007D
    DECLE   $02B5, $02AF, $024A, $02E2, $0153, $0167, $0333, $02A9
    DECLE   $02B3, $039A, $0351, $0147, $03CD, $0339, $02DA, $0000
_OR2.end:  ; 32 decles
;; ------------------------------------------------------------------------ ;;
_OW:
    DECLE   _OW.end - _OW - 1
    DECLE   $0310, $034C, $016E, $02AE, $03B1, $00CF, $0304, $0192
    DECLE   $018A, $022B, $0041, $0277, $015B, $0395, $03D1, $0082
    DECLE   $03CE, $00B6, $03BB, $02DA, $0000
_OW.end:  ; 21 decles
;; ------------------------------------------------------------------------ ;;
_OY:
    DECLE   _OY.end - _OY - 1
    DECLE   $0310, $014C, $016E, $02A6, $03AF, $00CF, $0304, $0192
    DECLE   $03CA, $01A8, $007F, $0155, $02B4, $027F, $00E2, $036A
    DECLE   $031F, $035D, $0116, $01D5, $02F4, $025F, $033A, $038A
    DECLE   $014F, $01B5, $03D5, $0297, $02DA, $03F2, $0167, $0124
    DECLE   $03FB, $0001
_OY.end:  ; 34 decles
;; ------------------------------------------------------------------------ ;;
_PA1:
    DECLE   _PA1.end - _PA1 - 1
    DECLE   $00F1, $0000
_PA1.end:  ; 2 decles
;; ------------------------------------------------------------------------ ;;
_PA2:
    DECLE   _PA2.end - _PA2 - 1
    DECLE   $00F4, $0000
_PA2.end:  ; 2 decles
;; ------------------------------------------------------------------------ ;;
_PA3:
    DECLE   _PA3.end - _PA3 - 1
    DECLE   $00F7, $0000
_PA3.end:  ; 2 decles
;; ------------------------------------------------------------------------ ;;
_PA4:
    DECLE   _PA4.end - _PA4 - 1
    DECLE   $00FF, $0000
_PA4.end:  ; 2 decles
;; ------------------------------------------------------------------------ ;;
_PA5:
    DECLE   _PA5.end - _PA5 - 1
    DECLE   $031D, $003F, $0000
_PA5.end:  ; 3 decles
;; ------------------------------------------------------------------------ ;;
_PP:
    DECLE   _PP.end - _PP - 1
    DECLE   $00FD, $0106, $0052, $0000, $022A, $03A5, $0277, $035F
    DECLE   $0184, $0000, $0055, $0391, $00EB, $00CF, $0000
_PP.end:  ; 15 decles
;; ------------------------------------------------------------------------ ;;
_RR1:
    DECLE   _RR1.end - _RR1 - 1
    DECLE   $0118, $01CD, $016C, $029E, $0171, $038E, $01E0, $0190
    DECLE   $0245, $0299, $01AA, $02E2, $01C7, $02DE, $0125, $00B5
    DECLE   $02C5, $028F, $024E, $035E, $01CB, $02EC, $0005, $0000
_RR1.end:  ; 24 decles
;; ------------------------------------------------------------------------ ;;
_RR2:
    DECLE   _RR2.end - _RR2 - 1
    DECLE   $0218, $03CC, $016C, $030C, $02C8, $0393, $02CD, $025E
    DECLE   $008A, $019D, $01AC, $02CB, $00BE, $0046, $017E, $01C2
    DECLE   $0174, $00A1, $01E5, $00E0, $010E, $0007, $0313, $0017
    DECLE   $0000
_RR2.end:  ; 25 decles
;; ------------------------------------------------------------------------ ;;
_SH:
    DECLE   _SH.end - _SH - 1
    DECLE   $0218, $0109, $0000, $007A, $0187, $02E0, $03F6, $0311
    DECLE   $0002, $0126, $0242, $0161, $03E9, $0219, $016C, $0300
    DECLE   $0013, $0045, $0124, $0005, $024C, $005C, $0182, $03C2
    DECLE   $0001
_SH.end:  ; 25 decles
;; ------------------------------------------------------------------------ ;;
_SS:
    DECLE   _SS.end - _SS - 1
    DECLE   $0218, $01CA, $0001, $0128, $001C, $0149, $01C6, $0000
_SS.end:  ; 8 decles
;; ------------------------------------------------------------------------ ;;
_TH:
    DECLE   _TH.end - _TH - 1
    DECLE   $0019, $0349, $0000, $00C6, $0212, $01D8, $01CA, $0000
_TH.end:  ; 8 decles
;; ------------------------------------------------------------------------ ;;
_TT1:
    DECLE   _TT1.end - _TT1 - 1
    DECLE   $00F6, $0046, $0142, $0100, $0042, $0088, $027E, $02EF
    DECLE   $01A4, $0200, $0049, $0290, $00FC, $00E8, $0000
_TT1.end:  ; 15 decles
;; ------------------------------------------------------------------------ ;;
_TT2:
    DECLE   _TT2.end - _TT2 - 1
    DECLE   $00F5, $00C6, $01D2, $0100, $0335, $00E9, $0042, $027A
    DECLE   $02A4, $0000, $0062, $01D1, $014C, $03EA, $02EC, $01E0
    DECLE   $0007, $03A7, $0000
_TT2.end:  ; 19 decles
;; ------------------------------------------------------------------------ ;;
_UH:
    DECLE   _UH.end - _UH - 1
    DECLE   $0018, $034E, $016E, $01FF, $0349, $00D2, $003C, $030C
    DECLE   $008B, $0005, $0000
_UH.end:  ; 11 decles
;; ------------------------------------------------------------------------ ;;
_UW1:
    DECLE   _UW1.end - _UW1 - 1
    DECLE   $0318, $014C, $016F, $029E, $03BD, $03BD, $0271, $0212
    DECLE   $0325, $0291, $016A, $027B, $014A, $03B4, $0133, $0001
_UW1.end:  ; 16 decles
;; ------------------------------------------------------------------------ ;;
_UW2:
    DECLE   _UW2.end - _UW2 - 1
    DECLE   $0018, $034E, $016E, $02F6, $0107, $02C2, $006D, $0090
    DECLE   $03AC, $01A4, $01DC, $03AB, $0128, $0076, $03E6, $0119
    DECLE   $014F, $03A6, $03A5, $0020, $0090, $0001, $02EE, $00BB
    DECLE   $0000
_UW2.end:  ; 25 decles
;; ------------------------------------------------------------------------ ;;
_VV:
    DECLE   _VV.end - _VV - 1
    DECLE   $0218, $030D, $016C, $010B, $010B, $0095, $034F, $03E4
    DECLE   $0108, $01B5, $01BE, $028B, $0160, $00AA, $03E4, $0106
    DECLE   $00EB, $02DE, $014C, $016E, $00F6, $0107, $00D2, $00CD
    DECLE   $0296, $00E4, $0006, $0000
_VV.end:  ; 28 decles
;; ------------------------------------------------------------------------ ;;
_WH:
    DECLE   _WH.end - _WH - 1
    DECLE   $0218, $00C9, $0000, $0084, $038E, $0147, $03A4, $0195
    DECLE   $0000, $012E, $0118, $0150, $02D1, $0232, $01B7, $03F1
    DECLE   $0237, $01C8, $03B1, $0227, $01AE, $0254, $0329, $032D
    DECLE   $01BF, $0169, $019A, $0307, $0181, $028D, $0000
_WH.end:  ; 31 decles
;; ------------------------------------------------------------------------ ;;
_WW:
    DECLE   _WW.end - _WW - 1
    DECLE   $0118, $034D, $016C, $00FA, $02C7, $0072, $03CC, $0109
    DECLE   $000B, $01AD, $019E, $016B, $0130, $0278, $01F8, $0314
    DECLE   $017E, $029E, $014D, $016D, $0205, $0147, $02E2, $001A
    DECLE   $010A, $026E, $0004, $0000
_WW.end:  ; 28 decles
;; ------------------------------------------------------------------------ ;;
_XR2:
    DECLE   _XR2.end - _XR2 - 1
    DECLE   $0318, $034C, $016E, $02A6, $03BB, $002F, $0290, $008E
    DECLE   $004B, $0392, $01DA, $024B, $013A, $01DA, $012F, $00B5
    DECLE   $02E5, $0297, $02DC, $0372, $014B, $016D, $0377, $00E7
    DECLE   $0376, $038A, $01CE, $026B, $02FA, $01AA, $011E, $0071
    DECLE   $00D5, $0297, $02BC, $02EA, $01C7, $02D7, $0135, $0155
    DECLE   $01DD, $0007, $0000
_XR2.end:  ; 43 decles
;; ------------------------------------------------------------------------ ;;
_YR:
    DECLE   _YR.end - _YR - 1
    DECLE   $0318, $03CC, $016E, $0197, $00FD, $0130, $0270, $0094
    DECLE   $0328, $0291, $0168, $007E, $01CC, $02F5, $0125, $02B5
    DECLE   $00F4, $0298, $01DA, $03F6, $0153, $0126, $03B9, $00AB
    DECLE   $0293, $03DB, $0175, $01B9, $0001
_YR.end:  ; 29 decles
;; ------------------------------------------------------------------------ ;;
_YY1:
    DECLE   _YY1.end - _YY1 - 1
    DECLE   $0318, $01CC, $016E, $0015, $00CB, $0263, $0320, $0078
    DECLE   $01CE, $0094, $001F, $0040, $0320, $03BF, $0230, $00A7
    DECLE   $000F, $01FE, $03FC, $01E2, $00D0, $0089, $000F, $0248
    DECLE   $032B, $03FD, $01CF, $0001, $0000
_YY1.end:  ; 29 decles
;; ------------------------------------------------------------------------ ;;
_YY2:
    DECLE   _YY2.end - _YY2 - 1
    DECLE   $0318, $01CC, $016E, $0015, $00CB, $0263, $0320, $0078
    DECLE   $01CE, $0094, $001F, $0040, $0320, $03BF, $0230, $00A7
    DECLE   $000F, $01FE, $03FC, $01E2, $00D0, $0089, $000F, $0248
    DECLE   $032B, $03FD, $01CF, $0199, $01EE, $008B, $0161, $0232
    DECLE   $0004, $0318, $01A7, $0198, $0124, $03E0, $0001, $0001
    DECLE   $030F, $0027, $0000
_YY2.end:  ; 43 decles
;; ------------------------------------------------------------------------ ;;
_ZH:
    DECLE   _ZH.end - _ZH - 1
    DECLE   $0310, $014D, $016E, $00C3, $03B9, $01BF, $0241, $0012
    DECLE   $0163, $00E1, $0000, $0080, $0084, $023F, $003F, $0000
_ZH.end:  ; 16 decles
;; ------------------------------------------------------------------------ ;;
_ZZ:
    DECLE   _ZZ.end - _ZZ - 1
    DECLE   $0218, $010D, $016F, $0225, $0351, $00B5, $02A0, $02EE
    DECLE   $00E9, $014D, $002C, $0360, $0008, $00EC, $004C, $0342
    DECLE   $03D4, $0156, $0052, $0131, $0008, $03B0, $01BE, $0172
    DECLE   $0000
_ZZ.end:  ; 25 decles

;;==========================================================================;;
;;                                                                          ;;
;;  Copyright information:                                                  ;;
;;                                                                          ;;
;;  The above allophone data was extracted from the SP0256-AL2 ROM image.   ;;
;;  The SP0256-AL2 allophones are NOT in the public domain, nor are they    ;;
;;  placed under the GNU General Public License.  This program is           ;;
;;  distributed in the hope that it will be useful, but WITHOUT ANY         ;;
;;  WARRANTY; without even the implied warranty of MERCHANTABILITY or       ;;
;;  FITNESS FOR A PARTICULAR PURPOSE.                                       ;;
;;                                                                          ;;
;;  Microchip, Inc. retains the copyright to the data and algorithms        ;;
;;  contained in the SP0256-AL2.  This speech data is distributed with      ;;
;;  explicit permission from Microchip, Inc.  All such redistributions      ;;
;;  must retain this notice of copyright.                                   ;;
;;                                                                          ;;
;;  No copyright claims are made on this data by the author(s) of SDK1600.  ;;
;;  Please see http://spatula-city.org/~im14u2c/sp0256-al2/ for details.    ;;
;;                                                                          ;;
;;==========================================================================;;

;* ======================================================================== *;
;*  These routines are placed into the public domain by their author.  All  *;
;*  copyright rights are hereby relinquished on the routines and data in    *;
;*  this file.  -- Joseph Zbiciak, 2008                                     *;
;* ======================================================================== *;

;; ======================================================================== ;;
;;  INTELLIVOICE DRIVER ROUTINES                                            ;;
;;  Written in 2002 by Joe Zbiciak <intvnut AT gmail.com>                   ;;
;;  http://spatula-city.org/~im14u2c/intv/                                  ;;
;; ======================================================================== ;;

;; ======================================================================== ;;
;;  GLOBAL VARIABLES USED BY THESE ROUTINES                                 ;;
;;                                                                          ;;
;;  Note that some of these routines may use one or more global variables.  ;;
;;  If you use these routines, you will need to allocate the appropriate    ;;
;;  space in either 16-bit or 8-bit memory as appropriate.  Each global     ;;
;;  variable is listed with the routines which use it and the required      ;;
;;  memory width.                                                           ;;
;;                                                                          ;;
;;  Example declarations for these routines are shown below, commented out. ;;
;;  You should uncomment these and add them to your program to make use of  ;;
;;  the routine that needs them.  Make sure to assign these variables to    ;;
;;  locations that aren't used for anything else.                           ;;
;; ======================================================================== ;;

                        ; Used by       Req'd Width     Description
                        ;-----------------------------------------------------
;IV.QH      EQU $110    ; IV_xxx        8-bit           Voice queue head
;IV.QT      EQU $111    ; IV_xxx        8-bit           Voice queue tail
;IV.Q       EQU $112    ; IV_xxx        8-bit           Voice queue  (8 bytes)
;IV.FLEN    EQU $11A    ; IV_xxx        8-bit           Length of FIFO data
;IV.FPTR    EQU $320    ; IV_xxx        16-bit          Current FIFO ptr.
;IV.PPTR    EQU $321    ; IV_xxx        16-bit          Current Phrase ptr.

;; ======================================================================== ;;
;;  MEMORY USAGE                                                            ;;
;;                                                                          ;;
;;  These routines implement a queue of "pending phrases" that will be      ;;
;;  played by the Intellivoice.  The user calls IV_PLAY to enqueue a        ;;
;;  phrase number.  Phrase numbers indicate either a RESROM sample or       ;;
;;  a compiled in phrase to be spoken.                                      ;;
;;                                                                          ;;
;;  The user must compose an "IV_PHRASE_TBL", which is composed of          ;;
;;  pointers to phrases to be spoken.  Phrases are strings of pointers      ;;
;;  and RESROM triggers, terminated by a NUL.                               ;;
;;                                                                          ;;
;;  Phrase numbers 1 through 42 are RESROM samples.  Phrase numbers         ;;
;;  43 through 255 index into the IV_PHRASE_TBL.                            ;;
;;                                                                          ;;
;;  SPECIAL NOTES                                                           ;;
;;                                                                          ;;
;;  Bit 7 of IV.QH and IV.QT is used to denote whether the Intellivoice     ;;
;;  is present.  If Intellivoice is present, this bit is clear.             ;;
;;                                                                          ;;
;;  Bit 6 of IV.QT is used to denote that we still need to do an ALD $00    ;;
;;  for FIFO'd voice data.                                                  ;;
;; ======================================================================== ;;
            

;; ======================================================================== ;;
;;  NAME                                                                    ;;
;;      IV_INIT     Initialize the Intellivoice                             ;;
;;                                                                          ;;
;;  AUTHOR                                                                  ;;
;;      Joseph Zbiciak <intvnut AT gmail.com>                               ;;
;;                                                                          ;;
;;  REVISION HISTORY                                                        ;;
;;      15-Sep-2002 Initial revision . . . . . . . . . . .  J. Zbiciak      ;;
;;                                                                          ;;
;;  INPUTS for IV_INIT                                                      ;;
;;      R5      Return address                                              ;;
;;                                                                          ;;
;;  OUTPUTS                                                                 ;;
;;      R0      0 if Intellivoice found, -1 if not.                         ;;
;;                                                                          ;;
;;  DESCRIPTION                                                             ;;
;;      Resets Intellivoice, determines if it is actually there, and        ;;
;;      then initializes the IV structure.                                  ;;
;; ------------------------------------------------------------------------ ;;
;;                   Copyright (c) 2002, Joseph Zbiciak                     ;;
;; ======================================================================== ;;

IV_INIT     PROC
            MVII    #$0400, R0          ;
            MVO     R0,     $0081       ; Reset the Intellivoice

            MVI     $0081,  R0          ; \
            RLC     R0,     2           ;  |-- See if we detect Intellivoice
            BOV     @@no_ivoice         ; /    once we've reset it.

            CLRR    R0                  ; 
            MVO     R0,     IV.FPTR     ; No data for FIFO
            MVO     R0,     IV.PPTR     ; No phrase being spoken
            MVO     R0,     IV.QH       ; Clear our queue
            MVO     R0,     IV.QT       ; Clear our queue
            JR      R5                  ; Done!

@@no_ivoice:
            CLRR    R0
            MVO     R0,     IV.FPTR     ; No data for FIFO
            MVO     R0,     IV.PPTR     ; No phrase being spoken
            DECR    R0
            MVO     R0,     IV.QH       ; Set queue to -1 ("No Intellivoice")
            MVO     R0,     IV.QT       ; Set queue to -1 ("No Intellivoice")
            JR      R5                  ; Done!
            ENDP

;; ======================================================================== ;;
;;  NAME                                                                    ;;
;;      IV_ISR      Interrupt service routine to feed Intellivoice          ;;
;;                                                                          ;;
;;  AUTHOR                                                                  ;;
;;      Joseph Zbiciak <intvnut AT gmail.com>                               ;;
;;                                                                          ;;
;;  REVISION HISTORY                                                        ;;
;;      15-Sep-2002 Initial revision . . . . . . . . . . .  J. Zbiciak      ;;
;;                                                                          ;;
;;  INPUTS for IV_ISR                                                       ;;
;;      R5      Return address                                              ;;
;;                                                                          ;;
;;  OUTPUTS                                                                 ;;
;;      R0, R1, R4 trashed.                                                 ;;
;;                                                                          ;;
;;  NOTES                                                                   ;;
;;      Call this from your main interrupt service routine.                 ;;
;; ------------------------------------------------------------------------ ;;
;;                   Copyright (c) 2002, Joseph Zbiciak                     ;;
;; ======================================================================== ;;
IV_ISR      PROC
            ;; ------------------------------------------------------------ ;;
            ;;  Check for Intellivoice.  Leave if none present.             ;;
            ;; ------------------------------------------------------------ ;;
            MVI     IV.QT,  R1          ; Get queue tail
            SWAP    R1,     2
            BPL     @@ok                ; Bit 7 set? If yes: No Intellivoice
@@ald_busy:
@@leave     JR      R5                  ; Exit if no Intellivoice.

     
            ;; ------------------------------------------------------------ ;;
            ;;  Check to see if we pump samples into the FIFO.
            ;; ------------------------------------------------------------ ;;
@@ok:       MVI     IV.FPTR, R4         ; Get FIFO data pointer
            TSTR    R4                  ; is it zero?
            BEQ     @@no_fifodata       ; Yes:  No data for FIFO.
@@fifo_fill:
            MVI     $0081,  R0          ; Read speech FIFO ready bit
            SLLC    R0,     1           ; 
            BC      @@fifo_busy     

            MVI@    R4,     R0          ; Get next word
            MVO     R0,     $0081       ; write it to the FIFO

            MVI     IV.FLEN, R0         ;\
            DECR    R0                  ; |-- Decrement our FIFO'd data length
            MVO     R0,     IV.FLEN     ;/
            BEQ     @@last_fifo         ; If zero, we're done w/ FIFO
            MVO     R4,     IV.FPTR     ; Otherwise, save new pointer
            B       @@fifo_fill         ; ...and keep trying to load FIFO

@@last_fifo MVO     R0,     IV.FPTR     ; done with FIFO loading.
                                        ; fall into ALD processing.


            ;; ------------------------------------------------------------ ;;
            ;;  Try to do an Address Load.  We do this in two settings:     ;;
            ;;   -- We have no FIFO data to load.                           ;;
            ;;   -- We've loaded as much FIFO data as we can, but we        ;;
            ;;      might have an address load command to send for it.      ;;
            ;; ------------------------------------------------------------ ;;
@@fifo_busy:
@@no_fifodata:
            MVI     $0080,  R0          ; Read LRQ bit from ALD register
            SLLC    R0,     1
            BNC     @@ald_busy          ; LRQ is low, meaning we can't ALD.
                                        ; So, leave.

            ;; ------------------------------------------------------------ ;;
            ;;  We can do an address load (ALD) on the SP0256.  Give FIFO   ;;
            ;;  driven ALDs priority, since we already started the FIFO     ;;
            ;;  load.  The "need ALD" bit is stored in bit 6 of IV.QT.      ;;
            ;; ------------------------------------------------------------ ;;
            ANDI    #$40,   R1          ; Is "Need FIFO ALD" bit set?
            BEQ     @@no_fifo_ald
            XOR     IV.QT,  R1          ;\__ Clear the "Need FIFO ALD" bit.
            MVO     R1,     IV.QT       ;/
            CLRR    R1
            MVO     R1,     $80         ; Load a 0 into ALD (trigger FIFO rd.)
            JR      R5                  ; done!

            ;; ------------------------------------------------------------ ;;
            ;;  We don't need to ALD on behalf of the FIFO.  So, we grab    ;;
            ;;  the next thing off our phrase list.                         ;;
            ;; ------------------------------------------------------------ ;;
@@no_fifo_ald:
            MVI     IV.PPTR, R4         ; Get phrase pointer.
            TSTR    R4                  ; Is it zero?
            BEQ     @@next_phrase       ; Yes:  Get next phrase from queue.

            MVI@    R4,     R0
            TSTR    R0                  ; Is it end of phrase?
            BNEQ    @@process_phrase    ; !=0:  Go do it.

            MVO     R0,     IV.PPTR     ; 
@@next_phrase:
            MVI     IV.QT,  R1          ; reload queue tail (was trashed above)
            MOVR    R1,     R0          ; copy QT to R0 so we can increment it
            ANDI    #$7,    R1          ; Mask away flags in queue head
            CMP     IV.QH,  R1          ; Is it same as queue tail?
            BEQ     @@leave             ; Yes:  No more speech for now.

            INCR    R0
            ANDI    #$F7,   R0          ; mask away the possible 'carry'
            MVO     R0,     IV.QT       ; save updated queue tail

            ADDI    #IV.Q,  R1          ; Index into queue
            MVI@    R1,     R4          ; get next value from queue
            CMPI    #43,    R4          ; Is it a RESROM or Phrase?
            BNC     @@play_resrom_r4
@@new_phrase:
;            ADDI    #IV_PHRASE_TBL - 43, R4 ; Index into phrase table
;            MVI@    R4,     R4          ; Read from phrase table
            MVO     R4,     IV.PPTR
            JR      R5                  ; we'll get to this phrase next time.

@@play_resrom_r4:
            MVO     R4,     $0080       ; Just ALD it
            JR      R5                  ; and leave.

            ;; ------------------------------------------------------------ ;;
            ;;  We're in the middle of a phrase, so continue interpreting.  ;;
            ;; ------------------------------------------------------------ ;;
@@process_phrase:
            
            MVO     R4,     IV.PPTR     ; save new phrase pointer
            CMPI    #43,    R0          ; Is it a RESROM cue?
            BC      @@play_fifo         ; Just ALD it and leave.
@@play_resrom_r0
            MVO     R0,     $0080       ; Just ALD it
            JR      R5                  ; and leave.
@@play_fifo:
            MVI     IV.FPTR,R1          ; Make sure not to stomp existing FIFO
            TSTR    R1                  ; data.
            BEQ     @@new_fifo_ok
            DECR    R4                  ; Oops, FIFO data still playing,
            MVO     R4,     IV.PPTR     ; so rewind.
            JR      R5                  ; and leave.

@@new_fifo_ok:
            MOVR    R0,     R4          ;
            MVI@    R4,     R0          ; Get chunk length
            MVO     R0,     IV.FLEN     ; Init FIFO chunk length
            MVO     R4,     IV.FPTR     ; Init FIFO pointer
            MVI     IV.QT,  R0          ;\
            XORI    #$40,   R0          ; |- Set "Need ALD" bit in QT
            MVO     R0,     IV.QT       ;/

  IF 1      ; debug code                ;\
            ANDI    #$40,   R0          ; |   Debug code:  We should only
            BNEQ    @@qtok              ; |-- be here if "Need FIFO ALD" 
            HLT     ;BUG!!              ; |   was already clear.         
@@qtok                                  ;/    
  ENDI
            JR      R5                  ; leave.

            ENDP


;; ======================================================================== ;;
;;  NAME                                                                    ;;
;;      IV_PLAY     Play a voice sample sequence.                           ;;
;;                                                                          ;;
;;  AUTHOR                                                                  ;;
;;      Joseph Zbiciak <intvnut AT gmail.com>                               ;;
;;                                                                          ;;
;;  REVISION HISTORY                                                        ;;
;;      15-Sep-2002 Initial revision . . . . . . . . . . .  J. Zbiciak      ;;
;;                                                                          ;;
;;  INPUTS for IV_PLAY                                                      ;;
;;      R5      Invocation record, followed by return address.              ;;
;;                  1 DECLE    Phrase number to play.                       ;;
;;                                                                          ;;
;;  INPUTS for IV_PLAY.1                                                    ;;
;;      R0      Address of phrase to play.                                  ;;
;;      R5      Return address                                              ;;
;;                                                                          ;;
;;  OUTPUTS                                                                 ;;
;;      R0, R1  trashed                                                     ;;
;;      Z==0    if item not successfully queued.                            ;;
;;      Z==1    if successfully queued.                                     ;;
;;                                                                          ;;
;;  NOTES                                                                   ;;
;;      This code will drop phrases if the queue is full.                   ;;
;;      Phrase numbers 1..42 are RESROM samples.  43..255 will index        ;;
;;      into the user-supplied IV_PHRASE_TBL.  43 will refer to the         ;;
;;      first entry, 44 to the second, and so on.  Phrase 0 is undefined.   ;;
;;                                                                          ;;
;; ------------------------------------------------------------------------ ;;
;;                   Copyright (c) 2002, Joseph Zbiciak                     ;;
;; ======================================================================== ;;
IV_PLAY     PROC
            MVI@    R5,     R0

@@1:        ; alternate entry point
            MVI     IV.QT,  R1          ; Get queue tail
            SWAP    R1,     2           ;\___ Leave if "no Intellivoice"
            BMI     @@leave             ;/    bit it set.
@@ok:       
            DECR    R1                  ;\
            ANDI    #$7,    R1          ; |-- See if we still have room
            CMP     IV.QH,  R1          ;/
            BEQ     @@leave             ; Leave if we're full

@@2:        MVI     IV.QH,  R1          ; Get our queue head pointer
            PSHR    R1                  ;\
            INCR    R1                  ; |
            ANDI    #$F7,   R1          ; |-- Increment it, removing
            MVO     R1,     IV.QH       ; |   carry but preserving flags.
            PULR    R1                  ;/

            ADDI    #IV.Q,  R1          ;\__ Store phrase to queue
            MVO@    R0,     R1          ;/

@@leave:    JR      R5                  ; Leave.
            ENDP

;; ======================================================================== ;;
;;  NAME                                                                    ;;
;;      IV_PLAYW    Play a voice sample sequence.  Wait for queue room.     ;;
;;                                                                          ;;
;;  AUTHOR                                                                  ;;
;;      Joseph Zbiciak <intvnut AT gmail.com>                               ;;
;;                                                                          ;;
;;  REVISION HISTORY                                                        ;;
;;      15-Sep-2002 Initial revision . . . . . . . . . . .  J. Zbiciak      ;;
;;                                                                          ;;
;;  INPUTS for IV_PLAY                                                      ;;
;;      R5      Invocation record, followed by return address.              ;;
;;                  1 DECLE    Phrase number to play.                       ;;
;;                                                                          ;;
;;  INPUTS for IV_PLAY.1                                                    ;;
;;      R0      Address of phrase to play.                                  ;;
;;      R5      Return address                                              ;;
;;                                                                          ;;
;;  OUTPUTS                                                                 ;;
;;      R0, R1  trashed                                                     ;;
;;                                                                          ;;
;;  NOTES                                                                   ;;
;;      This code will wait for a queue slot to open if queue is full.      ;;
;;      Phrase numbers 1..42 are RESROM samples.  43..255 will index        ;;
;;      into the user-supplied IV_PHRASE_TBL.  43 will refer to the         ;;
;;      first entry, 44 to the second, and so on.  Phrase 0 is undefined.   ;;
;;                                                                          ;;
;; ------------------------------------------------------------------------ ;;
;;                   Copyright (c) 2002, Joseph Zbiciak                     ;;
;; ======================================================================== ;;
IV_PLAYW    PROC
            MVI@    R5,     R0

@@1:        ; alternate entry point
            MVI     IV.QT,  R1          ; Get queue tail
            SWAP    R1,     2           ;\___ Leave if "no Intellivoice"
            BMI     IV_PLAY.leave       ;/    bit it set.
@@ok:       
            DECR    R1                  ;\
            ANDI    #$7,    R1          ; |-- See if we still have room
            CMP     IV.QH,  R1          ;/
            BEQ     @@1                 ; wait for room
            B       IV_PLAY.2

            ENDP

;; ======================================================================== ;;
;;  NAME                                                                    ;;
;;      IV_WAIT     Wait for voice queue to empty.                          ;;
;;                                                                          ;;
;;  AUTHOR                                                                  ;;
;;      Joseph Zbiciak <intvnut AT gmail.com>                               ;;
;;                                                                          ;;
;;  REVISION HISTORY                                                        ;;
;;      15-Sep-2002 Initial revision . . . . . . . . . . .  J. Zbiciak      ;;
;;                                                                          ;;
;;  INPUTS for IV_WAIT                                                      ;;
;;      R5      Return address                                              ;;
;;                                                                          ;;
;;  OUTPUTS                                                                 ;;
;;      R0      trashed.                                                    ;;
;;                                                                          ;;
;;  NOTES                                                                   ;;
;;      This waits until the Intellivoice is nearly completely quiescent.   ;;
;;      Some voice data may still be spoken from the last triggered         ;;
;;      phrase.  To truly wait for *that* to be spoken, speak a 'pause'     ;;
;;      (eg. RESROM.pa1) and then call IV_WAIT.                             ;;
;; ------------------------------------------------------------------------ ;;
;;                   Copyright (c) 2002, Joseph Zbiciak                     ;;
;; ======================================================================== ;;
IV_WAIT     PROC
            MVI     IV.QH,  R0
            SWAP    R0                  ;\___ test bit 7, leave if set.
            SWAP    R0                  ;/    (SWAP2 corrupts upper byte.)
            BMI     @@leave

            ; Wait for queue to drain.
@@q_loop:   CMP     IV.QT,  R0
            BNEQ    @@q_loop

            ; Wait for FIFO and LRQ to say ready.
@@s_loop:   MVI     $81,    R0          ; Read FIFO status.  0 == ready.
            COMR    R0
            AND     $80,    R0          ; Merge w/ ALD status.  1 == ready
            TSTR    R0
            BPL     @@s_loop            ; if bit 15 == 0, not ready.
            
@@leave:    JR      R5
            ENDP

;; ======================================================================== ;;
;;  End of File:  ivoice.asm                                                ;;
;; ======================================================================== ;;

;* ======================================================================== *;
;*  These routines are placed into the public domain by their author.  All  *;
;*  copyright rights are hereby relinquished on the routines and data in    *;
;*  this file.  -- Joseph Zbiciak, 2008                                     *;
;* ======================================================================== *;

;; ======================================================================== ;;
;;  NAME                                                                    ;;
;;      IV_SAYNUM16 Say a 16-bit unsigned number using RESROM digits        ;;
;;                                                                          ;;
;;  AUTHOR                                                                  ;;
;;      Joseph Zbiciak <intvnut AT gmail.com>                               ;;
;;                                                                          ;;
;;  REVISION HISTORY                                                        ;;
;;      16-Sep-2002 Initial revision . . . . . . . . . . .  J. Zbiciak      ;;
;;                                                                          ;;
;;  INPUTS for IV_INIT                                                      ;;
;;      R0      Number to "speak"                                           ;;
;;      R5      Return address                                              ;;
;;                                                                          ;;
;;  OUTPUTS                                                                 ;;
;;                                                                          ;;
;;  DESCRIPTION                                                             ;;
;;      "Says" a 16-bit number using IV_PLAYW to queue up the phrase.       ;;
;;      Because the number may be built from several segments, it could     ;;
;;      easily eat up the queue.  I believe the longest number will take    ;;
;;      7 queue entries -- that is, fill the queue.  Thus, this code        ;;
;;      could block, waiting for slots in the queue.                        ;;
;; ======================================================================== ;;

IV_SAYNUM16 PROC
            PSHR    R5

            TSTR    R0
            BEQ     @@zero          ; Special case:  Just say "zero"

            ;; ------------------------------------------------------------ ;;
            ;;  First, try to pull off 'thousands'.  We call ourselves      ;;
            ;;  recursively to play the the number of thousands.            ;;
            ;; ------------------------------------------------------------ ;;
            CLRR    R1
@@thloop:   INCR    R1
            SUBI    #1000,  R0
            BC      @@thloop

            ADDI    #1000,  R0
            PSHR    R0
            DECR    R1
            BEQ     @@no_thousand

            CALL    IV_SAYNUM16.recurse

            CALL    IV_PLAYW
            DECLE   36  ; THOUSAND
            
@@no_thousand
            PULR    R1

            ;; ------------------------------------------------------------ ;;
            ;;  Now try to play hundreds.                                   ;;
            ;; ------------------------------------------------------------ ;;
            MVII    #7-1, R0    ; ZERO
            CMPI    #100,   R1
            BNC     @@no_hundred

@@hloop:    INCR    R0
            SUBI    #100,   R1
            BC      @@hloop
            ADDI    #100,   R1

            PSHR    R1

            CALL    IV_PLAYW.1

            CALL    IV_PLAYW
            DECLE   35  ; HUNDRED

            PULR    R1
            B       @@notrecurse    ; skip "PSHR R5"
@@recurse:  PSHR    R5              ; recursive entry point for 'thousand'

@@no_hundred:
@@notrecurse:
            MOVR    R1,     R0
            BEQ     @@leave

            SUBI    #20,    R1
            BNC     @@teens

            MVII    #27-1, R0   ; TWENTY
@@tyloop    INCR    R0
            SUBI    #10,    R1
            BC      @@tyloop
            ADDI    #10,    R1

            PSHR    R1
            CALL    IV_PLAYW.1

            PULR    R0
            TSTR    R0
            BEQ     @@leave

@@teens:
@@zero:     ADDI    #7, R0  ; ZERO

            CALL    IV_PLAYW.1

@@leave     PULR    PC
            ENDP

;; ======================================================================== ;;
;;  End of File:  saynum16.asm                                              ;;
;; ======================================================================== ;;

    ENDI

        IF DEFINED intybasic_flash

;; ======================================================================== ;;
;;  JLP "Save Game" support                                                 ;;
;; ======================================================================== ;;
JF.first    EQU     $8023
JF.last     EQU     $8024
JF.addr     EQU     $8025
JF.row      EQU     $8026
                   
JF.wrcmd    EQU     $802D
JF.rdcmd    EQU     $802E
JF.ercmd    EQU     $802F
JF.wrkey    EQU     $C0DE
JF.rdkey    EQU     $DEC0
JF.erkey    EQU     $BEEF

JF.write:   DECLE   JF.wrcmd,   JF.wrkey    ; Copy JLP RAM to flash row  
JF.read:    DECLE   JF.rdcmd,   JF.rdkey    ; Copy flash row to JLP RAM  
JF.erase:   DECLE   JF.ercmd,   JF.erkey    ; Erase flash sector 

;; ======================================================================== ;;
;;  JF.INIT         Copy JLP save-game support routine to System RAM        ;;
;; ======================================================================== ;;
JF.INIT     PROC
            PSHR    R5            
            MVII    #@@__code,  R5
            MVII    #JF.SYSRAM, R4
            REPEAT  5       
            MVI@    R5,         R0      ; \_ Copy code fragment to System RAM
            MVO@    R0,         R4      ; /
            ENDR
            PULR    PC

            ;; === start of code that will run from RAM
@@__code:   MVO@    R0,         R1      ; JF.SYSRAM + 0: initiate command
            ADD@    R1,         PC      ; JF.SYSRAM + 1: Wait for JLP to return
            JR      R5                  ; JF.SYSRAM + 2:
            MVO@    R2,         R2      ; JF.SYSRAM + 3: \__ simple ISR
            JR      R5                  ; JF.SYSRAM + 4: /
            ;; === end of code that will run from RAM
            ENDP

;; ======================================================================== ;;
;;  JF.CMD          Issue a JLP Flash command                               ;;
;;                                                                          ;;
;;  INPUT                                                                   ;;
;;      R0  Slot number to operate on                                       ;;
;;      R1  Address to copy to/from in JLP RAM                              ;;
;;      @R5 Command to invoke:                                              ;;
;;                                                                          ;;
;;              JF.write -- Copy JLP RAM to Flash                           ;;
;;              JF.read  -- Copy Flash to JLP RAM                           ;;
;;              JF.erase -- Erase flash sector                              ;;
;;                                                                          ;;
;;  OUTPUT                                                                  ;;
;;      R0 - R4 not modified.  (Saved and restored across call)             ;;
;;      JLP command executed                                                ;;
;;                                                                          ;;
;;  NOTES                                                                   ;;
;;      This code requires two short routines in the console's System RAM.  ;;
;;      It also requires that the system stack reside in System RAM.        ;;
;;      Because an interrupt may occur during the code's execution, there   ;;
;;      must be sufficient stack space to service the interrupt (8 words).  ;;
;;                                                                          ;;
;;      The code also relies on the fact that the EXEC ISR dispatch does    ;;
;;      not modify R2.  This allows us to initialize R2 for the ISR ahead   ;;
;;      of time, rather than in the ISR.                                    ;;
;; ======================================================================== ;;
JF.CMD      PROC

            MVO     R4,         JF.SV.R4    ; \
            MVII    #JF.SV.R0,  R4          ;  |
            MVO@    R0,         R4          ;  |- Save registers, but not on
            MVO@    R1,         R4          ;  |  the stack.  (limit stack use)
            MVO@    R2,         R4          ; /

            MVI@    R5,         R4          ; Get command to invoke

            MVO     R5,         JF.SV.R5    ; save return address

            DIS
            MVO     R1,         JF.addr     ; \_ Save SG arguments in JLP
            MVO     R0,         JF.row      ; /
                                          
            MVI@    R4,         R1          ; Get command address
            MVI@    R4,         R0          ; Get unlock word
                                          
            MVII    #$100,      R4          ; \
            SDBD                            ;  |_ Save old ISR in save area
            MVI@    R4,         R2          ;  |
            MVO     R2,         JF.SV.ISR   ; /
                                          
            MVII    #JF.SYSRAM + 3, R2      ; \
            MVO     R2,         $100        ;  |_ Set up new ISR in RAM
            SWAP    R2                      ;  |
            MVO     R2,         $101        ; / 
                                          
            MVII    #$20,       R2          ; Address of STIC handshake
            JSRE    R5,  JF.SYSRAM          ; Invoke the command
                                          
            MVI     JF.SV.ISR,  R2          ; \
            MVO     R2,         $100        ;  |_ Restore old ISR 
            SWAP    R2                      ;  |
            MVO     R2,         $101        ; /
                                          
            MVII    #JF.SV.R0,  R5          ; \
            MVI@    R5,         R0          ;  |
            MVI@    R5,         R1          ;  |- Restore registers
            MVI@    R5,         R2          ;  |
            MVI@    R5,         R4          ; /
            MVI@    R5,         PC          ; Return

            ENDP


        ENDI

	IF DEFINED intybasic_fastmult

; Quarter Square Multiplication
; Assembly code by Joe Zbiciak, 2015
; Released to public domain.

QSQR8_TBL:  PROC
            DECLE   $3F80, $3F01, $3E82, $3E04, $3D86, $3D09, $3C8C, $3C10
            DECLE   $3B94, $3B19, $3A9E, $3A24, $39AA, $3931, $38B8, $3840
            DECLE   $37C8, $3751, $36DA, $3664, $35EE, $3579, $3504, $3490
            DECLE   $341C, $33A9, $3336, $32C4, $3252, $31E1, $3170, $3100
            DECLE   $3090, $3021, $2FB2, $2F44, $2ED6, $2E69, $2DFC, $2D90
            DECLE   $2D24, $2CB9, $2C4E, $2BE4, $2B7A, $2B11, $2AA8, $2A40
            DECLE   $29D8, $2971, $290A, $28A4, $283E, $27D9, $2774, $2710
            DECLE   $26AC, $2649, $25E6, $2584, $2522, $24C1, $2460, $2400
            DECLE   $23A0, $2341, $22E2, $2284, $2226, $21C9, $216C, $2110
            DECLE   $20B4, $2059, $1FFE, $1FA4, $1F4A, $1EF1, $1E98, $1E40
            DECLE   $1DE8, $1D91, $1D3A, $1CE4, $1C8E, $1C39, $1BE4, $1B90
            DECLE   $1B3C, $1AE9, $1A96, $1A44, $19F2, $19A1, $1950, $1900
            DECLE   $18B0, $1861, $1812, $17C4, $1776, $1729, $16DC, $1690
            DECLE   $1644, $15F9, $15AE, $1564, $151A, $14D1, $1488, $1440
            DECLE   $13F8, $13B1, $136A, $1324, $12DE, $1299, $1254, $1210
            DECLE   $11CC, $1189, $1146, $1104, $10C2, $1081, $1040, $1000
            DECLE   $0FC0, $0F81, $0F42, $0F04, $0EC6, $0E89, $0E4C, $0E10
            DECLE   $0DD4, $0D99, $0D5E, $0D24, $0CEA, $0CB1, $0C78, $0C40
            DECLE   $0C08, $0BD1, $0B9A, $0B64, $0B2E, $0AF9, $0AC4, $0A90
            DECLE   $0A5C, $0A29, $09F6, $09C4, $0992, $0961, $0930, $0900
            DECLE   $08D0, $08A1, $0872, $0844, $0816, $07E9, $07BC, $0790
            DECLE   $0764, $0739, $070E, $06E4, $06BA, $0691, $0668, $0640
            DECLE   $0618, $05F1, $05CA, $05A4, $057E, $0559, $0534, $0510
            DECLE   $04EC, $04C9, $04A6, $0484, $0462, $0441, $0420, $0400
            DECLE   $03E0, $03C1, $03A2, $0384, $0366, $0349, $032C, $0310
            DECLE   $02F4, $02D9, $02BE, $02A4, $028A, $0271, $0258, $0240
            DECLE   $0228, $0211, $01FA, $01E4, $01CE, $01B9, $01A4, $0190
            DECLE   $017C, $0169, $0156, $0144, $0132, $0121, $0110, $0100
            DECLE   $00F0, $00E1, $00D2, $00C4, $00B6, $00A9, $009C, $0090
            DECLE   $0084, $0079, $006E, $0064, $005A, $0051, $0048, $0040
            DECLE   $0038, $0031, $002A, $0024, $001E, $0019, $0014, $0010
            DECLE   $000C, $0009, $0006, $0004, $0002, $0001, $0000
@@mid:
            DECLE   $0000, $0000, $0001, $0002, $0004, $0006, $0009, $000C
            DECLE   $0010, $0014, $0019, $001E, $0024, $002A, $0031, $0038
            DECLE   $0040, $0048, $0051, $005A, $0064, $006E, $0079, $0084
            DECLE   $0090, $009C, $00A9, $00B6, $00C4, $00D2, $00E1, $00F0
            DECLE   $0100, $0110, $0121, $0132, $0144, $0156, $0169, $017C
            DECLE   $0190, $01A4, $01B9, $01CE, $01E4, $01FA, $0211, $0228
            DECLE   $0240, $0258, $0271, $028A, $02A4, $02BE, $02D9, $02F4
            DECLE   $0310, $032C, $0349, $0366, $0384, $03A2, $03C1, $03E0
            DECLE   $0400, $0420, $0441, $0462, $0484, $04A6, $04C9, $04EC
            DECLE   $0510, $0534, $0559, $057E, $05A4, $05CA, $05F1, $0618
            DECLE   $0640, $0668, $0691, $06BA, $06E4, $070E, $0739, $0764
            DECLE   $0790, $07BC, $07E9, $0816, $0844, $0872, $08A1, $08D0
            DECLE   $0900, $0930, $0961, $0992, $09C4, $09F6, $0A29, $0A5C
            DECLE   $0A90, $0AC4, $0AF9, $0B2E, $0B64, $0B9A, $0BD1, $0C08
            DECLE   $0C40, $0C78, $0CB1, $0CEA, $0D24, $0D5E, $0D99, $0DD4
            DECLE   $0E10, $0E4C, $0E89, $0EC6, $0F04, $0F42, $0F81, $0FC0
            DECLE   $1000, $1040, $1081, $10C2, $1104, $1146, $1189, $11CC
            DECLE   $1210, $1254, $1299, $12DE, $1324, $136A, $13B1, $13F8
            DECLE   $1440, $1488, $14D1, $151A, $1564, $15AE, $15F9, $1644
            DECLE   $1690, $16DC, $1729, $1776, $17C4, $1812, $1861, $18B0
            DECLE   $1900, $1950, $19A1, $19F2, $1A44, $1A96, $1AE9, $1B3C
            DECLE   $1B90, $1BE4, $1C39, $1C8E, $1CE4, $1D3A, $1D91, $1DE8
            DECLE   $1E40, $1E98, $1EF1, $1F4A, $1FA4, $1FFE, $2059, $20B4
            DECLE   $2110, $216C, $21C9, $2226, $2284, $22E2, $2341, $23A0
            DECLE   $2400, $2460, $24C1, $2522, $2584, $25E6, $2649, $26AC
            DECLE   $2710, $2774, $27D9, $283E, $28A4, $290A, $2971, $29D8
            DECLE   $2A40, $2AA8, $2B11, $2B7A, $2BE4, $2C4E, $2CB9, $2D24
            DECLE   $2D90, $2DFC, $2E69, $2ED6, $2F44, $2FB2, $3021, $3090
            DECLE   $3100, $3170, $31E1, $3252, $32C4, $3336, $33A9, $341C
            DECLE   $3490, $3504, $3579, $35EE, $3664, $36DA, $3751, $37C8
            DECLE   $3840, $38B8, $3931, $39AA, $3A24, $3A9E, $3B19, $3B94
            DECLE   $3C10, $3C8C, $3D09, $3D86, $3E04, $3E82, $3F01, $3F80
            DECLE   $4000, $4080, $4101, $4182, $4204, $4286, $4309, $438C
            DECLE   $4410, $4494, $4519, $459E, $4624, $46AA, $4731, $47B8
            DECLE   $4840, $48C8, $4951, $49DA, $4A64, $4AEE, $4B79, $4C04
            DECLE   $4C90, $4D1C, $4DA9, $4E36, $4EC4, $4F52, $4FE1, $5070
            DECLE   $5100, $5190, $5221, $52B2, $5344, $53D6, $5469, $54FC
            DECLE   $5590, $5624, $56B9, $574E, $57E4, $587A, $5911, $59A8
            DECLE   $5A40, $5AD8, $5B71, $5C0A, $5CA4, $5D3E, $5DD9, $5E74
            DECLE   $5F10, $5FAC, $6049, $60E6, $6184, $6222, $62C1, $6360
            DECLE   $6400, $64A0, $6541, $65E2, $6684, $6726, $67C9, $686C
            DECLE   $6910, $69B4, $6A59, $6AFE, $6BA4, $6C4A, $6CF1, $6D98
            DECLE   $6E40, $6EE8, $6F91, $703A, $70E4, $718E, $7239, $72E4
            DECLE   $7390, $743C, $74E9, $7596, $7644, $76F2, $77A1, $7850
            DECLE   $7900, $79B0, $7A61, $7B12, $7BC4, $7C76, $7D29, $7DDC
            DECLE   $7E90, $7F44, $7FF9, $80AE, $8164, $821A, $82D1, $8388
            DECLE   $8440, $84F8, $85B1, $866A, $8724, $87DE, $8899, $8954
            DECLE   $8A10, $8ACC, $8B89, $8C46, $8D04, $8DC2, $8E81, $8F40
            DECLE   $9000, $90C0, $9181, $9242, $9304, $93C6, $9489, $954C
            DECLE   $9610, $96D4, $9799, $985E, $9924, $99EA, $9AB1, $9B78
            DECLE   $9C40, $9D08, $9DD1, $9E9A, $9F64, $A02E, $A0F9, $A1C4
            DECLE   $A290, $A35C, $A429, $A4F6, $A5C4, $A692, $A761, $A830
            DECLE   $A900, $A9D0, $AAA1, $AB72, $AC44, $AD16, $ADE9, $AEBC
            DECLE   $AF90, $B064, $B139, $B20E, $B2E4, $B3BA, $B491, $B568
            DECLE   $B640, $B718, $B7F1, $B8CA, $B9A4, $BA7E, $BB59, $BC34
            DECLE   $BD10, $BDEC, $BEC9, $BFA6, $C084, $C162, $C241, $C320
            DECLE   $C400, $C4E0, $C5C1, $C6A2, $C784, $C866, $C949, $CA2C
            DECLE   $CB10, $CBF4, $CCD9, $CDBE, $CEA4, $CF8A, $D071, $D158
            DECLE   $D240, $D328, $D411, $D4FA, $D5E4, $D6CE, $D7B9, $D8A4
            DECLE   $D990, $DA7C, $DB69, $DC56, $DD44, $DE32, $DF21, $E010
            DECLE   $E100, $E1F0, $E2E1, $E3D2, $E4C4, $E5B6, $E6A9, $E79C
            DECLE   $E890, $E984, $EA79, $EB6E, $EC64, $ED5A, $EE51, $EF48
            DECLE   $F040, $F138, $F231, $F32A, $F424, $F51E, $F619, $F714
            DECLE   $F810, $F90C, $FA09, $FB06, $FC04, $FD02, $FE01
            ENDP

; R0 = R0 * R1, where R0 and R1 are unsigned 8-bit values
; Destroys R1, R4
qs_mpy8:    PROC
            MOVR    R0,             R4      ;   6
            ADDI    #QSQR8_TBL.mid, R1      ;   8
            ADDR    R1,             R4      ;   6   a + b
            SUBR    R0,             R1      ;   6   a - b
@@ok:       MVI@    R4,             R0      ;   8
            SUB@    R1,             R0      ;   8
            JR      R5                      ;   7
                                            ;----
                                            ;  49
            ENDP
            

; R1 = R0 * R1, where R0 and R1 are 16-bit values
; destroys R0, R2, R3, R4, R5
qs_mpy16:   PROC
            PSHR    R5                  ;   9
                                   
            ; Unpack lo/hi
            MOVR    R0,         R2      ;   6   
            ANDI    #$FF,       R0      ;   8   R0 is lo(a)
            XORR    R0,         R2      ;   6   
            SWAP    R2                  ;   6   R2 is hi(a)

            MOVR    R1,         R3      ;   6   R3 is orig 16-bit b
            ANDI    #$FF,       R1      ;   8   R1 is lo(b)
            MOVR    R1,         R5      ;   6   R5 is lo(b)
            XORR    R1,         R3      ;   6   
            SWAP    R3                  ;   6   R3 is hi(b)
                                        ;----
                                        ;  67
                                        
            ; lo * lo                   
            MOVR    R0,         R4      ;   6   R4 is lo(a)
            ADDI    #QSQR8_TBL.mid, R1  ;   8
            ADDR    R1,         R4      ;   6   R4 = lo(a) + lo(b)
            SUBR    R0,         R1      ;   6   R1 = lo(a) - lo(b)
                                        
@@pos_ll:   MVI@    R4,         R4      ;   8   R4 = qstbl[lo(a)+lo(b)]
            SUB@    R1,         R4      ;   8   R4 = lo(a)*lo(b)
                                        ;----
                                        ;  42
                                        ;  67 (carried forward)
                                        ;----
                                        ; 109
                                       
            ; lo * hi                  
            MOVR    R0,         R1      ;   6   R0 = R1 = lo(a)
            ADDI    #QSQR8_TBL.mid, R3  ;   8
            ADDR    R3,         R1      ;   6   R1 = hi(b) + lo(a)
            SUBR    R0,         R3      ;   6   R3 = hi(b) - lo(a)
                                       
@@pos_lh:   MVI@    R1,         R1      ;   8   R1 = qstbl[hi(b)-lo(a)]
            SUB@    R3,         R1      ;   8   R1 = lo(a)*hi(b)
                                        ;----
                                        ;  42
                                        ; 109 (carried forward)
                                        ;----
                                        ; 151
                                       
            ; hi * lo                  
            MOVR    R5,         R0      ;   6   R5 = R0 = lo(b)
            ADDI    #QSQR8_TBL.mid, R2  ;   8
            ADDR    R2,         R5      ;   6   R3 = hi(a) + lo(b)
            SUBR    R0,         R2      ;   6   R2 = hi(a) - lo(b)
                                       
@@pos_hl:   ADD@    R5,         R1      ;   8   \_ R1 = lo(a)*hi(b)+hi(a)*lo(b)
            SUB@    R2,         R1      ;   8   /
                                        ;----
                                        ;  42
                                        ; 151 (carried forward)
                                        ;----
                                        ; 193
                                       
            SWAP    R1                  ;   6   \_ shift upper product left 8
            ANDI    #$FF00,     R1      ;   8   /
            ADDR    R4,         R1      ;   6   final product
            PULR    PC                  ;  12
                                        ;----
                                        ;  32
                                        ; 193 (carried forward)
                                        ;----
                                        ; 225
            ENDP

	ENDI

	IF DEFINED intybasic_fastdiv

; Fast unsigned division/remainder
; Assembly code by Oscar Toledo G. Jul/10/2015
; Released to public domain.

	; Ultrafast unsigned division/remainder operation
	; Entry: R0 = Dividend
	;        R1 = Divisor
	; Output: R0 = Quotient
	;         R2 = Remainder
	; Worst case: 6 + 6 + 9 + 496 = 517 cycles
	; Best case: 6 + (6 + 7) * 16 = 214 cycles

uf_udiv16:	PROC
	CLRR R2		; 6
	SLLC R0,1	; 6
	BC @@1		; 7/9
	SLLC R0,1	; 6
	BC @@2		; 7/9
	SLLC R0,1	; 6
	BC @@3		; 7/9
	SLLC R0,1	; 6
	BC @@4		; 7/9
	SLLC R0,1	; 6
	BC @@5		; 7/9
	SLLC R0,1	; 6
	BC @@6		; 7/9
	SLLC R0,1	; 6
	BC @@7		; 7/9
	SLLC R0,1	; 6
	BC @@8		; 7/9
	SLLC R0,1	; 6
	BC @@9		; 7/9
	SLLC R0,1	; 6
	BC @@10		; 7/9
	SLLC R0,1	; 6
	BC @@11		; 7/9
	SLLC R0,1	; 6
	BC @@12		; 7/9
	SLLC R0,1	; 6
	BC @@13		; 7/9
	SLLC R0,1	; 6
	BC @@14		; 7/9
	SLLC R0,1	; 6
	BC @@15		; 7/9
	SLLC R0,1	; 6
	BC @@16		; 7/9
	JR R5

@@1:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@2:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@3:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@4:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@5:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@6:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@7:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@8:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@9:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@10:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@11:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@12:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@13:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@14:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@15:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
@@16:	RLC R2,1	; 6
	CMPR R1,R2	; 6
	BNC $+3		; 7/9
	SUBR R1,R2	; 6
	RLC R0,1	; 6
	JR R5
	
	ENDP

	ENDI

	IF DEFINED intybasic_ecs
	ORG $4800	; Available up to $4FFF

        ; Disable ECS ROMs so that they don't conflict with us
        MVII    #$2A5F, R0
        MVO     R0,     $2FFF
        MVII    #$7A5F, R0
        MVO     R0,     $7FFF
        MVII    #$EA5F, R0
        MVO     R0,     $EFFF

        B       $1041       ; resume boot

	ENDI

        ORG $200,$200,"-RWB"

Q2:	; Reserved label for #BACKTAB

	ORG $319,$319,"-RWB"
        ;
        ; 16-bits variables
	; Note IntyBASIC variables grow up starting in $308.
        ;
        IF DEFINED intybasic_voice
IV.Q:      RMB 8    ; IV_xxx        16-bit          Voice queue  (8 words)
IV.FPTR:   RMB 1    ; IV_xxx        16-bit          Current FIFO ptr.
IV.PPTR:   RMB 1    ; IV_xxx        16-bit          Current Phrase ptr.
        ENDI

        ORG $323,$323,"-RWB"

_scroll_buffer: RMB 20  ; Sometimes this is unused
_music_table:	RMB 1	; Note table
_music_start:	RMB 1	; Start of music
_music_p:	RMB 1	; Pointer to music
_frame:         RMB 1   ; Current frame
_read:          RMB 1   ; Pointer to DATA
_gram_bitmap:   RMB 1   ; Bitmap for definition
_gram2_bitmap:  RMB 1   ; Secondary bitmap for definition
_screen:    RMB 1       ; Pointer to current screen position
_color:     RMB 1       ; Current color

Q1:			; Reserved label for #MOBSHADOW
_mobs:      RMB 3*8     ; MOB buffer

_col0:      RMB 1       ; Collision status for MOB0
_col1:      RMB 1       ; Collision status for MOB1
_col2:      RMB 1       ; Collision status for MOB2
_col3:      RMB 1       ; Collision status for MOB3
_col4:      RMB 1       ; Collision status for MOB4
_col5:      RMB 1       ; Collision status for MOB5
_col6:      RMB 1       ; Collision status for MOB6
_col7:      RMB 1       ; Collision status for MOB7

SCRATCH:    ORG $100,$100,"-RWBN"
        ;
        ; 8-bits variables
        ;
ISRVEC:     RMB 2       ; Pointer to ISR vector (required by Intellivision ROM)
_int:       RMB 1       ; Signals interrupt received
_ntsc:      RMB 1       ; Signals NTSC Intellivision
_rand:      RMB 1       ; Pseudo-random value
_gram_target:   RMB 1   ; Contains GRAM card number
_gram_total:    RMB 1   ; Contains total GRAM cards for definition
_gram2_target:  RMB 1   ; Contains GRAM card number
_gram2_total:   RMB 1   ; Contains total GRAM cards for definition
_mode_select:   RMB 1   ; Graphics mode selection
_border_color:  RMB 1   ; Border color
_border_mask:   RMB 1   ; Border mask
    IF DEFINED intybasic_keypad
_cnt1_p0:   RMB 1       ; Debouncing 1
_cnt1_p1:   RMB 1       ; Debouncing 2
_cnt1_key:  RMB 1       ; Currently pressed key
_cnt2_p0:   RMB 1       ; Debouncing 1
_cnt2_p1:   RMB 1       ; Debouncing 2
_cnt2_key:  RMB 1       ; Currently pressed key
    ENDI
    IF DEFINED intybasic_scroll
_scroll_x:  RMB 1       ; Scroll X offset
_scroll_y:  RMB 1       ; Scroll Y offset
_scroll_d:  RMB 1       ; Scroll direction
    ENDI
    IF DEFINED intybasic_music
_music_mode: RMB 1      ; Music mode (0= Not using PSG, 2= Simple, 4= Full, add 1 if using noise channel for drums)
_music_frame: RMB 1     ; Music frame (for 50 hz fixed)
_music_tc:  RMB 1       ; Time counter
_music_t:   RMB 1       ; Time base
_music_i1:  RMB 1       ; Instrument 1 
_music_s1:  RMB 1       ; Sample pointer 1
_music_n1:  RMB 1       ; Note 1
_music_i2:  RMB 1       ; Instrument 2
_music_s2:  RMB 1       ; Sample pointer 2
_music_n2:  RMB 1       ; Note 2
_music_i3:  RMB 1       ; Instrument 3
_music_s3:  RMB 1       ; Sample pointer 3
_music_n3:  RMB 1       ; Note 3
_music_s4:  RMB 1       ; Sample pointer 4
_music_n4:  RMB 1       ; Note 4 (really it's drum)

_music_freq10:	RMB 1   ; Low byte frequency A
_music_freq20:	RMB 1   ; Low byte frequency B
_music_freq30:	RMB 1   ; Low byte frequency C
_music_freq11:	RMB 1   ; High byte frequency A
_music_freq21:	RMB 1   ; High byte frequency B
_music_freq31:	RMB 1   ; High byte frequency C
_music_mix:	RMB 1   ; Mixer
_music_noise:	RMB 1   ; Noise
_music_vol1:	RMB 1   ; Volume A
_music_vol2:	RMB 1   ; Volume B
_music_vol3:	RMB 1   ; Volume C
    ENDI
    IF DEFINED intybasic_music_volume
_music_vol:	RMB 1	; Global music volume
    ENDI
    IF DEFINED intybasic_voice
IV.QH:     RMB 1    ; IV_xxx        8-bit           Voice queue head
IV.QT:     RMB 1    ; IV_xxx        8-bit           Voice queue tail
IV.FLEN:   RMB 1    ; IV_xxx        8-bit           Length of FIFO data
    ENDI


var_AB:	RMB 1	; AB
var_BT:	RMB 1	; BT
var_C:	RMB 1	; C
var_COFX:	RMB 1	; COFX
var_COFY:	RMB 1	; COFY
var_COULEURIMPACT:	RMB 1	; COULEURIMPACT
var_D:	RMB 1	; D
var_DB:	RMB 1	; DB
var_DD:	RMB 1	; DD
var_DEADU:	RMB 1	; DEADU
var_DG:	RMB 1	; DG
var_DH:	RMB 1	; DH
var_DISTANCEARME:	RMB 1	; DISTANCEARME
var_DX_ARME:	RMB 1	; DX_ARME
var_E:	RMB 1	; E
var_EBUNKER:	RMB 1	; EBUNKER
var_F:	RMB 1	; F
var_I:	RMB 1	; I
var_ID:	RMB 1	; ID
var_IDIA:	RMB 1	; IDIA
var_IMPACTTOUCH:	RMB 1	; IMPACTTOUCH
var_J:	RMB 1	; J
var_LIFEPAPI:	RMB 1	; LIFEPAPI
var_MAP_HAUTEUR:	RMB 1	; MAP_HAUTEUR
var_MAP_LARGEUR:	RMB 1	; MAP_LARGEUR
var_MAXOBJET:	RMB 1	; MAXOBJET
var_MORTPAPI:	RMB 1	; MORTPAPI
var_NBBALLE:	RMB 1	; NBBALLE
var_NBBUNKER:	RMB 1	; NBBUNKER
var_NBGRENADE:	RMB 1	; NBGRENADE
var_NBGRENADEM:	RMB 1	; NBGRENADEM
var_NBIA:	RMB 1	; NBIA
var_NBROQUETTE:	RMB 1	; NBROQUETTE
var_OFFSET_D:	RMB 1	; OFFSET_D
var_OFFSET_Y:	RMB 1	; OFFSET_Y
var_POSBUNKER:	RMB 1	; POSBUNKER
var_POSITION_Y_ZONE:	RMB 1	; POSITION_Y_ZONE
var_SENSTIR:	RMB 1	; SENSTIR
var_START:	RMB 1	; START
var_STARTGAME:	RMB 1	; STARTGAME
var_TEMPOTILE:	RMB 1	; TEMPOTILE
var_TEMPOTIR:	RMB 1	; TEMPOTIR
var_TIRIA:	RMB 1	; TIRIA
var_TIRPAPI:	RMB 1	; TIRPAPI
var_TIRSOLDAT:	RMB 1	; TIRSOLDAT
var_TYPEARME:	RMB 1	; TYPEARME
var_VALUE:	RMB 1	; VALUE
var_VELOCITY:	RMB 1	; VELOCITY
var_VITESSEARME:	RMB 1	; VITESSEARME
array_COORDX:	RMB 10	; COORDX
array_COORDY:	RMB 10	; COORDY
array_DIEBUNKER:	RMB 8	; DIEBUNKER
array_DIEUNIT:	RMB 8	; DIEUNIT
array_IABUNKER_CX:	RMB 8	; IABUNKER_CX
array_IABUNKER_CY:	RMB 8	; IABUNKER_CY
array_IDB:	RMB 8	; IDB
array_LIFEBUNKER:	RMB 8	; LIFEBUNKER
array_MOVEIA:	RMB 4	; MOVEIA
array_SENSIA:	RMB 4	; SENSIA
array_SENSTIRBUNKER:	RMB 8	; SENSTIRBUNKER
array_SPEEDIA:	RMB 4	; SPEEDIA
array_TEMPO:	RMB 10	; TEMPO
array_TEMPOIA:	RMB 4	; TEMPOIA
array_TYPEIA:	RMB 4	; TYPEIA
array_VELOCITYIA:	RMB 4	; VELOCITYIA
array_VIESOLDAT:	RMB 4	; VIESOLDAT
array_VISIBLEIA:	RMB 4	; VISIBLEIA
_SCRATCH:	EQU $

SYSTEM:	ORG $2F0, $2F0, "-RWBN"
STACK:	RMB 24
var_&COL_TEST:	RMB 1	; #COL_TEST
var_&COULEUR:	RMB 1	; #COULEUR
var_&COULEUR3:	RMB 1	; #COULEUR3
var_&COULEUR4:	RMB 1	; #COULEUR4
var_&COULEUR5:	RMB 1	; #COULEUR5
var_&MIROIR:	RMB 1	; #MIROIR
var_&MIROIR1:	RMB 1	; #MIROIR1
var_&MIROIR3:	RMB 1	; #MIROIR3
var_&MIROIR4:	RMB 1	; #MIROIR4
var_&MIROIR5:	RMB 1	; #MIROIR5
var_&OFFSET:	RMB 1	; #OFFSET
var_&SPR3:	RMB 1	; #SPR3
var_&SPR4:	RMB 1	; #SPR4
var_&SPR5:	RMB 1	; #SPR5
var_&VISIBLE:	RMB 1	; #VISIBLE
var_&ZOOMOBJECTX:	RMB 1	; #ZOOMOBJECTX
var_&ZOOMOBJECTY:	RMB 1	; #ZOOMOBJECTY
_SYSTEM:	EQU $
